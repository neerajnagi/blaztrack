'use strict';

const fetch = require('node-fetch');

async function calculateHmr(vehiclesWithLastData, devicesData)
{
  console.log('inside calculateHmr');
  //let vehicles = vehiclesWithLastData.dataFromApi;
  let vehicles = vehiclesWithLastData;
  let vehicle;
  let deviceData;
  let vehicleHmrParam;
  let vehiclesForHmr = {};
  let calculatedHmr = {};
  let vehiclesForDevicePort = [];
  for(var i in vehicles)
  {
    vehicle = vehicles[i];
    deviceData = devicesData[i];
    if(undefined != vehicle && undefined != vehicle.hmrParam)
    {
      vehicleHmrParam = vehicle.hmrParam;
    }
    else {
      //continue;
    }
    if(undefined != deviceData && deviceData != "data not found")
    {
    vehiclesForDevicePort.push(vehicle.vehicleId);
    console.log('device data length', deviceData.length)
    vehiclesForHmr[vehicle.vehicleId] = {};
    vehiclesForHmr[vehicle.vehicleId].hmrParam = vehicleHmrParam;
    vehiclesForHmr[vehicle.vehicleId].deviceData = deviceData;
    }
    else {
      //continue;
    }
  }
  console.log('vehiclesForDevicePort',vehiclesForDevicePort);
  let vehiclesSensorDevicePort = await getVehiclesSensorDevicePort(vehiclesForDevicePort);
  console.log('vehiclesSensorDevicePort',vehiclesSensorDevicePort);
  console.log('vehiclesForHmr',vehiclesForHmr)
  //let sensorsForHmr = [];
  for(var i in vehiclesForHmr)
  {
    let sensorsForHmr = {};
    sensorsForHmr[i] = {};
    console.log('vehiclesSensorDevicePort[i], i:',i)
    for(var j in vehiclesSensorDevicePort[i])
      {
        if(vehiclesForHmr[i].hmrParam.hasOwnProperty(j))
        {
          console.log('inside if, j',j,', vehiclesSensorDevicePort[i][j]',vehiclesSensorDevicePort[i][j]);
          sensorsForHmr[i][j] = vehiclesSensorDevicePort[i][j];
        }
        else {
          console.log('inside else, j',j)
        }
      }
    console.log('sensorsForHmr::',sensorsForHmr);
    let vehicleHmr = await calculateVehicleHmr(sensorsForHmr[i], vehiclesForHmr[i].hmrParam, vehiclesForHmr[i].deviceData);
    //TODO need to chnage code
    calculatedHmr[i] = {};
    calculatedHmr[i].idle = vehicleHmr.idle;
    calculatedHmr[i].production = vehicleHmr.production;
  }

  for (var i in calculatedHmr)
  {
    //console.log('calculatedHmr loop, vehicle hmr',calculatedHmr[i])
  }
  console.log('calculatedHmr',JSON.stringify(calculatedHmr))
  return JSON.stringify(calculatedHmr);
}

async function getVehiclesSensorDevicePort(vehiclesForDevicePort)
{
  console.log('inside getVehiclesSensorDevicePort')
  let vehiclesSensorDevicePort = {};
  if(undefined != vehiclesForDevicePort && vehiclesForDevicePort.length > 0)
  {
    let payload = {
                    vehicles:vehiclesForDevicePort
                  }
    const requestOptions = {
      credentials: 'include',
      method: 'POST',
      headers: { 'Content-Type': 'application/json'
                },
      body : JSON.stringify(payload)
    };
    console.log('payload json',JSON.stringify(payload))
    const sensorWithDevicePort = await fetch('http://159.65.6.196:5500/api/vehicle/vehiclewithsensorassignmentatsummary', requestOptions);
    const res = await sensorWithDevicePort.json();
    console.log('sensorWithDevicePort, res',res)
    const data = res.data;
    for(var i in data)
    {
      var sensorAssignments = data[i].sensorAssignments;
      vehiclesSensorDevicePort[data[i].id] = {}
      if(sensorAssignments.length > 0)
      {
        vehiclesSensorDevicePort[data[i].id]['speed'] = 'speed';
      }
      for(var j in sensorAssignments)
      {
        //vehiclesSensorDevicePort[data[i].id][sensorAssignments[j].sensor.sensorType.lowercaseName] = sensorAssignments[j].devicePort.lowercaseName;
        vehiclesSensorDevicePort[data[i].id][sensorAssignments[j].sensor.sensorType.lowercaseName] = sensorAssignments[j].devicePort.dbColumnName;
      }
    }
  }
  return vehiclesSensorDevicePort;
}

async function calculateVehicleHmr(sensorsForHmr, vehicleHmrParam, deviceData)
{
  console.log('inside calculateVehicleHmr')
  console.log('sensorsForHmr',sensorsForHmr,', vehicleHmrParam',vehicleHmrParam)
  let vehicleHmr = {};
  //TODO we calculate hmr using data;
  var data;
  var sensor;
  var sensorValue;
  var min;
  var max;
  var status = {};
  for(var i in deviceData)
  {
    data = deviceData[i];
    for(var j in sensorsForHmr)
    {
      sensor = j;
      sensorValue = data[sensorsForHmr[j]]
      //console.log('sensor:',sensor,', sensor value:',sensorValue)
      status[sensor] = {};
      for(var k in vehicleHmrParam[j])
      {
        //TODO need to change get min/max for Speed sensor
        if(k == "speed")
        {
        min = vehicleHmrParam[j][k]['min'];
        max = vehicleHmrParam[j][k]['max'];
        }
        else {
          min = vehicleHmrParam[j]['min'];
          max = vehicleHmrParam[j]['max'];
        }
        //console.log('sensor status:',k,', min:',min,', max:',max)
        if(sensorValue >= min && sensorValue <= max)
        {
          //console.log('status is:', k)
          status[sensor][k] = true;
        }
        else {
          status[sensor][k] = false;
        }
      }
    }
    //console.log('now sensor status:',status)
  }
  vehicleHmr = {idle: '10 min', production: '20 min', marching: '5 min'}
  console.log('vehicleHmr',JSON.stringify(vehicleHmr))
  return vehicleHmr;
}

module.exports = {

        calculateHmr : async function(vehiclesData, devicesData, forDays) {
          //console.log('inside calculate hmr')
          var calculatedData  = await calculateHmr(vehiclesData, devicesData);
          console.log('calculatedData',calculatedData)
      		return calculatedData;
        }
}
