import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Form,
  FormGroup,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Label,
  FormText
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'

import Select from 'react-select';
import classnames from 'classnames';

import { NextAuth } from 'next-auth/client'

import Router from 'next/router'

//react-bootstrap-table2
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import cellEditFactory from 'react-bootstrap-table2-editor';
//For table search
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';

import DropdownTreeSelect from 'react-dropdown-tree-select'
import 'react-dropdown-tree-select/dist/styles.css'

import PropTypes from 'prop-types';

import { ClipLoader } from 'react-spinners';

//TODO for datetime
import * as Datetime from 'react-datetime';
import 'react-datetime/css/react-datetime.css'

import * as moment from 'moment';

const { SearchBar } = Search;

const onChange = (currentNode, selectedNodes) => {
  console.log('onChange::', currentNode, selectedNodes)
}
const onAction = ({ action, node }) => {
  console.log(`onAction:: [${action}]`, node)
}
const onNodeToggle = currentNode => {
  console.log('onNodeToggle::', currentNode)
}

const optionsSubDealer = [];

const optionsCustomerForVehicle = [];

const optionsCustomerForDevice = [];

const optionsGroup = [{ value: 'all', label: 'All' }];

const optionsSelectSubDealer = [];

const optionsSelectCustomer = [];

const optionsSelectGroup = [];

const optionsSelectVehicle = [];

const optionsSelectSimProvider = []

const selectRow = {
  mode: 'checkbox',
  //clickToSelect: true,
  //Disable single row selection
  clickToSelect: false,
  onSelect: (row, isSelect, rowIndex, e) => {
    console.log(row.id);
    console.log(isSelect);
    console.log(row);
    console.log(rowIndex);
    console.log(e);
  },
  onSelectAll: (isSelect, rows, e) => {
    console.log(isSelect);
    console.log(rows);
    console.log(e);
  }
};

const divStyle = {
  margin: '40px',
  border: '5px solid pink'
};

const dataVehiclesTable = [];
const dataDevicesTable = [];

let idForVehicleFetch;
let allForVehicleFetch;

let idForDeviceFetch;
let allForDeviceFetch;

let deleteRow;
let deleteRowDevice;

//const editFormatter = (cell, row, rowIndex, formatExtraData) => { console.log('editFormatter, row ',row); return ( <Button block color="primary" className="btn-pill" size="sm" >Edit</Button> ); }
//const deleteFormatter = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm">Delete</Button> ); }

class Vehicle extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleAddVehicle = this.toggleAddVehicle.bind(this);
    this.toggleEditVehicle = this.toggleEditVehicle.bind(this);
    this.editVehicle = this.editVehicle.bind(this);
    this.toggleDeleteVehicle = this.toggleDeleteVehicle.bind(this);
    this.confirmDeleteVehicle = this.confirmDeleteVehicle.bind(this);
    this.deleteVehicle = this.deleteVehicle.bind(this);
    this.handleAddVehicle = this.handleAddVehicle.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSaveVehicle = this.handleSaveVehicle.bind(this);
    this.handleUpdateVehicle = this.handleUpdateVehicle.bind(this);
    this.handleAddDevice = this.handleAddDevice.bind(this);
    this.toggleAddDevice = this.toggleAddDevice.bind(this);
    this.handleSaveDevice = this.handleSaveDevice.bind(this);
    this.editDevice = this.editDevice.bind(this);
    this.toggleEditDevice = this.toggleEditDevice.bind(this);
    this.handleUpdateDevice = this.handleUpdateDevice.bind(this);
    this.assignVehicle = this.assignVehicle.bind(this);
    this.unAssignVehicle = this.unAssignVehicle.bind(this);
    this.toggleAssignVehicle = this.toggleAssignVehicle.bind(this);
    this.toggleUnAssignVehicle = this.toggleUnAssignVehicle.bind(this);
    this.handleAssignVehicle = this.handleAssignVehicle.bind(this);
    this.handleUnAssignVehicle = this.handleUnAssignVehicle.bind(this);
    this.handleAssignVehicleRadio = this.handleAssignVehicleRadio.bind(this);
    this.confirmDeleteDevice = this.confirmDeleteDevice.bind(this);
    this.toggleDeleteDevice = this.toggleDeleteDevice.bind(this);
    this.deleteDevice = this.deleteDevice.bind(this);

    const manageCategory = this.props.managecategory;
    let isSubDealerShow = false;
    let isCustomerShow = false;
    let isAddVehicleButtonShow = false;
    let isDeviceManagementShow = false;
    let isAddDeviceButtonShow = false;
    if(manageCategory == "dealer" || manageCategory == "subdealer")
    {
      isSubDealerShow = true;
      isAddVehicleButtonShow = true;
      isAddDeviceButtonShow = true;
    }
    if(manageCategory == "dealer" || manageCategory == "subdealer" || manageCategory == "customer")
    {
      isCustomerShow = true;
      isDeviceManagementShow = true;
    }

    this.state = {
      session: props.session,
      manageCategory:manageCategory,
      dropdownOpen: false,
      radioSelected: 2,
      selectedOption: null,
      isClearable: true,
      activeTab: '1',
      isOpenModalAddVehicle: false,
      isOpenModalEditVehicle: false,
      isOpenModalDeleteVehicle: false,
      isOpenModalAddDevice: false,
      isOpenModalDeleteDevice: false,
      dataVehiclesTable:dataVehiclesTable,
      optionsSubDealer:optionsSubDealer,
      optionsCustomerForVehicle:optionsCustomerForVehicle,
      optionsCustomerForDevice:optionsCustomerForDevice,
      optionsGroup:optionsGroup,
      optionsSelectSubDealer:optionsSelectSubDealer,
      optionsSelectCustomer:optionsSelectCustomer,
      optionsSelectGroup:optionsSelectGroup,
      dataDevicesTable:dataDevicesTable,
      selectCustomerValue:'',
      selectGroupValue:'',
      isAdmin:false,
      isSubDealerShow:isSubDealerShow,
      isCustomerShow:isCustomerShow,
      isAddVehicleButtonShow:isAddVehicleButtonShow,
      isDeviceManagementShow:isDeviceManagementShow,
      isAddDeviceButtonShow:isAddDeviceButtonShow,
      isOpenModalEditDevice:false,
      isOpenModalAssignVehicle:false,
      isOpenModalUnAssignVehicle:false,
      isAssignVehicle:false,
      optionsSelectSimProvider:optionsSelectSimProvider,
      vehicleTableLoading:true,
      deviceTableLoading:false
    };
  }

  async componentDidMount() {
    console.log('inside componentDidMount')
    //TODO check user logged in or not:
    const session = await NextAuth.init({force: true})
    if(session.user)
    {
      console.log('user logged in')
    }
    else {
      console.log('user not logged in')
      Router.push('/auth/callback')
      return false;
    }
    console.log('after checking user state')
    //getting vehicles & devices
    this.getManages();
    this.getSimProviders();
  }

  async getSimProviders()
  {
    console.log('inside getSimProviders')
    let optionsSelectSimProvider = [];
    const fetchSimProvider = await fetch('http://159.65.6.196:5500/api/datadictionary?filter[where][lowercaseName]=simprovider');
    const res = await fetchSimProvider.json();
    console.log('sim provider res',res)
    const data = res[0].data;
    if(undefined != data)
    {
      const simProvider = data.simProvider;
      if(undefined != simProvider && simProvider.length >0)
      {
        for(var i in simProvider)
        {
          var item = simProvider[i];
          optionsSelectSimProvider.push({
            "value":item,
            "label":item
          });
        }
      }
    }
    console.log('optionsSelectSimProvider',optionsSelectSimProvider)
    this.setState({
                    optionsSelectSimProvider:optionsSelectSimProvider
                })
  }

  async getManages()
  {
    console.log('inside getManages')
    const {manageCategory} = this.state;
    let optionsSubDealer = []
    let optionsCustomer = []
    let optionsGroup = [{ value: 'all', label: 'All' }]
    let fetchManage;
    if(manageCategory == "group")
    {
      fetchManage = await fetch('/group/manage/fetch/'+this.state.manageCategory+'/vehicle');
    }
    else {
      fetchManage = await fetch('/group/manage/fetch/'+this.state.manageCategory+'/group');
    }
    const res = await fetchManage.json();
    console.log('res',res);
    const data = res.data;
    const manage = data.manage;
    for(var i in manage) {
      var item = manage[i];
      var subDealers = ""
      if(manageCategory == "dealer")
      {
        subDealers = item.dealer.subDealers;
        for(var j in subDealers)
        {
          var subDealer = subDealers[j];
          optionsSubDealer.push({
            "value":subDealer.id,
            "label":subDealer.name
          });
        }
      }
      else if(manageCategory == "subdealer") {
        subDealers = item.subDealer;
          var subDealer = subDealers;
          optionsSubDealer.push({
            "value":subDealer.id,
            "label":subDealer.name
          });
      }
      else if(manageCategory == "customer") {
            var customer = item.customer;
            optionsCustomer.push({
              "value":customer.id,
              "label":customer.name
            });
      }
      else if(manageCategory == "group") {
            var group = item.group;
              optionsGroup.push({
                "value":group.id,
                "label":group.name
              });
      }
      else {
        console.log('something went wrong')
      }


    }

    //TODO now getting customer and group according to manageCategory
      if(manageCategory == "dealer" || manageCategory == "subdealer")
      {
          //getting all customer of a subDealer
          const subDealer = optionsSubDealer[0];
          const subDealerId = subDealer.value;
          const customers = await this.getSubDealerCustomers(subDealerId);
          if(undefined != customers && customers.length > 0)
          {
            const customer = customers[0];
            const customerId = customer.value;
            //let customersForDevice = customers;
            let customersForDevice = [];
            for(var i in customers)
            {
              customersForDevice.push(customers[i])
            }
            customersForDevice.push({ value: 'all', label: 'All' });
            console.log('customersForDevice',customersForDevice)
            const customerForDevice = customersForDevice[0];
            const groups = await this.getCustomerGroups(customer);
            let group = groups[0]
            //optionsCustomerForDevice =
            this.setState({
                          optionsSubDealer:optionsSubDealer,
                          selectSubDealerValueForVehicle:subDealer,
                          optionsCustomerForVehicle:customers,
                          selectCustomerValueForVehicle:customer,
                          optionsGroup:groups,
                          selectGroupValue:group,
                          selectSubDealerValueForDevice:subDealer,
                          optionsCustomerForDevice:customersForDevice,
                          selectCustomerValueForDevice:customerForDevice
                          })
            //TODO calling get vehicle with all true case and customerId with manageCategory
            idForVehicleFetch = customerId;
            allForVehicleFetch = true;
            this.getVehicles(idForVehicleFetch, manageCategory, allForVehicleFetch);
            //Now fetching devices
            idForDeviceFetch = customerId;
            allForDeviceFetch = false;
            this.getDevices(idForDeviceFetch, manageCategory, allForDeviceFetch)
          }
          else {
            this.setState({
                          optionsSubDealer:optionsSubDealer,
                          selectSubDealerValueForVehicle:subDealer
                        })
          }

      }
      else if(manageCategory == "customer") {
         //getting all groups of a customer
         const customer = optionsCustomer[0];
         const customerId = customer.value;
         let customersForDevice = [{ value: 'all', label: 'All' }];
         for(var i in optionsCustomer)
         {
           var item = optionsCustomer[i]
           customersForDevice.push(item);
         }
         console.log('customersForDevice',customersForDevice)
         const customerForDevice = customersForDevice[0];
         const groups = await this.getCustomerGroups(customer);
         let groupValue = groups[0];
         this.setState({
                       optionsCustomerForVehicle:optionsCustomer,
                       selectCustomerValueForVehicle:customer,
                       optionsGroup:groups,
                       selectGroupValue:groupValue,
                       optionsCustomerForDevice:customersForDevice,
                       selectCustomerValueForDevice:customerForDevice
                       })

        idForVehicleFetch = customerId;
        allForVehicleFetch = true;
        this.getVehicles(idForVehicleFetch, manageCategory, allForVehicleFetch);
        //Now fetching devices
        const userId = this.state.session.user.id;
        idForDeviceFetch = userId;
        allForDeviceFetch = true;
        this.getDevices(idForDeviceFetch, manageCategory, allForDeviceFetch);
      }
      else if(manageCategory == "group")
      {
        const group = optionsGroup[0];
        this.setState({
                      optionsGroup:optionsGroup,
                      selectGroupValue:group
                      })
        //TODO calling get vehicle with all true case and userId with manageCategory
        const userId = this.state.session.user.id;
        idForVehicleFetch = userId;
        allForVehicleFetch = true;
        this.getVehicles(idForVehicleFetch, manageCategory, allForVehicleFetch);
      }
      else {
        console.log('something went wrong')
      }

      /*
    this.setState({
                  optionsSubDealer:optionsSubDealer,
                  optionsCustomer:optionsCustomer,
                  optionsGroup:optionsGroup,
                  selectCustomerValue:optionsCustomer[0],
                  selectGroupValue:optionsGroup[0]
                  })
    */

  }

  async getVehicles(id, manageCategory, all)
  {
    console.log('inside getVehicles with managecategory')
    console.log('id, manageCategory, all', id, ',',manageCategory, ',', all)
    const fetchVehicle = await fetch('/vehicle/fetchvehicleforsitedata/'+id+'/'+manageCategory+'/'+all);
    const res = await fetchVehicle.json();
    console.log('fetchVehicle, res',res);
    const data = res.data;
    const dataVehiclesTable = [];
    let dataFromApi = {};
    var snVehicles = 1;
    if(all)
    {
        if(manageCategory == "group")
        {
          //res came with manage
          const manage = data;
          for(var i in manage)
          {
            var item = manage[i];
            const group = item.group;
            const customerId = group.customerId;
            const vehicles = group.vehicles;
            for(var j in vehicles)
            {
              var itemVehicles = vehicles[j];

              var vehicleCategory = "";

              if(undefined != itemVehicles.vehicleCategory && itemVehicles.vehicleCategory != "")
              {
                vehicleCategory = itemVehicles.vehicleCategory
              }

              var driver;
              if(undefined != itemVehicles.driverName || undefined != itemVehicles.driverMobileNo)
              {
                driver = {name:itemVehicles.driverName, mobileNo:itemVehicles.driverMobileNo}
              }
              else
              {
                driver = {name:'', mobileNo:''}
              }

              let dataForTable = {
                                  group:group.name,
                                  groupId:group.id,
                                  customerId:customerId,
                                  vehicleId:itemVehicles.id,
                                  vehicle:itemVehicles.regNo,
                                  vehicleCategory:vehicleCategory,
                                  driver:driver
                                }
              dataFromApi[itemVehicles.id] = dataForTable
            }
          }
        }
        else {
          // res came with group
          const group = data
          for(var i in group)
          {
            var item = group[i];
            const customerId = item.customerId;
            var vehicles = item.vehicles;
            for(var j in vehicles)
            {
              var itemVehicles = vehicles[j];

              var vehicleCategory = "";

              if(undefined != itemVehicles.vehicleCategory && itemVehicles.vehicleCategory != "")
              {
                vehicleCategory = itemVehicles.vehicleCategory
              }

              var driver;
              if(undefined != itemVehicles.driverName || undefined != itemVehicles.driverMobileNo)
              {
                driver = {name:itemVehicles.driverName, mobileNo:itemVehicles.driverMobileNo}
              }
              else
              {
                driver = {name:'', mobileNo:''}
              }

              let dataForTable = {
                                  group:item.name,
                                  groupId:item.id,
                                  customerId:customerId,
                                  vehicleId:itemVehicles.id,
                                  vehicle:itemVehicles.regNo,
                                  vehicleCategory:vehicleCategory,
                                  driver:driver
                                }
              dataFromApi[itemVehicles.id] = dataForTable
            }
          }

        }
    }
    else
    {
      //res came with vehicle
      const vehicle = data
      for(var i in vehicle)
      {
        var item = vehicle[i];
        var group = item.group;
        const customerId = group.customerId;
        var vehicleCategory = "";

        if(undefined != item.vehicleCategory && item.vehicleCategory != "")
        {
          vehicleCategory = item.vehicleCategory
        }

        var driver;
        if(undefined != item.driverName || undefined != item.driverMobileNo)
        {
          driver = {name:item.driverName, mobileNo:item.driverMobileNo}
        }
        else
        {
          driver = {name:'', mobileNo:''}
        }

        let dataForTable = {
                            group:group.name,
                            groupId:group.id,
                            customerId:customerId,
                            vehicleId:item.id,
                            vehicle:item.regNo,
                            vehicleCategory:vehicleCategory,
                            driver:driver
                          }
        dataFromApi[item.id] = dataForTable
      }
    }

    for(var i in dataFromApi)
    {
      var item = dataFromApi[i];
      console.log('vehicle item',item)
      dataVehiclesTable.push({
          "id" : snVehicles,
          "group":item.group || 'NA',
          "groupId":item.groupId || '',
          "customerId":item.customerId,
          "vehicleId": item.vehicleId,
          "regNo"  : item.vehicle,
          "vehicleCategory": item.vehicleCategory.name,
          "vehicleCategoryId": item.vehicleCategory.id
        });
        snVehicles++;
    }

    this.setState({
                  vehicleTableLoading:false,
                  dataVehiclesTable:dataVehiclesTable
                  })

  }

  /*
  async getVehicles()
  {
    console.log('inside getVehicles')
    const res = await fetch('/vehicle/fetch/'+this.state.manageCategory)
    const resData = await res.json()
    console.log('resData, getVehicles',resData)
    //{ id: 1, phoneNumber: '9799605400', name:'Ashwin', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
    let dataVehiclesTable = []
    const data = resData.data;
    const vehicles = data;
    var snVehicles = 1;
    for (var i in vehicles)
    {
      var item = vehicles[i];
      var vehicleCategoryDoc = item.vehicleCategory;
      var vehicleCategory;
      var vehicleCategoryId;
      if(undefined != vehicleCategoryDoc)
      {
        vehicleCategory = vehicleCategoryDoc.name;
        vehicleCategoryId = vehicleCategoryDoc.id;
      }
      else {
        vehicleCategory = "";
        vehicleCategoryId = "";
      }

      dataVehiclesTable.push({
          "id" : snVehicles,
          "vehicleId": item.id,
          "groupId": item.groupId,
          "regNo"  : item.regNo,
          "vehicleCategory": vehicleCategory,
          "vehicleCategoryId": vehicleCategoryId
        });
        snVehicles++;
    }
    this.setState({
                  vehicleTableLoading:false,
                  dataVehiclesTable:dataVehiclesTable
                  })
  }
  */

  /*

  async getDevices()
  {
    console.log('inside getDevices')
    let dataDevicesTable = []
    const fetchDevice = await fetch('/device/fetch/'+this.state.manageCategory);
    const resData = await fetchDevice.json()
    console.log('resData, getDevices',resData)
    const devices = resData.data;
    var snDevices = 1;
    //{sn:1, deviceId:'23432323231234523', customer:'test_customer', type:'gps', deviceModal:'teltonic'}
    for(var i in devices)
    {
      var item = devices[i];
      var deviceModel = item.deviceModel;
      var customer = item.customer;
      var deviceAssignments = item.deviceAssignments;
      let vehicle = '';
      let vehicleRegNo = '';
      let vehicleId = '';
      if(undefined != deviceAssignments && deviceAssignments != "")
      {
        vehicle = deviceAssignments.vehicle;
        if(undefined != vehicle)
        {
          vehicleRegNo = vehicle.regNo;
          vehicleId = vehicle.id;
        }
      }
      dataDevicesTable.push({
          "sn" : snDevices,
          "id": item.id,
          "deviceId":item.deviceId,
          "imei":item.deviceId,
          "customer":customer.name,
          "customerId":item.customerId,
          "vehicle":vehicleRegNo,
          "vehicleId":vehicleId,
          "deviceModelId":item.deviceModelId,
          "type":item.type,
          "deviceModel":deviceModel.name
        });
        snDevices++;
    }
    this.setState({
                  deviceTableLoading:false,
                  dataDevicesTable:dataDevicesTable
                  })
  }

  */

  async getDevices(id, manageCategory, all)
  {
    console.log('inside getDevices, id, manageCategory, all',id,manageCategory,all)
    let dataDevicesTable = []
    const fetchDevice = await fetch('http://159.65.6.196:5500/api/gpsdevice/devicemgmt/'+id+'/'+manageCategory+'/'+all);
    const res = await fetchDevice.json()
    console.log('res, getDevices',res)
    const data = res.data;
    let snDevices = 1;
    for(var i in data)
    {
      var item = data[i];
      var customer;
      var gpsDevices;
      var deviceAssignments;
      var vehicle;
      var vehicleRegNo = '';
      var vehicleId = '';
      var deviceModel = '';
      if(all)
      {
        if(manageCategory == "dealer" || manageCategory == "subdealer")
        {
          if(undefined != item.customers)
          {
            let customers = item.customers;
            for(var i in customers)
            {
              customer = customers[i];
              if(undefined != customer.gpsDevices)
              {
                gpsDevices = customer.gpsDevices;
                for(var i in gpsDevices)
                {
                  var device = gpsDevices[i];
                  if(undefined != device.deviceAssignments)
                  {
                    deviceAssignments = device.deviceAssignments;
                    if(undefined != deviceAssignments)
                    {
                      vehicle = deviceAssignments.vehicle;
                    }
                    else {
                      vehicle = '';
                    }
                  }
                  else {
                    vehicle = '';
                  }
                  if(undefined != device.deviceModel)
                  {
                    deviceModel = device.deviceModel;
                  }
                  else {
                    deviceModel = '';
                  }
                  dataDevicesTable.push({
                      "sn" : snDevices,
                      "id": device.id,
                      "deviceId":device.deviceId,
                      "imei":device.deviceId,
                      "customer":customer.name || '',
                      "customerId":customer.id || '',
                      "vehicle":vehicle.regNo || '',
                      "vehicleId":vehicle.id || '',
                      "deviceModelId":deviceModel.id || '',
                      "type":device.type || 'NA',
                      "deviceModel":deviceModel.name || 'NA',
                      "simProvider":device.simProvider || 'NA',
                      "simMobileNumber":device.simMobileNumber || 'NA'
                    });
                    snDevices++;
                }
              }
            }
          }
        }
        else if(manageCategory == "customer")
        {
          if(undefined != item.customer)
          {
            customer = item.customer;
            if(undefined != customer.gpsDevices)
            {
              gpsDevices = customer.gpsDevices;
              for(var i in gpsDevices)
              {
                var device = gpsDevices[i];
                if(undefined != device.deviceAssignments)
                {
                  deviceAssignments = device.deviceAssignments;
                  if(undefined != deviceAssignments)
                  {
                    vehicle = deviceAssignments.vehicle;
                  }
                  else {
                    vehicle = '';
                  }
                }
                else {
                  vehicle = '';
                }
                if(undefined != device.deviceModel)
                {
                  deviceModel = device.deviceModel;
                }
                else {
                  deviceModel = '';
                }
                dataDevicesTable.push({
                    "sn" : snDevices,
                    "id": device.id,
                    "deviceId":device.deviceId,
                    "imei":device.deviceId,
                    "customer":customer.name || '',
                    "customerId":customer.id || '',
                    "vehicle":vehicle.regNo || '',
                    "vehicleId":vehicle.id || '',
                    "deviceModelId":deviceModel.id || '',
                    "type":device.type || 'NA',
                    "deviceModel":deviceModel.name || 'NA',
                    "simProvider":device.simProvider || 'NA',
                    "simMobileNumber":device.simMobileNumber || 'NA'
                  });
                  snDevices++;
              }
            }
          }
        }
      }
      else {
        if(undefined != item.customer)
        {
          customer = item.customer;
          if(undefined != item.deviceAssignments)
          {
            deviceAssignments = item.deviceAssignments;
            vehicle = deviceAssignments.vehicle;
            if(undefined != vehicle)
            {
              vehicleRegNo = vehicle.regNo;
              vehicleId = vehicle.id;
            }
          }
          if (undefined != item.deviceModel)
          {
            deviceModel = item.deviceModel;
          }
        }
        dataDevicesTable.push({
            "sn" : snDevices,
            "id": item.id,
            "deviceId":item.deviceId,
            "imei":item.deviceId,
            "customer":customer.name || 'NA',
            "customerId":customer.id || 'NA',
            "vehicle":vehicleRegNo,
            "vehicleId":vehicleId,
            "deviceModelId":item.deviceModelId,
            "type":item.type || 'NA',
            "deviceModel":deviceModel.name || 'NA',
            "simProvider":item.simProvider || 'NA',
            "simMobileNumber":item.simMobileNumber || 'NA'
          });
          snDevices++;
      }
    }
    console.log('dataDevicesTable:',dataDevicesTable)
    this.setState({
                  deviceTableLoading:false,
                  dataDevicesTable:dataDevicesTable
                  })
  }

  handleChange(e) {
      const { name, value } = e.target;
      this.setState({ [name]: value });
  }

  handleChangeSubDealerOption = async (from, selectedOption) => {
      var subDealerId = selectedOption.value;
      this.setState({ selectedOption });
      console.log('handleChangeSubDealerOption,from, Option selected:',from, selectedOption);
      console.log('subDealerId',subDealerId)

      if(undefined != subDealerId && subDealerId == "all")
      {

      }
      else if(undefined != subDealerId)
      {
        //getting customers of subDealer
        const customers = await this.getSubDealerCustomers(subDealerId);
        if(undefined != customers && customers.length > 0)
        {
          if(from == "vehicleManagement")
          {
            this.setState({
                          selectSubDealerValueForVehicle:selectedOption,
                          optionsCustomerForVehicle:customers,
                          selectCustomerValueForVehicle:'',
                          selectGroupValue:'',
                          optionsGroup:[]
                          })
          }
          else if(from == "deviceManagement") {
            //let customersForDevice = [{ value: 'all', label: 'All' }];
            let customersForDevice = customers
            customersForDevice.push({ value: 'all', label: 'All' });
            console.log('customersForDevice',customersForDevice)
            const customerForDevice = customersForDevice[0];
            this.setState({
                          selectSubDealerValueForDevice:selectedOption,
                          optionsCustomerForDevice:customersForDevice,
                          selectCustomerValueForDevice:''
                          })
          }
        }
        else {
          alert('customers not found')
        }
      }
    }

    /*

    async getAllCustomers()
    {
      let optionsSubDealer = [{ value: 'all', label: 'All' }]
      let optionsCustomer = [{ value: 'all', label: 'All' }]
      const fetchManage = await fetch('/group/manage/fetch/'+this.state.manageCategory+'/customer');
      const res = await fetchManage.json();
      console.log('res',res);
      const data = res.data;
      const manage = data.manage;
      var snGroup = 1;
      for(var i in manage) {
        var item = manage[i];
        var subDealers = ""
        if(this.state.manageCategory == "dealer")
        {
        subDealers = item.dealer.subDealers;
        for(var j in subDealers)
        {
          var subDealer = subDealers[j];
          optionsSubDealer.push({
            "value":subDealer.id,
            "label":subDealer.name
          });
          var customers = subDealers[j].customers;
          for(var k in customers){
            var customer = customers[k];

            optionsCustomer.push({
              "value":customer.id,
              "label":customer.name
            });
          }
        }
        }
        else if(this.state.manageCategory == "subdealer") {
          subDealers = item.subDealer;
            var subDealer = subDealers;
            optionsSubDealer.push({
              "value":subDealer.id,
              "label":subDealer.name
            });
            var customers = subDealer.customers;
            for(var k in customers){
              var customer = customers[k];
              optionsCustomer.push({
                "value":customer.id,
                "label":customer.name
              });
            }
        }
        else if(this.state.manageCategory == "customer") {
            var customer = item.customer;
              optionsCustomer.push({
                "value":customer.id,
                "label":customer.name
              });
        }
        else {
          subDealers = item.subDealer;
            var subDealer = subDealers;
            optionsSubDealer.push({
              "value":subDealer.id,
              "label":subDealer.name
            });
            var customers = subDealer.customers;
            for(var k in customers){
              var customer = customers[k];
              optionsCustomer.push({
                "value":customer.id,
                "label":customer.name
              });
            }
        }


      }

        this.setState({
                      optionsCustomer:optionsCustomer
                      })
    }

    */

    async getSubDealerCustomers(subDealerId)
    {
      console.log('inside getSubDealerCustomers, subDealerId',subDealerId)
      let optionsCustomer = []
      let optionsGroup = []
      const fetchSubDealerCustomers = await fetch('/subdealer/'+subDealerId+'/customers');
      const resData = await fetchSubDealerCustomers.json()
      console.log('resData, subDealerCustomers',resData)
      const data = resData;
      for(var i in data)
      {
        var customer = data[i]
        optionsCustomer.push({
          "value":customer.id,
          "label":customer.name
        });
      }
      /*
      this.setState({
                    optionsCustomer:optionsCustomer,
                    optionsGroup:optionsGroup
                    })
      */
      return optionsCustomer;
    }

  handleChangeCustomerOption = async (from, selectedOption) => {
      const {manageCategory} = this.state;
      var customerId = selectedOption.value;
      this.setState({ selectedOption });
      console.log(`handleChangeCustomerOption, Option selected:`, selectedOption);
      console.log('customerId',customerId)
      if(undefined != customerId && customerId == "all")
      {
        if(from == "vehicleManagement")
        {

        }
        else if(from == "deviceManagement") {
          //TODO getting customers devices
          this.setState({
                        selectCustomerValueForDevice:selectedOption,
                        deviceTableLoading:true
                        })
          //TODO getting devices for all customer of subDealer
          if(manageCategory == "customer")
          {
          //Now fetching devices
          const userId = this.state.session.user.id;
          idForDeviceFetch = userId;
          allForDeviceFetch = true;
          this.getDevices(idForDeviceFetch, manageCategory, allForDeviceFetch);
          }
          else {
            //Now fetching devices
            const subDealer = this.state.selectSubDealerValueForDevice;
            const subDealerId = subDealer.value;
            idForDeviceFetch = subDealerId;
            allForDeviceFetch = true;
            this.getDevices(idForDeviceFetch, manageCategory, allForDeviceFetch);
          }
        }
      }
      else if(undefined != customerId)
      {
        //getting customers of subDealer
        if(from == "vehicleManagement")
        {
          const groups = await this.getCustomerGroups(selectedOption);
          if(undefined != groups && groups.length > 0)
          {
            let group = groups[0]
            this.setState({
                          selectCustomerValueForVehicle:selectedOption,
                          optionsGroup:groups,
                          selectGroupValue:''
                          })
          }
          else {
            alert('groups not found')
          }
        }
        else if(from == "deviceManagement")
        {
          this.setState({
                        selectCustomerValueForDevice:selectedOption,
                        deviceTableLoading:true
                        })
          //TODO getting devices of customer
          idForDeviceFetch = customerId;
          allForDeviceFetch = false;
          this.getDevices(idForDeviceFetch, manageCategory, allForDeviceFetch);
        }
      }
      else {
        console.log('something went wrong')
      }

    }

    /*

    async getAllGroups(from)
    {
      let dataGroupsTable = []
      let optionsGroup = [{ value: 'all', label: 'All' }]
      const fetchManage = await fetch('/group/manage/fetch/'+this.state.manageCategory+'/group');
      const res = await fetchManage.json();
      console.log('res',res);
      const data = res.data;
      const manage = data.manage;
      var snGroup = 1;
      for(var i in manage) {
        var item = manage[i];
        var subDealers = ""
        if(this.state.manageCategory == "dealer")
        {
        subDealers = item.dealer.subDealers;
        for(var j in subDealers)
        {
          var subDealer = subDealers[j];
          optionsSubDealer.push({
            "value":subDealer.id,
            "label":subDealer.name
          });
          var customers = subDealers[j].customers;
          for(var k in customers){
            var customer = customers[k];
            optionsCustomer.push({
              "value":customer.id,
              "label":customer.name
            });

            var groups = customers[k].groups;
            for(var l in groups)
            {
              var group = groups[l];
              optionsGroup.push({
                "value":group.id,
                "label":group.name
              });
              dataGroupsTable.push({
                  "id" : snGroup,
                  "groupId":group.id,
                  "name"  : group.name,
                  "customer" : customer.name,
              });
              snGroup++;
            }
          }
        }
        }
        else if(this.state.manageCategory == "subdealer") {
          subDealers = item.subDealer;
            var subDealer = subDealers;
            optionsSubDealer.push({
              "value":subDealer.id,
              "label":subDealer.name
            });
            var customers = subDealer.customers;
            for(var k in customers){
              var customer = customers[k];
              optionsCustomer.push({
                "value":customer.id,
                "label":customer.name
              });
              var groups = customers[k].groups;
              for(var l in groups)
              {
                var group = groups[l];
                optionsGroup.push({
                  "value":group.id,
                  "label":group.name
                });
                dataGroupsTable.push({
                    "id" : snGroup,
                    "groupId":group.id,
                    "name"  : group.name,
                    "customer" : customer.name,
                });
                snGroup++;
              }
            }
        }
        else if(this.state.manageCategory == "customer") {
            var customer = item.customer;
              optionsCustomer.push({
                "value":customer.id,
                "label":customer.name
              });
              var groups = customer.groups;
              for(var l in groups)
              {
                var group = groups[l];
                optionsGroup.push({
                  "value":group.id,
                  "label":group.name
                });
                dataGroupsTable.push({
                    "id" : snGroup,
                    "groupId":group.id,
                    "name"  : group.name,
                    "customer" : customer.name,
                });
                snGroup++;
              }
        }
        else {
          subDealers = item.subDealer;
            var subDealer = subDealers;
            optionsSubDealer.push({
              "value":subDealer.id,
              "label":subDealer.name
            });
            var customers = subDealer.customers;
            for(var k in customers){
              var customer = customers[k];
              optionsCustomer.push({
                "value":customer.id,
                "label":customer.name
              });
              var groups = customers[k].groups;
              for(var l in groups)
              {
                var group = groups[l];
                optionsGroup.push({
                  "value":group.id,
                  "label":group.name
                });
                dataGroupsTable.push({
                    "id" : snGroup,
                    "groupId":group.id,
                    "name"  : group.name,
                    "customer" : customer.name,
                });
                snGroup++;
              }
            }
        }


      }

        this.setState({
                      optionsGroup:optionsGroup
                      })
    }

    */

    async getCustomerGroups(customer)
    {
      console.log('inside getCustomerGroups, customer',customer)
      let optionsGroup = []
      if(undefined != customer)
      {
        optionsGroup = [{ value: 'all', label: 'All Groups' }]
        let customerId = customer.value;
        let customerName = customer.label;
        const fetchCustomerGroups = await fetch('/customer/'+customerId+'/groups');
        const resData = await fetchCustomerGroups.json()
        console.log('resData',resData)
        const data = resData;
        for(var i in data)
        {
          var group = data[i]
          optionsGroup.push({
            "value":group.id,
            "label":group.name
          });
        }
      }
      /*
      let groupValue = optionsGroup[0];
      if(undefined == groupValue)
      {
        groupValue = [];
      }
      this.setState({
                    optionsGroup:optionsGroup,
                    selectGroupValue:groupValue
                    })
      */
      return optionsGroup;
    }

    handleChangeGroupOption = (selectedOption) => {
        var groupId = selectedOption.value;
        this.setState({ selectedOption });
        console.log(`handleChangeGroupOption, Option selected:`, selectedOption);
        console.log('groupId',groupId)
        const manageCategory = this.state.manageCategory;
        let all = false;
        this.setState({
                      selectGroupValue:selectedOption,
                      vehicleTableLoading:true
                    });
        if(undefined != groupId && groupId == "all")
        {
          all = true;
          //if manageCategory is group then groupId replace with userId
          if(manageCategory == "group")
          {
            //TODO
            const userId = this.state.session.user.id;
            idForVehicleFetch = userId;
            allForVehicleFetch = all;
            this.getVehicles(idForVehicleFetch, manageCategory, allForVehicleFetch)
          }
          else {
            //TODO in other case getVehicleForSite with customerId
            const customerId = this.state.selectCustomerValueForVehicle.value;
            idForVehicleFetch = customerId;
            allForVehicleFetch = all;
            this.getVehicles(idForVehicleFetch, manageCategory, allForVehicleFetch)
          }
        }
        else if(undefined != groupId)
        {
          //getting customers of subDealer
          //this.getGroupVehicles(groupId);
          all = false;
          idForVehicleFetch = groupId;
          allForVehicleFetch = all;
          this.getVehicles(idForVehicleFetch, manageCategory, allForVehicleFetch)
        }

      }

    /*
    async getGroupVehicles(groupId)
    {
      console.log('inside getGroupVehicles, groupId',groupId)
      const fetchGroupVehicles = await fetch('/group/'+groupId+'/vehicles');
      const resData = await fetchGroupVehicles.json()
      console.log('group vehicles, res',resData)
      const data = resData;
      let dataVehiclesTable = []
      var snVehicles = 1;
      for(var i in data)
      {
        var item = data[i]
        var vehicleCategoryDoc = item.vehicleCategory;
        var vehicleCategory;
        var vehicleCategoryId;
        if(undefined != vehicleCategoryDoc)
        {
          vehicleCategory = vehicleCategoryDoc.name;
          vehicleCategoryId = vehicleCategoryDoc.id;
        }
        else {
          vehicleCategory = "";
          vehicleCategoryId = "";
        }
        dataVehiclesTable.push({
            "id" : snVehicles,
            "customerId":item.customerId,
            "vehicleId": item.id,
            "groupId": item.groupId,
            "regNo"  : item.regNo,
            "vehicleCategory":vehicleCategory,
            "vehicleCategoryId":vehicleCategoryId
          });
          snVehicles++;
      }
      this.setState({
                      dataVehiclesTable:dataVehiclesTable
                      })
    }
    */

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  toggleAddVehicle() {
    console.log('inside toggleAddVehicle')
    this.setState({
      isOpenModalAddVehicle: !this.state.isOpenModalAddVehicle,
    });
  }

  async handleAddVehicle()
  {
    console.log('inside handleAddVehicle')
    //TODO call according to manageCategory
    const {manageCategory} = this.state;
    this.setState({
                    groupId:'',
                    vehicleCategoryId:''
                  })
    if(manageCategory == "dealer" || manageCategory == "subdealer")
    {
      this.getSubDealers();
      this.setState({
                    optionsSelectSubDealer:optionsSelectSubDealer
                    })
    }
    else if(manageCategory == "customer") {
      this.getCustomers();
    }
    else {
      this.getGroups();
    }

    //TODO getting vehicleCategories;
    const vehicleCategory = await this.getVehicleCategory();

    this.setState({
                  optionsVehicleCategory:vehicleCategory,
                  selectVehicleCategory:'',
                  hmrRequire:undefined,
                  kmrRequire:undefined,
                  isUtilizationAdd:undefined
                  })

    this.toggleAddVehicle();
  }

  async getSubDealers()
  {
    let subDealersData = [];
    const fetchManage = await fetch('/group/manage/fetch/'+this.state.manageCategory+'/group');
    const res = await fetchManage.json();
    console.log('res',res);
    const data = res.data;
    const manage = data.manage;
    for(var i in manage) {
      var item = manage[i];
      var subDealers = ""
      if(this.state.manageCategory == "dealer")
      {
      subDealers = item.dealer.subDealers;
      for(var j in subDealers)
      {
        var subDealer = subDealers[j];
        subDealersData.push({
          "value":subDealer.id,
          "label":subDealer.name
        });
      }
      }
      else if(this.state.manageCategory == "subdealer") {
        subDealers = item.subDealer;
          var subDealer = subDealers;
          subDealersData.push({
            "value":subDealer.id,
            "label":subDealer.name
          });
      }
      else {
        subDealers = item.subDealer;
          var subDealer = subDealers;
          subDealersData.push({
            "value":subDealer.id,
            "label":subDealer.name
          });
      }

    }

    this.setState({
                  optionsSelectSubDealer:subDealersData,
                  optionsSelectCustomer:optionsSelectCustomer,
                  selectCustomerValueOfAddVehicle:optionsSelectCustomer,
                  selectCustomerValueOfAddDevice:optionsSelectCustomer,
                  optionsSelectGroup:optionsSelectGroup,
                  selectGroupValueOfAddVehicle:optionsSelectGroup
                  })
  }

  handleSelectSubDealer = (from,selectedOption) => {
    console.log('inside handleSelectSubDealer');
    console.log(`Option selected:`, from,selectedOption);
    //TODO getting customer according to subDealer
    this.getCustomersOfSubDealer(selectedOption.value);
    //TODO need to add condition based on from
    this.setState({
                    subDealerId:selectedOption.value,
                    optionsSelectCustomer:optionsSelectCustomer,
                    selectCustomerValueOfAddVehicle:optionsSelectCustomer,
                    selectCustomerValueOfAddDevice:optionsSelectCustomer,
                    optionsSelectGroup:optionsSelectGroup,
                    selectGroupValueOfAddVehicle:optionsSelectGroup,
                    optionsSelectVehicle:optionsSelectVehicle,
                    selectVehicleValue:optionsSelectVehicle
                  })
  }

  async getCustomers()
  {
    let customersData = [];
    const fetchManage = await fetch('/group/manage/fetch/'+this.state.manageCategory+'/group');
    const res = await fetchManage.json();
    console.log('res',res);
    const data = res.data;
    const manage = data.manage;
    for(var i in manage) {
      var item = manage[i];
      var customer = item.customer;
      customersData.push({
        "value":customer.id,
        "label":customer.name
      });
    }
    this.setState({
                  optionsSelectCustomer:customersData,
                  optionsSelectGroup:optionsSelectGroup
                  })
  }

  async getCustomersOfSubDealer(subDealerId)
  {
    let customersData = [];
    const fetchCustomer = await fetch('/subdealer/'+subDealerId+'/customers');
    const customer = await fetchCustomer.json();
    console.log('customer',customer);
    for(var i in customer) {
      var item = customer[i];
      customersData.push({
          "label" : item.name,
          "value"  : item.id,
      });
    }
    this.setState({
                  optionsSelectCustomer:customersData,
                  optionsSelectGroup:optionsSelectGroup
                  })

  }

  handleSelectCustomer = async (from,selectedOption) => {
    console.log('inside handleSelectCustomer');
    var customerId = selectedOption.value;
    console.log(`Option selected:`, from,selectedOption);
    if(from == "fromAddVehicle")
    {
      //TODO getting customer according to subDealer
      let groupsData = await this.getGroupsOfCustomer(selectedOption.value);
      this.setState({
                      selectCustomerValueOfAddVehicle:selectedOption,
                      selectGroupValueOfAddVehicle:optionsSelectGroup,
                      optionsSelectGroup:groupsData
                    })

    }
    else if(from == "fromAddDevice")
    {
      console.log('customer selected for add device')
      //TODO now getting list of vehicle based of customerId
      this.setState({
                      selectCustomerValueOfAddDevice:selectedOption,
                      selectVehicleValue:'',
                      vehicleId:''
                    })
      this.getVehiclesOfCustomer(customerId)
    }
    else {
      console.log('something went wrong')
    }
    this.setState({
                  customerId:customerId
                })
  }

  async getGroups()
  {
    let groupsData = [];
    const fetchManage = await fetch('/group/manage/fetch/'+this.state.manageCategory+'/vehicle');
    const res = await fetchManage.json();
    console.log('res',res);
    const data = res.data;
    const manage = data.manage;
    for(var i in manage) {
      var item = manage[i];
      var group = item.group;
      groupsData.push({
        "value":group.id,
        "label":group.name
      });
    }
    this.setState({
                  optionsSelectGroup:groupsData
                  })

  }

  async getGroupsOfCustomer(customerId)
  {
    let groupsData = [];
    const fetchGroup = await fetch('/customer/'+customerId+'/groups');
    const group = await fetchGroup.json();
    console.log('group',group);
    for(var i in group) {
      var item = group[i];
      groupsData.push({
          "label" : item.name,
          "value"  : item.id,
      });
    }
    /*
    this.setState({
                  optionsSelectGroup:groupsData
                  })
    */
    return groupsData;

  }

  handleSelectGroup = (from,selectedOption) => {
    console.log('inside handleSelectGroup');
    console.log(`Option selected:`, from,selectedOption);
    if(from == "fromAddVehicle")
    {
    this.setState({
                    groupId:selectedOption.value,
                    selectGroupValueOfAddVehicle:selectedOption
                  })
    }
    else if(from == "fromEditVehicle")
    {
      this.setState({
                      groupId:selectedOption.value,
                      selectGroupValueOfEditVehicle:selectedOption
                    })
    }
  }

  async getVehicleCategory()
  {
    console.log('inside getVehicleCategory')
    let vehicleCategory = [];
    const fetchCategory = await fetch('/vehiclecategory');
    const category = await fetchCategory.json();
    console.log('vehicle category',category);
    for(var i in category) {
      var item = category[i];
      vehicleCategory.push({
          "label" : item.name,
          "value"  : item.id,
      });
    }
    return vehicleCategory;
  }

  handleSelectVehicleCategory = (from,selectedOption) => {
    console.log('inside handleSelectVehicleCategory');
    console.log(`Option selected:`, from,selectedOption);
    this.setState({
                    vehicleCategoryId:selectedOption.value,
                    selectVehicleCategory:selectedOption
                  })
  }

  handleHmrKmrRequireRadio(from, e)
  {
    const { name, value } = e.target;
    console.log('name, value',name, value)
    if(from == "addVehicle")
    {
      if(undefined != value && value == "yes" && name == "hmrRequire")
      {
        this.setState({ hmrRequire: true });
      }
      else if(undefined != value && value == "no" && name == "hmrRequire") {
        this.setState({ hmrRequire: false });
      }
      else if(undefined != value && value == "yes" && name == "kmrRequire") {
        this.setState({ kmrRequire: true });
      }
      else if(undefined != value && value == "no" && name == "kmrRequire") {
        this.setState({ kmrRequire: false });
      }
    }
    else if(from == "editVehicle")
    {
      if(undefined != value && value == "yes" && name == "editHmrRequire")
      {
        this.setState({ editHmrRequire: true, hmrRequiredYes: true, hmrRequiredNo: false });
      }
      else if(undefined != value && value == "no" && name == "editHmrRequire") {
        this.setState({ editHmrRequire: false, hmrRequiredYes: false, hmrRequiredNo: true });
      }
      else if(undefined != value && value == "yes" && name == "editKmrRequire") {
        this.setState({ editKmrRequire: true, kmrRequiredYes: true, kmrRequiredNo: false });
      }
      else if(undefined != value && value == "no" && name == "editKmrRequire") {
        this.setState({ editKmrRequire: false, kmrRequiredYes: false, kmrRequiredNo: true });
      }
    }

  }

  handleUtilizationRadio(from, e)
  {
    const { name, value } = e.target;
    console.log('name, value',name, value)
    if(from == "addVehicle")
    {
      if(undefined != value && value == "yes")
      {
        this.setState({ isUtilizationAdd: true });
      }
      else {
        this.setState({ isUtilizationAdd: false });
      }
    }
    else if(from == "editVehicle")
    {
      if(undefined != value && value == "yes")
      {
        this.setState({ isUtilizationAdded:true, utilizationYes: true, utilizationNo: false });
      }
      else {
        this.setState({ isUtilizationAdded:false, utilizationYes: false, utilizationNo: true });
      }
    }
  }

  handleSaveVehicle = async ()  =>
  {
    console.log('Vehicle -->> inside handleSaveVehicle')
    let { regNo, customerId ,groupId, vehicleCategoryId, manageCategory,
            odometer, speedLimit, stoppageLimit, timeLimitOverspeed, maxDataInterval, notReachable,
            litrePerHour, kmPerLitre, driverName, driverMobileNo, hmrRequire, kmrRequire, remark, isUtilizationAdd } = this.state;
    let manage = []
    if(this.validateSaveVehicle())
    {
      console.log('validated success')
      if(undefined != odometer && odometer.trim() != "")
      {
        odometer = Number(odometer.trim());
      }
      else {
        odometer = null;
      }
      if(undefined != speedLimit && speedLimit.trim() != "")
      {
        speedLimit = Number(speedLimit.trim());
      }
      else {
        speedLimit = null;
      }
      if(undefined != stoppageLimit && stoppageLimit.trim() != "")
      {
        stoppageLimit = Number(stoppageLimit.trim());
      }
      else {
        stoppageLimit = null;
      }
      if(undefined != timeLimitOverspeed && timeLimitOverspeed.trim() != "")
      {
        timeLimitOverspeed = Number(timeLimitOverspeed.trim());
      }
      else {
        timeLimitOverspeed = null;
      }
      if(undefined != litrePerHour && litrePerHour.trim() != "")
      {
        litrePerHour = Number(litrePerHour.trim());
      }
      else {
        litrePerHour = null;
      }
      if(undefined != kmPerLitre && kmPerLitre.trim() != "")
      {
        kmPerLitre = Number(kmPerLitre.trim());
      }
      else {
        kmPerLitre = null;
      }
      if(undefined != driverName)
      {
        driverName = driverName.trim();
      }
      else {
        driverName = null;
      }
      if(undefined != driverMobileNo && driverMobileNo.trim() != "")
      {
        driverMobileNo = Number(driverMobileNo.trim());
      }
      else {
        driverMobileNo = null;
      }
      if(undefined != remark)
      {
        remark = remark.trim();
      }
      else {
        remark = null;
      }

      let utilizationParam = {};

      if(undefined != isUtilizationAdd && isUtilizationAdd)
      {
        const {speedIdleMin, speedIdleMax, speedProdMin, speedProdMax, speedMarcMin, speedMarcMax} = this.state;
        utilizationParam =  { speed :
                              {
                              idle : { min: Number(speedIdleMin), max: Number(speedIdleMax)},
                              production : { min: Number(speedProdMin), max: Number(speedProdMax)},
                              marching : {min: Number(speedMarcMin) , max: Number(speedMarcMax)}
                              }
                            }
      }
      else {
        utilizationParam = {};
      }

      console.log('utilizationParam:',utilizationParam)

      let payload = {
                      regNo:regNo,
                      customerId:customerId,
                      groupId:groupId,
                      vehicleCategoryId:vehicleCategoryId,
                      odometer:odometer,
                      speedLimit:speedLimit,
                      stoppageLimit:stoppageLimit,
                      timeLimitOverspeed:timeLimitOverspeed,
                      maxDataInterval:maxDataInterval,
                      notReachable:notReachable,
                      litrePerHour:litrePerHour,
                      kmPerLitre:kmPerLitre,
                      driverName:driverName,
                      driverMobileNo:driverMobileNo,
                      hmrRequire:hmrRequire,
                      kmrRequire:kmrRequire,
                      utilizationParam:utilizationParam,
                      remark:remark
                    }
      console.log('payload: ',payload)
      console.log('payload stringify:',JSON.stringify(payload))
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json',
                    'X-CSRF-Token': await NextAuth.csrfToken()
                  },
        body : JSON.stringify(payload)
      };
      const res = await fetch('/vehicle/create', requestOptions)
      const data = await res.json()
      console.log('created vehicle data ',data)
      this.setState({
                    vehicleTableLoading:true
                  })
      this.getVehicles(idForVehicleFetch, manageCategory, allForVehicleFetch)
      this.toggleAddVehicle();
    }
    else
    {
      console.log('validation failed')
    }
  }

  validateSaveVehicle()
  {
    console.log('Vehicle -->> inside validateSaveVehicle')
    const { regNo, groupId, vehicleCategoryId,
            odometer, speedLimit, stoppageLimit, timeLimitOverspeed, litrePerHour,
            kmPerLitre, driverName, driverMobileNo, remark } = this.state;
    if(undefined != groupId && groupId != "")
    {

    }
    else {
      alert('please select group')
      return false;
    }
    if(undefined != vehicleCategoryId && vehicleCategoryId != "")
    {

    }
    else {
      alert('please select vehicle category')
      return false;
    }

    if(undefined != regNo && regNo.length > 4)
    {

    }
    else {
      alert('please enter valid regNo')
      return false;
    }

    if(undefined != odometer)
    {
      console.log('odometer',odometer)
      if(!this.validateTextIsNumber(odometer))
      {
        alert('please enter valid odometer')
        return false;
      }
    }

    if(undefined != speedLimit)
    {
      console.log('speedLimit',speedLimit)
      if(!this.validateTextIsNumber(speedLimit))
      {
        alert('please enter valid speed limit')
        return false;
      }
    }

    if(undefined != stoppageLimit)
    {
      console.log('stoppageLimit',stoppageLimit)
      if(!this.validateTextIsNumber(stoppageLimit))
      {
        alert('please enter valid stoppage limit')
        return false;
      }
    }

    if(undefined != timeLimitOverspeed)
    {
      console.log('timeLimitOverspeed',timeLimitOverspeed)
      if(!this.validateTextIsNumber(timeLimitOverspeed))
      {
        alert('please enter valid time limit overspeed')
        return false;
      }
    }

    if(undefined != litrePerHour)
    {
      console.log('litrePerHour',litrePerHour)
      if(!this.validateTextIsNumber(litrePerHour))
      {
        alert('please enter valid litre per hour')
        return false;
      }
    }

    if(undefined != kmPerLitre)
    {
      console.log('kmPerLitre',kmPerLitre)
      if(!this.validateTextIsNumber(kmPerLitre))
      {
        alert('please enter valid km per litre')
        return false;
      }
    }

    if(undefined != driverMobileNo)
    {
      console.log('driverMobileNo',driverMobileNo)
      if(driverMobileNo.trim() != "")
      {
      var item = Number(driverMobileNo);
      console.log('driverMobileNo in number',item)
      if(isNaN(item) || item.toString().length != 10)
      {
        alert('please enter valid driver mobile no value')
        return false;
      }
      }
    }

    return true;
  }

  validateTextIsNumber(value)
  {
    console.log('inside validateTextIsNumber, value',value)
    var status = true;
    if(value.trim() != "")
    {
    var item = Number(value);
    console.log(value,' in number',item)
    if(isNaN(item))
    {
      status = false;
    }
    }
    else {
      status = true;
    }
    return status;
  }


  toggleEditVehicle() {
    console.log('inside toggleEditVehicle')
    this.setState({
      isOpenModalEditVehicle: !this.state.isOpenModalEditVehicle,
    });
  }

  async editVehicle(row) {
    //console.log('inside editVehicle', e.target.innerHTML)
    console.log('inside editVehicle, row', row)
    const vehicleRes = await fetch('/vehicle/'+row.vehicleId)
    const vehicle = await vehicleRes.json();
    console.log('vehicle: ',vehicle)
    //TODO getting vehicleCategories;
    const vehicleCategory = await this.getVehicleCategory();
    const addedVehicleCategory = {value:row.vehicleCategoryId, label:row.vehicleCategory};
    console.log('addedVehicleCategory',addedVehicleCategory)
    const customerId = row.customerId;
    const groupsData = await this.getGroupsOfCustomer(customerId);
    console.log('groupsData',groupsData)
    let addedGroup = [];
    for(var i in groupsData)
    {
        if(groupsData[i].value == row.groupId)
        {
          addedGroup = {label:groupsData[i].label ,value:groupsData[i].value}
          break;
        }
    }

    let odometer, speedLimit, stoppageLimit,
    timeLimitOverspeed, maxDataInterval, notReachable, litrePerHour, kmPerLitre,
    driverName, driverMobileNo, hmrRequire, kmrRequire, hmrRequiredYes, hmrRequiredNo,
    kmrRequiredYes, kmrRequiredNo, utilizationParam, isUtilizationAdded, utilizationYes, utilizationNo,
    speedIdleMin, speedIdleMax, speedProdMin, speedProdMax, speedMarcMin, speedMarcMax, remark;
    //console.log('odometer',odometer)
    if(vehicle.hasOwnProperty('odometer'))
    {
      odometer = vehicle.odometer;
    }
    //console.log('odometer:',odometer, 'vehicle.odometer',vehicle.odometer)
    if(vehicle.hasOwnProperty('speedLimit'))
    {
      speedLimit = vehicle.speedLimit;
    }
    if(vehicle.hasOwnProperty('stoppageLimit'))
    {
      stoppageLimit = vehicle.stoppageLimit;
    }
    if(vehicle.hasOwnProperty('timeLimitOverspeed'))
    {
      timeLimitOverspeed = vehicle.timeLimitOverspeed;
    }
    if(vehicle.hasOwnProperty('maxDataInterval'))
    {
      maxDataInterval = vehicle.maxDataInterval;
    }
    if(vehicle.hasOwnProperty('notReachable'))
    {
      notReachable = vehicle.notReachable;
    }
    if(vehicle.hasOwnProperty('litrePerHour'))
    {
      litrePerHour = vehicle.litrePerHour;
    }
    if(vehicle.hasOwnProperty('kmPerLitre'))
    {
      kmPerLitre = vehicle.kmPerLitre;
    }
    if(vehicle.hasOwnProperty('driverName'))
    {
      driverName = vehicle.driverName;
    }
    if(vehicle.hasOwnProperty('driverMobileNo'))
    {
      driverMobileNo = vehicle.driverMobileNo;
    }
    if(vehicle.hasOwnProperty('hmrRequire'))
    {
      hmrRequire = vehicle.hmrRequire;
      if(hmrRequire)
      {
        hmrRequiredYes = true;
        hmrRequiredNo = false;
      }
      else {
        hmrRequiredNo= true;
        hmrRequiredYes = false;
      }
    }
    if(vehicle.hasOwnProperty('kmrRequire'))
    {
      kmrRequire = vehicle.kmrRequire;
      if(kmrRequire)
      {
        kmrRequiredYes = true;
        kmrRequiredNo = false;
      }
      else {
        kmrRequiredNo= true;
        kmrRequiredYes = false;
      }
    }
    if(vehicle.hasOwnProperty('utilizationParam'))
    {

      utilizationParam = vehicle.utilizationParam;
      let speed = utilizationParam.speed;
      if(undefined != speed)
      {
        utilizationYes = true;
        utilizationNo = false;
        isUtilizationAdded = true;

        speedIdleMin = speed.idle.min;
        speedIdleMax = speed.idle.max;

        speedProdMin = speed.production.min;
        speedProdMax = speed.production.max;

        speedMarcMin = speed.marching.min;
        speedMarcMax = speed.marching.max;
      }
      else {
        utilizationYes = false;
        utilizationNo = true;
        isUtilizationAdded = false;
      }

    }
    else {
      utilizationYes = false;
      utilizationNo = true;
      isUtilizationAdded = false;
    }
    if(vehicle.hasOwnProperty('remark'))
    {
      remark = vehicle.remark;
    }
    this.setState({
                  editRegNo:row.regNo,
                  editVehicleId:row.vehicleId,
                  optionsVehicleCategory:vehicleCategory,
                  selectVehicleCategory:addedVehicleCategory,
                  addedGroupId:row.groupId,
                  optionsSelectGroup:groupsData,
                  selectGroupValueOfEditVehicle:addedGroup,
                  groupId:'',
                  editOdometer:odometer,
                  editSpeedLimit:speedLimit,
                  editStoppageLimit:stoppageLimit,
                  editTimeLimitOverspeed:timeLimitOverspeed,
                  editMaxDataInterval:maxDataInterval,
                  editNotReachable:notReachable,
                  editLitrePerHour:litrePerHour,
                  editKmPerLitre:kmPerLitre,
                  editDriverName:driverName,
                  editDriverMobileNo:driverMobileNo,
                  editHmrRequire:hmrRequire,
                  editKmrRequire:kmrRequire,
                  hmrRequiredYes:hmrRequiredYes,
                  hmrRequiredNo:hmrRequiredNo,
                  kmrRequiredYes:kmrRequiredYes,
                  kmrRequiredNo:kmrRequiredNo,
                  utilizationYes:utilizationYes,
                  utilizationNo:utilizationNo,
                  isUtilizationAdded:isUtilizationAdded,
                  editSpeedIdleMin:speedIdleMin,
                  editSpeedIdleMax:speedIdleMax,
                  editSpeedProdMin:speedProdMin,
                  editSpeedProdMax:speedProdMax,
                  editSpeedMarcMin:speedMarcMin,
                  editSpeedMarcMax:speedMarcMax,
                  editRemark:remark
                  })

    this.toggleEditVehicle();
  }

  /*
  handleEditHmrKmrRequireRadio(from, e)
  {
    console.log('inside handleEditHmrKmrRequireRadio, from:',from)
    const { name, value } = e.target;
    console.log('name, value',name, value)
    if(undefined != value && value == "yes" && name == "hmrRequiredYes")
    {
      this.setState({ editHmrRequire: true, hmrRequiredYes: true, hmrRequiredNo: false });
    }
    else if(undefined != value && value == "no" && name == "hmrRequiredNo") {
      this.setState({ editHmrRequire: false, hmrRequiredYes: false, hmrRequiredNo: true });
    }
    else if(undefined != value && value == "yes" && name == "kmrRequiredYes") {
      this.setState({ editKmrRequire: true, kmrRequiredYes: true, kmrRequiredNo: false });
    }
    else if(undefined != value && value == "no" && name == "kmrRequiredNo") {
      this.setState({ editKmrRequire: false, kmrRequiredYes: false, kmrRequiredNo: true });
    }
  }
  */

  async handleUpdateVehicle ()
  {
    console.log('inside handleUpdateVehicle')
    let { editVehicleId, editRegNo, selectVehicleCategory, manageCategory, groupId, addedGroupId,
            editOdometer, editSpeedLimit, editStoppageLimit, editTimeLimitOverspeed, editMaxDataInterval,
            editNotReachable, editLitrePerHour, editKmPerLitre, editDriverName, editDriverMobileNo,
            editHmrRequire, editKmrRequire, utilizationParam, isUtilizationAdded, utilizationYes, utilizationNo,
            editSpeedIdleMin, editSpeedIdleMax, editSpeedProdMin, editSpeedProdMax, editSpeedMarcMin,
            editSpeedMarcMax ,editRemark} = this.state
    const vehicleCategoryId = selectVehicleCategory.value;
    if(this.validateUpdateVehicle())
    {
      if(undefined != editOdometer && editOdometer.toString().trim() != "")
      {
        editOdometer = Number(editOdometer.toString().trim());
      }
      else {
        editOdometer = null;
      }
      if(undefined != editSpeedLimit && editSpeedLimit.toString().trim() != "")
      {
        editSpeedLimit = Number(editSpeedLimit.toString().trim());
      }
      else {
        editSpeedLimit = null;
      }
      if(undefined != editStoppageLimit && editStoppageLimit.toString().trim() != "")
      {
        editStoppageLimit = Number(editStoppageLimit.toString().trim());
      }
      else {
        editStoppageLimit = null;
      }
      if(undefined != editTimeLimitOverspeed && editTimeLimitOverspeed.toString().trim() != "")
      {
        editTimeLimitOverspeed = Number(editTimeLimitOverspeed.toString().trim());
      }
      else {
        editTimeLimitOverspeed = null;
      }
      if(undefined != editLitrePerHour && editLitrePerHour.toString().trim() != "")
      {
        editLitrePerHour = Number(editLitrePerHour.toString().trim());
      }
      else {
        editLitrePerHour = null;
      }
      if(undefined != editKmPerLitre && editKmPerLitre.toString().trim() != "")
      {
        editKmPerLitre = Number(editKmPerLitre.toString().trim());
      }
      else {
        editKmPerLitre = null;
      }
      if(undefined != editDriverName)
      {
        editDriverName = editDriverName.trim();
      }
      else {
        editDriverName = null;
      }
      if(undefined != editDriverMobileNo && editDriverMobileNo.toString().trim() != "")
      {
        editDriverMobileNo = Number(editDriverMobileNo.toString().trim());
      }
      else {
        editDriverMobileNo = null;
      }
      if(undefined != editRemark)
      {
        editRemark = editRemark.trim();
      }
      else {
        editRemark = null;
      }
      if(undefined != isUtilizationAdded && isUtilizationAdded)
      {
        utilizationParam =  { speed :
                              {
                              idle : { min: Number(editSpeedIdleMin), max: Number(editSpeedIdleMax)},
                              production : { min: Number(editSpeedProdMin), max: Number(editSpeedProdMax)},
                              marching : {min: Number(editSpeedMarcMin) , max: Number(editSpeedMarcMax)}
                              }
                            }
      }
      else {
        utilizationParam = {};
      }

      console.log('groupId',groupId,', addedGroupId',addedGroupId);
      let payload = {};
      payload = {
                  vehicleCategoryId:vehicleCategoryId,
                  odometer:editOdometer,
                  speedLimit:editSpeedLimit,
                  stoppageLimit:editStoppageLimit,
                  timeLimitOverspeed:editTimeLimitOverspeed,
                  maxDataInterval:editMaxDataInterval,
                  notReachable:editNotReachable,
                  litrePerHour:editLitrePerHour,
                  kmPerLitre:editKmPerLitre,
                  driverName:editDriverName,
                  driverMobileNo:editDriverMobileNo,
                  hmrRequire:editHmrRequire,
                  kmrRequire:editKmrRequire,
                  utilizationParam:utilizationParam,
                  remark:editRemark
                 }
      if(undefined != groupId && groupId != "" && groupId != addedGroupId)
      {
        console.log('group changed')
        payload.groupId = groupId;
      }
      else
      {
        console.log('group not changed')
      }
      console.log('payload for update vehicle: ',payload)
      console.log('payload: ',JSON.stringify(payload))
      const requestOptions = {
        method: 'PATCH',
        headers: { 'Content-Type': 'application/json',
                    'X-CSRF-Token': await NextAuth.csrfToken()
                  },
        body : JSON.stringify(payload)
      };
      console.log('vehicleId',editVehicleId);
      const res = await fetch('http://159.65.6.196:5500/api/vehicle/update/'+editVehicleId, requestOptions)
      const data = await res.json()
      console.log('updated vehicle data ',data)
      this.getVehicles(idForVehicleFetch, manageCategory, allForVehicleFetch)
      this.toggleEditVehicle();
    }
    else {
      console.log('validation failed for update vehicle')
    }
  }

  validateUpdateVehicle()
  {
    console.log('Vehicle -->> inside validateSaveVehicle')
    let { editRegNo, editOdometer, editSpeedLimit, editStoppageLimit,
            editTimeLimitOverspeed, editLitrePerHour, editKmPerLitre,
            editDriverName, editDriverMobileNo, editRemark } = this.state;
    if(undefined != editRegNo && editRegNo.length > 4)
    {
    }
    else {
      alert('please enter valid regNo')
      return false;
    }

    if(undefined != editOdometer)
    {
      console.log('editOdometer',editOdometer)
      if(!this.validateTextIsNumber(editOdometer.toString()))
      {
        alert('please enter valid odometer')
        return false;
      }
    }

    if(undefined != editSpeedLimit)
    {
      console.log('editSpeedLimit',editSpeedLimit)
      if(!this.validateTextIsNumber(editSpeedLimit.toString()))
      {
        alert('please enter valid speed limit')
        return false;
      }
    }

    if(undefined != editStoppageLimit)
    {
      console.log('editStoppageLimit',editStoppageLimit)
      if(!this.validateTextIsNumber(editStoppageLimit.toString()))
      {
        alert('please enter valid stoppage limit')
        return false;
      }
    }

    if(undefined != editTimeLimitOverspeed)
    {
      console.log('editTimeLimitOverspeed',editTimeLimitOverspeed)
      if(!this.validateTextIsNumber(editTimeLimitOverspeed.toString()))
      {
        alert('please enter valid time limit overspeed')
        return false;
      }
    }

    if(undefined != editLitrePerHour)
    {
      console.log('editLitrePerHour',editLitrePerHour)
      if(!this.validateTextIsNumber(editLitrePerHour.toString()))
      {
        alert('please enter valid litre per hour')
        return false;
      }
    }

    if(undefined != editKmPerLitre)
    {
      console.log('editKmPerLitre',editKmPerLitre)
      if(!this.validateTextIsNumber(editKmPerLitre.toString()))
      {
        alert('please enter valid km per litre')
        return false;
      }
    }

    if(undefined != editDriverMobileNo)
    {
      console.log('editDriverMobileNo',editDriverMobileNo)
      if(editDriverMobileNo.toString().trim() != "")
      {
      var item = Number(editDriverMobileNo);
      console.log('editDriverMobileNo in number',item,item.toString().length)
      if(isNaN(item) || item.toString().length != 10)
      {
        alert('please enter valid driver mobile no value')
        return false;
      }
      }
    }

    return true;
  }

  confirmDeleteVehicle(row)
  {
    deleteRow = row;
    console.log('deleteRow',deleteRow)
    this.toggleDeleteVehicle();
  }

  toggleDeleteVehicle() {
    this.setState({
      isOpenModalDeleteVehicle: !this.state.isOpenModalDeleteVehicle,
    });
  }

  async deleteVehicle() {
    const row = deleteRow;
    console.log('inside deleteVehicle, row',row)
    const {manageCategory} = this.state;
    const vehicleId = row.vehicleId;
    const requestOptions = {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                }
    };
    const deleteVehicle = await fetch('http://159.65.6.196:5500/api/vehicle/delete/'+vehicleId, requestOptions)
    const res = await deleteVehicle.json()
    console.log('delete vehicle res ',res)
    //this.getVehicles();
    this.getVehicles(idForVehicleFetch, manageCategory, allForVehicleFetch)
    this.toggleDeleteVehicle();
  }

  /////////////////////////
  async handleAddDevice()
  {
    console.log('inside handleAddDevice')
    let customersData = [];
    let deviceModelData = [];
    //TODO first getting subDealer then customer
    this.getSubDealers();
    const deviceModelRes = await fetch('/devicemodel')
    const deviceModel = await deviceModelRes.json();
    console.log('deviceModel',deviceModel)
    for(var i in deviceModel) {
      var item = deviceModel[i];
      deviceModelData.push({
          "label" : item.name,
          "value"  : item.id,
      });
    }
    let date = new Date();
    let dateTimeFormatted = moment(date).format("YYYY-MM-DDTHH:mm:ss.SSS[Z]")
    console.log('dateTimeFormatted',dateTimeFormatted)
    const simProviders = this.state.optionsSelectSimProvider;
    this.setState({
                  optionsSelectCustomer:customersData,
                  optionsSelectVehicle:optionsSelectVehicle,
                  selectVehicleValue:optionsSelectVehicle,
                  optionsSelectDeviceModel:deviceModelData,
                  optionsSelectSimProvider:simProviders,
                  selectSimProvider:[],
                  simProvider:'',
                  isAssignVehicle:false,
                  customerId: undefined,
                  type:undefined,
                  deviceModelId: undefined,
                  installedTime:dateTimeFormatted
                  })
    this.toggleAddDevice();
  }

  toggleAddDevice()
  {
      console.log('inside toggleAddDevice')
      this.setState({
        isOpenModalAddDevice: !this.state.isOpenModalAddDevice,
      });
  }

  handleAssignVehicleRadio(e)
  {
    const { name, value } = e.target;
    console.log('name, value',name, value)
    if(undefined != value && value == "yes")
    {
      this.setState({ isAssignVehicle: true });
    }
    else {
      this.setState({ isAssignVehicle: false });
    }
  }

  handleSelectDeviceModel = (selectedOption) => {
    console.log('inside handleSelectDeviceModel');
    console.log(`Option selected:`, selectedOption);
    this.setState({
                    deviceModelId:selectedOption.value
                })
  }

  handleSelectSimProvider = (selectedOption) => {
    console.log('inside handleSelectSimProvider');
    console.log(`Option selected:`, selectedOption);
    this.setState({
                    selectSimProvider:selectedOption,
                    simProvider:selectedOption.value
                })
  }

  async getVehiclesOfCustomer(customerId)
  {
    console.log('inside getVehiclesOfCustomer, customerId',customerId)
    let vehiclesData = [];
    const fetchVehicle = await fetch('/vehicle/fetchvehiclebycustomer/'+customerId);
    const resVehicle = await fetchVehicle.json();
    console.log('resVehicle',resVehicle);
    const vehicle = resVehicle.data;
    for(var i in vehicle) {
      var item = vehicle[i];
      vehiclesData.push({
          "label" : item.regNo,
          "value"  : item.id,
      });
    }
    this.setState({
                  optionsSelectVehicle:vehiclesData
                  })
  }

  handleSelectVehicle = (selectedOption) => {
    console.log('inside handleSelectVehicle');
    console.log(`Option selected:`, selectedOption);
    this.setState({
                    vehicleId:selectedOption.value,
                    selectVehicleValue:selectedOption
                })
  }

  handleSaveDevice = async ()  =>
  {
    console.log('Vehicle -->> inside handleSaveDevice')
    //TODO deviceId is IMEI
    const { manageCategory, isAssignVehicle, deviceId, customerId, type, deviceModelId, simProvider, simMobileNumber, vehicleId, installedTime } = this.state;
    //TODO need to validate save device
    if(this.validateSaveDevice())
    {
    let payload = {};
    let isAssign;
    if(isAssignVehicle && undefined != vehicleId && vehicleId != "")
    {
      isAssign = true;
      payload = {
                      deviceId:deviceId,
                      customerId:customerId,
                      type:type,
                      deviceModelId:deviceModelId,
                      vehicleId:vehicleId,
                      installedTime:installedTime,
                      simProvider:simProvider,
                      simMobileNumber:simMobileNumber
                    }
    }
    else {
      isAssign = false;
      payload = {
                  deviceId:deviceId,
                  customerId:customerId,
                  type:type,
                  deviceModelId:deviceModelId,
                  simProvider:simProvider,
                  simMobileNumber:simMobileNumber
                }
    }

      console.log('payload: ',payload)
      console.log('payload: ',JSON.stringify(payload))
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json',
                    'X-CSRF-Token': await NextAuth.csrfToken()
                  },
        body : JSON.stringify(payload)
      };
      const res = await fetch('/device/createandassignvehicle/'+isAssign, requestOptions)
      const data = await res.json()
      console.log('added device data ',data)
      this.setState({
                    deviceTableLoading:true
                  })
      this.getDevices(idForDeviceFetch, manageCategory, allForDeviceFetch);
      this.toggleAddDevice();
    }
  }

  validateSaveDevice()
  {
    const { customerId, vehicleId, type, deviceId, deviceModelId, simProvider, simMobileNumber, isAssignVehicle, installedTime } = this.state;
    if(undefined != customerId && customerId != "")
    {

    }
    else {
        alert('please select cutomer')
        return false;
    }

    if(undefined != type && type != "" && type != "select")
    {

    }
    else {
        alert('please select type')
        return false;
    }

    if(undefined != deviceId && deviceId != "")
    {

    }
    else {
      if(type == "gps")
      {
      alert('please enter valid imei number')
      }
      else {
        alert('please enter valid mac address')
      }
      return false;
    }

    if(undefined != deviceModelId && deviceModelId != "")
    {

    }
    else {
        alert('please select device model')
        return false;
    }

    if(isAssignVehicle)
    {
      if(undefined != vehicleId && vehicleId != "")
      {
      }
      else {
        alert('please select vehicle or check on No to Assign Vehicle')
        return false;
      }
    }
    else {
    }

    if(undefined != simProvider && simProvider != "")
    {

    }
    else {
      alert('please select sim provider')
      return false;
    }

    if(undefined != simMobileNumber && simMobileNumber.length == 10)
    {

    }
    else {
      alert('please insert valid mobile number')
      return false;
    }
    //All conditions are valid
    return true;
  }

  async assignVehicle(row)
  {
    console.log('inside assignVehicle, row',row);
    let customerId = row.customerId;
    let gpsDeviceId = row.id;
    let vehiclesData = [];
    const fetchVehicle = await fetch('/vehicle/fetchvehiclebycustomer/'+customerId);
    const resVehicle = await fetchVehicle.json();
    console.log('resVehicle',resVehicle);
    const vehicle = resVehicle.data;
    for(var i in vehicle) {
      var item = vehicle[i];
      vehiclesData.push({
          "label" : item.regNo,
          "value"  : item.id,
      });
    }
    let date = new Date();
    let dateTimeFormatted = moment(date).format("YYYY-MM-DDTHH:mm:ss.SSS[Z]")
    console.log('dateTimeFormatted',dateTimeFormatted)
    this.setState({
                  selectVehicleValue:optionsSelectVehicle,
                  optionsSelectVehicle:vehiclesData,
                  gpsDeviceId:gpsDeviceId,
                  installedTime:dateTimeFormatted,
                  vehicleId:''
                  })
    this.toggleAssignVehicle();
  }

  toggleAssignVehicle() {
    this.setState({
      isOpenModalAssignVehicle: !this.state.isOpenModalAssignVehicle,
    });
  }

  async handleAssignVehicle()
  {
    console.log('inside handleAssignVehicle')
    const { vehicleId, gpsDeviceId, installedTime, manageCategory} = this.state;
    console.log('vehicleId',vehicleId)
    //TODO need to validate assign vehicle
    if(undefined != vehicleId && vehicleId != "")
    {
      let payload = {
                      installedTime:installedTime,
                      vehicleId:vehicleId,
                      gpsDeviceId:gpsDeviceId
                    };
      console.log('payload: ',payload)
      console.log('payload: ',JSON.stringify(payload))
      const requestOptions = {
              method: 'POST',
              headers: { 'Content-Type': 'application/json',
                          'X-CSRF-Token': await NextAuth.csrfToken()
                        },
              body : JSON.stringify(payload)
              };

      const res = await fetch('/deviceassignment/create', requestOptions)
      const data = await res.json()
      console.log('assign vehicle res data ',data)
      this.setState({
                    deviceTableLoading:true
                  })
      this.getDevices(idForDeviceFetch, manageCategory, allForDeviceFetch);
      this.toggleAssignVehicle();
    }
    else {
        alert('please select vehicle')
    }
  }

  handleSelectDateTime(date)
  {
    let dateTime = date._d;
    console.log('inside handleSelectDateTime, selected datetime',dateTime)
    let dateTimeFormatted = moment(dateTime).format("YYYY-MM-DDTHH:mm:ss.SSS[Z]")
    console.log('dateTimeFormatted',dateTimeFormatted)
    this.setState({
                    installedTime:dateTimeFormatted
                })
  }

  editDevice(row)
  {
    console.log('inside edit device, row',row)
    const simProvider = row.simProvider;
    const selectSimProvider = [{value:simProvider, label:simProvider}]
    this.setState({
                  editDeviceId:row.id,
                  simProvider:simProvider,
                  selectSimProvider:selectSimProvider,
                  editSimMobileNumber:row.simMobileNumber
                  })
    this.toggleEditDevice();
  }

  toggleEditDevice() {
    this.setState({
      isOpenModalEditDevice: !this.state.isOpenModalEditDevice,
    });
  }

  async handleUpdateDevice()
  {
    console.log('inside handleUpdateDevice');
    const {manageCategory, editDeviceId, simProvider, selectSimProvider, editSimMobileNumber} = this.state;
    console.log('editDeviceId',editDeviceId,'simProvider',simProvider,' selectSimProvider', selectSimProvider,' editSimMobileNumber',editSimMobileNumber)
    //TODO validate simProvider & simMobileNumber then update
    if(undefined != simProvider)
    {
      if(undefined != editSimMobileNumber && editSimMobileNumber.length == 10)
      {
        let payload = {
                        simProvider:simProvider,
                        simMobileNumber:editSimMobileNumber
                      }
        console.log('payload for update vehicle: ',payload)
        console.log('payload: ',JSON.stringify(payload))
        const requestOptions = {
          method: 'PATCH',
          headers: { 'Content-Type': 'application/json',
                      'X-CSRF-Token': await NextAuth.csrfToken()
                    },
          body : JSON.stringify(payload)
        };
        const res = await fetch('http://159.65.6.196:5500/api/gpsdevice/'+editDeviceId, requestOptions)
        const data = await res.json()
        console.log('updated device data ',data)
        //this.getVehicles();
        this.getDevices(idForDeviceFetch, manageCategory, allForDeviceFetch)
        this.toggleEditDevice();
      }
      else {
        alert('please insert valid mobile number')
      }
    }
    else {
      alert('please select valid data')
    }
  }

  unAssignVehicle(row)
  {
    console.log('inside unAssignVehicle, row',row);
    const vehicleId = row.vehicleId;
    const gpsDeviceId = row.id;
    this.setState({
                  gpsDeviceId:gpsDeviceId,
                  vehicleId:vehicleId
                  })
    this.toggleUnAssignVehicle();
  }

  toggleUnAssignVehicle() {
    this.setState({
      isOpenModalUnAssignVehicle: !this.state.isOpenModalUnAssignVehicle,
    });
  }

  async handleUnAssignVehicle()
  {
    console.log('inside handUnAssignVehicle')
    const { gpsDeviceId, vehicleId, manageCategory } = this.state;
    //TODO need to validate unAssignVehicle
    const requestOptions = {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                }
    };
    //TODO need to confirm with user wants to delete or not
    const res = await fetch('/deviceassignment/unassign/'+gpsDeviceId+'/'+vehicleId, requestOptions)
    const data = await res.json()
    console.log('unassign device response data ',data)
    this.setState({
                  deviceTableLoading:true
                })
    this.getDevices(idForDeviceFetch, manageCategory, allForDeviceFetch);
    this.toggleUnAssignVehicle();
  }

  confirmDeleteDevice(row)
  {
    deleteRowDevice = row;
    console.log('deleteRowDevice',deleteRowDevice)
    this.toggleDeleteDevice();
  }

  toggleDeleteDevice() {
    this.setState({
      isOpenModalDeleteDevice: !this.state.isOpenModalDeleteDevice,
    });
  }

  async deleteDevice()
  {
    console.log('inside deleteDevice')
    const {manageCategory} = this.state;
    const row = deleteRowDevice;
    console.log('row',row)
    const gpsDeviceId = row.id;
    //TODO need to validate unAssignVehicle
    const requestOptions = {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                }
    };
    //TODO need to confirm with user wants to delete or not
    const deleteDevice = await fetch('http://159.65.6.196:5500/api/gpsdevice/delete/'+gpsDeviceId, requestOptions)
    const res = await deleteDevice.json()
    console.log('delete device response data ',res)
    this.setState({
                  deviceTableLoading:true
                })
    this.getDevices(idForDeviceFetch, manageCategory, allForDeviceFetch);
    this.toggleDeleteDevice();
  }

//TODO End of code change

  render() {
        console.log('Vehicle -->> inside render')
        const { selectedOption, isClearable, regNo, odometer, speedLimit,
                stoppageLimit, timeLimitOverspeed, maxDataInterval, notReachable,
                litrePerHour, kmPerLitre, driverName, driverMobileNo, remark } = this.state;
        const {editRegNo, editVehicleId, editOdometer, editSpeedLimit,
                editStoppageLimit, editTimeLimitOverspeed, editMaxDataInterval,
                editNotReachable, editLitrePerHour, editKmPerLitre,
                editDriverName, editDriverMobileNo, editRemark } = this.state
        const {type, deviceId, simMobileNumber, editSimMobileNumber} = this.state;
        const {manageCategory} = this.state;

        //const editFormatter = (cell, row, rowIndex, formatExtraData) => { console.log('editFormatter, row ',row); return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editUser.bind(this, row)}>Edit</Button> ); }
        const editFormatter = (cell, row, rowIndex, formatExtraData) => { return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editVehicle.bind(this, row)}>Edit</Button> ); }

        const deleteFormatter = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.confirmDeleteVehicle.bind(this, row)}>Delete</Button> ); }

        const editFormatterDevice = (cell, row, rowIndex, formatExtraData) => { return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editDevice.bind(this, row)}>Edit</Button> ); }

        const assignUnAssignFormaterDevice = (cell, row, rowIndex, formatExtraData) => {
           const vehicleId = row.vehicleId;
           if(undefined != vehicleId && vehicleId != "")
            {
             return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.unAssignVehicle.bind(this, row)}>UnAssign Vehicle</Button> );
            }
            else {
              return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.assignVehicle.bind(this, row)}>Assign Vehicle</Button> );
            }
          }

        const deleteFormatterDevice = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.confirmDeleteDevice.bind(this, row)}>Delete</Button> ); }

        let columnsVehiclesTable = [];
        let otherColumnsVehicleTable = [];

        columnsVehiclesTable = [{
          dataField: 'id',
          text: 'SN',
          sort: true
        }, {
          dataField: 'group',
          text: 'Group',
          sort: true,
          filter: textFilter()
        }, {
          dataField: 'regNo',
          text: 'RegNo',
          sort: true,
          filter: textFilter()
        },
        {
          dataField: 'vehicleCategory',
          text: 'Vehicle Category',
          sort: true,
          filter: textFilter()
        }
      ];

        let columnsDevicesTable = [];
        let otherColumnsDeviceTable = [];

        columnsDevicesTable = [{
          dataField: 'sn',
          text: 'SN',
          sort: true
        },
        {
          dataField: 'imei',
          text: 'IMEI',
          sort: true,
          filter: textFilter()
        },
        {
          dataField: 'customer',
          text: 'Customer',
          sort: true,
          filter: textFilter()
        },
        {
          dataField: 'vehicle',
          text: 'Vehicle',
          sort: true,
          filter: textFilter()
        },
        {
          dataField: 'type',
          text: 'Type',
          filter: textFilter()
        },
        {
          dataField: 'deviceModel',
          text: 'Model',
          sort: true,
          filter: textFilter()
        },
        {
          dataField: 'simProvider',
          text: 'Sim Provider',
          sort: true,
          filter: textFilter()
        },
        {
          dataField: 'simMobileNumber',
          text: 'Mobile No',
          sort: true,
          filter: textFilter()
        }
      ];

        //TODO only dealer & subDealer user can perform edit/delete operaiton
        if(manageCategory == "dealer" || manageCategory == "subdealer")
        {
          otherColumnsVehicleTable = [
            {
              dataField: 'edit',
              text: '',
              formatter: editFormatter,
              style: {
                fontWeight: 'bold',
                fontSize: '16px',
                cursor:'pointer',
                color:'blue'
              }
            }, {
              dataField: 'delete',
              text: '',
              formatter: deleteFormatter,
              style: {
                fontWeight: 'bold',
                fontSize: '16px',
                cursor:'pointer',
                color:'blue'
              }
            }
          ];

          otherColumnsDeviceTable = [
            {
              dataField: 'edit',
              text: '',
              formatter: editFormatterDevice,
              style: {
                fontWeight: 'bold',
                fontSize: '16px',
                cursor:'pointer',
                color:'blue'
              }
            },
            {
              dataField: '',
              text: '',
              formatter: assignUnAssignFormaterDevice,
              style: {
                fontWeight: 'bold',
                fontSize: '16px',
                cursor:'pointer',
                color:'blue'
              }
            },
            {
            dataField: 'delete',
            text: '',
            formatter: deleteFormatterDevice,
            style: {
              fontWeight: 'bold',
              fontSize: '16px',
              cursor:'pointer',
              color:'blue'
              }
            }
          ];
        }

        for(var i in otherColumnsVehicleTable)
        {
          columnsVehiclesTable.push(otherColumnsVehicleTable[i]);
        }

        for(var i in otherColumnsDeviceTable)
        {
          columnsDevicesTable.push(otherColumnsDeviceTable[i]);
        }

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12">
            <Card>
              <CardBody>
                <p><b>Vehicle</b></p>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs="12" className="mb-4">
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '1' })}
                  onClick={() => { this.toggle('1'); }}
                >
                  Vehicle Management
                </NavLink>
              </NavItem>
              {
              this.state.isDeviceManagementShow &&
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '2' })}
                  onClick={() => { this.toggle('2'); }}
                >
                  Device Management
                </NavLink>
              </NavItem>
              }
            </Nav>
            <TabContent activeTab={this.state.activeTab}>
              <TabPane tabId="1">
              <Row>
                <Col>
                  <Card>
                    <CardBody>
                    <Modal isOpen={this.state.isOpenModalAddVehicle} toggle={this.toggleAddVehicle}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleAddVehicle}>Add New Vehicle</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          {
                          //TODO not to show when customer & group level user logged in
                          this.state.isSubDealerShow && <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">SubDealer</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Select
                              onChange={this.handleSelectSubDealer.bind(this,'fromAddVehicle')}
                              isClearable={isClearable}
                              options={this.state.optionsSelectSubDealer}
                              placeholder={'Select SubDealer'}
                              backspaceRemovesValue={false}
                            />
                            </Col>
                          </FormGroup>
                          }
                          {
                          this.state.isCustomerShow && //TODO not to show when group level user logged in
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Customer</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Select
                              onChange={this.handleSelectCustomer.bind(this,'fromAddVehicle')}
                              value={this.state.selectCustomerValueOfAddVehicle}
                              isClearable={isClearable}
                              options={this.state.optionsSelectCustomer}
                              placeholder={'Select Customer'}
                              backspaceRemovesValue={false}
                            />
                            </Col>
                          </FormGroup>
                          }
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Group</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Select
                              onChange={this.handleSelectGroup.bind(this,'fromAddVehicle')}
                              value={this.state.selectGroupValueOfAddVehicle}
                              isClearable={isClearable}
                              options={this.state.optionsSelectGroup}
                              placeholder={'Select Group'}
                              backspaceRemovesValue={false}
                            />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Vehicle Category</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Select
                              onChange={this.handleSelectVehicleCategory.bind(this,'fromAddVehicle')}
                              value={this.state.selectVehicleCategory}
                              isClearable={isClearable}
                              options={this.state.optionsVehicleCategory}
                              placeholder={'Select Category'}
                              backspaceRemovesValue={false}
                            />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">RegNo</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="regNo" name="regNo" placeholder="RegNo" value={regNo} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Odometer (Number)</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="odometer" name="odometer" placeholder="Odometer" value={odometer} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Speed Limit (Number)</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="speedLimit" name="speedLimit" placeholder="Speed Limit" value={speedLimit} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Stoppage Limit (Min)</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="stoppageLimit" name="stoppageLimit" placeholder="Stoppage Limit" value={stoppageLimit} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Time Limit Overspeed (Sec)</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="timeLimitOverspeed" name="timeLimitOverspeed" placeholder="Time Limit Overspeed" value={timeLimitOverspeed} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Max Data Interval (Min)</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="maxDataInterval" name="maxDataInterval" placeholder="Max Data Interval" value={maxDataInterval} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Not Reachable (Min)</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="notReachable" name="notReachable" placeholder="Not Reachable" value={notReachable} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Litre Per Hour</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="litrePerHour" name="litrePerHour" placeholder="Litre Per Hour" value={litrePerHour} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Km Per Litre</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="kmPerLitre" name="kmPerLitre" placeholder="Km Per Litre" value={kmPerLitre} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Driver Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="driverName" name="driverName" placeholder="Driver Name" value={driverName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Driver Mobile No.</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="driverMobileNo" name="driverMobileNo" placeholder="Driver Mobile No." value={driverMobileNo} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">HMR Require</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="hmrRequire-radio1" name="hmrRequire" value="yes" onChange={this.handleHmrKmrRequireRadio.bind(this, 'addVehicle')} />
                                <Label className="form-check-label" check htmlFor="hmrRequire-radio1">Yes</Label>
                              </FormGroup>
                              <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="hmrRequire-radio2" name="hmrRequire" value="no" onChange={this.handleHmrKmrRequireRadio.bind(this, 'addVehicle')} />
                                <Label className="form-check-label" check htmlFor="hmrRequire-radio2">No</Label>
                              </FormGroup>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">KMR Require</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="kmrRequire-radio1" name="kmrRequire" value="yes" onChange={this.handleHmrKmrRequireRadio.bind(this, 'addVehicle')}/>
                                <Label className="form-check-label" check htmlFor="kmrRequire-radio1">Yes</Label>
                              </FormGroup>
                              <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="kmrRequire-radio2" name="kmrRequire" value="no" onChange={this.handleHmrKmrRequireRadio.bind(this, 'addVehicle')}/>
                                <Label className="form-check-label" check htmlFor="kmrRequire-radio2">No</Label>
                              </FormGroup>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Add Utilization</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="utilization-radio1" name="utilization" value="yes" onChange={this.handleUtilizationRadio.bind(this, 'addVehicle')} />
                                <Label className="form-check-label" check htmlFor="utilization-radio1">Yes</Label>
                              </FormGroup>
                              <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="utilization-radio2" name="utilization" value="no" onChange={this.handleUtilizationRadio.bind(this, 'addVehicle')} />
                                <Label className="form-check-label" check htmlFor="utilization-radio2">No</Label>
                              </FormGroup>
                            </Col>
                          </FormGroup>
                          {
                            this.state.isUtilizationAdd &&
                            <p><b>Speed Parameters:</b></p>
                          }
                          { this.state.isUtilizationAdd &&
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Idle</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Row>
                              <Col>
                              <Input type="text" id="speedIdleMin" name="speedIdleMin" placeholder="Min" value={this.state.speedIdleMin} onChange={this.handleChange} />
                              </Col>
                              <Col>
                              <Input type="text" id="speedIdleMax" name="speedIdleMax" placeholder="Max" value={this.state.speedIdleMax} onChange={this.handleChange} />
                              </Col>
                              </Row>
                            </Col>
                          </FormGroup>
                          }
                          { this.state.isUtilizationAdd &&
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Production</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Row>
                              <Col>
                              <Input type="text" id="speedProdMin" name="speedProdMin" placeholder="Min" value={this.state.speedProdMin} onChange={this.handleChange} />
                              </Col>
                              <Col>
                              <Input type="text" id="speedProdMax" name="speedProdMax" placeholder="Max" value={this.state.speedProdMax} onChange={this.handleChange} />
                              </Col>
                              </Row>
                            </Col>
                          </FormGroup>
                          }
                          { this.state.isUtilizationAdd &&
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Marching</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Row>
                              <Col>
                              <Input type="text" id="speedMarcMin" name="speedMarcMin" placeholder="Min" value={this.state.speedMarcMin} onChange={this.handleChange} />
                              </Col>
                              <Col>
                              <Input type="text" id="speedMarcMax" name="speedMarcMax" placeholder="Max" value={this.state.speedMarcMax} onChange={this.handleChange} />
                              </Col>
                              </Row>
                            </Col>
                          </FormGroup>
                          }
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Remark</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="remark" name="remark" placeholder="Remark" value={remark} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleSaveVehicle}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleAddVehicle}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalEditVehicle} toggle={this.toggleEditVehicle}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleEditVehicle}>Edit Vehicle</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">RegNo</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <Label htmlFor="text-input">{editRegNo}</Label>
                            {
                              //<Input type="text" id="editRegNo" name="editRegNo" placeholder="RegNo" value={editRegNo} onChange={this.handleChange} />
                            }
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">Vehicle Category</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <Select
                              onChange={this.handleSelectVehicleCategory.bind(this,'fromEditVehicle')}
                              value={this.state.selectVehicleCategory}
                              isClearable={isClearable}
                              options={this.state.optionsVehicleCategory}
                              backspaceRemovesValue={false}
                            />
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="select">Group</Label>
                          </Col>
                          <Col xs="12" md="9">
                          <Select
                            onChange={this.handleSelectGroup.bind(this,'fromEditVehicle')}
                            value={this.state.selectGroupValueOfEditVehicle}
                            isClearable={isClearable}
                            options={this.state.optionsSelectGroup}
                            backspaceRemovesValue={false}
                          />
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">Odometer (Number)</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <Input type="text" id="editOdometer" name="editOdometer" placeholder="Odometer" value={editOdometer} onChange={this.handleChange} />
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">Speed Limit (Number)</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <Input type="text" id="editSpeedLimit" name="editSpeedLimit" placeholder="Speed Limit" value={editSpeedLimit} onChange={this.handleChange} />
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">Stoppage Limit (Min)</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <Input type="text" id="editStoppageLimit" name="editStoppageLimit" placeholder="Stoppage Limit" value={editStoppageLimit} onChange={this.handleChange} />
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">Time Limit Overspeed (Sec)</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <Input type="text" id="editTimeLimitOverspeed" name="editTimeLimitOverspeed" placeholder="Time Limit Overspeed" value={editTimeLimitOverspeed} onChange={this.handleChange} />
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">Max Data Interval (Min)</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <Input type="text" id="editMaxDataInterval" name="editMaxDataInterval" placeholder="Max Data Interval" value={editMaxDataInterval} onChange={this.handleChange} />
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">Not Reachable (Min)</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <Input type="text" id="editNotReachable" name="editNotReachable" placeholder="Not Reachable" value={editNotReachable} onChange={this.handleChange} />
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">Litre Per Hour</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <Input type="text" id="editLitrePerHour" name="editLitrePerHour" placeholder="Litre Per Hour" value={editLitrePerHour} onChange={this.handleChange} />
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">Km Per Litre</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <Input type="text" id="editKmPerLitre" name="editKmPerLitre" placeholder="Km Per Litre" value={editKmPerLitre} onChange={this.handleChange} />
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">Driver Name</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <Input type="text" id="editDriverName" name="editDriverName" placeholder="Driver Name" value={editDriverName} onChange={this.handleChange} />
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">Driver Mobile No.</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <Input type="text" id="editDriverMobileNo" name="editDriverMobileNo" placeholder="Driver Mobile No." value={editDriverMobileNo} onChange={this.handleChange} />
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">HMR Require</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <FormGroup check inline>
                              <Input className="form-check-input" type="radio" id="editHmrRequire-radio1" name="editHmrRequire" value="yes" checked={this.state.hmrRequiredYes} onChange={this.handleHmrKmrRequireRadio.bind(this,'editVehicle')} />
                              <Label className="form-check-label" check htmlFor="editHmrRequire-radio1">Yes</Label>
                            </FormGroup>
                            <FormGroup check inline>
                              <Input className="form-check-input" type="radio" id="editHmrRequire-radio2" name="editHmrRequire" value="no" checked={this.state.hmrRequiredNo} onChange={this.handleHmrKmrRequireRadio.bind(this, 'editVehicle')} />
                              <Label className="form-check-label" check htmlFor="editHmrRequire-radio2">No</Label>
                            </FormGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">KMR Require</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <FormGroup check inline>
                              <Input className="form-check-input" type="radio" id="editKmrRequire-radio1" name="editKmrRequire" value="yes" checked={this.state.kmrRequiredYes} onChange={this.handleHmrKmrRequireRadio.bind(this, 'editVehicle')}/>
                              <Label className="form-check-label" check htmlFor="editKmrRequire-radio1">Yes</Label>
                            </FormGroup>
                            <FormGroup check inline>
                              <Input className="form-check-input" type="radio" id="editKmrRequire-radio2" name="editKmrRequire" value="no" checked={this.state.kmrRequiredNo} onChange={this.handleHmrKmrRequireRadio.bind(this, 'editVehicle')}/>
                              <Label className="form-check-label" check htmlFor="editKmrRequire-radio2">No</Label>
                            </FormGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">Add Utilization</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <FormGroup check inline>
                              <Input className="form-check-input" type="radio" id="editUtilization-radio1" name="editUtilization" value="yes" checked={this.state.utilizationYes} onChange={this.handleUtilizationRadio.bind(this, 'editVehicle')} />
                              <Label className="form-check-label" check htmlFor="editUtilization-radio1">Yes</Label>
                            </FormGroup>
                            <FormGroup check inline>
                              <Input className="form-check-input" type="radio" id="editUtilization-radio2" name="editUtilization" value="no" checked={this.state.utilizationNo} onChange={this.handleUtilizationRadio.bind(this, 'editVehicle')} />
                              <Label className="form-check-label" check htmlFor="editUtilization-radio2">No</Label>
                            </FormGroup>
                          </Col>
                        </FormGroup>
                        {
                          this.state.isUtilizationAdded &&
                          <p><b>Speed Parameters:</b></p>
                        }
                        { this.state.isUtilizationAdded &&
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">Idle</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <Row>
                            <Col>
                            <Input type="text" id="editSpeedIdleMin" name="editSpeedIdleMin" placeholder="Min" value={this.state.editSpeedIdleMin} onChange={this.handleChange} />
                            </Col>
                            <Col>
                            <Input type="text" id="editSpeedIdleMax" name="editSpeedIdleMax" placeholder="Max" value={this.state.editSpeedIdleMax} onChange={this.handleChange} />
                            </Col>
                            </Row>
                          </Col>
                        </FormGroup>
                        }
                        { this.state.isUtilizationAdded &&
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">Production</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <Row>
                            <Col>
                            <Input type="text" id="editSpeedProdMin" name="editSpeedProdMin" placeholder="Min" value={this.state.editSpeedProdMin} onChange={this.handleChange} />
                            </Col>
                            <Col>
                            <Input type="text" id="editSpeedProdMax" name="editSpeedProdMax" placeholder="Max" value={this.state.editSpeedProdMax} onChange={this.handleChange} />
                            </Col>
                            </Row>
                          </Col>
                        </FormGroup>
                        }
                        { this.state.isUtilizationAdded &&
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">Marching</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <Row>
                            <Col>
                            <Input type="text" id="editSpeedMarcMin" name="editSpeedMarcMin" placeholder="Min" value={this.state.editSpeedMarcMin} onChange={this.handleChange} />
                            </Col>
                            <Col>
                            <Input type="text" id="editSpeedMarcMax" name="editSpeedMarcMax" placeholder="Max" value={this.state.editSpeedMarcMax} onChange={this.handleChange} />
                            </Col>
                            </Row>
                          </Col>
                        </FormGroup>
                        }
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input">Remark</Label>
                          </Col>
                          <Col xs="12" md="9">
                            <Input type="text" id="editRemark" name="editRemark" placeholder="Remark" value={editRemark} onChange={this.handleChange} />
                          </Col>
                        </FormGroup>
                      </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleUpdateVehicle}>Update</Button>{' '}
                        <Button color="secondary" onClick={this.toggleEditVehicle}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalDeleteVehicle} toggle={this.toggleDeleteVehicle}
                           className={'modal-danger ' + this.props.className}>
                      <ModalHeader toggle={this.toggleDeleteVehicle}>Vehicle Delete</ModalHeader>
                      <ModalBody>
                        Are you sure to delete Vehicle
                      </ModalBody>
                      <ModalFooter>
                        <Button color="danger" onClick={this.deleteVehicle}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteVehicle}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                      {
                        this.state.vehicleTableLoading &&
                        <div>
                        <div style={{marginLeft: '40%'}}>
                        <ClipLoader
                          sizeUnit={"px"}
                          size={50}
                          color={'#123abc'}
                          loading={this.state.vehicleTableLoading}
                        />
                        </div>
                        <div style={{marginLeft: '35%'}}>
                        <p>Loading ... Please wait!</p>
                        </div>
                        </div>
                      }
                      {
                      !this.state.vehicleTableLoading &&
                      <ToolkitProvider
                            keyField="id"
                            data={ this.state.dataVehiclesTable }
                            columns={ columnsVehiclesTable }
                            search
                          >
                            {
                              props => (
                                <div>
                                  <Row className="align-items-center">
                                  <Col>
                                  <Row>
                                  {
                                  //TODO not to show when customer & group level user logged in
                                  this.state.isSubDealerShow &&
                                  <Col md="4">
                                  <p>SubDealer</p>
                                  <Select
                                    onChange={this.handleChangeSubDealerOption.bind(this,'vehicleManagement')}
                                    value={this.state.selectSubDealerValueForVehicle}
                                    isClearable={isClearable}
                                    options={this.state.optionsSubDealer}
                                    placeholder={'Select SubDealer'}
                                    backspaceRemovesValue={false}
                                  />
                                  </Col>
                                  }
                                  {
                                  //TODO not to show when group level user logged in
                                  this.state.isCustomerShow &&
                                  <Col md="4">
                                  <p>Customer</p>
                                  <Select
                                    onChange={this.handleChangeCustomerOption.bind(this,'vehicleManagement')}
                                    value={this.state.selectCustomerValueForVehicle}
                                    isClearable={isClearable}
                                    options={this.state.optionsCustomerForVehicle}
                                    placeholder={'Select Customer'}
                                    backspaceRemovesValue={false}
                                  />
                                  </Col>
                                  }
                                  <Col md="4">
                                  <p>Group</p>
                                  <Select
                                    onChange={this.handleChangeGroupOption}
                                    value={this.state.selectGroupValue}
                                    isClearable={isClearable}
                                    options={this.state.optionsGroup}
                                    placeholder={'Select Group'}
                                    backspaceRemovesValue={false}
                                  />
                                  </Col>
                                  </Row>
                                  </Col>
                                  <Col md="2">
                                  {
                                    //<h4>Input something at below input field:</h4>
                                  }
                                  <SearchBar
                                    { ...props.searchProps }
                                  />
                                  </Col>
                                  { this.state.isAddVehicleButtonShow &&
                                  <Col md="2">
                                    <Button block color="primary" className="btn-pill" onClick={this.handleAddVehicle}>Add Vehicle</Button>
                                  </Col>
                                  }
                                  </Row>
                                  <hr />
                                  <div style={{overflow: 'overlay'}}>
                                  <BootstrapTable
                                    { ...props.baseProps }
                                    filter={ filterFactory() }
                                    pagination={ paginationFactory() }
                                    hover
                                  />
                                  </div>
                                </div>
                              )
                            }
                          </ToolkitProvider>
                          }
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </TabPane>
              {
              this.state.isDeviceManagementShow &&
              <TabPane tabId="2">
              <Row>
                <Col>
                  <Card>
                    <CardBody>
                    <Modal isOpen={this.state.isOpenModalAddDevice} toggle={this.toggleAddDevice}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleAddDevice}>Add New Device</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                      <FormGroup row>
                        <Col md="3">
                          <Label htmlFor="select">SubDealer</Label>
                        </Col>
                        <Col xs="12" md="9">
                        <Select
                          onChange={this.handleSelectSubDealer.bind(this,'fromAddDevice')}
                          isClearable={isClearable}
                          options={this.state.optionsSelectSubDealer}
                          placeholder={'Select SubDealer'}
                          backspaceRemovesValue={false}
                        />
                        </Col>
                      </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Customer</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Select
                              onChange={this.handleSelectCustomer.bind(this,'fromAddDevice')}
                              value={this.state.selectCustomerValueOfAddDevice}
                              isClearable={isClearable}
                              options={this.state.optionsSelectCustomer}
                              placeholder={'Select Customer'}
                              backspaceRemovesValue={false}
                            />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label>Assign Vehicle</Label>
                            </Col>
                            <Col md="9">
                              <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="isAssignVehicle-radio1" name="isAssignVehicleRadio" value="yes" onChange={this.handleAssignVehicleRadio} />
                                <Label className="form-check-label" check htmlFor="isAssignVehicle-radio1">Yes</Label>
                              </FormGroup>
                              <FormGroup check inline>
                                <Input className="form-check-input" type="radio" id="isAssignVehicle-radio2" name="isAssignVehicleRadio" value="no" onChange={this.handleAssignVehicleRadio}/>
                                <Label className="form-check-label" check htmlFor="isAssignVehicle-radio2">No</Label>
                              </FormGroup>
                            </Col>
                          </FormGroup>
                          {
                            this.state.isAssignVehicle &&
                            <FormGroup row>
                              <Col md="3">
                                <Label htmlFor="select">Vehicle</Label>
                              </Col>
                              <Col xs="12" md="9">
                              <Select
                                onChange={this.handleSelectVehicle}
                                value={this.state.selectVehicleValue}
                                isClearable={isClearable}
                                options={this.state.optionsSelectVehicle}
                                placeholder={'Select Vehicle'}
                                backspaceRemovesValue={false}
                              />
                              </Col>
                            </FormGroup>
                          }
                          {
                            this.state.isAssignVehicle &&
                            <FormGroup row>
                              <Col md="3">
                                <Label htmlFor="select">Installation Time</Label>
                              </Col>
                              <Col xs="12" md="9">
                              <Datetime
                                defaultValue = {new Date()}
                                onChange= {this.handleSelectDateTime.bind(this)}
                                dateFormat = "DD-MM-YYYY"
                               />
                              </Col>
                            </FormGroup>
                          }
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Type</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="type" id="selectDeviceType" value={type} onChange={this.handleChange}>
                                <option value="select">Please Select Type</option>
                                <option value="gps">GPS</option>
                                <option value="generator">Generator</option>
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">{this.state.type == "generator" ? "MAC" : "IMEI"}</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="text-input" name="deviceId" value={deviceId} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Device Model</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Select
                              onChange={this.handleSelectDeviceModel}
                              isClearable={isClearable}
                              options={this.state.optionsSelectDeviceModel}
                              placeholder={'Select Model'}
                              backspaceRemovesValue={false}
                            />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Sim Provider</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Select
                              onChange={this.handleSelectSimProvider}
                              value={this.state.selectSimProvider}
                              isClearable={isClearable}
                              options={this.state.optionsSelectSimProvider}
                              placeholder={'Select Sim Provider'}
                              backspaceRemovesValue={false}
                            />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Sim Mobile Number</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="text-input" name="simMobileNumber" value={simMobileNumber} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleSaveDevice}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleAddDevice}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalEditDevice} toggle={this.toggleEditDevice}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleEditDevice}>Update Device</ModalHeader>
                      <ModalBody>
                        <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                            <FormGroup row>
                              <Col md="3">
                                <Label htmlFor="select">Sim Provider</Label>
                              </Col>
                              <Col xs="12" md="9">
                              <Select
                                onChange={this.handleSelectSimProvider}
                                value={this.state.selectSimProvider}
                                isClearable={isClearable}
                                options={this.state.optionsSelectSimProvider}
                                placeholder={'Select Sim Provider'}
                                backspaceRemovesValue={false}
                              />
                              </Col>
                            </FormGroup>
                            <FormGroup row>
                              <Col md="3">
                                <Label htmlFor="select">Sim Mobile Number</Label>
                              </Col>
                              <Col xs="12" md="9">
                                <Input type="text" id="text-input" name="editSimMobileNumber" value={editSimMobileNumber} onChange={this.handleChange} />
                              </Col>
                            </FormGroup>
                          </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleUpdateDevice}>Update</Button>{' '}
                        <Button color="secondary" onClick={this.toggleEditDevice}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalAssignVehicle} toggle={this.toggleAssignVehicle}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleAssignVehicle}>Assign Vehicle</ModalHeader>
                      <ModalBody>
                        <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                            <FormGroup row>
                              <Col md="3">
                                <Label htmlFor="select">Vehicle</Label>
                              </Col>
                              <Col xs="12" md="9">
                              <Select
                                onChange={this.handleSelectVehicle}
                                value={this.state.selectVehicleValue}
                                isClearable={isClearable}
                                options={this.state.optionsSelectVehicle}
                                placeholder={'Select Vehicle'}
                                backspaceRemovesValue={false}
                              />
                              </Col>
                            </FormGroup>
                            <FormGroup row>
                              <Col md="3">
                                <Label htmlFor="select">Installation Time</Label>
                              </Col>
                              <Col xs="12" md="9">
                              <Datetime
                                defaultValue = {new Date()}
                                onChange= {this.handleSelectDateTime.bind(this)}
                                dateFormat = "DD-MM-YYYY"
                               />
                              </Col>
                            </FormGroup>
                          </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleAssignVehicle}>Assing</Button>{' '}
                        <Button color="secondary" onClick={this.toggleAssignVehicle}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalUnAssignVehicle} toggle={this.toggleUnAssignVehicle}
                           className={'modal-danger ' + this.props.className}>
                      <ModalHeader toggle={this.toggleUnAssignVehicle}>UnAssign Vehicle</ModalHeader>
                      <ModalBody>
                        Are you sure to unassign vehicle
                      </ModalBody>
                      <ModalFooter>
                        <Button color="danger" onClick={this.handleUnAssignVehicle}>UnAssign</Button>{' '}
                        <Button color="secondary" onClick={this.toggleUnAssignVehicle}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalDeleteDevice} toggle={this.toggleDeleteDevice}
                           className={'modal-danger ' + this.props.className}>
                      <ModalHeader toggle={this.toggleDeleteDevice}>Device Delete</ModalHeader>
                      <ModalBody>
                        Are you sure to delete Device
                      </ModalBody>
                      <ModalFooter>
                        <Button color="danger" onClick={this.deleteDevice}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteDevice}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    {
                      this.state.deviceTableLoading &&
                      <div>
                      <div style={{marginLeft: '40%'}}>
                      <ClipLoader
                        sizeUnit={"px"}
                        size={50}
                        color={'#123abc'}
                        loading={this.state.deviceTableLoading}
                      />
                      </div>
                      <div style={{marginLeft: '35%'}}>
                      <p>Loading ... Please wait!</p>
                      </div>
                      </div>
                    }
                    {
                    !this.state.deviceTableLoading &&
                    <ToolkitProvider
                          keyField="id"
                          data={ this.state.dataDevicesTable }
                          columns={ columnsDevicesTable }
                          search
                        >
                          {
                            props => (
                              <div>
                                <Row className="align-items-center">
                                <Col>
                                <Row>
                                {
                                //TODO not to show when customer & group level user logged in
                                this.state.isSubDealerShow &&
                                <Col md="4">
                                <p>SubDealer</p>
                                <Select
                                  onChange={this.handleChangeSubDealerOption.bind(this,'deviceManagement')}
                                  value={this.state.selectSubDealerValueForDevice}
                                  isClearable={isClearable}
                                  options={this.state.optionsSubDealer}
                                  placeholder={'Select SubDealer'}
                                  backspaceRemovesValue={false}
                                />
                                </Col>
                                }
                                {
                                //TODO not to show when group level user logged in
                                this.state.isCustomerShow &&
                                <Col md="4">
                                <p>Customer</p>
                                <Select
                                  onChange={this.handleChangeCustomerOption.bind(this,'deviceManagement')}
                                  value={this.state.selectCustomerValueForDevice}
                                  isClearable={isClearable}
                                  options={this.state.optionsCustomerForDevice}
                                  placeholder={'Select Customer'}
                                  backspaceRemovesValue={false}
                                />
                                </Col>
                                }
                                </Row>
                                </Col>
                                <Col md="2">
                                {
                                  //<h4>Input something at below input field:</h4>
                                }
                                <SearchBar
                                  { ...props.searchProps }
                                />
                                </Col>
                                {
                                  //TODO add new device show only dealer & subDealer level user
                                }
                                { this.state.isAddDeviceButtonShow &&
                                <Col md="2">
                                  <Button block color="primary" className="btn-pill" onClick={this.handleAddDevice}>Add Device</Button>
                                </Col>
                                }
                                </Row>
                                <hr />
                                <div style={{overflow: 'overlay'}}>
                                <BootstrapTable
                                  { ...props.baseProps }
                                  filter={ filterFactory() }
                                  pagination={ paginationFactory() }
                                  hover
                                />
                                </div>
                              </div>
                            )
                          }
                        </ToolkitProvider>
                        }
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </TabPane>
              }
            </TabContent>
          </Col>
        </Row>
      </div>
    );

}
}

export default Vehicle;
