import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Form,
  FormGroup,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Label,
  FormText
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'

import Select from 'react-select';
import classnames from 'classnames';

import { NextAuth } from 'next-auth/client'

import Router from 'next/router'

//react-bootstrap-table (deprecated)
//import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
//react-bootstrap-table2
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import cellEditFactory from 'react-bootstrap-table2-editor';
//For table search
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
// react-table not working properly (failing)
//import ReactTable from 'react-table'
//import FilterableTable from 'react-filterable-table'

import DropdownTreeSelect from 'react-dropdown-tree-select'
import 'react-dropdown-tree-select/dist/styles.css'

import PropTypes from 'prop-types';

import { ClipLoader } from 'react-spinners';

let selectedNodesManageCustomer = [];

const { SearchBar } = Search;

const dataManageDropDown = [{
  label: 'search me',
  value: 'searchme',
  children: [
    {
      label: 'search me too',
      value: 'searchmetoo',
      children: [
        {
          label: 'No one can get me',
          value: 'anonymous'
        }
      ]
    }
  ]
},
{
  label:'second',
  value:'second'
},
{
  label:'third',
  value:'third'
},
{
  label:'fourth',
  value:'fourth',
  checked:true
},
]

const onChange = (currentNode, selectedNodes) => {
  console.log('onChange::', currentNode, selectedNodes)
  selectedNodesManageCustomer = selectedNodes;
}
const onAction = ({ action, node }) => {
  console.log(`onAction:: [${action}]`, node)
}
const onNodeToggle = currentNode => {
  console.log('onNodeToggle::', currentNode)
}

const optionsSubDealer = [{ value: 'all', label: 'All' }];

const optionsCustomer = [{ value: 'all', label: 'All' }];

const optionsSelectSubDealer = [];

const selectRow = {
  mode: 'checkbox',
  clickToSelect: false,
  onSelect: (row, isSelect, rowIndex, e) => {
    console.log(row.id);
    console.log(isSelect);
    console.log(row);
    console.log(rowIndex);
    console.log(e);
  },
  onSelectAll: (isSelect, rows, e) => {
    console.log(isSelect);
    console.log(rows);
    console.log(e);
  }
};

const divStyle = {
  margin: '40px',
  border: '5px solid pink'
};

//
const dataBootstrapTable = [
  { id: 1, phoneNumber: '9799605400', name:'Ashwin', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
  { id: 2, phoneNumber: '9024038439', name:'Raji', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
  { id: 3, phoneNumber: '9024038434', name:'Aman', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
  { id: 4, phoneNumber: '9024038433', name:'Akshit', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
];

const dataUsersTable = [];
const dataCustomersTable = [];

const dataManageCustomerDropDown = [];

let deleteUserRow, deleteCustomerRow;

//const editFormatter = (cell, row, rowIndex, formatExtraData) => { console.log('editFormatter, row ',row); return ( <Button block color="primary" className="btn-pill" size="sm" >Edit</Button> ); }
//const deleteFormatter = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm">Delete</Button> ); }

class Customer extends Component {
  constructor(props) {
    super(props);

    console.log('Customer, props',this.props)

    this.toggle = this.toggle.bind(this);
    this.toggleAddUser = this.toggleAddUser.bind(this);
    this.toggleEditUser = this.toggleEditUser.bind(this);
    this.editUser = this.editUser.bind(this);
    this.toggleDeleteUser = this.toggleDeleteUser.bind(this);
    this.confirmDeleteUser = this.confirmDeleteUser.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
    this.handleAddUser = this.handleAddUser.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSaveUser = this.handleSaveUser.bind(this);
    this.handleUpdateUser = this.handleUpdateUser.bind(this);
    this.handleAddCustomer = this.handleAddCustomer.bind(this);
    this.toggleAddCustomer = this.toggleAddCustomer.bind(this);
    this.handleSaveCustomer = this.handleSaveCustomer.bind(this);
    this.editCustomer = this.editCustomer.bind(this);
    this.toggleEditCustomer = this.toggleEditCustomer.bind(this);
    this.handleUpdateCustomer = this.handleUpdateCustomer.bind(this);
    this.toggleDeleteCustomer = this.toggleDeleteCustomer.bind(this);
    this.confirmDeleteCustomer = this.confirmDeleteCustomer.bind(this);
    this.deleteCustomer = this.deleteCustomer.bind(this);

    this.state = {
      manageCategory:this.props.managecategory,
      dropdownOpen: false,
      radioSelected: 2,
      selectedOption: null,
      isClearable: true,
      activeTab: '1',
      isOpenModalAddUser: false,
      isOpenModalEditUser: false,
      isOpenModalDeleteUser: false,
      isOpenModalAddCustomer: false,
      isOpenModalEditCustomer: false,
      isOpenModalDeleteCustomer: false,
      dataManageDropDown:dataManageDropDown,
      dataUsersTable:dataUsersTable,
      dataCustomersTable:dataCustomersTable,
      optionsSubDealer:optionsSubDealer,
      optionsCustomer:optionsCustomer,
      optionsSelectSubDealer:optionsSelectSubDealer,
      dataManageCustomerDropDown:dataManageCustomerDropDown,
      selectCustomerValue:"",
      userTableLoading:true,
      customerTableLoading:true
    };
  }

  async componentDidMount() {
    console.log('Customer -->> inside componentDidMount')
    //TODO check user logged in or not:
    const session = await NextAuth.init({force: true})
    if(session.user)
    {
      console.log('user logged in')
    }
    else {
      console.log('user not logged in')
      Router.push('/auth/callback')
      return false;
    }
    console.log('after checking user state')
    //getting users
    this.getUsers();
    this.getManages();
  }

  async getUsers()
  {
    this.setState({dataUsersTable:[]})
    const res = await fetch('/customer/users/'+this.state.manageCategory)
    const resData = await res.json()
    console.log('data ',resData)
    //{ id: 1, phoneNumber: '9799605400', name:'Ashwin', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
    let dataUsersTable = []
    const data = resData.data;
    const users = data;
    var snUsers = 1;
    for (var i in users)
    {
      var item = users[i];
      var manages = item.manages;
      var manageCustomerName = "";
      for(var j in manages)
      {
        var manageCustomer = manages[j].customer;
        manageCustomerName += manageCustomer.name+"\n";
      }
      dataUsersTable.push({
          "id" : snUsers,
          "userId": item.id,
          "subDealerId":item.subDealerId,
          "manageCategoryId":item.manageCategoryId,
          "phoneNumber" : item.phone,
          "name"  : item.firstName+' '+item.lastName,
          "manage" : manageCustomerName,
          "role" : item.role,
          "status"  : item.status,
          "lastLogin"  : '15 Aug 2018',
        });
        snUsers++;
    }

    this.setState({
                  userTableLoading:false,
                  dataUsersTable:dataUsersTable
                  })
  }

  async getManages()
  {

    let dataCustomersTable = []
    this.setState({dataCustomersTable:dataCustomersTable})
    let optionsSubDealer = [{ value: 'all', label: 'All' }]
    let optionsCustomer = [{ value: 'all', label: 'All' }]
    const fetchManage = await fetch('/customer/manage/fetch/'+this.state.manageCategory+'/customer');
    const res = await fetchManage.json();
    console.log('res',res);
    const data = res.data;
    const manage = data.manage;
    var snCustomer = 1;
    for(var i in manage) {
      var item = manage[i];
      var subDealers = ""
      if(this.state.manageCategory == "dealer")
      {
      subDealers = item.dealer.subDealers;
      for(var j in subDealers)
      {
        var subDealer = subDealers[j];
        optionsSubDealer.push({
          "value":subDealer.id,
          "label":subDealer.name
        });
        var customers = subDealers[j].customers;
        for(var k in customers){
          var customer = customers[k];
          optionsCustomer.push({
            "value":customer.id,
            "label":customer.name
          });
          //TODO need to add some conditions(if/else) on customer table,
          //because we using single function getManages from different locations and updating data in table
          dataCustomersTable.push({
              "id" : snCustomer,
              "customerId":customer.id,
              "name"  : customer.name,
              "subDealer" : subDealer.name,
          });
          snCustomer++;
        }
      }
      }
      else {
        subDealers = item.subDealer;
          var subDealer = subDealers;
          optionsSubDealer.push({
            "value":subDealer.id,
            "label":subDealer.name
          });
          var customers = subDealer.customers;
          for(var k in customers){
            var customer = customers[k];
            optionsCustomer.push({
              "value":customer.id,
              "label":customer.name
            });
            //TODO need to add some conditions(if/else) on customer table,
            //because we using single function getManages from different locations and updating data in table
            dataCustomersTable.push({
                "id" : snCustomer,
                "customerId":customer.id,
                "name"  : customer.name,
                "subDealer" : subDealer.name,
            });
            snCustomer++;
          }
      }


    }

    this.setState({
                  customerTableLoading:false,
                  dataCustomersTable:dataCustomersTable,
                  optionsSubDealer:optionsSubDealer,
                  optionsCustomer:optionsCustomer,
                  selectCustomerValue:optionsCustomer[0]
                  })
  }

  async checkDuplicateInObject(propertyName, inputArray) {
    var seenDuplicate = false,
        manageUsersObject = [];

    inputArray.map(function(item) {
      var itemPropertyName = item.users[propertyName];
      if (itemPropertyName in manageUsersObject) {
        manageUsersObject[itemPropertyName].duplicate = true;
        item.duplicate = true;
        seenDuplicate = true;
      }
      else {
        manageUsersObject[itemPropertyName] = item;
        delete item.duplicate;
      }
    });
    //return seenDuplicate;
    return manageUsersObject;
  }

  handleChange(e) {
      const { name, value } = e.target;
      this.setState({ [name]: value });
  }

  handleChangeSubDealerOption = (from, selectedOption) => {
      var subDealerId = selectedOption.value;
      if(from == "userManagement")
      {
        this.setState({ selectedOption,
                        selectCustomerValue:"",
                        selectSubDealerValueForUser:selectedOption
                        });
      }
      console.log(`handleChangeSubDealerOption, Option selected:`, selectedOption);
      console.log('subDealerId',subDealerId)
      if(undefined != subDealerId && subDealerId == "all")
      {
        //getting all customers
        this.getAllCustomers(from);
      }
      else if(undefined != subDealerId)
      {
        //getting customers of subDealer
        this.getSubDealerCustomers(from, selectedOption);
      }
    }

    //Getting all customers according to from(UserManagement / CustomerManagement)
    async getAllCustomers(from)
    {
      let dataCustomersTable = []
      if(from == "customerManagement")
      {
        this.setState({
                      dataCustomersTable:dataCustomersTable
                      })
      }
      let optionsCustomer = [{ value: 'all', label: 'All' }]
      const fetchManage = await fetch('/customer/manage/fetch/'+this.state.manageCategory+'/customer');
      const res = await fetchManage.json();
      console.log('res',res);
      const data = res.data;
      const manage = data.manage;
      var snCustomer = 1;
      for(var i in manage) {
        var item = manage[i];
        var subDealers = ""
        if(this.state.manageCategory == "dealer")
        {
        subDealers = item.dealer.subDealers;
        for(var j in subDealers)
        {
          var subDealer = subDealers[j];
          optionsSubDealer.push({
            "value":subDealer.id,
            "label":subDealer.name
          });
          var customers = subDealers[j].customers;
          for(var k in customers){
            var customer = customers[k];
              optionsCustomer.push({
                "value":customer.id,
                "label":customer.name
              });
              dataCustomersTable.push({
                  "id" : snCustomer,
                  "customerId":customer.id,
                  "name"  : customer.name,
                  "subDealer" : subDealer.name,
              });
              snCustomer++;
          }
        }
        }
        else {
          subDealers = item.subDealer;
            var subDealer = subDealers;
            optionsSubDealer.push({
              "value":subDealer.id,
              "label":subDealer.name
            });
            var customers = subDealer.customers;
            for(var k in customers){
              var customer = customers[k];

              optionsCustomer.push({
                "value":customer.id,
                "label":customer.name
              });
              dataCustomersTable.push({
                  "id" : snCustomer,
                  "customerId":customer.id,
                  "name"  : customer.name,
                  "subDealer" : subDealer.name,
              });
              snCustomer++;
            }
        }

      }
      if(from == "userManagement")
      {
        this.setState({
                      optionsCustomer:optionsCustomer
                      })
      }
      else if(from == "customerManagement")
      {
        this.setState({
                      dataCustomersTable:dataCustomersTable
                      })
      }
      else {
        console.log('something went wrong')
      }
    }

    async getSubDealerCustomers(from, selectedOption)
    {
      if(from == "customerManagement")
      {
        this.setState({
                      dataCustomersTable:[]
                      })
      }
      let subDealerId = selectedOption.value;
      let subDealerName = selectedOption.label;
      console.log('inside getSubDealerUsers, subDealerId',subDealerId)
      let optionsCustomer = []
      const fetchSubDealerCustomers = await fetch('/subdealer/'+subDealerId+'/customers');
      const resData = await fetchSubDealerCustomers.json()
      console.log('resData',resData)
      const data = resData;
      if(from == "userManagement")
      {
        if(undefined != data && data.length > 0)
        {
          optionsCustomer.push({ value: 'all', label: 'All' });
          for(var i in data)
          {
            var customer = data[i]
            optionsCustomer.push({
              "value":customer.id,
              "label":customer.name
            });
          }
        }
      this.setState({
                    optionsCustomer:optionsCustomer
                    })
      }
      else if(from == "customerManagement")
      {
        let dataCustomersTable = []
        let snCustomer = 1;
        for(var i in data)
        {
          var customer = data[i]
          dataCustomersTable.push({
              "id" : snCustomer,
              "customerId":customer.id,
              "name"  : customer.name,
              "subDealer" : subDealerName,
          });
          snCustomer++;
        }
        this.setState({
                      dataCustomersTable:dataCustomersTable
                      })
      }
      else {
        console.log('something went wrong')
      }
    }

  handleChangeCustomerOption = (selectedOption) => {
      var customerId = selectedOption.value;
      this.setState({ selectedOption,
                      selectCustomerValue:selectedOption});
      console.log(`handleChangeCustomerOption, Option selected:`, selectedOption);
      console.log('customerId',customerId)
      if(undefined != customerId && customerId == "all")
      {
        //TODO need to get users based on subdealer all customers
        //If selectSubDealerValueForUser is any subdealer not all subdealer
        //So getting all customer users based on subdealer id
        const {selectSubDealerValueForUser} = this.state;
        if(undefined != selectSubDealerValueForUser && selectSubDealerValueForUser.value != "all")
        {
            this.getAllCustomerUsersOfSubDealer(selectSubDealerValueForUser);
        }
        else {
          //getting all customer users
          this.setState({dataUsersTable:[]},() =>
          {
          this.getUsers();
          });

        }
      }
      else if(undefined != customerId && customerId == "select")
      {
        //getting all customers of selected subDealer
      }
      else if(undefined != customerId)
      {
        //getting customers of subDealer
        this.getCustomerUsers(customerId);
      }
      }

      async getAllCustomerUsersOfSubDealer(selectedSubDealer)
      {
        this.setState({dataUsersTable:[]})
        console.log('inside getAllCustomerUsersOfSubDealer, selectedSubDealer',selectedSubDealer)
        const subDealer = selectedSubDealer;
        const subDealerId= subDealer.value;
        const fetchCustomerUsers = await fetch('http://159.65.6.196:5500/api/subdealer/'+subDealerId+'?filter[include][users][manages][customer]');
        const res = await fetchCustomerUsers.json()
        console.log('all customer users of subDealer, res',res)
        const users = res.users;
        let snUsers = 1;
        let dataUsersTable = [];
        for(var i in users)
        {
          var item = users[i];
          var manages = item.manages;
          var manageCustomerName = "";
          for(var j in manages)
          {
            var manageCustomer = manages[j].customer;
            manageCustomerName += manageCustomer.name+"\n";
          }
          dataUsersTable.push({
              "id" : snUsers,
              "userId": item.id,
              "subDealerId":item.subDealerId,
              "manageCategoryId":item.manageCategoryId,
              "phoneNumber" : item.phone,
              "name"  : item.firstName+' '+item.lastName,
              "manage" : manageCustomerName,
              "role" : item.role,
              "status"  : item.status,
              "lastLogin"  : '15 Aug 2018',
            });
            snUsers++;
        }

        this.setState({
                      userTableLoading:false,
                      dataUsersTable:dataUsersTable
                      })
      }

      async getCustomerUsers(customerId)
      {
        this.setState({dataUsersTable:[]})
        console.log('inside getCustomerUsers, customerId',customerId)
        const fetchCustomerUsers = await fetch('/customer/fetchusersanditsmanages/'+customerId);
        const resData = await fetchCustomerUsers.json()
        let dataUsersTable = []
        var snUsers = 1;
        console.log('subDealerUsers data',resData)
        const data = resData.data;
        for(var i in data) {
          var user = data[i].users
          var manages = user.manages;
          var manageCustomerName = "";
          for(var j in manages)
          {
            var manageCustomer = manages[j].customer;
            manageCustomerName += manageCustomer.name+"\n";
          }
            dataUsersTable.push({
                "id" : snUsers,
                "userId": user.id,
                "subDealerId":user.subDealerId,
                "manageCategoryId":user.manageCategoryId,
                "phoneNumber" : user.phone,
                "name"  : user.firstName+' '+user.lastName,
                "manage" : manageCustomerName,
                "role" : user.role,
                "status"  : user.status,
                "lastLogin"  : '15 Aug 2018',
              });
              snUsers++;
          }
          this.setState({
                        dataUsersTable:dataUsersTable
                        })
      }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  toggleAddUser() {
    console.log('inside toggleAddUser')
    this.setState({
      isOpenModalAddUser: !this.state.isOpenModalAddUser,
    });
  }

  async handleAddUser()
  {
    console.log('inside handleAddUser')
    //TODO getting subDealers
    this.getSubDealers();
    this.toggleAddUser();
  }

  async getSubDealers()
  {
    //let subDealersData = [{ value: 'select subdealer', label: 'Select SubDealer' }];
    let subDealersData = [];
    const fetchManage = await fetch('/customer/manage/fetch/'+this.state.manageCategory+'/customer');
    const res = await fetchManage.json();
    console.log('res',res);
    const data = res.data;
    const manage = data.manage;
    for(var i in manage) {
      var item = manage[i];
      var subDealers = ""
      if(this.state.manageCategory == "dealer")
      {
      subDealers = item.dealer.subDealers;
      for(var j in subDealers)
      {
        var subDealer = subDealers[j];
        subDealersData.push({
          "value":subDealer.id,
          "label":subDealer.name
        });
      }
      }
      else {
        subDealers = item.subDealer;
          var subDealer = subDealers;
          subDealersData.push({
            "value":subDealer.id,
            "label":subDealer.name
          });
      }
    }
    //let optionsSubDealer = this.createSubDealerItems(subDealersData);
    this.setState({
                  optionsSelectSubDealer:subDealersData,
                  dataManageCustomerDropDown:dataManageCustomerDropDown
                  })
  }

  /*
  createSubDealerItems(subDealers)
  {
    //TODO roles came from database
    let subDealer = [{id:'1',name:'Select SubDealer'}]
    console.log('inside createSelectRoleItems, role, roles',subDealers,subDealer)
    let items = [];
    let i = 0;
    items.push(<option key={i} value={subDealer[i].id}>{subDealer[i].name}</option>);
    if(undefined != subDealers)
    {
      for(var j in subDealers)
      {
        items.push(<option key={++i} value={subDealers[j].id}>{subDealers[j].name}</option>);
      }
    }
     return items;
  }
*/

  handleSelectSubDealer = (from,selectedOption) => {
    console.log('inside handleSelectSubDealer');
    console.log(`Option selected:`, from,selectedOption);
    if(from == "userManagement")
    {
      //TODO getting customer according to subDealer
      this.getCustomers(selectedOption.value);
    }
    this.setState({subDealerId:selectedOption.value})
  }

  async getCustomers(subDealerId)
  {
    const fetchCustomer = await fetch('/subdealer/'+subDealerId+'/customers');
    const customer = await fetchCustomer.json();
    console.log('customer',customer);
    let dataManageCustomerDropDown = [];
    for(var i in customer) {
      var item = customer[i];
      dataManageCustomerDropDown.push({
          "label" : item.name,
          "value"  : item.id,
      });
    }
    this.setState({
                  dataManageCustomerDropDown:dataManageCustomerDropDown
                  })

  }

  onChangeCustomers = (from, currentNode, selectedNodes) => {
    console.log('onChangeCustomers::,from, currentNode, selectedNodes', from, currentNode, selectedNodes)
    selectedNodesManageCustomer = selectedNodes;
    const {dataManageCustomerDropDown, dataManagedCustomerDropDown} = this.state;
    if(from == "fromAddUser")
    {
      for(var i in dataManageCustomerDropDown)
      {
        var item = dataManageCustomerDropDown[i];
        if(item.value == currentNode.value)
        {
          dataManageCustomerDropDown[i].checked = currentNode.checked;
        }
      }
      this.setState({
                    dataManageCustomerDropDown:dataManageCustomerDropDown
                    })
    }
    else if(from == "fromEditUser") {
      for(var i in dataManagedCustomerDropDown)
      {
        var item = dataManagedCustomerDropDown[i];
        if(item.value == currentNode.value)
        {
          dataManagedCustomerDropDown[i].checked = currentNode.checked;
        }
      }
      this.setState({
                    dataManagedCustomerDropDown:dataManagedCustomerDropDown
                    })
    }
  }

  handleSaveUser = async ()  =>
  {
    console.log('Customer -->> inside handleSaveUser')
    const { phone,firstName,lastName,password,address,city,state,country,zip,role,status,subDealerId } = this.state;
    let manageCustomer = []
    if(this.validateSaveUser())
    {
      console.log('validated success')
      console.log('selectedNodesManageCustomer ', selectedNodesManageCustomer)

      for(var i in selectedNodesManageCustomer) {
          var item = selectedNodesManageCustomer[i];
          manageCustomer.push({
              "customerId" : item.value,
            });
        }

      console.log('manageCustomer as: ',manageCustomer)
      let payload = {
                      phone:'+91'+phone,
                      firstName:firstName,
                      lastName:lastName,
                      password:password,
                      address:address,
                      city:city,
                      state:state,
                      country:country,
                      zipCode:zip,
                      role:role,
                      status:status,
                      subDealerId:subDealerId,
                      manageCategory: 'customer',
                      userLevel:'customer',
                      manage:manageCustomer,
                    }
      console.log('payload: ',payload)
      console.log('payload: ',JSON.stringify(payload))
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json',
                    'X-CSRF-Token': await NextAuth.csrfToken()
                  },
        body : JSON.stringify(payload)
      };
      const res = await fetch('/customer/user/register', requestOptions)
      const data = await res.json()
      console.log('register customer user data ',data)
      selectedNodesManageCustomer = [];
      this.getUsers();
      this.toggleAddUser();
    }
    else
    {
      console.log('validation failed')
    }
  }

  validateSaveUser()
  {
    console.log('Customer -->> inside validateSaveUser')
    const { phone,firstName,lastName,password,address,city,state,country,zip,role,status } = this.state;
    console.log('form as',phone,firstName,lastName,password,address,city,state,country,zip,role,status)
    if(undefined != phone && phone.length > 9)
    {
    }
    else {
      alert('please enter valid phone number')
      return false;
    }
    if(undefined != firstName && undefined != lastName)
    {
    }
    else {
      alert('please enter name')
      return false;
    }
    if(undefined != password && password.length > 4)
    {
    }
    else {
      alert('please enter password atleast 5 character')
      return false;
    }
    if(undefined != address && undefined != city && undefined != state && undefined != country)
    {
    }
    else {
      alert('please enter valid address')
      return false;
    }
    if(undefined != zip && zip.length > 5)
    {
    }
    else {
      alert('please enter valid zipcode, alteast 6 digit')
      return false;
    }
    if(undefined != role && role != "select")
    {
    }
    else {
      alert('please select valid role')
      return false;
    }
    if(undefined != status && status != "select")
    {
    }
    else {
      alert('please select valid status')
      return false;
    }
    if(selectedNodesManageCustomer.length > 0)
    {
    }
    else {
      alert('please select atleast one customer manage')
      return false;
    }
    return true;
  }

  toggleEditUser() {
    console.log('inside toggleEditUser')
    this.setState({
      isOpenModalEditUser: !this.state.isOpenModalEditUser,
    });
  }

  async editUser(row) {
    //console.log('inside editUser', e.target.innerHTML)
    console.log('inside editUser, row', row)
    selectedNodesManageCustomer = [];
    const userRes = await fetch('/customer/user/'+row.userId)
    const user = await userRes.json();
    console.log('user: ',user)
    //TODO here manageCategory is customer because user is customer level and we fetch manages of customer level user
    const manageCategory = "customer";
    const manageRes = await fetch('/customer/fetchusermanagesandavailablemanages/'+row.userId+'/'+manageCategory+'/'+row.subDealerId)
    const manageByUser = await manageRes.json();
    console.log('manageByUser',manageByUser)
    const data = manageByUser.data;
    const allAvailableManages = data.allAvailableManages.docs.docs;
    const manage = data.manage;
    console.log('manage: ',manageByUser)
    let dataManagedCustomerDropDown = [];
    for(var i in allAvailableManages) {
      var item = allAvailableManages[i];
      var itemFound = false;
      for(var j in manage)
      {
        var managedItem = manage[j]
        console.log('customer: ',item.id)
        console.log('managedCustomer ', managedItem.customerId)
        if (item.id == managedItem.customerId )
        {
         itemFound = true;
        break;
        }
        else {
          itemFound = false;
          continue;
        }

      }
      console.log('itemFound', itemFound)
      if(itemFound)
        {
          dataManagedCustomerDropDown.push({
            "label" : item.name,
            "value"  : item.id,
            checked: true
          });
        }
        else {
          dataManagedCustomerDropDown.push({
            "label" : item.name,
            "value"  : item.id,
          });
        }

    }
    console.log('dataManagedCustomerDropDown:', dataManagedCustomerDropDown);
    selectedNodesManageCustomer = dataManagedCustomerDropDown;
    //
    const roles = this.createSelectRoleItems(user.role)
    const statuses = this.createSelectStatusItems(user.status)
    this.setState({
                  editUserId:row.userId,
                  editFirstName:user.firstName,
                  editLastName:user.lastName,
                  editAddress:user.address,
                  editCity:user.city,
                  editState:user.state,
                  editCountry:user.country,
                  editZip:user.zipCode,
                  editRole:user.role,
                  roles:roles,
                  editStatus:user.status,
                  statuses:statuses,
                  dataManagedCustomerDropDown:dataManagedCustomerDropDown,
                  editManageCategoryId:row.manageCategoryId
                  })

    this.toggleEditUser();
  }

  createSelectRoleItems(role)
  {
    //TODO roles came from database
    let roles = [{id:'1',name:'admin'},{id:'2',name:'non-admin'}]
    console.log('inside createSelectRoleItems, role, roles',role,roles)
    let items = [];
    if(undefined != role)
    {
      items.push(<option key={0} value={role}>{role}</option>);
      for(var i in roles)
      {
        if(roles[i].name != role)
        {
        items.push(<option key={i} value={roles[i].name}>{roles[i].name}</option>);
        }
      }
  }
  else {
     items = roles;
  }
     return items;
  }

  createSelectStatusItems(status)
  {
    //TODO roles came from database
    let statuses = [{id:'1',name:'active'},{id:'2',name:'inactive'}]
    let items = [];
    if(undefined != status)
    {
      items.push(<option key={0} value={status}>{status}</option>);
      for(var i in statuses)
      {
        if(statuses[i].name != status)
        {
        items.push(<option key={i} value={statuses[i].name}>{statuses[i].name}</option>);
        }
      }
  }
  else {
    items = statuses;
  }
     return items;
  }

  async handleUpdateUser ()
  {
    console.log('inside handleUpdateUser')
    const {editUserId, editManageCategoryId, editFirstName, editLastName, editAddress, editCity, editState, editCountry, editZip, editRole, editStatus} = this.state
    console.log('selectedNodesManageCustomer',selectedNodesManageCustomer);
    let manageCustomer = []
    if(this.validateUpdateUser())
    {
    for(var i in selectedNodesManageCustomer) {
        var item = selectedNodesManageCustomer[i];
        if(item.checked)
        {
        manageCustomer.push({
            "customerId" : item.value,
            "name"  : item.label,
          });
        }
      }
    console.log('manageCustomer as: ',manageCustomer)
    let payload = {
                    firstName:editFirstName,
                    lastName:editLastName,
                    address:editAddress,
                    city:editCity,
                    state:editState,
                    country:editCountry,
                    zipCode:editZip,
                    role:editRole,
                    status:editStatus,
                    manage:manageCustomer,
                    manageCategoryId: editManageCategoryId
                  }
    console.log('payload for update user: ',payload)
    console.log('payload: ',JSON.stringify(payload))
    const requestOptions = {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    const res = await fetch('/customer/updateuser/'+editUserId, requestOptions)
    const data = await res.json()
    console.log('updated customer user data ',data)
    selectedNodesManageCustomer = [];
    this.getUsers();
    this.toggleEditUser();
    }
    else {
      console.log('validation failed for update user')
    }
  }

  validateUpdateUser()
  {
    console.log('User_Management -->> inside validateSaveUser')
    const {editFirstName, editLastName, editAddress, editCity, editState, editCountry, editZip, editRole, editStatus} = this.state

    if(undefined != editFirstName && undefined != editLastName)
    {
    }
    else {
      alert('please enter name')
      return false;
    }
    if(undefined != editAddress && undefined != editCity && undefined != editState && undefined != editCountry)
    {
    }
    else {
      alert('please enter valid address')
      return false;
    }
    if(undefined != editZip && editZip.length > 5)
    {
    }
    else {
      alert('please enter valid zipcode, alteast 6 digit')
      return false;
    }
    if(undefined != editRole && editRole != "select")
    {
    }
    else {
      alert('please select valid role')
      return false;
    }
    if(undefined != editStatus && editStatus != "select")
    {
    }
    else {
      alert('please select valid status')
      return false;
    }
    return true;
  }

  confirmDeleteUser(row)
  {
    deleteUserRow = row;
    console.log('deleteUserRow',deleteUserRow)
    this.toggleDeleteUser();
  }

  toggleDeleteUser() {
    this.setState({
      isOpenModalDeleteUser: !this.state.isOpenModalDeleteUser,
    });
  }

  async deleteUser() {
    const row = deleteUserRow;
    console.log('inside deleteUser, row',row)
    const requestOptions = {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                }
    };
    //TODO need to confirm with user wants to delete or not
    const res = await fetch('/customer/deleteuser/'+row.userId, requestOptions)
    const data = await res.json()
    console.log('delete user response data ',data)
    this.getUsers();
    this.toggleDeleteUser();
  }

  //////////

  async handleAddCustomer()
  {
    console.log('inside handleAddCustomer')
    this.getSubDealers();
    this.toggleAddCustomer();
  }

  toggleAddCustomer()
  {
      console.log('inside toggleAddCustomer')
      this.setState({
        isOpenModalAddCustomer: !this.state.isOpenModalAddCustomer,
      });
  }

  handleSaveCustomer = async ()  =>
  {
    console.log('Customer -->> inside handleSaveCustomer')
    const { customerName, subDealerId } = this.state;
    console.log('customerName, subDealerId',customerName,subDealerId)
    if(undefined == subDealerId)
    {
      alert('please select subdearl')
      return false;
    }
    if(undefined != subDealerId && undefined != customerName && customerName != "")
    {
    let payload = {
                    subDealerId:subDealerId,
                    name:customerName
                  }
      console.log('payload: ',payload)
      console.log('payload: ',JSON.stringify(payload))
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json',
                    'X-CSRF-Token': await NextAuth.csrfToken()
                  },
        body : JSON.stringify(payload)
      };
      const custCreate = await fetch('/customer/create', requestOptions)
      const resCustCreate = await custCreate.json()
      console.log('register customer:',resCustCreate)
      this.setState({
                    customerTableLoading:true
                  })
      this.getManages();
      this.getUsers();
      this.toggleAddCustomer();
    }
    else {
      alert('please enter valid customer name')
    }
  }


  toggleEditCustomer() {
    console.log('inside toggleEditCustomer')
    this.setState({
      isOpenModalEditCustomer: !this.state.isOpenModalEditCustomer,
    });
  }

  async editCustomer(row)
  {
    console.log('inside editCustomer, row',row)
    //TODO need custom patch api for this
    const customerRes = await fetch('/customer/'+row.customerId)
    const customer = await customerRes.json();
    console.log('customer ',customer)
    this.setState({
                  editCustomerName:customer.name,
                  editCustomerId:customer.id
    })
    this.toggleEditCustomer();
  }

  async handleUpdateCustomer ()
  {
    console.log('inside handleUpdateCustomer')
    const {editCustomerName, editCustomerId} = this.state;
    console.log('edited customerName',editCustomerName, editCustomerId)
    if(undefined != editCustomerName && editCustomerName != "")
    {
    let payload = {
                    name:editCustomerName
                  }
    const requestOptions = {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    console.log('payload: ',JSON.stringify(payload))
    const res = await fetch('/customer/'+editCustomerId, requestOptions)
    const data = await res.json()
    console.log('updated customer data ',data)
    this.setState({
                  customerTableLoading:true
                })
    this.getManages();
    this.getUsers();
    this.toggleEditCustomer();
    }
    else {
      alert('please enter valid customer name')
    }
  }

  confirmDeleteCustomer(row)
  {
    deleteCustomerRow = row;
    console.log('deleteCustomerRow',deleteCustomerRow)
    this.toggleDeleteCustomer();
  }

  toggleDeleteCustomer() {
    this.setState({
      isOpenModalDeleteCustomer: !this.state.isOpenModalDeleteCustomer,
    });
  }

  async deleteCustomer()
  {
    const row = deleteCustomerRow;
    console.log('inside deleteCustomer, row',row)
    const requestOptions = {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                }
    };
    const res = await fetch('/customer/'+row.customerId, requestOptions)
    const data = await res.json()
    console.log('delete customer response data ',data)
    this.setState({
                  customerTableLoading:true
                })
    //TODO if delete success:false then show msg that came from server
    this.getManages();
    this.getUsers();
    this.toggleDeleteCustomer();
  }

  render() {
        console.log('Customer -->> inside render')
        const { selectedOption,isClearable,phone,firstName,lastName,password,address,city,state,country,zip } = this.state;
        const {editFirstName, editLastName, editAddress, editCity, editState, editCountry, editZip} = this.state
        const {customerName, editCustomerName} = this.state;

        //const editFormatter = (cell, row, rowIndex, formatExtraData) => { console.log('editFormatter, row ',row); return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editUser.bind(this, row)}>Edit</Button> ); }
        const editFormatter = (cell, row, rowIndex, formatExtraData) => { return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editUser.bind(this, row)}>Edit</Button> ); }

        const deleteFormatter = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.confirmDeleteUser.bind(this, row)}>Delete</Button> ); }

        const editFormatterCustomer = (cell, row, rowIndex, formatExtraData) => { return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editCustomer.bind(this, row)}>Edit</Button> ); }

        const deleteFormatterCustomer = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.confirmDeleteCustomer.bind(this, row)}>Delete</Button> ); }

        const columnsUsersTable = [{
          dataField: 'id',
          text: 'SN',
          sort: true
        }, {
          dataField: 'phoneNumber',
          text: 'Phone Number',
          filter: textFilter()
        }, {
          dataField: 'name',
          text: 'Name',
          sort: true,
          filter: textFilter()
        }, {
          dataField: 'role',
          text: 'Role'
        }, {
          dataField: 'status',
          text: 'Status'
        }, {
          dataField: 'manage',
          text: 'Manage'
        }, {
          dataField: 'lastLogin',
          text: 'Last Login',
          hidden: true
        }, {
          dataField: 'edit',
          text: '',
          formatter: editFormatter,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          }
        }, {
          dataField: 'delete',
          text: '',
          formatter: deleteFormatter,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          },
          /*
          events: {
            onClick: e => { this.deleteUser() }
          }
          */
        }];

        const columnsCustomersTable = [{
          dataField: 'id',
          text: 'SN',
          sort: true
        }, {
          dataField: 'name',
          text: 'Name',
          sort: true,
          filter: textFilter()
        }, {
          dataField: 'subDealer',
          text: 'SubDealer',
          sort: true,
          filter: textFilter()
        }, {
          dataField: 'edit',
          text: '',
          formatter: editFormatterCustomer,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          }
        }, {
          dataField: 'delete',
          text: '',
          formatter: deleteFormatterCustomer,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          },
          /*
          events: {
            onClick: e => { this.deleteUser() }
          }
          */
        }];

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12">
            <Card>
              <CardBody>
                <p><b>Customer</b></p>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs="12" className="mb-4">
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '1' })}
                  onClick={() => { this.toggle('1'); }}
                >
                  User Management
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '2' })}
                  onClick={() => { this.toggle('2'); }}
                >
                  Customer Management
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={this.state.activeTab}>
              <TabPane tabId="1">
              <Row>
                <Col>
                  <Card>
                    <CardBody>
                    <Modal isOpen={this.state.isOpenModalAddUser} toggle={this.toggleAddUser}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleAddUser}>Add New User</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Phone</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="text-input" name="phone" placeholder="Phone" value={phone} onChange={this.handleChange} />
                              {
                                //<FormText color="muted">This is a help text</FormText>
                              }
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">First Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="firstName" name="firstName" placeholder="First Name" value={firstName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Last Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="lastName" name="lastName" placeholder="Last Name" value={lastName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Password</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="password" id="password" name="password" placeholder="Password" value={password} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Address</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="address" name="address" placeholder="Address" value={address} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">City</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="city" name="city" placeholder="City" value={city} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">State</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="state" name="state" placeholder="State" value={state} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Country</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="country" name="country" placeholder="Country" value={country} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Zip</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="zip" name="zip" placeholder="Zip" value={zip} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Role</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="role" id="selectRole" onChange={this.handleChange}>
                                <option value="select">Please Select</option>
                                <option value="admin">Admin</option>
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Status</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="status" id="selectStatus" onChange={this.handleChange}>
                                <option value="select">Please Select</option>
                                <option value="active">Active</option>
                                <option value="inactive">InActive</option>
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">SubDealer</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Select
                              onChange={this.handleSelectSubDealer.bind(this,'userManagement')}
                              isClearable={isClearable}
                              options={this.state.optionsSelectSubDealer}
                              placeholder={'Select SubDealer'}
                              backspaceRemovesValue={false}
                            />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Manage Customer</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <DropdownTreeSelect
                                data={this.state.dataManageCustomerDropDown}
                                onChange={this.onChangeCustomers.bind(this, 'fromAddUser')}
                                onAction={onAction}
                                onNodeToggle={onNodeToggle}
                              />
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleSaveUser}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleAddUser}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalEditUser} toggle={this.toggleEditUser}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleEditUser}>Edit User</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">First Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="firstName" name="editFirstName" placeholder="First Name" value={editFirstName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Last Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="lastName" name="editLastName" placeholder="Last Name" value={editLastName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Address</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="address" name="editAddress" placeholder="Address" value={editAddress} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">City</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="city" name="editCity" placeholder="City" value={editCity} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">State</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="state" name="editState" placeholder="State" value={editState} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Country</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="country" name="editCountry" placeholder="Country" value={editCountry} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Zip</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="zip" name="editZip" placeholder="Zip" value={editZip} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Role</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="editRole" id="selectRole" onChange={this.handleChange}>
                                {
                                this.state.roles
                                }
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Status</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="editStatus" id="selectStatus" onChange={this.handleChange}>
                              {
                                this.state.statuses
                              }
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Manage Customer</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <DropdownTreeSelect
                                data={this.state.dataManagedCustomerDropDown}
                                onChange={this.onChangeCustomers.bind(this, 'fromEditUser')}
                                onAction={onAction}
                                onNodeToggle={onNodeToggle}
                              />
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleUpdateUser}>Update</Button>{' '}
                        <Button color="secondary" onClick={this.toggleEditUser}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalDeleteUser} toggle={this.toggleDeleteUser}
                           className={'modal-danger ' + this.props.className}>
                      <ModalHeader toggle={this.toggleDeleteUser}>User Delete</ModalHeader>
                      <ModalBody>
                        Are you sure to delete user
                      </ModalBody>
                      <ModalFooter>
                        <Button color="danger" onClick={this.deleteUser}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteUser}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                      {
                        this.state.userTableLoading &&
                        <div>
                        <div style={{marginLeft: '40%'}}>
                        <ClipLoader
                          sizeUnit={"px"}
                          size={50}
                          color={'#123abc'}
                          loading={this.state.userTableLoading}
                        />
                        </div>
                        <div style={{marginLeft: '35%'}}>
                        <p>Loading ... Please wait!</p>
                        </div>
                        </div>
                      }
                      {
                      !this.state.userTableLoading &&
                      <ToolkitProvider
                            keyField="id"
                            data={ this.state.dataUsersTable }
                            columns={ columnsUsersTable }
                            search
                          >
                            {
                              props => (
                                <div>
                                  <Row className="align-items-center">
                                  <Col>
                                  <Row>
                                  <Col md="4">
                                  <p>SubDealer</p>
                                  <Select
                                    onChange={this.handleChangeSubDealerOption.bind(this,'userManagement')}
                                    defaultValue={this.state.optionsSubDealer[0]}
                                    isSearchable={true}
                                    isClearable={isClearable}
                                    options={this.state.optionsSubDealer}
                                    placeholder={'Select SubDealer'}
                                    backspaceRemovesValue={false}
                                  />
                                  </Col>
                                  <Col md="4">
                                  <p>Customer</p>
                                  <Select
                                    onChange={this.handleChangeCustomerOption}
                                    value={this.state.selectCustomerValue}
                                    isClearable={isClearable}
                                    options={this.state.optionsCustomer}
                                    placeholder={'Select Customer'}
                                    backspaceRemovesValue={false}
                                  />
                                  </Col>
                                  </Row>
                                  </Col>

                                  <Col md="2">
                                  {
                                    //<h4>Input something at below input field:</h4>
                                  }
                                  <SearchBar
                                    { ...props.searchProps }
                                  />
                                  </Col>
                                  <Col md="2">
                                    <Button block color="primary" className="btn-pill" onClick={this.handleAddUser}>Add User</Button>
                                  </Col>
                                  </Row>
                                  <hr />
                                  <div style={{overflow: 'overlay'}}>
                                  <BootstrapTable
                                    { ...props.baseProps }
                                    filter={ filterFactory() }
                                    hover
                                  />
                                  </div>
                                </div>
                              )
                            }
                          </ToolkitProvider>
                        }
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="2">
              <Row>
                <Col>
                  <Card>
                    <CardBody>
                    <Modal isOpen={this.state.isOpenModalAddCustomer} toggle={this.toggleAddCustomer}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleAddCustomer}>Add New Customer</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">SubDealer</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Select
                              onChange={this.handleSelectSubDealer.bind(this,'customerManagement')}
                              isClearable={isClearable}
                              options={this.state.optionsSelectSubDealer}
                              placeholder={'Select SubDealer'}
                              backspaceRemovesValue={false}
                            />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="text-input" name="customerName" placeholder="Name" value={customerName} onChange={this.handleChange} />
                              {
                                //<FormText color="muted">This is a help text</FormText>
                              }
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleSaveCustomer}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleAddCustomer}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalEditCustomer} toggle={this.toggleEditCustomer}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleEditCustomer}>Edit Customer</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="text-input" name="editCustomerName" placeholder="Name" value={editCustomerName} onChange={this.handleChange} />
                              {
                                //<FormText color="muted">This is a help text</FormText>
                              }
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleUpdateCustomer}>Update</Button>{' '}
                        <Button color="secondary" onClick={this.toggleEditCustomer}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalDeleteCustomer} toggle={this.toggleDeleteCustomer}
                           className={'modal-danger ' + this.props.className}>
                      <ModalHeader toggle={this.toggleDeleteCustomer}>Customer Delete</ModalHeader>
                      <ModalBody>
                        Are you sure to delete customer
                      </ModalBody>
                      <ModalFooter>
                        <Button color="danger" onClick={this.deleteCustomer}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteCustomer}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    {
                      this.state.customerTableLoading &&
                      <div>
                      <div style={{marginLeft: '40%'}}>
                      <ClipLoader
                        sizeUnit={"px"}
                        size={50}
                        color={'#123abc'}
                        loading={this.state.customerTableLoading}
                      />
                      </div>
                      <div style={{marginLeft: '35%'}}>
                      <p>Loading ... Please wait!</p>
                      </div>
                      </div>
                    }
                    {
                    !this.state.customerTableLoading &&
                    <ToolkitProvider
                          keyField="id"
                          data={ this.state.dataCustomersTable }
                          columns={ columnsCustomersTable }
                          search
                        >
                          {
                            props => (
                              <div>
                                <Row className="align-items-center">
                                <Col>
                                <Row>
                                <Col md="4">
                                <p>SubDealer</p>
                                <Select
                                  onChange={this.handleChangeSubDealerOption.bind(this,'customerManagement')}
                                  defaultValue={this.state.optionsSubDealer[0]}
                                  isClearable={isClearable}
                                  options={this.state.optionsSubDealer}
                                  placeholder={'Select SubDealer'}
                                  backspaceRemovesValue={false}
                                />
                                </Col>
                                </Row>
                                </Col>
                                <Col md="2">
                                {
                                  //<h4>Input something at below input field:</h4>
                                }
                                <SearchBar
                                  { ...props.searchProps }
                                />
                                </Col>
                                <Col md="2">
                                  <Button block color="primary" className="btn-pill" onClick={this.handleAddCustomer}>Add Customer</Button>
                                </Col>
                                </Row>
                                <hr />
                                <div style={{overflow: 'overlay'}}>
                                <BootstrapTable
                                  { ...props.baseProps }
                                  filter={ filterFactory() }
                                  hover
                                />
                                </div>
                              </div>
                            )
                          }
                        </ToolkitProvider>
                        }
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </TabPane>
            </TabContent>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Customer;
