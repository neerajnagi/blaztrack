import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Form,
  FormGroup,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Label
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import classnames from 'classnames';

import { NextAuth } from 'next-auth/client'

//
import Chart from "react-google-charts";

//
// Pond
import { TimeSeries, percentile } from "pondjs";
import {
    Charts,
    ChartContainer,
    ChartRow,
    YAxis,
    LineChart,
    Resizable,
    Legend,
    EventMarker,
    ScatterChart,
    BandChart,
    styler
} from "react-timeseries-charts";

import ReactModal from 'react-modal';

//react-spinners
import { css } from 'react-emotion';
// First way to import
import { ClipLoader } from 'react-spinners';

import _ from "underscore";
import moment from "moment";
import { format } from "d3-format";
//

//TODO for datetime
import * as Datetime from 'react-datetime';
import 'react-datetime/css/react-datetime.css'

//google maps
import { compose, withProps, withHandlers, lifecycle } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"
const { MarkerClusterer } = require("react-google-maps/lib/components/addons/MarkerClusterer");

//TODO start react-timeseries-charts

const requests = [];

let requestsSeries;

//
// Styles
//

//DDos example
const style = styler([
    { key: "requests", color: "#9467bd", width: 1 }
]);

const axisStyle = {
    values: {
        labelColor: "grey",
        labelWeight: 100,
        labelSize: 11
    },
    axis: {
        axisColor: "grey",
        axisWidth: 1
    }
};

const darkAxis = {
    label: {
        stroke: "none",
        fill: "#AAA", // Default label color
        fontWeight: 200,
        fontSize: 14,
        font: '"Goudy Bookletter 1911", sans-serif"'
    },
    values: {
        stroke: "none",
        fill: "#888",
        fontWeight: 100,
        fontSize: 11,
        font: '"Goudy Bookletter 1911", sans-serif"'
    },
    ticks: {
        fill: "none",
        stroke: "#AAA",
        opacity: 0.2
    },
    axis: {
        fill: "none",
        stroke: "#AAA",
        opacity: 1
    }
};

//TODO end react-timeseries-charts

const customStyles = {
  content : {
    top : '47%',
    left : '58%',
    right : '-30%',
    bottom : '-20%',
    marginRight : '-10%',
    transform : 'translate(-50%, -50%)'
  },
  overlay: {
    backgroundColor: 'white'
            },
};

const speedChartOptions = {
  width: 400,
  height: 120,
  redFrom: 90,
  redTo: 200,
  yellowFrom: 60,
  yellowTo: 90,
  max:200,
  minorTicks: 5
};

const fuelChartOptions = {
  width: 400,
  height: 120,
  max:500,
  minorTicks: 5
};

const batteryChartOptions = {
  width: 400,
  height: 120,
  max:20,
  minorTicks: 5
};

//react-spinners
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

const MapComponent = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `250px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props) =>
  <GoogleMap
    defaultZoom={8}
    center={new google.maps.LatLng(props.center.lat, props.center.lng)}
  >
    {
      props.isMarkerShown && <Marker position={{ lat: props.latitude, lng: props.longitude }} onClick={props.onMarkerClick} />
    }
  </GoogleMap>
)


class VehicleDetails extends Component {

  constructor(props) {
    super(props);
    console.log('VehicleDetails -->>, props',this.props)
    const row = this.props.row;
    this.toggleCalibrationModal = this.toggleCalibrationModal.bind(this);
    this.state = {
                  row: row,
                  customerId:row.customerId,
                  vehicleId:row.vehicleId,
                  latitude: -34.397,
                  longitude: 150.644,
                  markers: [],
                  center:{lat:-34.397, lng: 150.644},
                  max: 6000,
                  active: {
                        requests: false
                    },
                  timerange: null,
                  hover: null,
                  highlight: null,
                  selection: null,
                  isOpenModalCalibration:false
                 }
  }

  componentDidMount()
  {
    console.log('inside componentDidMount')
    const {customerId, vehicleId} = this.state;
    this.trackVehicle(customerId, vehicleId);
  }

  delayedShowMarker = () => {
    setTimeout(() => {
      this.setState({ isMarkerShown: true })
    }, 3000)
  }

  delayedShowMarkerWithLatLng = (lat,lng) => {
    let center = {lat: lat, lng: lng}
    setTimeout(() => {
      this.setState({
                    center:center,
                    isMarkerShown: true,
                    latitude:lat,
                    longitude:lng,
                   })
    }, 3000)
  }

  handleMarkerClick = () => {
    this.setState({ isMarkerShown: false })
    this.delayedShowMarker()
  }

  async trackVehicle(customerId, vehicleId)
  {
    console.log('inside trackVehicle')
    //TODO need to finialize interval seconds
    var intervalId = setInterval(this.trackVehicleTimer.bind(this,customerId, vehicleId), 15000);
    // store intervalId in the state so it can be accessed later:
    this.setState({intervalId: intervalId});
  }



  componentWillUnmount() {
   // use intervalId from the state to clear the interval
   console.log('inside componentWillUnmount')
   clearInterval(this.state.intervalId);
  }

  async trackVehicleTimer(customerId, vehicleId) {
    // setState method is used to update the state
    console.log('inside trackVehicleTimer')
    console.log('fetching data for, customerId, vehicleId', customerId,',',vehicleId)
    const data = {customerId:customerId,vehicleId:vehicleId}
    const dataForCassQry = [data];
    let payload = {
                    data:dataForCassQry
                  }
    const requestOptions = {
      credentials: 'include',
      method: 'POST',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    const vehicleFetch = await fetch('/vehicle/fetch/',requestOptions);
    const resVehicle = await vehicleFetch.json();
    console.log('resVehicle',resVehicle);
    const vehicleData = resVehicle.data;
    const vehicleDataByVehicleId = vehicleData[vehicleId];
    console.log('vehicleDataByVehicleId',vehicleDataByVehicleId)
    if(vehicleDataByVehicleId != "data not found" && vehicleDataByVehicleId != "some error occured"){
      const vehicle = vehicleDataByVehicleId[0];
      const latitude = vehicle.latitude;
      const longitude = vehicle.longitude;
      this.setState({
                      speed: vehicle.speed,
                      battery: vehicle.battery || 'NA',
                      fuel: vehicle.a6 || 'NA',
                    })
      this.delayedShowMarkerWithLatLng(latitude,longitude)
    }
  }

  getSpeedData = () => {
    return [
      ["Label", "Value"],
      ["Speed", this.state.speed || 0]
    ];
  };

  getFuelData = () => {
    return [
      ["Label", "Value"],
      ["Fuel", this.state.a6 || 0]
    ];
  };

  getBatteryData = () => {
    return [
      ["Label", "Value"],
      ["Battery", this.state.battery || 0]
    ];
  };

  handleSelectStartDateTime(date)
  {
    console.log('inside handleSelectStartDateTime')
    let dateTime = date._d;
    console.log('inside handleSelectDateTime, selected datetime',dateTime)
    let dateTimeFormatted = moment(dateTime).format("YYYY-MM-DDTHH:mm:ss.SSS[Z]")
    console.log('dateTimeFormatted',dateTimeFormatted)
    this.setState({
                    graphStartDateTime:dateTimeFormatted
                })
  }

  handleSelectEndDateTime(date)
  {
    console.log('inside handleSelectEndDateTime')
    let dateTime = date._d;
    console.log('inside handleSelectDateTime, selected datetime',dateTime)
    let dateTimeFormatted = moment(dateTime).format("YYYY-MM-DDTHH:mm:ss.SSS[Z]")
    console.log('dateTimeFormatted',dateTimeFormatted)
    this.setState({
                    graphEndDateTime:dateTimeFormatted
                })
  }

  showGraph()
  {
    console.log('show graph');
    const {customerId, vehicleId, graphStartDateTime, graphEndDateTime} = this.state;
    console.log('graphStartDateTime', graphStartDateTime)
    console.log('graphEndDateTime', graphEndDateTime)
    const startTimestamp = Math.round(new Date(graphStartDateTime).getTime())
    const endTimestamp = Math.round(new Date(graphEndDateTime).getTime())
    //TODO getting graph data from cassandra for showing graph
    this.getGraphData(customerId, vehicleId, startTimestamp, endTimestamp)
  }

  async getGraphData(customerId, vehicleId, startTimestamp, endTimestamp)
  {
    requestsSeries = null;
    let requests = [];
    console.log('inside getGraphData')
    var d_1 = new Date();
    var utc_1 = d_1.toUTCString();
    console.log('start time :',utc_1)
    //TODO need to remove and use came from function parameter
    const custId = '5b6053fe17cefc25afb7b954';
    const vehId = '5b6057cfa2ea1b2935bb1881'
    const startDT = startTimestamp;
    const endDT = endTimestamp;
    const fetchGraphData = await fetch('/vehicle/fetchgraphdata/'+custId+'/'+vehId+'/'+startDT+'/'+endDT);
    var d_2 = new Date();
    var utc_2 = d_2.toUTCString();
    console.log('end time :',utc_2)
    const resGraphData = await fetchGraphData.json();
    var d_3 = new Date();
    var utc_3 = d_3.toUTCString();
    console.log('after convert res to josn, end time :',utc_3)
    console.log('resGraphData',resGraphData);
    const data = resGraphData.data;
    console.log('data length',data.length);
    var d_4 = new Date();
    var utc_4 = d_4.toUTCString();
    console.log('before loop, start time :',utc_4)
    for(var i in data)
    {
      var item = data[i];
      //console.log('insert_datetime',item.insert_datetime)
      const timestamp = moment(new Date(item.insert_datetime));
      const value = item.a6;
      //console.log('timestamp',timestamp)
      requests.push([timestamp.toDate().getTime(), value]);
      //console.log('requests[i]',requests[i])
    }
    var d_5 = new Date();
    var utc_5 = d_5.toUTCString();
    console.log('after loop, end time :',utc_5)
    console.log('requests',requests[0], requests[data.length - 1])
    var d_6 = new Date();
    var utc_6 = d_6.toUTCString();
    console.log('before TimeSeries create, start time :',utc_6)
    requestsSeries = new TimeSeries({
        name: "requests",
        columns: ["time", "requests"],
        points: requests
    });
    var d_7 = new Date();
    var utc_7 = d_7.toUTCString();
    console.log('after TimeSeries create, end time :',utc_7)
    this.setState({
                  timerange:requestsSeries.range(),
                  active:{requests:true}
                })
    this.rescale();
    var d_8 = new Date();
    var utc_8 = d_8.toUTCString();
    console.log('after seState timerange, end time :',utc_8)
  }

  //TODO Start react-timeseries-charts
  //DDos example
  rescale(timerange, active = this.state.active) {
    if(this.state.active.requests)
    {
      let max = 100;
      const maxRequests = requestsSeries.crop(this.state.timerange).max("requests");
      if (maxRequests > max && active.requests) max = maxRequests;
      this.setState({ max });
    }
  }

  handleTimeRangeChange = timerange => {
      this.setState({ timerange });
      this.handleRescale(timerange);
  };

  handleActiveChange = key => {
      const active = this.state.active;
      active[key] = !active[key];
      this.setState({ active });
      this.handleRescale(this.state.timerange, active);
  };

  handleSelectionChanged = point => {
      this.setState({
          selection: point
      });
  };

  handleMouseNear = point => {
      this.setState({
          highlight: point
      });
  };

  handleCalibration()
  {
    console.log('inside handleCalibration')
    this.toggleCalibrationModal();
  }

  toggleCalibrationModal()
  {
    this.setState({
      isOpenModalCalibration: !this.state.isOpenModalCalibration
    });
  }

  render() {
    console.log('VehicleDetails inside render')

    //DDos example
    const legend = [
            {
                key: "requests",
                label: "Requests",
                disabled: !this.state.active.requests
            }
        ];

    //
    //For showing value with date time on mouse near of line or mounse on point/dot(scatter)
    const highlight = this.state.highlight;
    const formatter = format(".2f");
    let text = `Speed: - mph, time: -:--`;
    let infoValues = [];
    if (highlight) {
        const speedText = `${formatter(highlight.event.get(highlight.column))} mph`;
        text = `
            Speed: ${speedText},
            time: ${this.state.highlight.event.timestamp().toLocaleTimeString()}
        `;
        infoValues = [{ label: "Speed", value: speedText }];
    }

  //

    return (
        <div>
        <Row>
          <Col>
          <p>Vehicle : {this.state.vehicleRegNo}</p>
          <Row>
          <Col md="4">
          <Chart
            chartType="Gauge"
            width="100%"
            height="125px"
            data={this.getSpeedData()}
            options={speedChartOptions}
          />
          <p style={{textAlign:'center'}}> Speed : {this.state.speed}</p>
          </Col>
          <Col md="4">
          <Chart
            chartType="Gauge"
            width="100%"
            height="125px"
            data={this.getFuelData()}
            options={fuelChartOptions}
          />
          <p style={{textAlign:'center'}}> Fuel : {this.state.Litre}</p>
          <Button color="primary" className="btn-pill" size="sm" onClick={this.handleCalibration.bind(this)}>Calibrate Sensor</Button>
          </Col>
          <Col md="4">
          <Chart
            chartType="Gauge"
            width="100%"
            height="125px"
            data={this.getBatteryData()}
            options={batteryChartOptions}
          />
          <p style={{textAlign:'center'}}> Battery : {this.state.battery}</p>
          </Col>
          </Row>
          </Col>
          <Col md="6">
            <p>Track Vehicle</p>
            {
            <MapComponent
              center={this.state.center}
              isMarkerShown={this.state.isMarkerShown}
              latitude={this.state.latitude}
              longitude={this.state.longitude}
            />
            }
          </Col>
        </Row>
        <Row>
          <Col>
            <hr/>
            <Row>
            <Col md="3">
            <Label>Start Date Time</Label>
            <Datetime
              defaultValue = {new Date()}
              onChange= {this.handleSelectStartDateTime.bind(this)}
              dateFormat = "DD-MM-YYYY"
             />
            </Col>
            <Col md="3">
            <Label>End Date Time</Label>
            <Datetime
              defaultValue = {new Date()}
              onChange= {this.handleSelectEndDateTime.bind(this)}
              dateFormat = "DD-MM-YYYY"
             />
            </Col>
            <Col md="2">
            <Button style={{marginTop:'35px'}} color="primary" className="btn-pill" size="sm" onClick={this.showGraph.bind(this)}>View Graph</Button>
            </Col>
            </Row>
          </Col>
          </Row>
          <Col style={{marginTop:'15px'}}>
          {
          //this.state.active.requests && <Resizable>{this.renderChart()}</Resizable>
          }
          {
            this.state.active.requests &&
            <Resizable>
            <ChartContainer
                title="Graph"
                style={{
                    background: "#201d1e",
                    borderRadius: 8,
                    borderStyle: "solid",
                    borderWidth: 1,
                    borderColor: "#232122"
                }}
                timeAxisStyle={darkAxis}
                titleStyle={{
                    color: "#EEE",
                    fontWeight: 500
                }}
                padding={20}
                paddingTop={5}
                paddingBottom={0}
                enableDragZoom
                onTimeRangeChanged={this.handleTimeRangeChange}
                timeRange={this.state.timerange}
                maxTime={this.state.active.requests ? requestsSeries.range().end() : 0}
                minTime={this.state.active.requests ? requestsSeries.range().begin() : 0}
            >
                <ChartRow height="300">
                    <YAxis
                        id="axis1"
                        label="Values"
                        showGrid
                        hideAxisLine
                        transition={300}
                        style={darkAxis}
                        labelOffset={-10}
                        min={0}
                        max={this.state.max}
                        format=",.0f"
                        width="60"
                        type="linear"
                    />
                    <Charts>
                    <LineChart
                        key="requests"
                        axis="axis1"
                        series={requestsSeries}
                        columns={["requests"]}
                        style={style}
                    />
                    <ScatterChart
                          axis="axis1"
                          series={requestsSeries}
                          columns={["requests"]}
                          style={style}
                          info={infoValues}
                          infoTimeFormat="%d-%m-%Y %I:%M %p"
                          infoHeight={28}
                          infoWidth={110}
                          infoStyle={{
                              fill: "black",
                              color: "#DDD"
                            }}
                          selected={this.state.selection}
                          onSelectionChange={p => this.handleSelectionChanged(p)}
                          onMouseNear={p => this.handleMouseNear(p)}
                          highlight={this.state.highlight}
                      />
                    </Charts>
                </ChartRow>
            </ChartContainer>
            </Resizable>
          }
          </Col>
          <Row>
          </Row>
          //TODO calibration model came here
            <ReactModal
              ariaHideApp={false}
              isOpen={this.state.isOpenModalCalibration}
              style={customStyles}>
            <ModalHeader toggle={this.toggleCalibrationModal}>Calibration</ModalHeader>
            <ModalBody>
              <Calibration {...this.props} row={this.state.row}/>
            </ModalBody>
          </ReactModal>
        </div>
      )

    }

}

export default VehicleDetails;
