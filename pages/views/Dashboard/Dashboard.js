import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Form,
  FormGroup,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Label
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import classnames from 'classnames';

import { NextAuth } from 'next-auth/client'

import Router from 'next/router'

//
//import VehicleDetails from './VehicleDetails';

//
import SensorSetup from './SensorSetup';

//
import HmrSetup from './hmrSetup';
//
import Calibration from './Calibration';

import Select from 'react-select';

import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter, selectFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import cellEditFactory from 'react-bootstrap-table2-editor';
//For table search
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
//

import Geocode from "react-geocode";

//Geocode.setApiKey("AIzaSyDtgi5Fa7jR5nmqAAJdmhKS9_mT9xV-n-I");
//
Geocode.setApiKey("AIzaSyAlxIjIkj-zR6Mx8v3qc1rN9DOtSw2UFAE");

import ReactModal from 'react-modal';

//speedometer widget
//import ReactSpeedometer from "react-d3-speedometer";

//
import Chart from "react-google-charts";

//
// Pond
import { TimeSeries, percentile } from "pondjs";
import {
    Charts,
    ChartContainer,
    ChartRow,
    YAxis,
    LineChart,
    Resizable,
    Legend,
    EventMarker,
    ScatterChart,
    BandChart,
    styler
} from "react-timeseries-charts";

//react-spinners
import { css } from 'react-emotion';
// First way to import
import { ClipLoader } from 'react-spinners';

import _ from "underscore";
import moment from "moment";
import { format } from "d3-format";
//

//TODO for datetime
import * as Datetime from 'react-datetime';
import 'react-datetime/css/react-datetime.css'

//google maps
import { compose, withProps, withHandlers, lifecycle } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap, Marker, Polyline } from "react-google-maps"
const { MarkerClusterer } = require("react-google-maps/lib/components/addons/MarkerClusterer");

const { SearchBar } = Search;

//TODO start react-timeseries-charts

const requests = [];

let requestsSeries;

//
// Styles
//

//DDos example
const style = styler([
    { key: "requests", color: "#9467bd", width: 1 }
]);

const axisStyle = {
    values: {
        labelColor: "grey",
        labelWeight: 100,
        labelSize: 11
    },
    axis: {
        axisColor: "grey",
        axisWidth: 1
    }
};

const darkAxis = {
    label: {
        stroke: "none",
        fill: "#AAA", // Default label color
        fontWeight: 200,
        fontSize: 14,
        font: '"Goudy Bookletter 1911", sans-serif"'
    },
    values: {
        stroke: "none",
        fill: "#888",
        fontWeight: 100,
        fontSize: 11,
        font: '"Goudy Bookletter 1911", sans-serif"'
    },
    ticks: {
        fill: "none",
        stroke: "#AAA",
        opacity: 0.2
    },
    axis: {
        fill: "none",
        stroke: "#AAA",
        opacity: 1
    }
};

//TODO end react-timeseries-charts

const customStyles = {
  content : {
    top : '47%',
    left : '58%',
    right : '-30%',
    bottom : '-20%',
    marginRight : '-10%',
    transform : 'translate(-50%, -50%)'
  },
  overlay: {
    backgroundColor: 'white'
            },
};

const reactModalStyle = {
  content : {
    position: 'absolute',
    top: '280px',
    left: '550px',
    right: '40px',
    bottom: '40px',
    border: '1px solid rgb(204, 204, 204)',
    background: 'rgb(255, 255, 255)',
    overflow: 'auto',
    borderRadius: '4px',
    outline: 'none',
    padding: '20px',
    marginRight : '-50%',
    transform : 'translate(-50%, -50%)',
    color:'lightsteelblue'
  },
  overlay: {
    //backgroundColor: 'papayawhip'
            },
}

//var appElement = document.getElementById('app');

// Make sure to bind modal to your appElement (http://reactcommunity.org/react-modal/accessibility/)
//ReactModal.setAppElement('#app')

const optionsDealer = [
  { value: 'all', label: 'All' },
  { value: 'dealer1', label: 'Dealer1' },
  { value: 'dealer2', label: 'Dealer2' }
];

const optionsSubDealer = [];

const optionsCustomer = [];

const optionsGroup = [{ value: 'all', label: 'All' }];

const dataVehiclesTable = [];

const selectRow = {
  mode: 'checkbox',
  clickToSelect: false,
  onSelect: (row, isSelect, rowIndex, e) => {
    console.log(row.sn);
    console.log(isSelect);
    console.log(row);
    console.log(rowIndex);
    console.log(e);
  },
  onSelectAll: (isSelect, rows, e) => {
    console.log(isSelect);
    console.log(rows);
    console.log(e);
  }
};

const options = {
  onSizePerPageChange: (sizePerPage, page) => {
    console.log('Size per page change!!!');
    console.log('Newest size per page:' + sizePerPage);
    console.log('Newest page:' + page);
  },
  onPageChange: (page, sizePerPage) => {
    console.log('Page change!!!');
    console.log('Newest size per page:' + sizePerPage);
    console.log('Newest page:' + page);
  }
};

const speedChartOptions = {
  width: 400,
  height: 120,
  redFrom: 90,
  redTo: 200,
  yellowFrom: 60,
  yellowTo: 90,
  max:200,
  minorTicks: 5
};

const fuelChartOptions = {
  width: 400,
  height: 120,
  max:500,
  minorTicks: 5
};

const batteryChartOptions = {
  width: 400,
  height: 120,
  max:24,
  minorTicks: 5
};

const MyExportCSV = (props) => {
  const handleClick = () => {
    props.onExport();
  };
  return (
    <div>
      <button className="btn btn-success" onClick={ handleClick }>Export to CSV</button>
    </div>
  );
};

//react-spinners
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

let pathCoordinatesForTest = [{lat:26.888741, lng:75.737389}, {lat:26.866632, lng:75.770337}, {lat:26.852158, lng:75.767613}, {lat:26.841070, lng:75.770136}]
let indexPathCoordinates = 0;
let pathCoordinatesArray = [];

const MapComponent = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAlxIjIkj-zR6Mx8v3qc1rN9DOtSw2UFAE&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `250px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props) =>
  <GoogleMap
    defaultZoom={8}
    center={new google.maps.LatLng(props.center.lat, props.center.lng)}
  >
    {
      props.isMarkerShown && <Marker position={{ lat: props.latitude, lng: props.longitude }} onClick={props.onMarkerClick} />
    }
    {
      <Polyline path={props.pathCoordinates}/>
    }
  </GoogleMap>
)

const cassandraColName = {
                          'Digital 1':'din1', 'Digital 2':'din2', 'Digital 3':'din3', 'Digital 4':'din4',
                          'Analog 1':'ain1', 'Analog 2':'ain2', 'Analog 3':'ain3', 'Analog 4':'ain4',
                          'External Bat':'power', 'Internal Bat':'battery', 'Speed':'speed'
                          };

class Dashboard extends Component {
  constructor(props) {
    super(props);
    console.log('Dashboard -->>, props',this.props)
    this.detail = this.detail.bind(this);
    this.toggleDetail = this.toggleDetail.bind(this);
    this.toggleCalibrationModal = this.toggleCalibrationModal.bind(this);

    this.handleRescale = _.debounce(this.rescale, 300);

    const manageCategory = this.props.managecategory;
    let isDealerShow, isSubDealerShow, isCustomerShow = false;

    if(manageCategory == "dealer" || manageCategory == "subdealer")
    {
      isSubDealerShow = true;
    }
    else {
      isSubDealerShow = false;
    }
    if(manageCategory == "dealer" || manageCategory == "subdealer" || manageCategory == "customer")
    {
      isCustomerShow = true;
    }
    else {
      isCustomerShow = false;
    }

    this.state = {
      session: props.session,
      manageCategory: manageCategory,
      loading: true,
      selectedOption: null,
      isClearable: true,
      isDealerShow:isDealerShow,
      isSubDealerShow:isSubDealerShow,
      isCustomerShow:isCustomerShow,
      optionsDealer:optionsDealer,
      optionsSubDealer:optionsSubDealer,
      optionsCustomer:optionsCustomer,
      optionsGroup:optionsGroup,
      activeTab: '1',
      dataVehiclesTable:dataVehiclesTable,
      isOpenModalDetail:false,
      isMarkerShown: false,
      latitude: -34.397,
      longitude: 150.644,
      pathCoordinates:pathCoordinatesArray,
      markers: [],
      center:{lat:-34.397, lng: 150.644},
      max: 500,
      min: 0,
      active: {
            requests: false
        },
      timerange: null,
      hover: null,
      highlight: null,
      selection: null,
      isOpenModalCalibration:false,
      graphLoading:false
    };
  }

  async componentDidMount() {
    console.log('inside componentDidMount')
    //TODO check user logged in or not:
    const session = await NextAuth.init({force: true})
    if(session.user)
    {
      console.log('user logged in')
    }
    else {
      console.log('user not logged in')
      Router.push('/auth/callback')
      return false;
    }
    console.log('after checking user state')
    this.getManages();
  }

  async getManages()
  {
    console.log('inside getManages')
    const {manageCategory} = this.state;
    let optionsSubDealer = []
    let optionsCustomer = []
    let optionsGroup = [{ value: 'all', label: 'All' }]
    let fetchManage;
    if(manageCategory == "group")
    {
      fetchManage = await fetch('/manage/fetch/'+this.state.manageCategory+'/vehicle');
    }
    else {
      fetchManage = await fetch('/manage/fetch/'+this.state.manageCategory+'/group');
    }
    const res = await fetchManage.json();
    console.log('res',res);
    const data = res.data;
    const manage = data.manage;
    for(var i in manage) {
      var item = manage[i];
      var subDealers = ""
      if(manageCategory == "dealer")
      {
      subDealers = item.dealer.subDealers;
      for(var j in subDealers)
      {
        var subDealer = subDealers[j];
        optionsSubDealer.push({
          "value":subDealer.id,
          "label":subDealer.name
        });
      }
      }
      else if(manageCategory == "subdealer") {
        subDealers = item.subDealer;
          var subDealer = subDealers;
          optionsSubDealer.push({
            "value":subDealer.id,
            "label":subDealer.name
          });
      }
      else if(manageCategory == "customer") {
            var customer = item.customer;
            optionsCustomer.push({
              "value":customer.id,
              "label":customer.name
            });
      }
      else if(manageCategory == "group") {
            var group = item.group;
              optionsGroup.push({
                "value":group.id,
                "label":group.name
              });
      }
      else {
          console.log('something went wrong')
      }
    }
    //TODO now getting customer and group according to manageCategory
    if(manageCategory == "dealer" || manageCategory == "subdealer")
      {
          //getting all customer of a subDealer
          const subDealer = optionsSubDealer[0];
          const subDealerId = subDealer.value;
          this.getSubDealerCustomers(subDealerId);
          this.setState({
                        optionsSubDealer:optionsSubDealer,
                        selectSubDealerValue:subDealer
                        })
      }
    else if(manageCategory == "customer") {
         //getting all groups of a customer
         const customer = optionsCustomer[0];
         this.getCustomerGroups(customer);
         this.setState({
                       optionsCustomer:optionsCustomer,
                       selectCustomerValue:customer
                       })
      }
    else if(manageCategory == "group")
      {
        const group = optionsGroup[0];
        this.setState({
                      optionsGroup:optionsGroup,
                      selectGroupValue:group
                      })
        //TODO calling get vehicle with all true case and userId with manageCategory
        const userId = this.state.session.user.id;
        const all = true;
        this.getVehicleForSite(userId, manageCategory, all)

      }
    else {
        console.log('something went wrong')
      }
  }

  async getSubDealerCustomers(subDealerId)
  {
    console.log('inside getSubDealerCustomers, subDealerId',subDealerId)
    let optionsCustomer = []
    if(undefined != subDealerId)
    {
        const fetchSubDealerCustomers = await fetch('/subdealer/'+subDealerId+'/customers');
        const resData = await fetchSubDealerCustomers.json()
        console.log('resData',resData)
        const data = resData;
        for(var i in data)
        {
          var customer = data[i]
          optionsCustomer.push({
            "value":customer.id,
            "label":customer.name
          });
        }
      }
    //TODO now getting all groups of customer
    let customerValue = optionsCustomer[0];
    console.log('customer',customerValue);
    this.getCustomerGroups(customerValue);
    if(undefined == customerValue)
    {
      customerValue = [];
    }
    else {

    }
    this.setState({
                  optionsCustomer:optionsCustomer,
                  selectCustomerValue:customerValue
                  })
  }

  async getCustomerGroups(customer)
  {
    console.log('inside getCustomerGroups, customer',customer)
    let optionsGroup = []
    if(undefined != customer)
    {
      optionsGroup = [{ value: 'all', label: 'All Groups' }]
      let customerId = customer.value;
      let customerName = customer.label;
      const fetchCustomerGroups = await fetch('/customer/'+customerId+'/groups');
      const resData = await fetchCustomerGroups.json()
      console.log('resData',resData)
      const data = resData;
      for(var i in data)
      {
        var group = data[i]
        optionsGroup.push({
          "value":group.id,
          "label":group.name
        });
      }

      //TODO calling get vehicle with all true case and customerId with manageCategory
      const manageCategory = this.state.manageCategory;
      const all = true;
      this.getVehicleForSite(customerId, manageCategory, all)
    }
    let groupValue = optionsGroup[0];
    if(undefined == groupValue)
    {
      groupValue = [];
    }
    this.setState({
                  optionsGroup:optionsGroup,
                  selectGroupValue:groupValue
                  })
  }


  handleChangeSubDealerOption = (selectedOption) => {
    console.log('inside handleChangeSubDealerOption, selectedOption',selectedOption)
    //getting all customer of a subDealer
    const subDealer = selectedOption;
    const subDealerId = subDealer.value;
    this.getSubDealerCustomers(subDealerId);
    this.setState({
                  selectSubDealerValue:selectedOption,
                  selectCustomerValue:[],
                  selectGroupValue:[]
                  });
  }

  handleChangeCustomerOption = (selectedOption) => {
    console.log('inside handleChangeCustomerOption, selectedOption',selectedOption)
    //getting all groups of a customer
    const customer = selectedOption;
    this.getCustomerGroups(customer);
    this.setState({
                  selectCustomerValue:selectedOption,
                  selectGroupValue:[]
                  });
  }

  handleChangeGroupOption = (selectedOption) => {
    console.log('inside handleChangeGroupOption, selectedOption',selectedOption)
    //getting vehicle of all & selected Group
    const group = selectedOption;
    const groupId = group.value;
    const manageCategory = this.state.manageCategory;
    let all = false;
    this.setState({
                  selectGroupValue:selectedOption
                  });

  //TODO calling get vehicle with all true/false case and userId/groupId with manageCategory
  if(groupId == "all")
  {
    all = true;
    //if manageCategory is group then groupId replace with userId
    if(manageCategory == "group")
    {
      //TODO
      const userId = this.state.session.user.id;
      this.getVehicleForSite(userId, manageCategory, all)
    }
    else {
      //TODO in other case getVehicleForSite with customerId
      const customerId = this.state.selectCustomerValue.value;
      this.getVehicleForSite(customerId, manageCategory, all)
    }
  }
  else {
    all = false;
    this.getVehicleForSite(groupId, manageCategory, all)
  }
  }

  async getVehicleForSite(id, manageCategory, all)
  {
    console.log('inside getVehicleForSite')
    this.setState({dataVehiclesTable:[]})
    console.log('id, manageCategory, all', id, ',',manageCategory, ',', all)
    const fetchVehicle = await fetch('/vehicle/fetchvehicleforsitedata/'+id+'/'+manageCategory+'/'+all);
    const res = await fetchVehicle.json();
    console.log('fetchVehicle, res',res);
    const data = res.data;
    const dataVehiclesTable = [];
    let dataForCassQry = [];
    let dataFromApi = {};
    var snVehicles = 1;
    if(all)
    {
        if(manageCategory == "group")
        {
          //res came with manage
          const manage = data;
          for(var i in manage)
          {
            var item = manage[i];
            const group = item.group;
            const vehicles = group.vehicles;
            for(var j in vehicles)
            {
              var itemVehicles = vehicles[j];
              dataForCassQry.push({
                "customerId":group.customerId,
                "vehicleId":itemVehicles.id
              });
              var sensorAssignments = itemVehicles.sensorAssignments;
              //console.log('sensorAssignments',sensorAssignments)
              for(var l in sensorAssignments)
              {
                var itemSensorAssignment = sensorAssignments[l]
              }

              var vehicleCategory = "";

              if(undefined != itemVehicles.vehicleCategory && itemVehicles.vehicleCategory != "")
              {
                vehicleCategory = itemVehicles.vehicleCategory
              }

              var driver;
              if(undefined != itemVehicles.driverName || undefined != itemVehicles.driverMobileNo)
              {
                driver = {name:itemVehicles.driverName, mobileNo:itemVehicles.driverMobileNo}
              }
              else
              {
                driver = {name:'', mobileNo:''}
              }

              var hmrParam;
              if(undefined != itemVehicles.hmrParam)
              {
                hmrParam = itemVehicles.hmrParam;
              }
              else {
                hmrParam = "NA";
              }

              var remark;
              if(undefined != itemVehicles.remark)
              {
                remark = itemVehicles.remark;
              }
              else {
                remark = "";
              }

              let dataForTable = {
                                  customerId:group.customerId,
                                  vehicleId:itemVehicles.id,
                                  group:group.name,
                                  vehicle:itemVehicles.regNo,
                                  sensorAssignments:sensorAssignments,
                                  vehicleCategory:vehicleCategory,
                                  driver:driver,
                                  hmrParam:hmrParam,
                                  remark:remark
                                }
              dataFromApi[itemVehicles.id] = dataForTable
            }
          }
        }
        else {
          // res came with group
          const group = data
          for(var i in group)
          {
            var item = group[i];
            const customerId = item.customerId;
            var vehicles = item.vehicles;
            for(var j in vehicles)
            {
              var itemVehicles = vehicles[j];
              dataForCassQry.push({
                "customerId":customerId,
                "vehicleId":itemVehicles.id
              });
              var sensorAssignments = itemVehicles.sensorAssignments;
              //console.log('sensorAssignments',sensorAssignments)
              for(var l in sensorAssignments)
              {
                var itemSensorAssignment = sensorAssignments[l]
              }

              var vehicleCategory = "";

              if(undefined != itemVehicles.vehicleCategory && itemVehicles.vehicleCategory != "")
              {
                vehicleCategory = itemVehicles.vehicleCategory
              }

              var driver;
              if(undefined != itemVehicles.driverName || undefined != itemVehicles.driverMobileNo)
              {
                driver = {name:itemVehicles.driverName, mobileNo:itemVehicles.driverMobileNo}
              }
              else
              {
                driver = {name:'', mobileNo:''}
              }

              var hmrParam;
              if(undefined != itemVehicles.hmrParam)
              {
                hmrParam = itemVehicles.hmrParam;
              }
              else {
                hmrParam = "NA";
              }

              var remark;
              if(undefined != itemVehicles.remark)
              {
                remark = itemVehicles.remark;
              }
              else {
                remark = "";
              }

              let dataForTable = {
                                  customerId:customerId,
                                  vehicleId:itemVehicles.id,
                                  group:item.name,
                                  vehicle:itemVehicles.regNo,
                                  sensorAssignments:sensorAssignments,
                                  vehicleCategory:vehicleCategory,
                                  driver:driver,
                                  hmrParam:hmrParam,
                                  remark:remark
                                }
              dataFromApi[itemVehicles.id] = dataForTable
            }
          }

        }
    }
    else
    {
      //res came with vehicle
      const vehicle = data
      for(var i in vehicle)
      {
        var item = vehicle[i];
        var group = item.group;
        dataForCassQry.push({
          "customerId":group.customerId,
          "vehicleId":item.id
        });
        var sensorAssignments = item.sensorAssignments;
        //console.log('sensorAssignments',sensorAssignments)
        for(var k in sensorAssignments)
        {
          var itemSensorAssignment = sensorAssignments[k]
        }

        var vehicleCategory = "";

        if(undefined != item.vehicleCategory && item.vehicleCategory != "")
        {
          vehicleCategory = item.vehicleCategory
        }

        var driver;
        if(undefined != item.driverName || undefined != item.driverMobileNo)
        {
          driver = {name:item.driverName, mobileNo:item.driverMobileNo}
        }
        else
        {
          driver = {name:'', mobileNo:''}
        }

        var hmrParam;
        if(undefined != item.hmrParam)
        {
          hmrParam = item.hmrParam;
        }
        else {
          hmrParam = "NA";
        }

        var remark;
        if(undefined != item.remark)
        {
          remark = item.remark;
        }
        else {
          remark = "";
        }

        let dataForTable = {
                            customerId:group.customerId,
                            vehicleId:item.id,
                            group:group.name,
                            vehicle:item.regNo,
                            sensorAssignments:sensorAssignments,
                            vehicleCategory:vehicleCategory,
                            driver:driver,
                            hmrParam:hmrParam,
                            remark:remark
                          }
        dataFromApi[item.id] = dataForTable
      }
    }

    //TODO now getting data from cassandra with dataForCassQry array, its have customerId and vehicleId
    console.log('fetching data for vehicles',dataForCassQry)
    console.log(JSON.stringify(dataForCassQry))
    let payload = {
                    data:dataForCassQry
                  }
    const requestOptions = {
      credentials: 'include',
      method: 'POST',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    const vehicleFetch = await fetch('/vehicle/fetch/',requestOptions);
    const resVehicle = await vehicleFetch.json();
    console.log('resVehicle',resVehicle);
    var dataFromCassandra = resVehicle.data;
    console.log('dataFromApi',dataFromApi)
    console.log('dataFromCassandra',dataFromCassandra)

    //TODO convert fuel litre for voltage
    const dataForFuelSensor = dataFromApi;
    const vehiclesFuelSensorDetails = await this.getVehiclesFuelSensorDetails(dataForFuelSensor);
    console.log('vehiclesFuelSensorDetails', vehiclesFuelSensorDetails);

    //TODO getting address(location) of vehicle from lat/lng;
    const dataForAddress = dataFromCassandra;
    const vehiclesAddress = await this.getVehiclesAddress(dataForAddress);
    console.log('vehiclesAddress', vehiclesAddress);
    //now showing data on table from both data(loopback, cassandra)

    //TODO getting today device data, using this we can calcuate distance travelled(kms), hmr and other informations
    //TODO not required here, use in on details button click for further calculation on basis of today data
    const todayData = await this.getTodaysVehiclesData(dataForCassQry);

    for(var i in dataForCassQry)
    {
      var item = dataForCassQry[i]
      var vehicleId = item.vehicleId
      var apiData = dataFromApi[vehicleId];
      var cassDataArray = dataFromCassandra[vehicleId];
      var cassData = cassDataArray[0];
      var vehicleAddress = vehiclesAddress[vehicleId];
      var vehicleFuelSensorDetails = vehiclesFuelSensorDetails[vehicleId];

      //TODO need to get vehicle status and assigned sensor status using function
      //using apiData & cassData
      let vehicleAndSensorStatus = await this.getVehicleAndSensorStatus(apiData, cassDataArray);
      let status;
      if(undefined != vehicleAndSensorStatus)
      {
        if(vehicleAndSensorStatus.hasOwnProperty('vehiclestatus'))
        {
          status = vehicleAndSensorStatus['vehiclestatus'];
        }
        else {
          status = "NA";
        }
      }
      else {
        status = "NA";
      }
      let engine;
      if(undefined != vehicleAndSensorStatus)
      {
        if(vehicleAndSensorStatus.hasOwnProperty('engine'))
        {
          engine = vehicleAndSensorStatus['engine'];
        }
        else {
          engine = "NA";
        }
      }
      else {
        engine = "NA";
      }
      let power;
      if(undefined != vehicleAndSensorStatus)
      {
        if(vehicleAndSensorStatus.hasOwnProperty('power'))
        {
          power = vehicleAndSensorStatus['power'];
        }
        else {
          power = "NA";
        }
      }
      else {
        power = "NA";
      }
      let ignition;
      if(undefined != vehicleAndSensorStatus)
      {
        if(vehicleAndSensorStatus.hasOwnProperty('ignition'))
        {
          ignition = vehicleAndSensorStatus['ignition'];
        }
        else {
          ignition = "NA";
        }
      }
      else {
        ignition = "NA";
      }
      let fuelInLitre;
      if(undefined != cassData.adc1 && undefined != vehicleFuelSensorDetails.fuelSensorFound && vehicleFuelSensorDetails.fuelSensorFound)
      {
        if(undefined != vehicleFuelSensorDetails.equation && vehicleFuelSensorDetails.equation != "Not Found")
        {
        const e = vehicleFuelSensorDetails.equation;
        const mV = cassData.adc1;
        fuelInLitre = e[0]*mV*mV*mV*mV + e[1]*mV*mV*mV + e[2]*mV*mV + e[3] *mV + e[4]
        fuelInLitre = Number(fuelInLitre.toFixed(2));
        }
        else {
          fuelInLitre = "NA"
        }
      }
      else {
        fuelInLitre = "NA";
      }
      let battery;
      if(undefined != cassData.power)
      {
        battery = Number(cassData.power.toFixed(2));
      }
      else {
        battery = 'NA'
      }
      let insert_datetime;
      if(undefined != cassData.insert_datetime)
      {
        insert_datetime = moment(cassData.insert_datetime).format("DD/MM/YYYY LT")
      }
      else {
        insert_datetime = "NA";
      }
      let category;
      if(apiData.vehicleCategory != "")
      {
        category = apiData.vehicleCategory.name;
      }
      else {
        category = "NA";
      }
      let location;
      if(undefined != vehicleAddress && undefined != vehicleAddress.address)
      {
        location = vehicleAddress.address;
      }
      else {
        location = "NA";
      }
      let speed;
      if(undefined != cassData.speed)
      {
        speed = parseInt(cassData.speed);
      }
      else {
        speed = "NA";
      }
      let driverMobile;
      let driver;
      if(apiData.driver != "")
      {
        driver = apiData.driver;
        driverMobile = driver.mobileNo;
      }
      else {
        driver = "NA"
        driverMobile = "NA";
      }

      //console.log('data from api through vehicleId',apiData)
      //console.log('data from cassandra through vehicleId',cassData)
      dataVehiclesTable.push({
          "sn" : snVehicles,
          "group": apiData.group || 'NA',
          "vehicle":apiData.vehicle || 'NA',
          "vehicleId":apiData.vehicleId,
          "customerId":apiData.customerId,
          "vehicleCategory":apiData.vehicleCategory,
          "category":category,
          "status" : status,
          "date"  : insert_datetime,
          "sensorAssignments":apiData.sensorAssignments,
          "engine":engine,
          "power":power,
          "a6": cassData.a6,
          "adc1":cassData.adc1,
          "fuel":fuelInLitre,
          "battery" : battery,
          "ignition" : ignition,
          "latitude": cassData.latitude,
          "longitude": cassData.longitude,
          "detail"  : 'Detail',
          "location"  : location,
          "speed": speed,
          "idletime": 'NA',
          "driver":driver,
          "mobile": driverMobile || 'NA',
          "remark":apiData.remark,
          "imei": cassData.deviceid || 'NA',
          "hmrParam":apiData.hmrParam || 'NA',
          "rawData":cassData
        });
        snVehicles++;
    }
    let totalVehicleCount = 0;
    let movingCount = 0;
    let engineIdleCount = 0;
    let stoppageCount = 0;
    let notReachableCount = 0;
    totalVehicleCount = dataVehiclesTable.length;
    console.log('dataVehiclesTable length',dataVehiclesTable.length)
    for(var i in dataVehiclesTable)
    {
      let status = dataVehiclesTable[i].status;
      if(undefined != status && status != "NA")
      {
        if(status == "Moving")
        {
          ++movingCount;
        }
        else if(status == "Engine Idle")
        {
          ++engineIdleCount;
        }
        else if(status == "Stoppage")
        {
          ++stoppageCount;
        }
        else if(status == "Not Reachable")
        {
          ++notReachableCount;
        }
      }
      else {
        ++notReachableCount;
      }
    }
    console.log('totalVehicleCount:',totalVehicleCount)
    console.log('movingCount:',movingCount)
    console.log('engineIdleCount:',engineIdleCount)
    console.log('stoppageCount:',stoppageCount)
    console.log('notReachableCount:',notReachableCount)
    this.setState({
                  loading:false,
                  dataVehiclesTable:dataVehiclesTable,
                  totalVehicleCount:totalVehicleCount,
                  movingCount:movingCount,
                  engineIdleCount:engineIdleCount,
                  stoppageCount:stoppageCount,
                  notReachableCount:notReachableCount
                  })

  }

  async getVehiclesFuelSensorDetails(dataForFuelSensor)
  {
    console.log('inside getVehiclesFuelSensorDetails')
    let vehiclesFuelSensorDetails = {};
    for(var i in dataForFuelSensor)
    {
      var fuelSensorLength = 0;
      var fuelSensorFound = false;
      var fuelSensorDetails;
      var item = dataForFuelSensor[i];
      var sensorAssignments = item.sensorAssignments;
      console.log('item',item)
      if(undefined != sensorAssignments)
      {
        for(var i in sensorAssignments)
        {
          var item = sensorAssignments[i];
          if(undefined != item.sensor && undefined != item.sensor.calibrationParam && undefined != item.sensor.sensorType)
          {
            const sensor = item.sensor;
            const calibrationParam = sensor.calibrationParam;
            const sensorType = sensor.sensorType;
            if(sensorType.name.toLowerCase() == "fuel")
            {
              console.log('fuel sensor found')
              fuelSensorDetails = item;
              fuelSensorFound = true;
              fuelSensorLength++;
            }
            else {
              console.log('not fuel sensor')
            }
          }
          else {
            console.log('not fuel sensor')
          }
        }
      }
      else {
        console.log('sensorAssignments undefined')
      }

      if(fuelSensorFound && fuelSensorLength == 1)
      {
        console.log('single fuelSensorFound')
        const sensor = fuelSensorDetails.sensor;
        //TODO need to check calibrationParam found, if found then fuelSensorCalibrated true other wise false
        if(undefined != sensor.calibrationParam)
        {
          const calibrationParam = sensor.calibrationParam;
          const equationResult = calibrationParam.equationResult;
          if(undefined != equationResult && undefined != equationResult.equation)
          {
            const equation = equationResult.equation;
            console.log('fuel litre equation', equation)
            vehiclesFuelSensorDetails[item.vehicleId] = {fuelSensorFound:true, equation:equation}
            //vehiclesFuelSensorDetails[item.vehicleId].litre = 100;
          }
          else
          {
            console.log('equationResult undefined')
            vehiclesFuelSensorDetails[item.vehicleId] = {fuelSensorFound:true, equation:'Not Found'};
          }
        }
        else
        {
          console.log('calibrationParam undefined')
          vehiclesFuelSensorDetails[item.vehicleId] = {fuelSensorFound:true, equation:'Not Found'};
        }
      }
      else {
        console.log('fuel sensor not found or found more than once')
        vehiclesFuelSensorDetails[item.vehicleId] = {fuelSensorFound:false, equation:'Not Found'};
      }

    }

    return vehiclesFuelSensorDetails;

  }

  async getVehiclesAddress(dataForAddress)
  {
    console.log('inside getVehiclesAddress')
    console.log('dataForAddress',dataForAddress)
    let vehiclesAddress = {};
    for(var i in dataForAddress)
    {
      var item = dataForAddress[i];
      console.log('item',item)
      if(undefined != item[0].latitude && undefined != item[0].longitude)
      {
        console.log('lat/lng not undefined')
        var deviceData = item[0];
        var latitude = deviceData.latitude;
        var longitude = deviceData.longitude;
        var geocodeRes = await Geocode.fromLatLng(latitude, longitude).then(
          response => {
            const address = response.results[0].formatted_address;
            console.log('address',address);
            return  {'address':address};
          },
          error => {
            console.error('error',error);
            return  {'address':'Not Found'};
          }
        );
        console.log('geocodeRes',geocodeRes)
        vehiclesAddress[i] = geocodeRes;
      }
      else {
        console.log('lat/lng undefined')
        vehiclesAddress[i] = {'address':'Not Found'};
      }
    }
    return vehiclesAddress;
  }

  async getVehicleAndSensorStatus(apiData, deviceData)
  {
    console.log('inside getVehicleAndSensorStatus')
    console.log('apiData',apiData);
    //console.log('deviceData:',deviceData);
    let vehicleAndSensorStatus = {};
    let vehicleStatus;
    if(undefined != deviceData && deviceData != "data not found" && deviceData != "some error occured")
    {
      let rawData = deviceData[0]
      console.log('raw data',rawData)
      let sensorAssignments = apiData.sensorAssignments;
      let assignedSensorsDetails;
      let isSensorAssignmentsFound;
      if(undefined != sensorAssignments && sensorAssignments.length > 0)
      {
        assignedSensorsDetails = await this.getAssignedSensorsDetails(sensorAssignments, rawData);
        console.log('assignedSensorsDetails',assignedSensorsDetails);
        for(var i in assignedSensorsDetails)
        {
          let sensorType = i;
          let sensorStatus;
          let min = assignedSensorsDetails[i].min;
          let max = assignedSensorsDetails[i].max;
          let raw = assignedSensorsDetails[i].raw;
          console.log('min',min,'max',max)
          console.log('raw',raw);
          if(undefined != raw && raw != "NA")
          {
            if(raw >= min && raw <= max)
            {
              sensorStatus = true;
              vehicleAndSensorStatus[i.toLowerCase()] = "on"
            }
            else {
              sensorStatus = false;
              vehicleAndSensorStatus[i.toLowerCase()] = "off"
            }
          }
          else {
            vehicleAndSensorStatus[i.toLowerCase()] = "NA"
          }
        }
        //TODO now get other sensors status using assignedSensorsDetails & rawData
        isSensorAssignmentsFound = true;
      }
      else {
        isSensorAssignmentsFound = false;
        //TODO need to discuss if its false then what to show on table, NA//other msg//raw of some sensors
      }

      let speed = rawData.speed;
      if(undefined != speed)
      {
        if(speed > 0)
        {
          vehicleStatus = "Moving"
        }
        else
        {
          vehicleStatus = "Stoppage";
        }
      }
      else
      {
        console.log('speed not found')
        vehicleStatus = "NA";
      }
      //TODO now check engine status
      if(isSensorAssignmentsFound)
      {
        let hmrParam = apiData.hmrParam;
        if(undefined != hmrParam && hmrParam != "NA")
        {
          let engineStatus = await this.checkEngineStatus(assignedSensorsDetails, hmrParam);
          console.log('enginestatus',engineStatus)
          if(undefined != engineStatus)
          {
            let status = engineStatus.status;
            let msg = engineStatus.msg;
            if(status == "on")
            {
              if(vehicleStatus != "Moving")
              {
                vehicleStatus = "Engine Idle";
              }
              vehicleAndSensorStatus['engine'] = status;
            }
            else if(status == "off")
            {
              vehicleAndSensorStatus['engine'] = status;
            }
            else
            {
              vehicleAndSensorStatus['engine'] = status;
            }
          }
        }
      }
      //TODO now check raw data last insert_datetime is not less then defined notReachableTime
      //if less then vehicleStatus is Not Reachable;
      let insert_datetime = rawData.insert_datetime;
      if(undefined != insert_datetime)
      {
        console.log('insert_datetime',insert_datetime);
        //TODO if insert_datetime < notReachableTime then vehcile status is "Not Reachable"
      }
      else {
        vehicleStatus = "NA";
      }
    }
    else
    {
      console.log(deviceData)
      vehicleStatus = 'NA';
    }
    //TODO now check raw data is not
    console.log('vehicleStatus',vehicleStatus);
    vehicleAndSensorStatus['vehiclestatus'] = vehicleStatus;
    console.log('vehicleAndSensorStatus:',vehicleAndSensorStatus)
    return vehicleAndSensorStatus;
  }

  async getAssignedSensorsDetails(sensorAssignments, rawData)
  {
    console.log('inside getAssignedSensorsDetails');
    let assignedSensorsDetails = {};
    for(var i in sensorAssignments)
    {
      var item = sensorAssignments[i];
      var gpsDevice = item.gpsDevice;
      var devicePort= item.devicePort;
      var devicePortName;
      var sensor = item.sensor;
      var sensorType = sensor.sensorType;
      var min = item.min;
      var max = item.max;
      var raw;
      if(undefined != sensorType && undefined != sensorType.name)
      {
        let sensorTypeName = sensorType.name.toLowerCase();
        if(sensorTypeName == "fuel" || sensorTypeName == "enginestatus")
        {
          continue;
        }

        if(undefined == min)
        {
          min = "NA";
        }
        if(undefined == max)
        {
          max = "NA";
        }
        if(undefined != rawData)
        {
          if(undefined != devicePort && undefined != devicePort.name)
          {
            let column = cassandraColName[devicePort.name];
            console.log('getting raw data, column',column)
            console.log('deviceData[column]',rawData[column])
            if(undefined != rawData[column])
            {
              raw = rawData[column];
            }
            else
            {
              raw = "NA";
            }
          }
          else {
            raw = "NA";
          }
        }
        if(undefined != devicePort && undefined != devicePort.name)
        {
          devicePortName = devicePort.name;
        }
        else {
          devicePortName = "NA";
        }
        console.log('sensorTypeName:',sensorType.name);
        console.log('min:',min,', max:',max);
        console.log('raw:',raw)
        console.log('devicePortName:',devicePortName);
        assignedSensorsDetails[sensorType.name] = {devicePort:devicePortName,min:min,max:max,raw:raw}
      }
    }
    return assignedSensorsDetails;
  }

  //TODO check engine status using assignedSensorsDetails & hmrParam
  async checkEngineStatus(assignedSensorsDetails, hmrParam)
  {
    console.log('inside checkEngineStatus');
    //TODO need to validate availability of hmrParam;
    //TODO first check primarySensor;
    let engineStatus = {};
    let primarySensor;
    if(hmrParam.hasOwnProperty('primary'))
    {
      primarySensor = hmrParam.primary;
    }
    else {
      engineStatus = {status: 'not known', msg:'primary sensor not found'}
      return engineStatus;
    }
    let secondarySensor;
    if(hmrParam.hasOwnProperty('secondary'))
    {
      secondarySensor = hmrParam.secondary;
    }
    let dataSelectedSecANDType;
    let dataSelectedSecORType;
    if(undefined != secondarySensor)
    {
      if(secondarySensor.hasOwnProperty('and'))
      {
        dataSelectedSecANDType = secondarySensor.and;
      }
      if(secondarySensor.hasOwnProperty('or'))
      {
        dataSelectedSecORType = secondarySensor.or;
      }
    }
    console.log('primarySensor',primarySensor);
    console.log('secondarySensor',secondarySensor);
    console.log('dataSelectedSecANDType',dataSelectedSecANDType);
    console.log('dataSelectedSecORType',dataSelectedSecORType);
    //TODO need to first check if primary and secondary sensors not undefined
    if(undefined == primarySensor && undefined == dataSelectedSecANDType && undefined == dataSelectedSecORType)
    {
      console.log('primary and secondary sensors not found in hmrParam')
      engineStatus = {status:'not known', msg:'hmr not defined'}
      return engineStatus;
    }
    let primarySensorStatus = false;
    let secondaryANDSensorsStatus = false;
    let secondaryORSensorsStatus = false;

    if(undefined != primarySensor)
    {
      console.log('assignedSensorsDetails',assignedSensorsDetails)
      console.log('primarySensor',primarySensor)
      for(var i in assignedSensorsDetails)
      {
        let sensorType = i.toLowerCase();
        if(sensorType == primarySensor)
        {
          //first found primary sensor raw value, min and max from dataAssignedSensorTable
          //then make decision
          let min = assignedSensorsDetails[i].min;
          let max = assignedSensorsDetails[i].max;
          let raw = assignedSensorsDetails[i].raw;
          console.log('min',min,'max',max)
          console.log('raw',raw);
          if(undefined != raw)
          {
            if(raw >= min && raw <= max)
            {
              primarySensorStatus = true;
            }
            else {
              primarySensorStatus = false;
            }
          }
        }
      }
    }
    else {
      console.log('primary sensor not defined')
    }
    if(primarySensorStatus)
    {
      console.log('engine on')
      engineStatus = {status:'on', msg:'checked with primary sensor'}
      return engineStatus;
    }
    //TODO if primarySensorStatus false then check secondary sensors status
    else
    {
      //TODO now check secondary sensors
      //First check secondary sensors with AND conditions (always true)
      console.log('dataSelectedSecANDType',dataSelectedSecANDType)
      console.log('dataSelectedSecORType',dataSelectedSecORType)
      if(undefined != dataSelectedSecANDType)
      {
        for(var i in assignedSensorsDetails)
        {
          let sensorType = i.toLowerCase();;
          if(dataSelectedSecANDType.hasOwnProperty(sensorType))
          {
            console.log('now checked secondary AND for sensor',sensorType);
            let min = assignedSensorsDetails[i].min;
            let max = assignedSensorsDetails[i].max;
            let raw = assignedSensorsDetails[i].raw;
            console.log('min',min,'max',max)
            console.log('raw',raw);
            if(undefined != raw)
            {
              if(raw >= min && raw <= max)
              {
                secondaryANDSensorsStatus = true;
              }
              else {
                secondaryANDSensorsStatus = false;
                break;
              }
            }
          }
        }
      }
      else {
        console.log('secondary AND type sensors not defined')
      }
      if(secondaryANDSensorsStatus)
      {
        console.log('engine on')
        engineStatus = {status:'on', msg:'checked with secondary AND type sensors'}
        return engineStatus;
      }
      else
      {
        //Second check secondary sensors with OR conditions (atleast one true)
        if(undefined != dataSelectedSecORType)
        {
          for(var i in assignedSensorsDetails)
          {
            let sensorType = i.toLowerCase();
            if(dataSelectedSecORType.hasOwnProperty(sensorType))
            {
              console.log('now checked secondary OR for sensor',sensorType);
              let min = assignedSensorsDetails[i].min;
              let max = assignedSensorsDetails[i].max;
              let raw = assignedSensorsDetails[i].raw;
              console.log('min',min,'max',max)
              console.log('raw',raw);
              if(undefined != raw)
              {
                if(raw >= min && raw <= max)
                {
                  secondaryORSensorsStatus = true;
                  break;
                }
                else {
                  secondaryORSensorsStatus = false;
                }
              }
            }
          }
        }
        else {
          console.log('secondary OR type sensors not defined')
        }

        if(secondaryORSensorsStatus)
        {
          console.log('engine on')
          engineStatus = {status:'on', msg:'checked with secondary OR type sensors'}
          return engineStatus;
        }
        else {
          console.log('engine off')
          engineStatus = {status:'off', msg:'checked with secondary OR type sensors'}
          return engineStatus;
        }
      }
    }
    return engineStatus;
  }

  async getTodaysVehiclesData(dataForCassQry)
  {
    console.log('inside getTodaysVehiclesData')
    let todaysVehiclesDeviceData = [];
    let payload = {
                    data:dataForCassQry
                  }
    const requestOptions = {
      credentials: 'include',
      method: 'POST',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    let startDateTime = new Date();
    //TODO Date.setHours(hour, min, sec, millisec)
    startDateTime.setHours(0,0,0,0);
    console.log('startDateTime',startDateTime)
    let endDateTime = new Date();
    console.log('endDateTime',endDateTime)
    let startDT = Math.floor(startDateTime.getTime());
    let endDT = Math.floor(endDateTime.getTime());
    console.log('startDT', startDT);
    console.log('endDT', endDT);
    /*
    const todayData = await fetch('/vehicle/todaydata/'+startDT+'/'+endDT,requestOptions);
    const res = await todayData.json();
    console.log('todayData res as startDT & endDT',res)
    todaysVehiclesDeviceData = res;
    */
    return todaysVehiclesDeviceData;;
  }

  handleChange = (selectedOption) => {
      this.setState({ selectedOption });
      console.log(`Option selected:`, selectedOption);
    }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  async detail(row)
    {
      console.log('inside detail, row data',row)
      console.log('this.state.pathCoordinates',this.state.pathCoordinates)
      pathCoordinatesArray = [];
      const fuelSensorFound = await this.checkFuelSensorAvailability(row);
      const customerId = row.customerId;
      const vehicleId = row.vehicleId;
      const latitude = row.latitude;
      const longitude = row.longitude;
      const driver = row.driver;
      let fuel;
      //TODO now calling continuos to get current data from cassandra using customerId and vehicleId
      if(undefined != fuelSensorFound && fuelSensorFound)
      {
        fuel = this.calculateFuelInLitre(row.adc1);
      }
      else {

      }
      pathCoordinatesArray.push({lat:Number(latitude),lng:Number(longitude)})
      let dateTime = new Date();
      //dateTime.toUTCString();
      let dateTimeTS = Math.floor(dateTime.getTime());
      let graphStartDateTimeTS = dateTimeTS;
      let graphEndDateTimeTS = dateTimeTS;
      this.setState({
                      row:row,
                      customerId:customerId,
                      vehicleId:vehicleId,
                      vehicleRegNo:row.vehicle,
                      vehicleCategory:row.category,
                      driverName:driver.name || 'NA',
                      driverMobileNo:driver.mobileNo || 'NA',
                      speed: row.speed,
                      battery: row.battery,
                      ignition: row.ignition,
                      fuel:fuel,
                      isMarkerShown:false,
                      pathCoordinates:pathCoordinatesArray,
                      graphStartDateTime: new Date(),
                      graphEndDateTime: new Date(),
                      graphStartDateTimeTS:dateTimeTS,
                      graphEndDateTimeTS:dateTimeTS
                    })

      this.toggleDetail();
      this.delayedShowMarkerWithLatLng(latitude,longitude)
      this.trackVehicle(customerId, vehicleId);
      //
      /*
      console.log('pathCoordinatesArray, before push in details ***', pathCoordinatesArray, pathCoordinatesArray.length)
      pathCoordinatesArray.push({lat:Number(latitude),lng:Number(longitude)})
      console.log('pathCoordinatesArray, after push in details ***', pathCoordinatesArray, pathCoordinatesArray.length)
      */
    }

    toggleDetail() {
      console.log('inside toggleDetail, isOpenModalDetail',this.state.isOpenModalDetail)
      if(this.state.isOpenModalDetail)
      {
        clearInterval(this.state.intervalId);
        //TODO for graph
        this.setState({
                      active:{requests:false}
                    })
      }
      this.setState({
        isOpenModalDetail: !this.state.isOpenModalDetail,
      });
    }

    checkFuelSensorAvailability(row)
    {
      console.log('inside checkFuelSensorAvailability')
      const sensorAssignments = row.sensorAssignments
      var fuelSensorLength = 0;
      var fuelSensorFound = false;
      var fuelSensorDetails;
      for(var i in sensorAssignments)
      {
        var item = sensorAssignments[i];
        if(undefined != item.sensor && undefined != item.sensor.calibrationParam && undefined != item.sensor.sensorType)
        {
          const sensor = item.sensor;
          const calibrationParam = sensor.calibrationParam;
          const sensorType = sensor.sensorType;
          if(sensorType.name.toLowerCase() == "fuel")
          {
            console.log('fuel sensor found')
            fuelSensorDetails = item;
            fuelSensorFound = true;
            fuelSensorLength++;
          }
          else {
            console.log('not fuel sensor')
          }
        }
        else {
          console.log('not fuel sensor')
        }
      }

      if(fuelSensorFound && fuelSensorLength == 1)
      {
        console.log('single fuelSensorFound')
        const sensor = fuelSensorDetails.sensor;
        //TODO need to check calibrationParam found, if found then fuelSensorCalibrated true other wise false
        if(undefined != sensor.calibrationParam)
        {
          const calibrationParam = sensor.calibrationParam;
          const equationResult = calibrationParam.equationResult;
          if(undefined != equationResult && undefined != equationResult.equation)
          {
            const equation = equationResult.equation;
            const isFuelSensorCalibrated = calibrationParam.calibrated;
            console.log('fuel litre equation', equation)
            if(isFuelSensorCalibrated)
            {
              this.setState({
                              fuelSensorFound:true,
                              fuelSensorCalibrated:isFuelSensorCalibrated,
                              calibrationDateTime:calibrationParam.calibrationDateTime,
                              calibrationParamEquation:equation,
                              sensorId:sensor.id
                            })
            }
            else {
              this.setState({
                              fuelSensorFound:true,
                              fuelSensorCalibrated:isFuelSensorCalibrated,
                              calibrationParamEquation:equation,
                              sensorId:sensor.id
                            })
            }
          }
          else {
            this.setState({
                            fuelSensorFound:true,
                            fuelSensorCalibrated:false,
                            calibrationParamEquation:undefined,
                            sensorId:sensor.id
                          })
          }

        }
        else {
          this.setState({
                          fuelSensorFound:true,
                          fuelSensorCalibrated:false,
                          calibrationParamEquation:undefined,
                          sensorId:sensor.id
                        })
        }
      }
      else {
        console.log('fuel sensor not found or found more than once')
        this.setState({
                        fuelSensorFound:false,
                        calibrationParamEquation:undefined
                      })
      }
      return fuelSensorFound;
    }

    calculateFuelInLitre(voltage)
    {
      console.log('inside calculateFuelInLitre, for voltage',voltage)
      const {calibrationParamEquation, fuelSensorFound} = this.state;
      var litre = 0;
      if(undefined != fuelSensorFound && fuelSensorFound)
      {
        if(undefined != voltage && undefined != calibrationParamEquation && calibrationParamEquation.length == 5)
        {
          const e = calibrationParamEquation;
          const mV = voltage;
          litre = e[0]*mV*mV*mV*mV + e[1]*mV*mV*mV + e[2]*mV*mV + e[3] *mV + e[4]
          litre = Number(litre.toFixed(2));
        }
        else {
          console.log('calibrationParamEquation not found')
          //TODO calibrationParamEquation not found so returning static value as 10 litre
          //litre = 0;
          litre = 10;
        }
        console.log('voltage:',voltage, 'litre',litre);
      }
      return litre;
    }


    delayedShowMarker = () => {
      setTimeout(() => {
        this.setState({ isMarkerShown: true })
      }, 3000)
    }

    delayedShowMarkerWithLatLng = (lat,lng) => {
      let center = {lat: lat, lng: lng}
      setTimeout(() => {
        this.setState({
                      center:center,
                      isMarkerShown: true,
                      latitude:lat,
                      longitude:lng,
                     })
      }, 3000)
    }

    handleMarkerClick = () => {
      this.setState({ isMarkerShown: false })
      this.delayedShowMarker()
    }

    delayedShowPolyline = (lat, lng) => {
      console.log('inside delayedShowPolyline, lat/lng',lat,'/',lng)
      if(undefined != lat && undefined != lng)
      {
        if(lat != this.state.latitude && lng != this.state.longitude)
        //if(true)
        {
          console.log('different lat/lng found');
          let currentLatLng = {lat:Number(lat), lng:Number(lng)};
          //console.log('pathCoordinatesArray:, before push',pathCoordinatesArray, pathCoordinatesArray.length);
          //pathCoordinatesArray.push(currentLatLng);
          //console.log('pathCoordinatesArray:, after push',pathCoordinatesArray, pathCoordinatesArray.length);
          //pathCoordinatesForTest.push({lat:(pathCoordinatesForTest.length - 1).lat + 1, lng:(pathCoordinatesForTest.length - 1).lng - 1})
          console.log('this.state.pathCoordinates',this.state.pathCoordinates)
          console.log('...this.state.pathCoordinates',...this.state.pathCoordinates)
          console.log('currentLatLng',currentLatLng)
          /*
          setTimeout(() => {
            this.setState({
                          pathCoordinates:pathCoordinatesArray
                         })
          }, 3000)
          */
          setTimeout(() => {
            this.setState({pathCoordinates:[...this.state.pathCoordinates, currentLatLng]})
          }, 3000)
        }
        else {
          console.log('last and current lat/lng are same')
        }
      }
    }

    async trackVehicle(customerId, vehicleId)
    {
      console.log('inside trackVehicle')
      //TODO need to finialize interval seconds
      var intervalId = setInterval(this.trackVehicleTimer.bind(this,customerId, vehicleId), 15000);
      // store intervalId in the state so it can be accessed later:
      this.setState({intervalId: intervalId});
    }

    componentWillUnmount() {
     // use intervalId from the state to clear the interval
     console.log('inside componentWillUnmount')
     clearInterval(this.state.intervalId);
    }

    async trackVehicleTimer(customerId, vehicleId) {
      // setState method is used to update the state
      const {fuelSensorFound} = this.state;
      console.log('inside trackVehicleTimer')
      console.log('fetching data for, customerId, vehicleId', customerId,',',vehicleId)
      const data = {customerId:customerId,vehicleId:vehicleId}
      const dataForCassQry = [data];
      let payload = {
                      data:dataForCassQry
                    }
      const requestOptions = {
        credentials: 'include',
        method: 'POST',
        headers: { 'Content-Type': 'application/json',
                    'X-CSRF-Token': await NextAuth.csrfToken()
                  },
        body : JSON.stringify(payload)
      };
      const vehicleFetch = await fetch('/vehicle/fetch/',requestOptions);
      const resVehicle = await vehicleFetch.json();
      console.log('resVehicle',resVehicle);
      const vehicleData = resVehicle.data;
      const vehicleDataByVehicleId = vehicleData[vehicleId];
      console.log('vehicleDataByVehicleId',vehicleDataByVehicleId)
      if(vehicleDataByVehicleId != "data not found" && vehicleDataByVehicleId != "some error occured"){
        const vehicle = vehicleDataByVehicleId[0];
        const latitude = vehicle.latitude;
        const longitude = vehicle.longitude;
        const battery = vehicle.power ? vehicle.power.toFixed(2) : vehicle.power;
        let fuel;
        if(undefined != fuelSensorFound && fuelSensorFound)
        {
          fuel = this.calculateFuelInLitre(vehicle.adc1);
        }
        else {

        }
        this.setState({
                        speed: parseInt(vehicle.speed),
                        battery: Number(battery),
                        ignition : vehicle.ignition,
                        a6:vehicle.a6,
                        adc1:vehicle.adc1,
                        fuel:fuel
                      })

        //
        //let pathCoorForTestData = pathCoordinatesForTest[indexPathCoordinates];
        //this.delayedShowPolyline(pathCoorForTestData.lat,pathCoorForTestData.lng);
        //this.delayedShowMarkerWithLatLng(pathCoorForTestData.lat,pathCoorForTestData.lng)
        //indexPathCoordinates++;
        this.delayedShowPolyline(latitude,longitude);
        this.delayedShowMarkerWithLatLng(latitude,longitude)

      }
    }

    getSpeedData = () => {
      return [
        ["Label", "Value"],
        ["Speed", this.state.speed || 0]
      ];
    };

    getFuelData = () => {
        if(this.state.isOpenModalDetail)
        {
        const fuel = this.state.fuel;
        return [
          ["Label", "Value"],
          ["Fuel", fuel || 0]
        ];
      }
      else {
        return [
          ["Label", "Value"],
          ["Fuel", 0]
        ];
      }
    };

    getBatteryData = () => {
      return [
        ["Label", "Value"],
        ["Battery", this.state.battery || 0]
      ];
    };

    handleSelectStartDateTime(date)
    {
      console.log('inside handleSelectStartDateTime, date',date)
      var dateTime = date._d;
      console.log('inside handleSelectDateTime, selected datetime',dateTime)
      if(undefined == dateTime)
      {
        dateTime = new Date();
      }
      console.log('datetime',dateTime)
      //dateTime.toUTCString();
      //console.log('datetime.toUTCString',dateTime.toUTCString());
      var dateTimeTS = Math.floor(dateTime.getTime());
      console.log('dateTimeTS',dateTimeTS);
      var dateTimeFormatted = moment(dateTime).format("YYYY-MM-DDTHH:mm:ss.SSS[Z]")
      console.log('dateTimeFormatted',dateTimeFormatted)
      this.setState({
                      graphStartDateTime:dateTimeFormatted,
                      graphStartDateTimeTS:dateTimeTS
                  })
    }

    handleSelectEndDateTime(date)
    {
      console.log('inside handleSelectEndDateTime, date',date)
      var dateTime = date._d;
      console.log('inside handleSelectDateTime, selected datetime',dateTime)
      if(undefined == dateTime)
      {
        dateTime = new Date();
      }
      console.log('datetime',dateTime)
      //dateTime.toUTCString();
      //console.log('datetime.toUTCString',dateTime.toUTCString());
      var dateTimeTS = Math.floor(dateTime.getTime());
      console.log('dateTimeTS',dateTimeTS);
      var dateTimeFormatted = moment(dateTime).format("YYYY-MM-DDTHH:mm:ss.SSS[Z]")
      console.log('dateTimeFormatted',dateTimeFormatted)
      this.setState({
                      graphEndDateTime:dateTimeFormatted,
                      graphEndDateTimeTS:dateTimeTS
                  })
    }

    showGraph()
    {
      console.log('show graph');
      const {customerId, vehicleId, graphStartDateTime, graphEndDateTime, graphStartDateTimeTS, graphEndDateTimeTS} = this.state;
      console.log('graphStartDateTime', graphStartDateTime)
      console.log('graphEndDateTime', graphEndDateTime)
      console.log('graphStartDateTimeTS',graphStartDateTimeTS)
      console.log('graphEndDateTimeTS',graphEndDateTimeTS)
      this.setState({graphLoading:true})
      this.getGraphData(customerId, vehicleId, graphStartDateTimeTS, graphEndDateTimeTS)
    }

    async getGraphData(customerId, vehicleId, startTimestamp, endTimestamp)
    {
      requestsSeries = null;
      let requests = [];
      console.log('inside getGraphData')
      var d_1 = new Date();
      var utc_1 = d_1.toUTCString();
      console.log('start time :',utc_1)
      //TODO need to remove and use came from function parameter
      const custId = customerId; //'5b868c4ffe33d83448b417c6'//'5b6053fe17cefc25afb7b954';
      const vehId =  vehicleId; //'5b963f8b9e2f8b2c2b2d1544'//'5b6057cfa2ea1b2935bb1881'
      const startDT = startTimestamp;
      const endDT = endTimestamp;
      const fetchGraphData = await fetch('/vehicle/fetchgraphdata/'+custId+'/'+vehId+'/'+startDT+'/'+endDT);
      var d_2 = new Date();
      var utc_2 = d_2.toUTCString();
      console.log('end time :',utc_2)
      const resGraphData = await fetchGraphData.json();
      var d_3 = new Date();
      var utc_3 = d_3.toUTCString();
      console.log('after convert res to josn, end time :',utc_3)
      console.log('resGraphData',resGraphData);
      const data = resGraphData.data;
      //TODO chec
      if(undefined != data && data != "data not found" && data != "some error occured")
      {
        console.log('data length',data.length);
        var d_4 = new Date();
        var utc_4 = d_4.toUTCString();
        console.log('before loop, start time :',utc_4)
        for(var i in data)
        {
          var item = data[i];
          //console.log('insert_datetime',item.insert_datetime)
          const timestamp = moment(new Date(item.insert_datetime));
          const value = item.adc1; //item.a6;
          //console.log('timestamp',timestamp)
          const fuel = this.calculateFuelInLitre(value);
          //requests.push([timestamp.toDate().getTime(), value]);
          requests.push([timestamp.toDate().getTime(), fuel]);
          //console.log('requests[i]',requests[i])
        }
        var d_5 = new Date();
        var utc_5 = d_5.toUTCString();
        console.log('after loop, end time :',utc_5)
        console.log('requests',requests[0], requests[data.length - 1])
        var d_6 = new Date();
        var utc_6 = d_6.toUTCString();
        console.log('before TimeSeries create, start time :',utc_6)
        requestsSeries = new TimeSeries({
            name: "requests",
            columns: ["time", "requests"],
            points: requests
        });
        var d_7 = new Date();
        var utc_7 = d_7.toUTCString();
        console.log('after TimeSeries create, end time :',utc_7)
        this.setState({
                      timerange:requestsSeries.range(),
                      active:{requests:true},
                      graphLoading:false
                    })
        this.rescale();
        var d_8 = new Date();
        var utc_8 = d_8.toUTCString();
        console.log('after seState timerange, end time :',utc_8)
      }
      else {
        console.log('data not found or some error occured')
        this.setState({
                      graphLoading:false
                    })
        alert('data not found')
      }
    }

    //TODO Start react-timeseries-charts
    //DDos example
    rescale(timerange, active = this.state.active) {
      if(this.state.active.requests)
      {
        let max = 100;
        let min = 0;
        const maxRequests = requestsSeries.crop(this.state.timerange).max("requests");
        const minRequests = requestsSeries.crop(this.state.timerange).min("requests");
        console.log('maxRequests',maxRequests,', minRequests',minRequests);
        if (maxRequests > max && active.requests) max = maxRequests;
        if (minRequests > min && active.requests) min = minRequests;
        console.log('max',max,', min',min);
        this.setState({ max, min });
      }
    }

    handleTimeRangeChange = timerange => {
        this.setState({ timerange });
        this.handleRescale(timerange);
    };

    handleActiveChange = key => {
        const active = this.state.active;
        active[key] = !active[key];
        this.setState({ active });
        this.handleRescale(this.state.timerange, active);
    };

    handleSelectionChanged = point => {
        this.setState({
            selection: point
        });
    };

    handleMouseNear = point => {
        this.setState({
            highlight: point
        });
    };

    //DDoS attack example
    /*
    renderChart = () => {
        let charts = [];
        let max = 1000;
        //For showing value with date time on mouse near of line or mounse on point/dot(scatter)
        const highlight = this.state.highlight;
        const formatter = format(".2f");
        let text = `Fuel: - ltr, time: -:--`;
        let infoValues = [];
        if (highlight) {
            const fuelText = `${formatter(highlight.event.get(highlight.column))} ltr`;
            text = `
                Fuel: ${fuelText},
                time: ${this.state.highlight.event.timestamp().toLocaleTimeString()}
            `;
            infoValues = [{ label: "Fuel", value: fuelText }];
        }

        return (
          this.state.active.requests &&
            <ChartContainer
                title="Graph"
                style={{
                    background: "#201d1e",
                    borderRadius: 8,
                    borderStyle: "solid",
                    borderWidth: 1,
                    borderColor: "#232122"
                }}
                timeAxisStyle={darkAxis}
                titleStyle={{
                    color: "#EEE",
                    fontWeight: 500
                }}
                padding={20}
                paddingTop={5}
                paddingBottom={0}
                enableDragZoom
                onTimeRangeChanged={this.handleTimeRangeChange}
                timeRange={this.state.timerange}
                maxTime={this.state.active.requests ? requestsSeries.range().end() : 0}
                minTime={this.state.active.requests ? requestsSeries.range().begin() : 0}
            >
                <ChartRow height="300">
                    <YAxis
                        id="axis1"
                        label="Values"
                        showGrid
                        hideAxisLine
                        transition={300}
                        style={darkAxis}
                        labelOffset={-10}
                        min={this.state.min}
                        max={this.state.max}
                        format=",.0f"
                        width="60"
                        type="linear"
                    />
                    <Charts>
                    <LineChart
                        key="requests"
                        axis="axis1"
                        series={requestsSeries}
                        columns={["requests"]}
                        style={style}
                    />
                    <ScatterChart
                          axis="axis1"
                          series={requestsSeries}
                          columns={["requests"]}
                          style={style}
                          info={infoValues}
                          infoTimeFormat="%d-%m-%Y %I:%M %p"
                          infoHeight={28}
                          infoWidth={110}
                          infoStyle={{
                              fill: "black",
                              color: "#DDD"
                            }}
                          selected={this.state.selection}
                          onSelectionChange={p => this.handleSelectionChanged(p)}
                          onMouseNear={p => this.handleMouseNear(p)}
                          highlight={this.state.highlight}
                      />
                    </Charts>
                </ChartRow>
            </ChartContainer>
        );
    };
    */

    //TODO End react-timeseries-charts

   handleCalibration()
   {
     console.log('inside handleCalibration')
     const {sensorId} = this.state;
     if(undefined != sensorId)
     {
       this.toggleCalibrationModal();
     }
     else {
       alert('something went wrong')
     }
   }

   handleReCalibration()
   {
     console.log('inside handleReCalibration')
     const {sensorId, calibrationDateTime} = this.state;
     if(undefined != sensorId)
     {
       //TODO need to show dialog/model for confirm ReCalibrate and showing date time
       alert('previous calibrated time : '+moment(calibrationDateTime).format('DD-MM-YYYY LT'))
       this.toggleCalibrationModal();
     }
     else {
       alert('something went wrong')
     }
   }

   toggleCalibrationModal()
   {
     this.setState({
       isOpenModalCalibration: !this.state.isOpenModalCalibration
     });
   }

  render() {
    console.log('Dashboard -->> inside render')
    const { selectedOption,isClearable } = this.state;

    const detailFormatter = (cell, row, rowIndex, formatExtraData) => { return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.detail.bind(this, row)}>Detail</Button> ); }

    const dateFormatter = (cell, row, rowIndex, formatExtraData) => { return ( <Label> {cell} </Label> ); }

    const selectOptions = {
      0: 'Moving',
      1: 'Engine Idle',
      2: 'Stoppage',
      3: 'Not Reachable',
      4: 'NA'
    };

    const columnsVehiclesTable = [{
      dataField: 'sn',
      text: 'SN',
      sort: true
    },
    {
      dataField: 'group',
      text: 'Group'
    },
    {
      dataField: 'vehicle',
      text: 'Vehicle',
      filter: textFilter({
        placeholder: 'Vehicle',
      })
    },
    {
      dataField: 'category',
      text: 'Category',
      filter: textFilter({
        placeholder: 'Category',
      })
    },
    {
      dataField: 'status',
      text: 'Status',
      filter: textFilter({
        placeholder: 'Status',
      })
    },
    {
      dataField: 'date',
      text: 'Date',
      filter: textFilter({
        placeholder: 'Date',
      })
    },
    {
      dataField: 'speed',
      text: 'Speed',
      filter: textFilter({
        placeholder: 'Speed',
      })
    },
    {
      dataField: 'engine',
      text: 'Engine',
      filter: textFilter({
        placeholder: 'Engine',
      })
    },
    {
      dataField: 'fuel',
      text: 'Fuel',
      filter: textFilter({
        placeholder: 'Fuel',
      })
    },
    {
      dataField: 'power',
      text: 'Power',
      filter: textFilter({
        placeholder: 'Power',
      })
    },
    {
      dataField: 'ignition',
      text: 'Ignition',
      filter: textFilter({
        placeholder: 'Ignition',
      })
    },
    {
      dataField: 'location',
      text: 'Location',
      filter: textFilter({
        placeholder: 'Location',
      })
    },
    {
      dataField: 'detail',
      text: 'Detail',
      csvExport: false,
      formatter: detailFormatter,
      style: {
        fontWeight: 'bold',
        fontSize: '16px',
        cursor:'pointer',
        color:'blue'
      }
    },
    {
      dataField: 'idletime',
      text: 'Idle Time',
      hidden: true,
      csvExport: false,
    },
    {
      dataField: 'mobile',
      text: 'Mobile',
      filter: textFilter({
        placeholder: 'Mobile',
      })
    },
    {
      dataField: 'remark',
      text: 'Remark',
      filter: textFilter({
        placeholder: 'Remark',
      })
    },
    {
      dataField: 'imei',
      text: 'IMEI',
      filter: textFilter({
        placeholder: 'Imei',
      })
    },
    ];

    //DDos example
    const legend = [
            {
                key: "requests",
                label: "Requests",
                disabled: !this.state.active.requests
            }
        ];

    //
    //For showing value with date time on mouse near of line or mounse on point/dot(scatter)
    const highlight = this.state.highlight;
    const formatter = format(".2f");
    let text = `Fuel: - ltr, time: -:--`;
    let infoValues = [];
    if (highlight) {
        const fuelText = `${formatter(highlight.event.get(highlight.column))} ltr`;
        text = `
            Fuel: ${fuelText},
            time: ${this.state.highlight.event.timestamp().toLocaleTimeString()}
        `;
        infoValues = [{ label: "Fuel", value: fuelText }];
    }

  //

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12">
            <Card>
              <CardBody>
                <p><b>Dashboard</b></p>
                {
                !this.state.loading &&
                <Row>
                {
                this.state.isDealerShow &&
                <Col md="3">
                <p>Dealer</p>
                <Select
                  onChange={this.handleChange}
                  defaultValue={this.state.optionsDealer[0]}
                  isClearable={isClearable}
                  options={this.state.optionsDealer}
                />
                </Col>
                }
                {
                this.state.isSubDealerShow &&
                <Col md="3">
                <p>SubDealer</p>
                <Select
                  onChange={this.handleChangeSubDealerOption.bind(this)}
                  value={this.state.selectSubDealerValue}
                  isClearable={isClearable}
                  options={this.state.optionsSubDealer}
                />
                </Col>
                }
                {
                this.state.isCustomerShow &&
                <Col md="3">
                <p>Customer</p>
                <Select
                  onChange={this.handleChangeCustomerOption.bind(this)}
                  value={this.state.selectCustomerValue}
                  isClearable={isClearable}
                  options={this.state.optionsCustomer}
                />
                </Col>
                }
                <Col md="3">
                <p>Group</p>
                <Select
                  onChange={this.handleChangeGroupOption.bind(this)}
                  value={this.state.selectGroupValue}
                  isClearable={isClearable}
                  options={this.state.optionsGroup}
                />
                </Col>
                </Row>
                }
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          { !this.state.loading &&
            <Col xs="8" sm="4" lg="2" style={{marginLeft:'2%'}}>
              <Card className="text-white" style={{background:'cornflowerblue'}}>
                <CardBody className="pb-0">
                  <div className="text-value">{this.state.totalVehicleCount}</div>
                  <div>Total</div>
                </CardBody>
              </Card>
            </Col>
          }
          { !this.state.loading &&
            <Col xs="8" sm="4" lg="2">
              <Card className="text-white" style={{background:'green'}}>
                <CardBody className="pb-0">
                  <div className="text-value">{this.state.movingCount}</div>
                  <div>Moving</div>
                </CardBody>
              </Card>
            </Col>
          }
          { !this.state.loading &&
            <Col xs="8" sm="4" lg="2">
              <Card className="text-white bg-info">
                <CardBody className="pb-0">
                  <div className="text-value">{this.state.engineIdleCount}</div>
                  <div>Engine Idle</div>
                </CardBody>
              </Card>
            </Col>
          }
          { !this.state.loading &&
            <Col xs="8" sm="4" lg="2">
              <Card className="text-white bg-warning">
                <CardBody className="pb-0">
                  <div className="text-value">{this.state.stoppageCount}</div>
                  <div>Stoppage</div>
                </CardBody>
              </Card>
            </Col>
          }
          { !this.state.loading &&
            <Col xs="8" sm="4" lg="2">
              <Card className="text-white bg-danger">
                <CardBody className="pb-0">
                  <div className="text-value">{this.state.notReachableCount}</div>
                  <div>Not Reachable</div>
                </CardBody>
              </Card>
            </Col>
          }
        </Row>
        <Row>
          <Col>
            <Card>
              <CardBody>
              {
              this.state.loading &&
              <div>
              <div style={{marginLeft: '40%'}}>
              <ClipLoader
                sizeUnit={"px"}
                size={50}
                color={'#123abc'}
                loading={this.state.loading}
              />
              </div>
              <div style={{marginLeft: '35%'}}>
              <p>Loading ... Please wait!</p>
              </div>
              </div>
              }
              { !this.state.loading &&
              <ToolkitProvider
                    keyField="sn"
                    data={ this.state.dataVehiclesTable }
                    columns={ columnsVehiclesTable }
                    search
                  >
                    {
                      props => (
                        <div>
                          <Row className="align-items-center">
                          <Col>
                          <Row>
                          <Col md="3">
                          {
                            //<h4>Input something at below input field:</h4>
                          }
                          <SearchBar
                            { ...props.searchProps }
                          />
                          </Col>
                          </Row>
                          </Col>
                          <Col md="2">
                          <MyExportCSV { ...props.csvProps } />

                          </Col>
                          </Row>
                          <hr />
                          <div style={{overflow: 'overlay'}}>
                          <BootstrapTable
                            { ...props.baseProps }
                            filter={ filterFactory() }
                            hover
                          />
                          </div>
                        </div>
                      )
                    }
                  </ToolkitProvider>
                  }
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card>
              <CardBody>
                <ReactModal
                    ariaHideApp={false}
                    isOpen={this.state.isOpenModalDetail}
                    style={customStyles}
                  >
                  <ModalHeader toggle={this.toggleDetail}>
                      {
                        this.state.isSubDealerShow ? 'Vehicle Details & Sensor Setup' : 'Vehicle Details'
                      }
                    </ModalHeader>
                  <ModalBody>
                    <Row>
                      <Col xs="12" className="mb-4">
                        <Nav tabs>
                          <NavItem>
                            <NavLink
                              className={classnames({ active: this.state.activeTab === '1' })}
                              onClick={() => { this.toggle('1'); }}
                            >
                              Vehicle Details
                            </NavLink>
                          </NavItem>
                          {
                          this.state.isSubDealerShow &&
                          <NavItem>
                            <NavLink
                              className={classnames({ active: this.state.activeTab === '2' })}
                              onClick={() => { this.toggle('2'); }}
                            >
                              Sensor Setup
                            </NavLink>
                          </NavItem>
                          }
                          {
                          this.state.isSubDealerShow &&
                          <NavItem>
                            <NavLink
                              className={classnames({ active: this.state.activeTab === '3' })}
                              onClick={() => { this.toggle('3'); }}
                            >
                              Engine(HMR) Setup
                            </NavLink>
                          </NavItem>
                          }
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                          <TabPane tabId="1">
                            <Row>
                              <Col>
                                <Card>
                                  <CardBody>
                                    <Row>
                                      <Col>
                                      {
                                        //<p>Vehicle : {this.state.vehicleRegNo}</p>
                                      }
                                      <Row>
                                      <Col md="6">
                                      <p style={{textAlign:'center'}}><b> Vehicle : </b></p>
                                      <Table hover bordered responsive size="sm">
                                        <tbody>
                                          <tr>
                                            <td>RegNo</td>
                                            <td>{this.state.vehicleRegNo}</td>
                                          </tr>
                                          <tr>
                                            <td>Category</td>
                                            <td>{this.state.vehicleCategory}</td>
                                          </tr>
                                        </tbody>
                                      </Table>
                                      </Col>
                                      <Col md="6">
                                      <p style={{textAlign:'center'}}><b> Driver : </b></p>
                                      <Table hover bordered responsive size="sm">
                                        <tbody>
                                          <tr>
                                            <td>Name</td>
                                            <td>{this.state.driverName}</td>
                                          </tr>
                                          <tr>
                                            <td>Mobile</td>
                                            <td>{this.state.driverMobileNo}</td>
                                          </tr>
                                        </tbody>
                                      </Table>
                                      </Col>
                                      </Row>
                                      <Row>
                                      <Col md="4">
                                      <Chart
                                        chartType="Gauge"
                                        width="100%"
                                        height="125px"
                                        data={this.getSpeedData()}
                                        options={speedChartOptions}
                                      />
                                      <p style={{textAlign:'center'}}> Speed : {this.state.speed}</p>
                                      </Col>
                                      {
                                      this.state.fuelSensorFound &&
                                      <Col md="4">
                                      <Chart
                                        chartType="Gauge"
                                        width="100%"
                                        height="125px"
                                        data={this.getFuelData()}
                                        options={fuelChartOptions}
                                      />
                                      <p style={{textAlign:'center'}}> Fuel : {this.state.fuel}</p>
                                      {
                                        //TODO if user manageCategory is dealer & subDealer
                                        this.state.isSubDealerShow && !this.state.fuelSensorCalibrated &&
                                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleCalibration.bind(this)}>Calibrate Sensor</Button>
                                      }
                                      {
                                        this.state.isSubDealerShow && this.state.fuelSensorCalibrated &&
                                        <Button color="secondary" className="btn-pill" size="sm" onClick={this.handleReCalibration.bind(this)}>ReCalibrate Sensor</Button>
                                      }
                                      </Col>
                                      }
                                      <Col md="4">
                                      <Chart
                                        chartType="Gauge"
                                        width="100%"
                                        height="125px"
                                        data={this.getBatteryData()}
                                        options={batteryChartOptions}
                                      />
                                      <p style={{textAlign:'center'}}> Battery : {this.state.battery}</p>
                                      </Col>
                                      </Row>
                                      </Col>
                                      <Col md="6">
                                        <p>Track Vehicle</p>
                                        {
                                        <MapComponent
                                          center={this.state.center}
                                          isMarkerShown={this.state.isMarkerShown}
                                          latitude={this.state.latitude}
                                          longitude={this.state.longitude}
                                          pathCoordinates={this.state.pathCoordinates}
                                        />
                                        }
                                      </Col>
                                    </Row>
                                    {
                                    this.state.fuelSensorFound &&
                                    <Row>
                                      <Col>
                                        <hr/>
                                        <p><b>Fuel Graph :</b></p>
                                        <Row>
                                        <Col md="3">
                                        <Label>Start Date Time</Label>
                                        <Datetime
                                          defaultValue = {new Date()}
                                          onChange= {this.handleSelectStartDateTime.bind(this)}
                                          dateFormat = "DD-MM-YYYY"
                                         />
                                        </Col>
                                        <Col md="3">
                                        <Label>End Date Time</Label>
                                        <Datetime
                                          defaultValue = {new Date()}
                                          onChange= {this.handleSelectEndDateTime.bind(this)}
                                          dateFormat = "DD-MM-YYYY"
                                         />
                                        </Col>
                                        <Col md="2">
                                        <Button style={{marginTop:'35px'}} color="primary" className="btn-pill" size="sm" onClick={this.showGraph.bind(this)}>View Graph</Button>
                                        </Col>
                                        </Row>
                                      </Col>
                                      </Row>
                                      }
                                      <Col style={{marginTop:'15px'}}>
                                      {
                                        this.state.graphLoading &&
                                        <div>
                                        <div style={{marginLeft: '40%'}}>
                                        <ClipLoader
                                          sizeUnit={"px"}
                                          size={50}
                                          color={'#123abc'}
                                          loading={this.state.graphLoading}
                                        />
                                        </div>
                                        <div style={{marginLeft: '35%'}}>
                                        <p>Loading ... Please wait!</p>
                                        </div>
                                        </div>
                                      }
                                      {
                                      //this.state.active.requests && <Resizable>{this.renderChart()}</Resizable>
                                      }
                                      {
                                        this.state.active.requests && !this.state.graphLoading &&
                                        <Resizable>
                                        <ChartContainer
                                            title="Graph"
                                            timeAxisStyle={darkAxis}
                                            titleStyle={{
                                                color: "#EEE",
                                                fontWeight: 500
                                            }}
                                            padding={20}
                                            paddingTop={5}
                                            paddingBottom={0}
                                            enableDragZoom
                                            onTimeRangeChanged={this.handleTimeRangeChange}
                                            timeRange={this.state.timerange}
                                            maxTime={this.state.active.requests ? requestsSeries.range().end() : 0}
                                            minTime={this.state.active.requests ? requestsSeries.range().begin() : 0}
                                        >
                                            <ChartRow height="300">
                                                <YAxis
                                                    id="axis1"
                                                    label="Values"
                                                    showGrid
                                                    hideAxisLine
                                                    transition={300}
                                                    style={darkAxis}
                                                    labelOffset={-10}
                                                    min={this.state.min}
                                                    max={this.state.max}
                                                    format=",.0f"
                                                    width="60"
                                                    type="linear"
                                                />
                                                <Charts>
                                                <LineChart
                                                    key="requests"
                                                    axis="axis1"
                                                    series={requestsSeries}
                                                    columns={["requests"]}
                                                    style={style}
                                                />
                                                <ScatterChart
                                                      axis="axis1"
                                                      series={requestsSeries}
                                                      columns={["requests"]}
                                                      style={style}
                                                      info={infoValues}
                                                      infoTimeFormat="%d-%m-%Y %I:%M %p"
                                                      infoHeight={28}
                                                      infoWidth={110}
                                                      infoStyle={{
                                                          fill: "black",
                                                          color: "#DDD"
                                                        }}
                                                      selected={this.state.selection}
                                                      onSelectionChange={p => this.handleSelectionChanged(p)}
                                                      onMouseNear={p => this.handleMouseNear(p)}
                                                      highlight={this.state.highlight}
                                                  />
                                                </Charts>
                                            </ChartRow>
                                        </ChartContainer>
                                        </Resizable>
                                      }
                                      </Col>
                                    {
                                      //<VehicleDetails {...this.props} row={this.state.row} />
                                    }
                                  </CardBody>
                                </Card>
                              </Col>
                            </Row>
                          </TabPane>
                          {
                          //TODO if user manageCategory is dealer & subDealer
                          this.state.isSubDealerShow &&
                          <TabPane tabId="2">
                            <Row>
                              <Col>
                                <Card>
                                  <CardBody>
                                  <SensorSetup {...this.props} row={this.state.row} />
                                  </CardBody>
                                </Card>
                              </Col>
                            </Row>
                          </TabPane>
                        }
                        {
                          //TODO if user manageCategory is dealer & subDealer
                          this.state.isSubDealerShow &&
                          <TabPane tabId="3">
                            <Row>
                              <Col>
                                <Card>
                                  <CardBody>
                                  <HmrSetup {...this.props} row={this.state.row} />
                                  </CardBody>
                                </Card>
                              </Col>
                            </Row>
                          </TabPane>
                        }
                        </TabContent>
                      </Col>
                    </Row>
                  </ModalBody>
                </ReactModal>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {
        //TODO calibration model came here
          <ReactModal
            ariaHideApp={false}
            isOpen={this.state.isOpenModalCalibration}
            style={customStyles}>
          <ModalHeader toggle={this.toggleCalibrationModal}>Calibration</ModalHeader>
          <ModalBody>
            <Calibration {...this.props} row={this.state.row} sensorId={this.state.sensorId} toggleCalibrationModal={this.toggleCalibrationModal}/>
          </ModalBody>
        </ReactModal>
        }
      </div>
    );
  }
}

export default Dashboard;
