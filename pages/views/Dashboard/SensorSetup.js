import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Form,
  FormGroup,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Input,
  Label
} from 'reactstrap';

import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import classnames from 'classnames';

import { NextAuth } from 'next-auth/client'

import Select from 'react-select';

import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';
//For table search
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';

const { SearchBar } = Search;

const dataAssignedSensorTable = [];

const selectRow = {
  mode: 'checkbox',
  //clickToSelect: true,
  //Disable single row selection
  clickToSelect: false,
  onSelect: (row, isSelect, rowIndex, e) => {
    console.log(row.sn);
    console.log(isSelect);
    console.log(row);
    console.log(rowIndex);
    console.log(e);
  },
  onSelectAll: (isSelect, rows, e) => {
    console.log(isSelect);
    console.log(rows);
    console.log(e);
  }
};

const optionsDealer = [
  { value: 'all', label: 'All' },
  { value: 'dealer1', label: 'Dealer1' },
  { value: 'dealer2', label: 'Dealer2' },
  { value: 'dealer3', label: 'Dealer3' },
  { value: 'dealer4', label: 'Dealer4' },
];

let unAssignSensorRow;

const optionsSensorType = [];

let sensorSetupRow = {};

const cassandraColName = {
                          'Digital 1':'din1', 'Digital 2':'din2', 'Digital 3':'din3', 'Digital 4':'din4',
                          'Analog 1':'ain1', 'Analog 2':'ain2', 'Analog 3':'ain3', 'Analog 4':'ain4',
                          'External Bat':'power', 'Internal Bat':'battery', 'Speed':'speed'
                          };

class SensorSetup extends Component {

  constructor(props) {
    super(props);
    console.log('SensorSetup -->>, props',this.props)
    const row = this.props.row;
    this.toggleEditSensor = this.toggleEditSensor.bind(this);
    this.handleUpdateSensor = this.handleUpdateSensor.bind(this);
    this.toggleUnAssignSensor = this.toggleUnAssignSensor.bind(this);
    this.handleUnAssignSensor = this.handleUnAssignSensor.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.state = {
                  row: row,
                  dataAssignedSensorTable:dataAssignedSensorTable,
                  isDeviceAssigned:false,
                  isSensorAssigned:false,
                  isOpenModalEditSensor:false,
                  deleteSubDealer:true,
                  isOpenModalUnAssignSensor:false
                 }
  }

  async componentDidMount() {

    //TODO need to check first deviceAssignments against vehicle
    //If deviceAssignments not found then show msg please first assgin device then setup sensors
    const checkDeviceAssigned = await this.checkDeviceAssigned();
    console.log('checkDeviceAssigned',checkDeviceAssigned)
    if(undefined != checkDeviceAssigned && checkDeviceAssigned)
    {
      this.setState({
                      isDeviceAssigned:true
                    })
      this.getRawData();
      this.getAssignedSensor();
      this.getSensorType();
    }
    else {
      console.log('Device not assigned')
      this.setState({
                      isDeviceAssigned:false
                    })
    }

  }

////Start Assigned Sensor Details

  async checkDeviceAssigned()
  {
    console.log('inside checkDeviceAssigned');
    const vehicleId = this.state.row.vehicleId;
    const fetchDeviceAssignment = await fetch('http://159.65.6.196:5500/api/deviceassignment?filter[where][vehicleId]='+vehicleId);
    const res = await fetchDeviceAssignment.json();
    console.log('device assignment res', res)
    if(undefined != res && res.length > 0)
    {
    return true;
    }
    else {
      return false;
    }
  }

  async getRawData()
  {
    const vehicleDeviceData = await this.getCurrentDeviceData();
    console.log('vehicleDeviceData:',vehicleDeviceData);
    const vehicleId = this.state.row.vehicleId;
    const deviceData = vehicleDeviceData.data[vehicleId];
    console.log('deviceData:',deviceData)
    if(undefined != deviceData && deviceData != "data not found" && deviceData != "some error occured")
    {
        const data = deviceData[0];
        const digital1 = data[cassandraColName['Digital 1']];
        const digital2 = data[cassandraColName['Digital 2']];
        const digital3 = data[cassandraColName['Digital 3']];
        const digital4 = data[cassandraColName['Digital 4']];
        const analog1 = data[cassandraColName['Analog 1']];
        const analog2 = data[cassandraColName['Analog 2']];
        const analog3 = data[cassandraColName['Analog 3']];
        const analog4 = data[cassandraColName['Analog 4']];
        const vehicleBattery = Number(data[cassandraColName['External Bat']].toFixed(3));
        const deviceBattery = Number(data[cassandraColName['Internal Bat']].toFixed(3));
        this.setState({
                      digital1:digital1,
                      digital2:digital2,
                      digital3:digital3,
                      digital4:digital4,
                      analog1:analog1,
                      analog2:analog2,
                      analog3:analog3,
                      analog4:analog4,
                      vehicleBattery:vehicleBattery,
                      deviceBattery:deviceBattery
        })
    }

  }

  async getCurrentDeviceData()
  {
    console.log('inside getCurrentDeviceData')
    const vehicleId = this.state.row.vehicleId;
    const customerId = this.state.row.customerId;
    let deviceData;
    const data = {customerId:customerId,vehicleId:vehicleId}
    const dataForCassQry = [data];
    let payload = {
                    data:dataForCassQry
                  }
    const requestOptions = {
      credentials: 'include',
      method: 'POST',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    console.log('payload:',payload);
    const vehicleDeviceData = await fetch('/vehicle/fetch/',requestOptions);
    const res = await vehicleDeviceData.json();
    console.log('vehicleDeviceData res:',res);
    deviceData = res;
    return deviceData;
  }

  async getAssignedSensor()
  {
    console.log('inside getAssignedSensor')
    this.setState({dataAssignedSensorTable:[]})
    const vehicleId = this.state.row.vehicleId;
    const fetchAssignedSensor = await fetch('http://159.65.6.196:5500/api/vehicle/fetchsensorassignmentbyvehicleid/'+vehicleId);
    const res = await fetchAssignedSensor.json();
    const data = res.data;
    console.log('fetchAssignedSensor data', data, data.length)
    if(undefined != data && data.length > 0)
    {
    let dataAssignedSensorTable = [];
    var sn = 1;
    for (var i in data)
    {
      var item = data[i];
      var gpsDevice = item.gpsDevice;
      var devicePort= item.devicePort;
      var sensor = item.sensor;
      var sensorType = sensor.sensorType;
      var sensorModel;
      var sensorMake;
      var min = item.min;
      var max = item.max;
      if(undefined != sensorType && sensorType.name.toLowerCase() == "fuel")
      {
        sensorModel = sensor.sensorModel;
        sensorMake = sensorModel.sensorMake;
      }
      else {
        sensorModel = "NA";
        sensorMake = "NA";
      }
      if(undefined == min)
      {
        min = "NA";
      }
      if(undefined == max)
      {
        max = "NA";
      }
      dataAssignedSensorTable.push({
        "sn" : sn,
        "sensorAssignmentId":item.id,
        "sensorId": item.sensorId,
        "gpsDeviceId":item.gpsDeviceId,
        "devicePortId":item.devicePortId,
        "vehicleId":item.vehicleId,
        "createdOn":item.createdOn,
        "deviceModelId":gpsDevice.deviceModelId,
        "sensorType":sensorType.name,
        "sensorModel":sensorModel.name || 'NA',
        "sensorMake":sensorMake.name || 'NA',
        "devicePort":devicePort.name,
        "min":min,
        "max":max
      });
      sn++;
    }

    this.setState({
                  isSensorAssigned:true,
                  dataAssignedSensorTable:dataAssignedSensorTable,
                  })

    }
    else
    {
      this.setState({
                    isSensorAssigned:false
                    })
    }
  }

  async editAssignedSensor(row)
  {
    console.log('inside editAssignedSensor')
    console.log('row',row)
    const devicePort = await this.getDevicePortByDeviceModelId(row)
    console.log('devicePort',devicePort);
    this.setState({
                  sensorType: row.sensorType,
                  optionsDevicePort: devicePort,
                  sensorAssignmentId:row.sensorAssignmentId,
                  selectDevicePort: { value: row.devicePortId, label: row.devicePort },
                  min:row.min,
                  max:row.max
                  })
    this.toggleEditSensor();
  }

  async getDevicePortByDeviceModelId(row)
  {
    console.log('inside getDevicePortByDeviceModelId')
    const deviceModelId = row.deviceModelId;
    const fetchDevicePort = await fetch('http://159.65.6.196:5500/api/devicePort?filter[where][deviceModelId]='+deviceModelId);
    const res = await fetchDevicePort.json();
    let optionsDevicePort = []
    for(var i in res)
    {
      var item = res[i];
      optionsDevicePort.push({
        "value":item.id,
        "label":item.name
      });
    }
    return optionsDevicePort;
  }

  toggleEditSensor()
  {
    this.setState({
      isOpenModalEditSensor: !this.state.isOpenModalEditSensor,
    });
  }

  handleChangeDevicePortForEdit = (selectedOption) => {
    console.log('inside handleChangeDevicePort, selectedOption', selectedOption)
    this.setState({
                  selectDevicePort : selectedOption
                  })
  }

  async handleUpdateSensor()
  {
    console.log('inside handleUpdateSensor')
    const {sensorAssignmentId, selectDevicePort, min, max} = this.state;
    const devicePortId = selectDevicePort.value;
    //TODO need to add min, max on payload
    console.log('selectDevicePort:',selectDevicePort,', min:',min,', max:',max)
    let payload = {
                    devicePortId:devicePortId,
                    min:Number(min),
                    max:Number(max)
                  }
    console.log('payload: ',payload)
    console.log('payload: ',JSON.stringify(payload))
    const requestOptions = {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    const updateSensor = await fetch('http://159.65.6.196:5500/api/SensorAssignment/update/'+sensorAssignmentId, requestOptions)
    const res = await updateSensor.json()
    console.log('Update Sensor res',res)
    this.getAssignedSensor();
    this.toggleEditSensor();
  }

  confirmUnAssignSensor(row)
  {
    console.log('inside confirmUnAssignSensor')
    unAssignSensorRow = row;
    this.toggleUnAssignSensor();
  }

  toggleUnAssignSensor()
  {
    this.setState({
      isOpenModalUnAssignSensor: !this.state.isOpenModalUnAssignSensor,
    });
  }

  async handleUnAssignSensor()
  {
    console.log('inside handleUnAssignSensor')
    const row = unAssignSensorRow;
    console.log('row',row)
    const sensorId = row.sensorId;
    const vehicleId = row.vehicleId;
    const requestOptions = {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                }
    };
    const unAssginSensor = await fetch('http://159.65.6.196:5500/api/SensorAssignment/unassign/'+sensorId+'/'+vehicleId, requestOptions)
    const res = await unAssginSensor.json()
    console.log('UnAssgin Sensor res',res)
    this.getAssignedSensor();
    this.toggleUnAssignSensor();
  }

  ////End Assigned Sensor Details

  async getSensorType()
  {
    console.log('inside getSensorType')
    const fetchSensorType = await fetch('/sensorType');
    const res = await fetchSensorType.json();
    const data = res;
    let optionsSensorType = []
    for(var i in data)
    {
      var item = data[i]
      optionsSensorType.push({
        "value":item.id,
        "label":item.name,
        "sn":i
      });

    }
    this.setState({
                  optionsSensorType:optionsSensorType
                  })
  }

  handleChangeSensorType = (index, selectedOption) => {
    console.log('inside handleChangeSensorType, selectedOption', selectedOption, index)
    const sensorType = selectedOption;
    const sensorTypeId = sensorType.value;
    const sensorTypeName = sensorType.label;
    console.log('sensorTypeName',sensorTypeName)
    console.log('sensorTypeId',sensorTypeId)
    ////
    //TODO need to check same sensor type not already assigned from dataAssignedSensorTable array
    if(sensorTypeName.toLowerCase() == "fuel")
    {
      const isFuelSensorAssigned = this.checkFuelSensorAssignedOrNot();
      if(undefined != isFuelSensorAssigned && isFuelSensorAssigned)
      {
        alert(sensorTypeName.toLowerCase()+' already assigned')
        return false;
      }
    }
    //TODO need to check same sensor type not selected
    console.log('check same sensor type not added')
    const addedRows = sensorSetupRow;
    if(undefined != addedRows && Object.keys(addedRows).length > 0)
    {
      for(var i in addedRows)
      {
        var item = addedRows[i]
        console.log('addedRows item',item[0])
        var addedSensorType = item[0].sensorType;
        if(undefined != addedSensorType && addedSensorType == "fuel" && addedSensorType == sensorTypeName.toLowerCase())
        {
          alert(addedSensorType+' already added')
          return false;
        }

      }

    }
    console.log('same sensor not added')
    ////
    const optionsSensorMake = [];
    const optionsSensorModel = [];
    const selectDevicePort = [];
    this.setState({['selectSensorType'+index] : selectedOption})
    if(sensorTypeName.toLowerCase() == "fuel")
    {
      //TODO now fetching sensor Make
      this.getSensorMake(sensorTypeId, index);
      this.setState({
                    ['checkbox_row'+index]:false
                    })
    }
    else {
      this.setState({
                    ['optionsSensorMake'+index]:optionsSensorMake,
                    ['selectSensorMake'+index]:'',
                    ['optionsSensorModel'+index]:optionsSensorModel,
                    ['selectSensorModel'+index]:'',
                    ['selectDevicePort'+index]:'',
                    ['checkbox_row'+index]:false
                    })
    }
    //TODO getting devicePort
    this.getDevicePort(index);

    //TODO clear sensorSetupRow;
    this.clearSensorSetupRow(index);
  }

  async getSensorMake(sensorTypeId, index)
  {
    console.log('inside getSensorMake')
    const fetchSensorMake = await fetch('/sensorMake');
    const res = await fetchSensorMake.json();
    const data = res;
    let optionsSensorMake = []
    for(var i in data)
    {
      var item = data[i]
      optionsSensorMake.push({
        "value":item.id,
        "label":item.name,
        "sn":i
      });

    }
    this.setState({
                  ['optionsSensorMake'+index]:optionsSensorMake
                  })
  }

  handleChangeSensorMake = (index, selectedOption) => {
    console.log('inside handleChangeSensorMake, selectedOption', selectedOption, index)
    const sensorMake = selectedOption.label;
    const sensorMakeId = selectedOption.value;
    const selectSensorModel = [];
    this.setState({
                  ['selectSensorMake'+index] : selectedOption,
                  ['selectSensorModel'+index] : '',
                  ['checkbox_row'+index]:false
                })
    //TODO getting sensorModel of sensorMake
    this.getSensorModel(sensorMakeId, index);
    //TODO clear sensorSetupRow;
    this.clearSensorSetupRow(index);
  }

  async getSensorModel(sensorMakeId, index)
  {
    console.log('inside getSensorModel')
    const fetchSensorModel = await fetch('/sensorModel/'+sensorMakeId);
    const res = await fetchSensorModel.json();
    const data = res;
    let optionsSensorModel = []
    for(var i in data)
    {
      var item = data[i]
      optionsSensorModel.push({
        "value":item.id,
        "label":item.name,
        "sn":i
      });

    }
    this.setState({
                  ['optionsSensorModel'+index]:optionsSensorModel
                  })
  }

  handleChangeSensorModel = (index, selectedOption) => {
    console.log('inside handleChangeSensorModel, selectedOption', selectedOption, index)
    const sensorModel = selectedOption.label;
    const sensorModelId = selectedOption.value;
    this.setState({
                  ['selectSensorModel'+index] : selectedOption,
                  ['checkbox_row'+index]:false
                  })
    //TODO clear sensorSetupRow;
    this.clearSensorSetupRow(index);
  }

  handleChangeDevicePort = (index, selectedOption) => {
    console.log('inside handleChangeDevicePort, selectedOption', selectedOption, index)
    const devicePort = selectedOption.label;
    const devicePortId = selectedOption.value;
    this.setState({
                  ['selectDevicePort'+index] : selectedOption,
                  ['checkbox_row'+index]:false
                  })
    //TODO clear sensorSetupRow;
    this.clearSensorSetupRow(index);
  }

  async getDevicePort(index)
  {
    console.log('inside getDevicePort')
    const vehicleId = this.state.row.vehicleId;
    console.log('vehicleId',vehicleId)
    const fetchDevicePort = await fetch('/device/fetchdeviceport/'+vehicleId);
    const res = await fetchDevicePort.json();
    console.log('res devicePort',res)
    const data = res.data[0];
    const gpsDevice = data.gpsDevice;
    const deviceModel = gpsDevice.deviceModel;
    const devicePorts = deviceModel.devicePorts;
    let optionsDevicePort = []
    for(var i in devicePorts)
    {
      var item = devicePorts[i]
      optionsDevicePort.push({
        "value":item.id,
        "label":item.name,
        "gpsDeviceId":gpsDevice.id,
        "sn":i,
      });

    }
    this.setState({
                  ['optionsDevicePort'+index]:optionsDevicePort
                  })
  }

  handleChange(e) {
      const { name, value } = e.target;
      this.setState({ [name]: value });
  }

  toggleCheckbox = (index, event) => {
    console.log('inside toggleCheckbox, index', index)
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name
    console.log('target, name', name,' value', value)
    //TODO validate checked row then change checkbox value
    //TODO if true then add row in array after validate
    let row = [];
    let rowValidateSuccess = false;
    if(value && this.validateRow(index))
    {
    const sensorType = this.state['selectSensorType'+index];
    console.log('sensor type', sensorType)
    const sensorTypeName = sensorType.label;
    const sensorTypeId = sensorType.value;
    if (sensorTypeName.toLowerCase() == "fuel")
    {
      //checking sensor make value
      const sensorMake = this.state['selectSensorMake'+index];
      const sensorMakeName = sensorMake.label;
      const sensorMakeId = sensorMake.value;

      //checking sensor model value
      const sensorModel = this.state['selectSensorModel'+index];
      const sensorModelName = sensorModel.label;
      const sensorModelId = sensorModel.value;

      //checking device port value
      const devicePort = this.state['selectDevicePort'+index];
      const devicePortName = devicePort.label;
      const devicePortId = devicePort.value;
      const gpsDeviceId = devicePort.gpsDeviceId;
      const min = this.state['min'+index];
      const max = this.state['max'+index];

      row.push({
        "sensorTypeId":sensorTypeId,
        "sensorType":sensorTypeName.toLowerCase(),
        "sensorModelId":sensorModelId,
        "customerId":this.state.row.customerId,
        "gpsDeviceId":gpsDeviceId,
        "devicePortId":devicePortId,
        "min":Number(min),
        "max":Number(max)
      });

      sensorSetupRow['row'+index] = row;

      console.log('row',row)
      rowValidateSuccess = true;
    }
    else {
      const devicePort = this.state['selectDevicePort'+index];
      const devicePortName = devicePort.label;
      const devicePortId = devicePort.value;
      const gpsDeviceId = devicePort.gpsDeviceId;
      const min = this.state['min'+index];
      const max = this.state['max'+index];

      row.push({
        "sensorTypeId":sensorTypeId,
        "sensorType":sensorTypeName.toLowerCase(),
        "sensorModelId":null,
        "customerId":this.state.row.customerId,
        "gpsDeviceId":gpsDeviceId,
        "devicePortId":devicePortId,
        "min":Number(min),
        "max":Number(max)
      });

      sensorSetupRow['row'+index] = row;

      console.log('row',row)
      rowValidateSuccess = true;
    }
    }
    else {
      //TODO remove row from array
      delete sensorSetupRow['row'+index];
    }
    console.log('sensorSetupRow', sensorSetupRow)
    this.setState({
      [name]: rowValidateSuccess
    });
  }

  validateRow(index)
  {
    console.log('inside validateRow, index',index)
    const sensorType = this.state['selectSensorType'+index];
    console.log('sensorType',sensorType)
    if(undefined == sensorType)
    {
      alert('please select Sensor Type');
      return false;
    }
    //TODO need to check same sensor type not selected
    console.log('check same sensor type not added')
    const addedRows = sensorSetupRow;
    if(undefined != addedRows && Object.keys(addedRows).length > 0)
    {
      for(var i in addedRows)
      {
        var item = addedRows[i]
        console.log('addedRows item',item[0])
        var addedSensorType = item[0].sensorType;
        //if(undefined != addedSensorType && addedSensorType == sensorType.label.toLowerCase())
        if(undefined != addedSensorType && addedSensorType == "fuel" && addedSensorType == sensorType.label.toLowerCase())
        {
          alert(addedSensorType+' already added')
          return false;
        }

      }

    }
    console.log('same sensor not added')
    //Checking other conditions
    if (sensorType.label.toLowerCase() == "fuel")
    {
      const sensorMake = this.state['selectSensorMake'+index];
      console.log('sensorMake',sensorMake)
      if(undefined == sensorMake || sensorMake == "")
      {
        alert('please select Sensor Make');
        return false;
      }
      const sensorModel = this.state['selectSensorModel'+index];
      console.log('sensorModel',sensorModel)
      if(undefined == sensorModel || sensorModel == "")
      {
        alert('please select Sensor Model');
        return false;
      }

    }
    const devicePort = this.state['selectDevicePort'+index];
    console.log('devicePort',devicePort)
    if(undefined == devicePort || devicePort == "")
    {
      alert('please select Device Port');
      return false;
    }

    const min = this.state['min'+index];
    console.log('min',min)
    if(undefined != min && min.trim() != "")
    {
      if(isNaN(Number(min)))
      {
      alert('please insert min value');
      return false;
      }
    }
    else {
      alert('please insert min value');
      return false;
    }

    const max = this.state['max'+index];
    console.log('max',max)
    if(undefined != max && max.trim() != "")
    {
      if(isNaN(Number(max)))
      {
      alert('please insert max value');
      return false;
      }
    }
    else {
      alert('please insert max value');
      return false;
    }

    return true;
  }

  checkFuelSensorAssignedOrNot()
  {
    console.log('inside checkFuelSensorAssignedOrNot')
    const {dataAssignedSensorTable} = this.state;
    let isFuelSensorAssigned = false;
    for(var i in dataAssignedSensorTable)
    {
      var item = dataAssignedSensorTable[i];
      var sensorType = item.sensorType;
      if(undefined != sensorType && sensorType.toLowerCase() == "fuel")
      {
        isFuelSensorAssigned = true;
        break;
      }
    }
    return isFuelSensorAssigned;
  }

  async handleSubmit()
  {
    console.log('inside handleSubmit')
    const allRows = sensorSetupRow;
    console.log('allRows',allRows, 'allRows length',Object.keys(allRows).length)
    if(undefined != allRows && Object.keys(allRows).length > 0)
    {
      let rowsArray = [];
      for(var i in allRows)
      {
        var item = allRows[i]
        console.log('item',item[0])
        rowsArray.push(item[0]);
      }
      console.log('rowsArray',rowsArray)
      //TODO for now in payload vehicle is vehicleId
      let payload = {
                    sensors:rowsArray,
                    vehicle:this.state.row.vehicleId
                    }
      console.log('payload',payload);
      console.log('payload json',JSON.stringify(payload));
      const requestOptions = {
        credentials: 'include',
        method: 'POST',
        headers: { 'Content-Type': 'application/json',
                    'X-CSRF-Token': await NextAuth.csrfToken()
                  },
        body : JSON.stringify(payload)
      };
      const sensorSetup = await fetch('/sensorsetup',requestOptions);
      const resSensorSetup = await sensorSetup.json();
      console.log('resSensorSetup',resSensorSetup);
      alert('Sensor Assigned Success');
      this.getAssignedSensor();
      this.uncheckAddedSensor();
      //sensorSetupRow = {};
      //TODO after successfull submit clear sensorSetupRow array
    }
    else {
      alert('please add alteast one sensor')
    }
  }

  uncheckAddedSensor()
  {
    console.log('inside uncheck added sensor')
    let index = 1;
    for(var i in sensorSetupRow)
    {
      console.log('sensorSetupRow item',sensorSetupRow[i])
      this.setState({['checkbox_row'+index]: false})
      index++;
    }
    sensorSetupRow = {};
  }

  clearSensorSetupRow(index)
  {
    console.log('inside clearSensorSetupRow, index',index)
    delete sensorSetupRow['row'+index];
    console.log('sensorSetupRow', sensorSetupRow)
  }

  componentWillUnmount() {
   // use intervalId from the state to clear the interval
   console.log('SensorSetup -->> inside componentWillUnmount')
   sensorSetupRow = {};
  }

  render() {
    console.log('SensorSetup inside render')

    const editFormatter = (cell, row, rowIndex, formatExtraData) => { return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editAssignedSensor.bind(this, row)}>Edit</Button> ); }

    const unAssignFormatter = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.confirmUnAssignSensor.bind(this, row)}>UnAssign</Button> ); }

    const columnsAssignedSensorTable = [{
      dataField: 'sn',
      text: 'SN'
    }, {
      dataField: 'sensorType',
      text: 'Sensor Type'
    }, {
      dataField: 'sensorMake',
      text: 'Sensor Make'
    }, {
      dataField: 'sensorModel',
      text: 'Sensor Model'
    }, {
      dataField: 'devicePort',
      text: 'Device Port'
    }, {
      dataField: 'min',
      text: 'Min'
    }, {
      dataField: 'max',
      text: 'Max'
    }, {
      dataField: 'edit',
      text: 'Edit',
      formatter: editFormatter,
      style: {
        fontWeight: 'bold',
        fontSize: '16px',
        cursor:'pointer',
        color:'blue'
      }
    },{
      dataField: 'unassign',
      text: 'UnAssign',
      formatter: unAssignFormatter,
      style: {
        fontWeight: 'bold',
        fontSize: '16px',
        cursor:'pointer',
        color:'blue'
      },
    }];

    return (
        <div>
        <Row>
          <Col>
            <p><b>Raw Data:</b></p>
          </Col>
          <Col md="2">
            <Button block color="primary" className="btn-pill" size="sm" onClick={this.getRawData.bind(this)}>Refresh</Button>
          </Col>
         </Row>
         <Row>
            <Col> Digital 1 : {this.state.digital1} </Col>
            <Col>Digital 2 : {this.state.digital2} </Col>
            <Col>Digital 3 : {this.state.digital3} </Col>
            <Col>Digital 4 : {this.state.digital4} </Col>
         </Row>
         <Row>
            <Col>Analgo 1 : {this.state.analog1}</Col>
            <Col>Analgo 2 : {this.state.analog2}</Col>
            <Col>Analgo 3 : {this.state.analog3}</Col>
            <Col>Analgo 4 : {this.state.analog4}</Col>
         </Row>
         <Row>
          <Col>
            <Row>
              <Col>Vehicle Battery : {this.state.vehicleBattery}</Col>
              <Col>Device Battery : {this.state.deviceBattery}</Col>
            </Row>
          </Col>
         </Row>
         <p></p>
         <Row>
          {
           this.state.isSensorAssigned &&
            <Col>
              <p><b>Assigned Sensor:</b></p>
              <BootstrapTable
                keyField='sn'
                hover bordered
                data={ this.state.dataAssignedSensorTable }
                columns={ columnsAssignedSensorTable }
              />
            </Col>
          }
          <Modal isOpen={this.state.isOpenModalEditSensor} toggle={this.toggleEditSensor}
                 className={'modal-primary ' + this.props.className}>
            <ModalHeader toggle={this.toggleEditSensor}>Edit Sensor</ModalHeader>
            <ModalBody>
            <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                <FormGroup row>
                  <Col md="3">
                    <Label htmlFor="text-input">Sensor Type</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <Label htmlFor="text-input">{this.state.sensorType}</Label>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="3">
                    <Label htmlFor="text-input">Min</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <Input type="text" id="text-input" name={"min"} value={this.state.min} onChange={this.handleChange}/>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="3">
                    <Label htmlFor="text-input">Max</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <Input type="text" id="text-input" name={"max"} value={this.state.max} onChange={this.handleChange}/>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="3">
                    <Label htmlFor="text-input">Device Port</Label>
                  </Col>
                  <Col xs="12" md="9">
                  <Select
                    onChange={this.handleChangeDevicePortForEdit.bind(this)}
                    value={this.state.selectDevicePort}
                    isClearable={this.state.isClearable}
                    options={this.state.optionsDevicePort}
                  />
                  </Col>
                </FormGroup>
              </Form>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.handleUpdateSensor}>Update</Button>{' '}
              <Button color="secondary" onClick={this.toggleEditSensor}>Cancel</Button>
            </ModalFooter>
          </Modal>
          <Modal isOpen={this.state.isOpenModalUnAssignSensor} toggle={this.toggleUnAssignSensor}
                 className={'modal-danger ' + this.props.className}>
            <ModalHeader toggle={this.toggleUnAssignSensor}>UnAssign Sensor</ModalHeader>
            <ModalBody>
              Are you sure to UnAssign Sensor
            </ModalBody>
            <ModalFooter>
              <Button color="danger" onClick={this.handleUnAssignSensor}>Delete</Button>{' '}
              <Button color="secondary" onClick={this.toggleUnAssignSensor}>Cancel</Button>
            </ModalFooter>
          </Modal>
         </Row>

         <Row>
         <Col>
          {
            this.state.isDeviceAssigned &&
            <p><b>Please Select Below Sensors To Assign</b></p>
          }
          {
            !this.state.isDeviceAssigned &&
            <p><b>Please Assign Vehicle On Device</b></p>
          }
         </Col>
         </Row>

         {
           this.state.isDeviceAssigned &&
         <Row>
             <Col md="1">
             <p>SN</p>
             </Col>
             <Col md="2">
             <p>Sensor Type</p>
             </Col>
             <Col md="2">
             <p>Sensor Make</p>
             </Col>
             <Col md="2">
             <p>Sensor Model</p>
             </Col>
             <Col md="2">
             <p>Device Port</p>
             </Col>
             <Col md="1">
             <p>Min</p>
             </Col>
             <Col md="1">
             <p>Max</p>
             </Col>
             <Col md="1">
             <p>Check To Add</p>
             </Col>
         </Row>
         }

         {
          this.state.optionsSensorType && this.state.optionsSensorType.map((item, index) =>
          <div key={item.value}>
           <Row>
           <Col md="1">
           <label> {++index} </label>
           </Col>
           <Col md="2">
           <Select
            id={index}
            onChange={this.handleChangeSensorType.bind(this, index)}
            value={this.state['selectSensorType'+index]}
            options={this.state.optionsSensorType}
            backspaceRemovesValue={false}
           />
           </Col>
           <Col md="2">
           <Select
            onChange={this.handleChangeSensorMake.bind(this, index)}
            value={this.state['selectSensorMake'+index]}
            options={this.state['optionsSensorMake'+index]}
            backspaceRemovesValue={false}
           />
           </Col>
           <Col md="2">
           <Select
            onChange={this.handleChangeSensorModel.bind(this, index)}
            value={this.state['selectSensorModel'+index]}
            options={this.state['optionsSensorModel'+index]}
            backspaceRemovesValue={false}
           />
           </Col>
           <Col md="2">
           <Select
           onChange={this.handleChangeDevicePort.bind(this, index)}
           value={this.state['selectDevicePort'+index]}
           options={this.state['optionsDevicePort'+index]}
             backspaceRemovesValue={false}
           />
           </Col>
           <Col md="1">
           <Input type="text" name={'min'+index} value={this.state['min'+index]} onChange={this.handleChange}/>
           </Col>
           <Col md="1">
           <Input type="text" name={'max'+index} value={this.state['max'+index]} onChange={this.handleChange}/>
           </Col>
           <Col md="1">
           <FormGroup check>
              <Input type="checkbox" name={"checkbox_row"+index} checked={this.state['checkbox_row'+index]} onChange={this.toggleCheckbox.bind(this, index)} />
           </FormGroup>
           </Col>
           </Row>
           <hr/>
           </div>
         )}

         {
           this.state.isDeviceAssigned &&
           <Row>
              <Col>
              </Col>
              <Col md="2">
              <Button block color="primary" className="btn-pill" size="sm" onClick={this.handleSubmit.bind(this)}>Assign</Button>
              </Col>
           </Row>
         }

         </div>
    )

  }

}

export default SensorSetup;
