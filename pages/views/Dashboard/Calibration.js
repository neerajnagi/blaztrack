import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Form,
  FormGroup,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Input,
  Label
} from 'reactstrap';

import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import classnames from 'classnames';

import { NextAuth } from 'next-auth/client'

import Select from 'react-select';

import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';
//For table search
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';

const { SearchBar } = Search;

let xyCoordinatesProcess3 = [];

class Calibration extends Component {

  constructor(props) {
    super(props);
    console.log('Calibration -->>, props',this.props)
    const row = this.props.row;
    this.handleSelectProcess = this.handleSelectProcess.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
                  row: row,
                  isProcessSelected: false,
                  isProcess1Selected : false,
                  isProcess2Selected : false,
                  isProcess3Selected : false,
                  isProcess1Step1Completed : false,
                  calibrationProcessMaxV:4000,
                  calibrationProcessMaxVProcess2:5000,
                  process1Step1Voltage:'',
                  process1Step2Voltage:'',
                  process1Step2LitreRow1:'',
                  process1Step2LitreRow2:'',
                  process1Step2LitreRow3:'X',
                  editProcess1Step2LitreRow2:'',
                  process2Voltage:'',
                  process2LitreRow1:'',
                  process2LitreRow2:'',
                  process2VoltageRow3:400,
                  process3VoltageRow20:0,
                  process3LitreRow20:0
                 }
  }

  componentDidMount() {
    console.log('Calibration -->> inside componentDidMount')
  }
  componentWillUnmount() {
   console.log('Calibration -->> inside componentWillUnmount')
  }

  handleSelectProcess(e)
  {
    console.log('inside handleSelectProcess')
    const { name, value } = e.target;
    console.log('name, value',name, value)
    this.setState({ selectedProcess: value });
  }

  async handleNextProcess()
  {
    console.log('inside handleNextProcess')
    const {selectedProcess} = this.state;
    console.log('selectedProcess',selectedProcess)
    if(undefined != selectedProcess)
    {
      if(selectedProcess == "Process1")
      {
        console.log('inside Process1 selected')
        const voltage = await this.getVoltage();
        console.log('voltage',voltage)
        this.setState({
                      isProcessSelected: true,
                      isProcess1Selected : true,
                      isProcess2Selected : false,
                      isProcess3Selected : false,
                      process1Step1Voltage:voltage
                    })
      }
      else if(selectedProcess == "Process2")
      {
        console.log('inside Process2 selected')
        this.setState({
                      isProcessSelected: true,
                      isProcess1Selected : false,
                      isProcess2Selected : true,
                      isProcess3Selected : false
                    })
      }
      else if(selectedProcess == "Process3")
      {
        console.log('inside Process3 selected')
        xyCoordinatesProcess3 = [];
        xyCoordinatesProcess3.push([0,0]);
        this.setState({
                      isProcessSelected: true,
                      isProcess1Selected : false,
                      isProcess2Selected : false,
                      isProcess3Selected : true,
                    })
      }
      else {
        console.log('something went wrong')
        this.setState({
                      isProcessSelected: false,
                      isProcess1Selected : false,
                      isProcess2Selected : false,
                      isProcess3Selected : false,
                    })
      }
    }
  }

  async getVoltage()
  {
    console.log('inside getVoltage')
    const {customerId, vehicleId} = this.state.row
    //const custId = '5b6053fe17cefc25afb7b954';
    //const vehId = '5b6057cfa2ea1b2935bb1881'
    const custId = customerId;
    const vehId = vehicleId;
    //TODO need to remove and use came from state
    const fetchVoltage = await fetch('/device/fetchvoltage/'+custId+'/'+vehId);
    const res = await fetchVoltage.json();
    console.log('res fetchVoltage',res);
    const data = res.data;
    const adc1 = data[0].adc1;
    console.log('adc1(voltage) value',adc1)
    return adc1;
  }

  handleChange(e) {
      const { name, value } = e.target;
      this.setState({ [name]: value });
  }

  handleBackProcess1()
  {
    console.log('inside handleBackProcess1')
    this.setState({
                  isProcessSelected: false,
                  isProcess1Selected : false,
                  isProcess2Selected : false,
                  isProcess3Selected : false,
                  selectedProcess:null
                })
  }

  //for changing Voltage value of row at step 1
  async handleProcess1SyncOnStep1()
  {
    console.log('inside handleProcess1SyncOnStep1')
    const voltage = await this.getVoltage();
    this.setState({
                    process1Step1Voltage:voltage
                })
  }

  handleNextProcess1()
  {
    console.log('inside handleNextProcess1')
    this.setState({
                  isProcess1Step1Completed : true
                  })
  }

  async handleProcess1SyncOnStep2()
  {
    console.log('inside handleProcess1SyncOnStep2')
    const {editProcess1Step2LitreRow2} = this.state
    console.log('entered litre is',editProcess1Step2LitreRow2)
    const voltage = await this.getVoltage();
    console.log('voltage', voltage)
    const currentVoltage = voltage;
    const forProcess = "Process1"
    this.calculateFuelTankCapacity(forProcess, currentVoltage);
  }

  async calculateFuelTankCapacity(forProcess, currentVoltage)
  {

    console.log('inside calculateFuelTankCapacity for ',forProcess)
    const {calibrationProcessMaxV, process1Step1Voltage, editProcess1Step2LitreRow2 } = this.state;
    let diffVoltage = currentVoltage - process1Step1Voltage;
    if( undefined != diffVoltage && diffVoltage > 0)
    {
      let oneLtrVoltage = diffVoltage / editProcess1Step2LitreRow2;
      let row3Ltr = process1Step1Voltage / oneLtrVoltage;
      let row2Ltr = currentVoltage / oneLtrVoltage;
      let row1Ltr = calibrationProcessMaxV / oneLtrVoltage;

      this.setState({
                      process1Step2LitreRow3: row3Ltr,
                      process1Step2LitreRow2: row2Ltr,
                      process1Step2LitreRow1: row1Ltr,
                      process1Step2Voltage: currentVoltage
                    })
    }
    else {
      //TODO show msg
      alert('something went wrong')
    }
  }

  async handleFinishProcess1()
  {
    console.log('inside handleFinishProcess1')
    var xyCoordinatesProcess1 = [
        [0,0],
        [this.state.process1Step1Voltage,this.state.process1Step2LitreRow3],
        [this.state.process1Step2Voltage,this.state.process1Step2LitreRow2],
        [this.state.calibrationProcessMaxV,this.state.process1Step2LitreRow1]
      ]
    console.log('xyCoordinates',xyCoordinatesProcess1)
    const payload = {
                      sensorId:this.props.sensorId,
                      processName:'Process1',
                      xyCoordinates:xyCoordinatesProcess1
                    }
    console.log('payload',payload)
    console.log('payload json', JSON.stringify(payload))
    const requestOptions = {
      credentials: 'include',
      method: 'POST',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    const calibrationTank = await fetch('sensor/calibratelineartank',requestOptions);
    const res = await calibrationTank.json();
    console.log('res calibrationTank',res);
    //TODO need to exit & refreash whole dashboard page for showing latest data
    //Because in dashboard vehicle table have old data(non-calibrated sensor)
    //So for showing litre through calibrated equation we need to get latest data on table records
    //Need to disucss for this
    this.handleExitProcess1();
  }
  handleExitProcess1()
  {
    console.log('inside handleExitProcess1')
    this.setState({
                  isProcessSelected: false,
                  isProcess1Selected : false,
                  isProcess2Selected : false,
                  isProcess3Selected : false,
                  selectedProcess:null,
                  isProcess1Step1Completed : false,
                  process1Step1Voltage:'',
                  process1Step2Voltage:'',
                  process1Step2LitreRow1:'',
                  process1Step2LitreRow2:'',
                  editProcess1Step2LitreRow2:'',
                  process1Step2LitreRow3:'X'
                })

    //TODO need to close this modal using toggleCalibrationModal function
  }

  async handleOkProcess2()
  {
    console.log('handleOkProcess2');
    const {process2VoltageRow1, process2LitreRow1, process2VoltageRow3, process2LitreRow3} = this.state;
    const voltage = await this.getVoltage();
    console.log('voltage',voltage)
    const calculateCapacity = process2LitreRow1 / (process2VoltageRow1 - process2VoltageRow3) * voltage;
    console.log('calculateCapacity', calculateCapacity);
    this.setState({
                  process2Voltage:voltage,
                  process2LitreRow2:Number(calculateCapacity.toFixed(2))
                });
  }

  async handleFinishProcess2()
  {
    console.log('inside handleFinishProcess2')
    const {process2VoltageRow3, process2LitreRow3, process2Voltage, process2LitreRow2, process2VoltageRow1, process2LitreRow1} = this.state;
    var xyCoordinatesProcess2 = [
        [process2VoltageRow3,process2LitreRow3],
        [process2Voltage,process2LitreRow2],
        [process2VoltageRow1,Number(process2LitreRow1)]
      ]
    console.log('xyCoordinates',xyCoordinatesProcess2)
    const payload = {
                      sensorId:this.props.sensorId,
                      processName:'Process2',
                      xyCoordinates:xyCoordinatesProcess2
                    }
    console.log('payload',payload)
    console.log('payload json', JSON.stringify(payload))
    const requestOptions = {
      credentials: 'include',
      method: 'POST',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    const calibrationTank = await fetch('/sensor/calibratelineartank',requestOptions);
    const res = await calibrationTank.json();
    console.log('res calibrationTank',res);
    this.handleExitProcess2();
  }

  handleExitProcess2()
  {
    console.log('inside handleExitProcess2')
    this.setState({
                  isProcessSelected: false,
                  isProcess1Selected : false,
                  isProcess2Selected : false,
                  isProcess3Selected : false,
                  selectedProcess:null,
                  process2Voltage:'',
                  process2LitreRow2:'',
                  process2LitreRow1:''
                })

    //TODO need to close this modal using toggleCalibrationModal function
  }

  renderRowsForProcess3()
  {

    const {} = this.state;
    let rows = [];
    for(var i=9; i <=19; i++ )
    {
      rows.push(
        <tr>
          <td>
          {this.state['process3VoltageRow'+i]}
          </td>
          <td>
          { !this.state['isProcess3Row'+i+'LitreAdded'] &&
          <div>
            <Row style={{margin:'initial'}}>
              <Input type="text" style={{width:'40%', marginLeft:'30%', marginRight:'10%'}} id="text-input" name={'process3LitreRow'+i} value={this.state['process3LitreRow'+i]} onChange={this.handleChange}/>
            </Row>
          </div>
          }
          {
            this.state['isProcess3Row'+i+'LitreAdded'] &&
            this.state['process3LitreRow'+i]
          }
          </td>
          <td>
          {
          !this.state['isProcess3Row'+i+'LitreAdded'] &&
          <Col md="2">
            <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, 'Row'+i)}>Sync</Button>
          </Col>
          }
          </td>
          <td>
          {
          !this.state['isProcess3Row'+i+'LitreAdded'] &&
          <Col md="2">
            <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, 'Row'+i)}>OK</Button>
          </Col>
          }
          </td>
        </tr>
      );
    }

    return (
        {rows}
    )

  }

  async handleSyncProcess3(row)
  {
    console.log('inside handleSyncProcess3, row',row)
    const voltage = await this.getVoltage();
    console.log('voltage',voltage)
    this.setState({['process3VoltageRow'+row]:voltage})
  }

  async handleOkProcess3(row)
  {
    console.log('inside handleOkProcess3, row',row)
    const addedLitre = Number(this.state['process3LitreRow'+row]);
    if(undefined != addedLitre && addedLitre != "" && addedLitre > 0)
    {
      console.log('addedLitreRow',addedLitre)
      const lastAddedLitre = xyCoordinatesProcess3[xyCoordinatesProcess3.length - 1][1]
      console.log('last added litre', lastAddedLitre)
      if(row != "1")
      {
        if(undefined != this.state['process3VoltageRow'+row] && this.state['process3VoltageRow'+row] != "")
        {
          xyCoordinatesProcess3.push([this.state['process3VoltageRow'+row], Number(this.state['process3LitreRow'+row]) + lastAddedLitre])
          this.setState({
                        ['process3LitreRow'+row]: Number(this.state['process3LitreRow'+row]) + lastAddedLitre,
                        lastAddedLitre: Number(this.state['process3LitreRow'+row]) + lastAddedLitre
                       })
        }
        else {
          alert('please first sync voltage')
          return false;
        }

      }
      else {
        xyCoordinatesProcess3.push([this.state.calibrationProcessMaxV, Number(this.state['process3LitreRow'+row]) + lastAddedLitre])
        this.setState({
                      ['process3LitreRow'+row]: Number(this.state['process3LitreRow'+row]) + lastAddedLitre,
                      lastAddedLitre: Number(this.state['process3LitreRow'+row]) + lastAddedLitre
                     })
      }
      console.log('xyCoordinatesProcess3',xyCoordinatesProcess3)
      this.setState({['isProcess3Row'+row+'LitreAdded'] : true})
    }
    else {
      alert('please add valid litre')
    }
    console.log('xyCoordinatesProcess3 length,',xyCoordinatesProcess3.length);
    if(xyCoordinatesProcess3.length > 9)
    {
      console.log('now show ok button Process3Row1')
      this.setState({isProcess3MininumLitreAdded:true})
    }
  }

  async handleFinishProcess3()
  {
    console.log('inside handleFinishProcess3')
    const {process3LitreRow1} = this.state;
    console.log('xyCoordinatesProcess3',xyCoordinatesProcess3)

    if(xyCoordinatesProcess3.length > 9)
    {
      if(undefined != process3LitreRow1 && process3LitreRow1 != "")
      {
      const payload = {
                        sensorId:this.props.sensorId,
                        processName:'Process3',
                        xyCoordinates:xyCoordinatesProcess3
                      }
      console.log('payload',payload)
      console.log('payload json', JSON.stringify(payload))
      const requestOptions = {
        credentials: 'include',
        method: 'POST',
        headers: { 'Content-Type': 'application/json',
                    'X-CSRF-Token': await NextAuth.csrfToken()
                  },
        body : JSON.stringify(payload)
      };
      const calibrationTank = await fetch('/sensor/calibratecanonicaltank',requestOptions);
      const res = await calibrationTank.json();
      console.log('res calibrationTank',res);
      this.handleExitProcess3();
    }
    else {
      alert('please add fuel at 4000 voltage row')
    }
  }
  else {
    alert('please fill fuel atleast to 10 row')
  }
  }

  handleExitProcess3()
  {
    console.log('inside handleFinishProcess3')
    this.clearProcess3Table();
  }

  clearProcess3Table()
  {
    console.log('clearProcess3Table')
    this.setState({
                  isProcessSelected: false,
                  isProcess1Selected : false,
                  isProcess2Selected : false,
                  isProcess3Selected : false,
                  selectedProcess:null,
                  process3VoltageRow1:'',
                  process3LitreRow1:'',
                  isProcess3Row1LitreAdded:false,
                  process3VoltageRow2:'',
                  process3LitreRow2:'',
                  isProcess3Row2LitreAdded:false,
                  process3VoltageRow3:'',
                  process3LitreRow3:'',
                  isProcess3Row3LitreAdded:false,
                  process3VoltageRow4:'',
                  process3LitreRow4:'',
                  isProcess3Row4LitreAdded:false,
                  process3VoltageRow5:'',
                  process3LitreRow5:'',
                  isProcess3Row5LitreAdded:false,
                  process3VoltageRow6:'',
                  process3LitreRow6:'',
                  isProcess3Row6LitreAdded:false,
                  process3VoltageRow7:'',
                  process3LitreRow7:'',
                  isProcess3Row7LitreAdded:false,
                  process3VoltageRow8:'',
                  process3LitreRow8:'',
                  isProcess3Row8LitreAdded:false,
                  process3VoltageRow9:'',
                  process3LitreRow9:'',
                  isProcess3Row9LitreAdded:false,
                  process3VoltageRow10:'',
                  process3LitreRow10:'',
                  isProcess3Row10LitreAdded:false,
                  process3VoltageRow11:'',
                  process3LitreRow11:'',
                  isProcess3Row11LitreAdded:false,
                  process3VoltageRow12:'',
                  process3LitreRow12:'',
                  isProcess3Row12LitreAdded:false,
                  process3VoltageRow13:'',
                  process3LitreRow13:'',
                  isProcess3Row13LitreAdded:false,
                  process3VoltageRow14:'',
                  process3LitreRow14:'',
                  isProcess3Row14LitreAdded:false,
                  process3VoltageRow15:'',
                  process3LitreRow15:'',
                  isProcess3Row15LitreAdded:false,
                  process3VoltageRow16:'',
                  process3LitreRow16:'',
                  isProcess3Row16LitreAdded:false,
                  process3VoltageRow17:'',
                  process3LitreRow17:'',
                  isProcess3Row17LitreAdded:false,
                  process3VoltageRow18:'',
                  process3LitreRow18:'',
                  isProcess3Row18LitreAdded:false,
                  process3VoltageRow19:'',
                  process3LitreRow19:'',
                  isProcess3Row19LitreAdded:false,
                  isProcess3MininumLitreAdded:false,
                  lastAddedLitre:'',
                })
  }

  render() {
    console.log('Calibration inside render')
    const {editProcess1Step2LitreRow2, process2VoltageRow1, process2LitreRow1, process2VoltageRow3, process2LitreRow3} = this.state
    const {
        process3LitreRow1, process3LitreRow2, process3LitreRow3, process3LitreRow4,
        process3LitreRow5, process3LitreRow6, process3LitreRow7, process3LitreRow8,
        process3LitreRow9, process3LitreRow10, process3LitreRow11, process3LitreRow12,
        process3LitreRow13, process3LitreRow14, process3LitreRow15, process3LitreRow16,
        process3LitreRow17, process3LitreRow18, process3LitreRow19, process3LitreRow20
        } = this.state
    return (
        <div className="animated fadeIn">
          {
          !this.state.isProcessSelected &&
          <Row>
            <Col>
              <Card>
                <CardBody>
                  <Table hover bordered responsive size="sm">
                    <thead>
                    <tr>
                      <th>SN</th>
                      <th>Process</th>
                      <th>Process Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>1</td>
                      <td>
                        <FormGroup check inline>
                          <Label className="form-check-label" check htmlFor="processRadio1">Process 1</Label>{' '}
                          <Input style={{margin:'10px'}} className="form-check-input" type="radio" id="processRadio1" name="processRadio" value="Process1" onChange={this.handleSelectProcess} />
                        </FormGroup>
                      </td>
                      <td>(Linear Tank) When Voltage and Current Litre both Unknown</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>
                      <FormGroup check inline>
                        <Label className="form-check-label" check htmlFor="processRadio2">Process 2</Label>{' '}
                        <Input style={{margin:'10px'}} className="form-check-input" type="radio" id="processRadio2" name="processRadio" value="Process2" onChange={this.handleSelectProcess} />
                      </FormGroup>
                      </td>
                      <td>(Linear Tank)When Voltage and Current Litre Both Known </td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>
                      <FormGroup check inline>
                        <Label className="form-check-label" check htmlFor="processRadio3">Process 3</Label>{' '}
                        <Input style={{margin:'10px'}} className="form-check-input" type="radio" id="processRadio3" name="processRadio" value="Process3" onChange={this.handleSelectProcess} />
                      </FormGroup>
                      </td>
                      <td>This is for Canonical Tank</td>
                    </tr>
                    </tbody>
                  </Table>
                  <Row>
                  <Col>
                  </Col>
                  <Col md="2">
                  <Button block color="primary" className="btn-pill" size="sm" onClick={this.props.toggleCalibrationModal.bind(this)}>Exit</Button>
                  </Col>
                  <Col md="2">
                  <Button block color="primary" className="btn-pill" size="sm" onClick={this.handleNextProcess.bind(this)}>Next</Button>
                  </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
          }
          {
            this.state.isProcess1Selected &&
            <Row>
              <Col>
                <Card>
                  <CardBody>
                   <Col md="6" style={{marginLeft:'20%'}}>
                    <p>Process 1</p>
                    {
                      this.state.isProcess1Step1Completed &&
                      <p>Please Fill 10 Litre Fuel</p>
                    }
                    <Table hover bordered responsive size="sm" style={{textAlign:'center'}}>
                      <thead>
                      <tr>
                        <th>Voltage {'(mV)'}</th>
                        <th>Tank In Litre</th>
                        <th>Sync Tank Voltage</th>
                      </tr>
                      </thead>
                      {
                        !this.state.isProcess1Step1Completed &&
                        <tbody>
                        <tr>
                          <td>{this.state.calibrationProcessMaxV}</td>
                          <td>{''}</td>
                          <td>{''}</td>
                        </tr>
                        <tr style={{height:'30px'}}>
                          <td>{''}</td>
                          <td>{''}</td>
                          <td>{''}</td>
                        </tr>
                        <tr>
                          <td>{this.state.process1Step1Voltage}</td>
                          <td>{'X'}</td>
                          <td>
                            <Col md="2">
                              <Button color="primary" className="btn-pill" size="sm" onClick={this.handleProcess1SyncOnStep1.bind(this)}>Sync</Button>
                            </Col>
                          </td>
                        </tr>
                        <tr>
                          <td>0</td>
                          <td>0</td>
                          <td>{''}</td>
                        </tr>
                        </tbody>
                      }
                      {
                        this.state.isProcess1Step1Completed &&
                        <tbody>
                        <tr>
                          <td>{this.state.calibrationProcessMaxV}</td>
                          <td>{this.state.process1Step2LitreRow1}</td>
                          <td>{''}</td>
                        </tr>
                        <tr>
                          <td>{this.state.process1Step2Voltage}</td>
                          {
                          //this.state.isProcess1Step2SyncCompleted
                          <td>

                            <div>
                              <Row style={{margin:'initial'}}>
                              { 'X + ' }
                              {
                                <Input type="text" style={{width:'30%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="editProcess1Step2LitreRow2" value={editProcess1Step2LitreRow2} onChange={this.handleChange}/>
                              }
                              {
                                ' = '+ this.state.process1Step2LitreRow2
                              }
                              </Row>
                            </div>

                          </td>
                          //this.state.process1Step2LitreRow2
                          }
                          <td>
                            <Col md="2">
                              <Button color="primary" className="btn-pill" size="sm" onClick={this.handleProcess1SyncOnStep2.bind(this)}>Sync</Button>
                            </Col>
                          </td>
                        </tr>
                        <tr>
                          <td>{this.state.process1Step1Voltage}</td>
                          <td>{this.state.process1Step2LitreRow3}</td>
                          <td>{''}</td>
                        </tr>
                        <tr>
                          <td>0</td>
                          <td>0</td>
                          <td>{''}</td>
                        </tr>
                        </tbody>
                      }
                    </Table>
                    <Row>
                    <Col>
                    </Col>
                    <Col md="3">
                    {
                      !this.state.isProcess1Step1Completed &&
                      <Button block color="primary" className="btn-pill" size="sm" onClick={this.handleBackProcess1.bind(this)}>Back</Button>
                    }
                    {
                        this.state.isProcess1Step1Completed &&
                        <Button block color="primary" className="btn-pill" size="sm" onClick={this.handleExitProcess1.bind(this)}>Exit</Button>
                    }
                    </Col>
                    <Col md="3">
                    {
                      !this.state.isProcess1Step1Completed &&
                      <Button block color="primary" className="btn-pill" size="sm" onClick={this.handleNextProcess1.bind(this)}>Next</Button>
                    }
                    {
                        this.state.isProcess1Step1Completed &&
                        <Button block color="primary" className="btn-pill" size="sm" onClick={this.handleFinishProcess1.bind(this)}>Finish</Button>
                    }
                    </Col>
                    </Row>
                   </Col>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          }
          {
            this.state.isProcess2Selected &&
            <Row>
              <Col>
                <Card>
                  <CardBody>
                   <Col md="6" style={{marginLeft:'20%'}}>
                    <p>Process 2</p>
                    <p>Input Tank Capacity</p>
                    <Table hover bordered responsive size="sm" style={{textAlign:'center'}}>
                      <thead>
                      <tr>
                      <th>Voltage {'(mV)'}</th>
                      <th>Tank In Litre</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      {
                        //<td>{this.state.calibrationProcessMaxVProcess2}</td>
                      }
                      <td>
                      <div>
                        <Row style={{margin:'initial'}}>
                          <Input type="text" style={{width:'40%', marginLeft:'30%', marginRight:'10%'}} id="text-input" name="process2VoltageRow1" value={process2VoltageRow1} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      </td>
                      <td>
                        <div>
                          <Row style={{margin:'initial'}}>
                            <Input type="text" style={{width:'40%', marginLeft:'30%', marginRight:'10%'}} id="text-input" name="process2LitreRow1" value={process2LitreRow1} onChange={this.handleChange}/>
                          </Row>
                        </div>
                      </td>
                      <td>
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess2.bind(this)}>OK</Button>
                      </Col>
                      </td>
                    </tr>
                    <tr style={{height:'30px'}}>
                      <td>{this.state.process2Voltage}</td>
                      <td>{this.state.process2LitreRow2}</td>
                      <td>{''}</td>
                    </tr>
                    <tr>
                      {
                      /*
                      <td>{this.state.process2VoltageRow3}</td>
                      <td>0</td>
                      <td>{''}</td>
                      */
                      }
                      <td>
                        <Row style={{margin:'initial'}}>
                          <Input type="text" style={{width:'40%', marginLeft:'30%', marginRight:'10%'}} id="text-input" name="process2VoltageRow3" value={process2VoltageRow3} onChange={this.handleChange}/>
                        </Row>
                      </td>
                      <td>
                      <Row style={{margin:'initial'}}>
                        <Input type="text" style={{width:'40%', marginLeft:'30%', marginRight:'10%'}} id="text-input" name="process2LitreRow3" value={process2LitreRow3} onChange={this.handleChange}/>
                      </Row>
                      </td>
                      <td>{''}</td>
                    </tr>
                    </tbody>
                    </Table>
                    <Row>
                    <Col>
                    </Col>
                    <Col md="3">
                    <Button block color="primary" className="btn-pill" size="sm" onClick={this.handleExitProcess2.bind(this)}>Exit</Button>
                    </Col>
                    <Col md="3">
                    <Button block color="primary" className="btn-pill" size="sm" onClick={this.handleFinishProcess2.bind(this)}>Finish</Button>
                    </Col>
                    </Row>
                   </Col>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          }
          {
            this.state.isProcess3Selected &&
            <Row>
              <Col>
                <Card>
                  <CardBody>
                   <Col md="6" style={{marginLeft:'20%'}}>
                    <p>Process 3</p>
                    <Table hover bordered responsive size="sm" style={{textAlign:'center'}}>
                      <thead>
                      <tr>
                      <th>SN</th>
                      <th>Voltage {'(mV)'}</th>
                      <th>Tank In Litre</th>
                      <th>Sync Tank Voltage</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                      <td>{'20'}</td>
                      <td>{this.state.calibrationProcessMaxV}</td>
                      <td>
                      { this.state.isProcess3MininumLitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow1" value={process3LitreRow1} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row1LitreAdded &&
                        this.state.process3LitreRow1
                      }
                      </td>
                      <td>
                      {''}
                      </td>
                      <td>
                      { this.state.isProcess3MininumLitreAdded && !this.state.isProcess3Row1LitreAdded &&
                        <Col md="2">
                          <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '1')}>OK</Button>
                        </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'19'}</td>
                      <td>
                      {this.state.process3VoltageRow2}
                      </td>
                      <td>
                      { !this.state.isProcess3Row2LitreAdded && this.state.isProcess3Row3LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow2" value={process3LitreRow2} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row2LitreAdded &&
                        this.state.process3LitreRow2
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row2LitreAdded && this.state.isProcess3Row3LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '2')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row2LitreAdded && this.state.isProcess3Row3LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '2')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'18'}</td>
                      <td>
                      {this.state.process3VoltageRow3}
                      </td>
                      <td>
                      { !this.state.isProcess3Row3LitreAdded && this.state.isProcess3Row4LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow3" value={process3LitreRow3} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row3LitreAdded &&
                        this.state.process3LitreRow3
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row3LitreAdded && this.state.isProcess3Row4LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '3')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row3LitreAdded && this.state.isProcess3Row4LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '3')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'17'}</td>
                      <td>
                      {this.state.process3VoltageRow4}
                      </td>
                      <td>
                      { !this.state.isProcess3Row4LitreAdded && this.state.isProcess3Row5LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow4" value={process3LitreRow4} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row4LitreAdded &&
                        this.state.process3LitreRow4
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row4LitreAdded && this.state.isProcess3Row5LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '4')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row4LitreAdded && this.state.isProcess3Row5LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '4')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'16'}</td>
                      <td>
                      {this.state.process3VoltageRow5}
                      </td>
                      <td>
                      { !this.state.isProcess3Row5LitreAdded && this.state.isProcess3Row6LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow5" value={process3LitreRow5} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row5LitreAdded &&
                        this.state.process3LitreRow5
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row5LitreAdded && this.state.isProcess3Row6LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '5')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row5LitreAdded && this.state.isProcess3Row6LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '5')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'15'}</td>
                      <td>
                      {this.state.process3VoltageRow6}
                      </td>
                      <td>
                      { !this.state.isProcess3Row6LitreAdded && this.state.isProcess3Row7LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow6" value={process3LitreRow6} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row6LitreAdded &&
                        this.state.process3LitreRow6
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row6LitreAdded && this.state.isProcess3Row7LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '6')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row6LitreAdded && this.state.isProcess3Row7LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '6')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'14'}</td>
                      <td>
                      {this.state.process3VoltageRow7}
                      </td>
                      <td>
                      { !this.state.isProcess3Row7LitreAdded && this.state.isProcess3Row8LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow7" value={process3LitreRow7} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row7LitreAdded &&
                        this.state.process3LitreRow7
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row7LitreAdded && this.state.isProcess3Row8LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '7')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row7LitreAdded && this.state.isProcess3Row8LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '7')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'13'}</td>
                      <td>
                      {this.state.process3VoltageRow8}
                      </td>
                      <td>
                      { !this.state.isProcess3Row8LitreAdded && this.state.isProcess3Row9LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow8" value={process3LitreRow8} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row8LitreAdded &&
                        this.state.process3LitreRow8
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row8LitreAdded && this.state.isProcess3Row9LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '8')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row8LitreAdded && this.state.isProcess3Row9LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '8')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'12'}</td>
                      <td>
                      {this.state.process3VoltageRow9}
                      </td>
                      <td>
                      { !this.state.isProcess3Row9LitreAdded && this.state.isProcess3Row10LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow9" value={process3LitreRow9} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row9LitreAdded &&
                        this.state.process3LitreRow9
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row9LitreAdded && this.state.isProcess3Row10LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '9')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row9LitreAdded && this.state.isProcess3Row10LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '9')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'11'}</td>
                      <td>
                      {this.state.process3VoltageRow10}
                      </td>
                      <td>
                      { !this.state.isProcess3Row10LitreAdded && this.state.isProcess3Row11LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow10" value={process3LitreRow10} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row10LitreAdded &&
                        this.state.process3LitreRow10
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row10LitreAdded && this.state.isProcess3Row11LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '10')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row10LitreAdded && this.state.isProcess3Row11LitreAdded && !this.state.isProcess3Row1LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '10')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'10'}</td>
                      <td>
                      {this.state.process3VoltageRow11}
                      </td>
                      <td>
                      { !this.state.isProcess3Row11LitreAdded && this.state.isProcess3Row12LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow11" value={process3LitreRow11} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row11LitreAdded &&
                        this.state.process3LitreRow11
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row11LitreAdded && this.state.isProcess3Row12LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '11')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row11LitreAdded && this.state.isProcess3Row12LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '11')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'9'}</td>
                      <td>
                      {this.state.process3VoltageRow12}
                      </td>
                      <td>
                      { !this.state.isProcess3Row12LitreAdded && this.state.isProcess3Row13LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow12" value={process3LitreRow12} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row12LitreAdded &&
                        this.state.process3LitreRow12
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row12LitreAdded && this.state.isProcess3Row13LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '12')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row12LitreAdded && this.state.isProcess3Row13LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '12')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'8'}</td>
                      <td>
                      {this.state.process3VoltageRow13}
                      </td>
                      <td>
                      { !this.state.isProcess3Row13LitreAdded && this.state.isProcess3Row14LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow13" value={process3LitreRow13} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row13LitreAdded &&
                        this.state.process3LitreRow13
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row13LitreAdded && this.state.isProcess3Row14LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '13')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row13LitreAdded && this.state.isProcess3Row14LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '13')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'7'}</td>
                      <td>
                      {this.state.process3VoltageRow14}
                      </td>
                      <td>
                      { !this.state.isProcess3Row14LitreAdded && this.state.isProcess3Row15LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow14" value={process3LitreRow14} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row14LitreAdded &&
                        this.state.process3LitreRow14
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row14LitreAdded && this.state.isProcess3Row15LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '14')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row14LitreAdded && this.state.isProcess3Row15LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '14')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'6'}</td>
                      <td>
                      {this.state.process3VoltageRow15}
                      </td>
                      <td>
                      { !this.state.isProcess3Row15LitreAdded && this.state.isProcess3Row16LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow15" value={process3LitreRow15} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row15LitreAdded &&
                        this.state.process3LitreRow15
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row15LitreAdded && this.state.isProcess3Row16LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '15')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row15LitreAdded && this.state.isProcess3Row16LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '15')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'5'}</td>
                      <td>
                      {this.state.process3VoltageRow16}
                      </td>
                      <td>
                      { !this.state.isProcess3Row16LitreAdded && this.state.isProcess3Row17LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow16" value={process3LitreRow16} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row16LitreAdded &&
                        this.state.process3LitreRow16
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row16LitreAdded && this.state.isProcess3Row17LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '16')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row16LitreAdded && this.state.isProcess3Row17LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '16')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'4'}</td>
                      <td>
                      {this.state.process3VoltageRow17}
                      </td>
                      <td>
                      { !this.state.isProcess3Row17LitreAdded && this.state.isProcess3Row18LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow17" value={process3LitreRow17} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row17LitreAdded &&
                        this.state.process3LitreRow17
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row17LitreAdded && this.state.isProcess3Row18LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '17')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row17LitreAdded && this.state.isProcess3Row18LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '17')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'3'}</td>
                      <td>
                      {this.state.process3VoltageRow18}
                      </td>
                      <td>
                      { !this.state.isProcess3Row18LitreAdded && this.state.isProcess3Row19LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                        { this.state.lastAddedLitre + ' + ' }
                          <Input type="text" style={{width:'40%', marginLeft:'10%', marginRight:'10%'}} id="text-input" name="process3LitreRow18" value={process3LitreRow18} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row18LitreAdded &&
                        this.state.process3LitreRow18
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row18LitreAdded && this.state.isProcess3Row19LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '18')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row18LitreAdded && this.state.isProcess3Row19LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '18')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    <tr style={{height:'30px'}}>
                      <td>{'2'}</td>
                      <td>
                      {this.state.process3VoltageRow19}
                      </td>
                      <td>
                      { !this.state.isProcess3Row19LitreAdded &&
                      <div>
                        <Row style={{margin:'initial'}}>
                          <Input type="text" style={{width:'40%', marginLeft:'30%', marginRight:'10%'}} id="text-input" name="process3LitreRow19" value={process3LitreRow19} onChange={this.handleChange}/>
                        </Row>
                      </div>
                      }
                      {
                        this.state.isProcess3Row19LitreAdded &&
                        this.state.process3LitreRow19
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row19LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleSyncProcess3.bind(this, '19')}>Sync</Button>
                      </Col>
                      }
                      </td>
                      <td>
                      {
                      !this.state.isProcess3Row19LitreAdded &&
                      <Col md="2">
                        <Button color="primary" className="btn-pill" size="sm" onClick={this.handleOkProcess3.bind(this, '19')}>OK</Button>
                      </Col>
                      }
                      </td>
                    </tr>

                    {
                      //this.renderRowsForProcess3()
                    }

                    <tr>
                      <td>{'1'}</td>
                      <td>{this.state.process3VoltageRow20}</td>
                      <td>{this.state.process3LitreRow20}</td>
                      <td>{''}</td>
                      <td>{''}</td>
                    </tr>

                    </tbody>
                    </Table>
                    <Row>
                    <Col>
                    </Col>
                    <Col md="3">
                    <Button block color="primary" className="btn-pill" size="sm" onClick={this.handleExitProcess3.bind(this)}>Exit</Button>
                    </Col>
                    <Col md="3">
                    <Button block color="primary" className="btn-pill" size="sm" onClick={this.handleFinishProcess3.bind(this)}>Finish</Button>
                    </Col>
                    </Row>
                   </Col>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          }
        </div>
    )

  }

}

export default Calibration;
