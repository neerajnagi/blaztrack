import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Form,
  FormGroup,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Input,
  Label
} from 'reactstrap';

import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import classnames from 'classnames';

import { NextAuth } from 'next-auth/client'

import Select from 'react-select';

import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';
//For table search
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';

const { SearchBar } = Search;

const dataAssignedSensorTable = [];

const dataCheckedSensorTable = [];

const dataSelectedSecANDType = [];

const dataSelectedSecORType = [];

const selectRow = {
  mode: 'checkbox',
  //clickToSelect: true,
  //Disable single row selection
  clickToSelect: false,
  onSelect: (row, isSelect, rowIndex, e) => {
    console.log(row.sn);
    console.log(isSelect);
    console.log(row);
    console.log(rowIndex);
    console.log(e);
  },
  onSelectAll: (isSelect, rows, e) => {
    console.log(isSelect);
    console.log(rows);
    console.log(e);
  }
};

let unAssignSensorRow;

const optionsSensorType = [];

let sensorSetupRow = {};

const cassandraColName = {
                          'Digital 1':'din1', 'Digital 2':'din2', 'Digital 3':'din3', 'Digital 4':'din4',
                          'Analog 1':'ain1', 'Analog 2':'ain2', 'Analog 3':'ain3', 'Analog 4':'ain4',
                          'External Bat':'power', 'Internal Bat':'battery', 'Speed':'speed'
                          };

class HmrSetup extends Component {

  constructor(props) {
    super(props);
    console.log('SensorSetup -->>, props',this.props)
    const row = this.props.row;
    this.handleChange = this.handleChange.bind(this);
    console.log('dataCheckedSensorTable',dataCheckedSensorTable)
    this.state = {
                  row: row,
                  dataAssignedSensorTable:dataAssignedSensorTable,
                  dataCheckedSensorTable:dataCheckedSensorTable,
                  dataSelectedSecANDType:dataSelectedSecANDType,
                  dataSelectedSecORType:dataSelectedSecORType,
                  isSensorAdded:false
                }
    //

  }

  async componentDidMount() {
    console.log('HmrSetup, inside componentDidMount')
    const checkDeviceAssigned = await this.checkDeviceAssigned();
    console.log('checkDeviceAssigned',checkDeviceAssigned)
    console.log('dataCheckedSensorTable',dataCheckedSensorTable)
    if(undefined != checkDeviceAssigned && checkDeviceAssigned)
    {
      this.setState({
                      isDeviceAssigned:true,
                      dataCheckedSensorTable:[],
                      dataSelectedSecANDType:[],
                      dataSelectedSecORType:[]
                    })
      this.getAssignedSensor();
    }
    else {
      console.log('Device not assigned')
      this.setState({
                      isDeviceAssigned:false,
                      dataCheckedSensorTable:[],
                      dataSelectedSecANDType:[],
                      dataSelectedSecORType:[]
                    })
    }
  }

  async checkDeviceAssigned()
  {
    console.log('inside checkDeviceAssigned');
    const vehicleId = this.state.row.vehicleId;
    const fetchDeviceAssignment = await fetch('http://159.65.6.196:5500/api/deviceassignment?filter[where][vehicleId]='+vehicleId);
    const res = await fetchDeviceAssignment.json();
    console.log('device assignment res', res)
    if(undefined != res && res.length > 0)
    {
    return true;
    }
    else {
      return false;
    }
  }

  async getAssignedSensor()
  {
    console.log('inside getAssignedSensor')
    const vehicleId = this.state.row.vehicleId;
    const fetchAssignedSensor = await fetch('http://159.65.6.196:5500/api/vehicle/fetchsensorassignmentbyvehicleid/'+vehicleId);
    const res = await fetchAssignedSensor.json();
    const data = res.data;
    console.log('fetchAssignedSensor data', data, data.length)
    let defaultSensors = ['SPEED'];
    if(undefined != data && data.length > 0)
    {
      const vehicleDeviceData = await this.getCurrentDeviceData();
      console.log('vehicleDeviceData:',vehicleDeviceData);
      const deviceData = vehicleDeviceData.data[vehicleId][0];
      let dataAssignedSensorTable = [];
      var sn = 1;
      //TODO sensor that not in assginedSensors list
      for(var i in defaultSensors)
      {
        dataAssignedSensorTable.push({
          "sn" : sn,
          "sensorType":defaultSensors[i]
        });
        sn++;
      }
      for (var i in data)
      {
        var item = data[i];
        var gpsDevice = item.gpsDevice;
        var devicePort= item.devicePort;
        var sensor = item.sensor;
        var sensorType = sensor.sensorType;
        var min = item.min;
        var max = item.max;
        var raw;
        if(undefined != sensorType && undefined != sensorType.name)
        {
          let sensorTypeName = sensorType.name.toLowerCase();
          if(sensorTypeName == "fuel" || sensorTypeName == "enginestatus")
          {
            continue;
          }
        }
        var sensorModel;
        var sensorMake;
        if(undefined != sensorType && sensorType.name.toLowerCase() == "fuel")
        {
          sensorModel = sensor.sensorModel;
          sensorMake = sensorModel.sensorMake;
        }
        else {
          sensorModel = "NA";
          sensorMake = "NA";
        }
        if(undefined == min)
        {
          min = "NA";
        }
        if(undefined == max)
        {
          max = "NA";
        }

        if(undefined != deviceData)
        {
            let column = cassandraColName[devicePort.name];
            console.log('getting raw data, column',column)
            console.log('deviceData[column]',deviceData[column])
            if(undefined != deviceData[column])
            {
            raw = Number(deviceData[column].toFixed(3));
            }
            else
            {
              raw = "";
            }
        }
        //TODO for speed, speed alway in zero index
        dataAssignedSensorTable[0].raw = deviceData['speed'];

        dataAssignedSensorTable.push({
          "sn" : sn,
          "sensorAssignmentId":item.id,
          "sensorId": item.sensorId,
          "gpsDeviceId":item.gpsDeviceId,
          "devicePortId":item.devicePortId,
          "vehicleId":item.vehicleId,
          "createdOn":item.createdOn,
          "deviceModelId":gpsDevice.deviceModelId,
          "sensorType":sensorType.name,
          "sensorModel":sensorModel.name || 'NA',
          "sensorMake":sensorMake.name || 'NA',
          "devicePort":devicePort.name || 'NA',
          "min":min,
          "max":max,
          "raw":raw
        });
        sn++;
      }
      ////await this.getAlreadyAddedSensorForHmr(dataAssignedSensorTable);
      this.setState({
                    isSensorAssigned:true,
                    dataAssignedSensorTable:dataAssignedSensorTable
                    }, () =>
                    {
                    this.getAlreadyAddedSensorForHmr(dataAssignedSensorTable);
                    })
      ////await this.getAlreadyAddedSensorForHmr(dataAssignedSensorTable);
    }
    else
    {
      this.setState({
                    isSensorAssigned:false
                    })
    }
  }

  async getCurrentDeviceData()
  {
    console.log('inside getCurrentDeviceData');
    const vehicleId = this.state.row.vehicleId;
    const customerId = this.state.row.customerId;
    let deviceData;
    const data = {customerId:customerId,vehicleId:vehicleId}
    const dataForCassQry = [data];
    let payload = {
                    data:dataForCassQry
                  }
    const requestOptions = {
      credentials: 'include',
      method: 'POST',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    console.log('payload:',payload);
    const vehicleDeviceData = await fetch('/vehicle/fetch/',requestOptions);
    const res = await vehicleDeviceData.json();
    console.log('vehicleDeviceData res:',res);
    deviceData = res;
    return deviceData;
  }

  async getAlreadyAddedSensorForHmr(dataAssignedSensorTable)
  {
    console.log('inside getAlreadyAddedSensorForHmr')
    const vehicleId = this.state.row.vehicleId;
    const fetchVehicle = await fetch('http://159.65.6.196:5500/api/vehicle/'+vehicleId);
    const res = await fetchVehicle.json();
    //let hmrParam = this.state.row.hmrParam;
    let hmrParam = res.hmrParam;
    console.log('inside getAlreadyAddedSensorForHmr, hmrParam',hmrParam)
    console.log('inside getAlreadyAddedSensorForHmr, dataAssignedSensorTable',dataAssignedSensorTable)
    if(undefined != hmrParam && hmrParam != "NA")
    {
      for(var i in dataAssignedSensorTable)
      {
        var sensorType = dataAssignedSensorTable[i].sensorType;
        //TODO for primary sensor
        let primarySensor = hmrParam.primary;
        if(undefined != primarySensor)
        {
          for(var i in primarySensor)
          {
            if(primarySensor.hasOwnProperty(sensorType.toLowerCase()))
            {
              console.log('added primary sensor:', sensorType);
              this.showAddedSensorForHmr(sensorType, primarySensor[sensorType.toLowerCase()], 'primary');
            }
          }
        }
        //TODO for secondary sensors
        let secondarySensors = hmrParam.secondary;
        if(undefined != secondarySensors)
        {
          //TODO secondary AND sensors
          let secondaryANDSensors = secondarySensors.and;
          if(undefined != secondaryANDSensors)
          {
            for(var i in secondaryANDSensors)
            {
              if(secondaryANDSensors.hasOwnProperty(sensorType.toLowerCase()))
              {
                console.log('added secondary AND sensor:', sensorType);
                this.showAddedSensorForHmr(sensorType, secondaryANDSensors[sensorType.toLowerCase()], 'secondaryAND');
              }
            }
          }

          //TODO secondary OR sensors
          let secondaryORSensors = secondarySensors.or;
          if(undefined != secondaryORSensors)
          {
            for(var i in secondaryORSensors)
            {
              if(secondaryORSensors.hasOwnProperty(sensorType.toLowerCase()))
              {
                console.log('added secondary OR sensor:', sensorType);
                this.showAddedSensorForHmr(sensorType, secondaryANDSensors[sensorType.toLowerCase()], 'secondaryOR');
              }
            }
          }

        }
      }
    }
    else {
      console.log('hmr not setup for this vehicle')
      this.setState({isSensorAdded:false})
    }
  }

  showAddedSensorForHmr(sensorType, hmrParam, from)
  {
    console.log('inside showAddedSensorForHmr, sensorType, hmrParam',sensorType,hmrParam)
    let dataCheckedSensorTable = this.state.dataCheckedSensorTable;
    let dataSelectedSecANDType = this.state.dataSelectedSecANDType;
    let dataSelectedSecORType = this.state.dataSelectedSecORType;
    let dataAssignedSensorTable = this.state.dataAssignedSensorTable;
    console.log('dataCheckedSensorTable',dataCheckedSensorTable,'dataCheckedSensorTable.includes(sensorType)',dataCheckedSensorTable.includes(sensorType))
    if(!dataCheckedSensorTable.includes(sensorType))
    {
      dataCheckedSensorTable.push(sensorType)
    }
    if(from == "primary")
    {
      this.setState({
                  ['radio_primary_'+sensorType]:true,
                  primarySensor:sensorType
                })
    }
    else if(from == "secondaryAND")
    {
      if(!dataSelectedSecANDType.includes(sensorType))
      {
        dataSelectedSecANDType.push(sensorType);
      }
      this.setState({
                      ['checkbox_row_and_'+sensorType]:true
                    })
    }
    else if(from == "secondaryOR")
    {
      if(!dataSelectedSecORType.includes(sensorType))
      {
        dataSelectedSecORType.push(sensorType);
      }
      this.setState({
                      ['checkbox_row_or_'+sensorType]:true
                    })
    }
    else {
      console.log('something went wrong')
    }

    if(sensorType == "SPEED")
    {
      const min = hmrParam.min;
      const max = hmrParam.max;
      dataAssignedSensorTable[0].min = min;
      dataAssignedSensorTable[0].max = max;
      this.setState({
                      ['min'+sensorType]:min,
                      ['max'+sensorType]:max,
                      isSensorAdded:true
                    })
    }
    else {
      this.setState({
                      isSensorAdded:true
                    })
    }
    console.log('dataCheckedSensorTable',dataCheckedSensorTable);
  }

  handlePrimarySensorRadio(e)
  {
    const { name, value } = e.target;
    console.log('name:',name,', value:',value)
    let {dataCheckedSensorTable, dataSelectedSecANDType, dataSelectedSecORType, primarySensor} = this.state;
    console.log('dataCheckedSensorTable',dataCheckedSensorTable)
    console.log('dataSelectedSecANDType',dataSelectedSecANDType)
    console.log('dataSelectedSecORType',dataSelectedSecORType)
    if(!dataCheckedSensorTable.includes(value))
    {
      dataCheckedSensorTable.push(value)
    }
    console.log('dataCheckedSensorTable',dataCheckedSensorTable)
    if(value != "SPEED" && !dataSelectedSecANDType.includes('SPEED') && !dataSelectedSecORType.includes('SPEED'))
    {
      console.log('need to remove speed from dataCheckedSensorTable')
      dataCheckedSensorTable.splice( dataCheckedSensorTable.indexOf('SPEED'), 1 );
    }
    console.log('dataCheckedSensorTable',dataCheckedSensorTable)
    this.setState({
                  primarySensor:value,
                  isSensorAdded:true,
                  ['radio_primary_'+primarySensor]: false,
                  ['radio_primary_'+value]: true
                })
  }

  toggleCheckbox = (row, event) => {
    console.log('inside toggleCheckbox, row',row)
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    let name = target.name
    console.log('target, name:', name,' value', value)
    let nameSplited = name.split('_');
    let sensorType = nameSplited[2];
    let dataCheckedSensorTable = this.state.dataCheckedSensorTable;
    if(value)
    {
      console.log(sensorType,'checked true')
      if(!dataCheckedSensorTable.includes(sensorType))
      {
      dataCheckedSensorTable.push(sensorType)
      }
    }
    else {
      console.log(sensorType,'checked false')
      console.log('dataCheckedSensorTable',dataCheckedSensorTable)
      dataCheckedSensorTable.splice( dataCheckedSensorTable.indexOf(sensorType), 1 );
    }
    console.log('dataCheckedSensorTable',dataCheckedSensorTable);
    this.setState({[name]:value, isSensorAdded:true, ['checkbox_row_'+sensorType]: value})
  }

  toggleCheckboxAndType = (row, event) => {
    console.log('inside toggleCheckboxAndType, row',row)
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    let name = target.name
    console.log('target, name:', name,' value', value)
    let nameSplited = name.split('_');
    let sensorType = nameSplited[3];
    let dataCheckedSensorTable = this.state.dataCheckedSensorTable;
    let dataSelectedSecANDType = this.state.dataSelectedSecANDType;
    let {primarySensor} = this.state;
    if(value)
    {
      console.log(sensorType,'checked true')
      if(!dataCheckedSensorTable.includes(sensorType))
      {
      dataCheckedSensorTable.push(sensorType)
      }
      if(!dataSelectedSecANDType.includes(sensorType))
      {
      dataSelectedSecANDType.push(sensorType)
      }
    }
    else {
      console.log(sensorType,'checked false')
      console.log('dataCheckedSensorTable',dataCheckedSensorTable)
      if(undefined != primarySensor && primarySensor == "SPEED")
      {
      }
      else {
        dataCheckedSensorTable.splice( dataCheckedSensorTable.indexOf(sensorType), 1 );
      }
      dataSelectedSecANDType.splice( dataSelectedSecANDType.indexOf(sensorType), 1 );
    }
    console.log('dataChecked AND type SensorTable',dataCheckedSensorTable);
    console.log('dataSelectedSecANDType',dataSelectedSecANDType);
    this.setState({[name]:value, isSensorAdded:true, ['checkbox_row_and_'+sensorType]: value})
  }

  toggleCheckboxOrType = (row, event) => {
    console.log('inside toggleCheckboxOrType, row',row)
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    let name = target.name
    console.log('target, name:', name,' value', value)
    let nameSplited = name.split('_');
    let sensorType = nameSplited[3];
    let dataCheckedSensorTable = this.state.dataCheckedSensorTable;
    let dataSelectedSecORType = this.state.dataSelectedSecORType;
    let {primarySensor} = this.state;
    if(value)
    {
      console.log(sensorType,'checked true')
      if(!dataCheckedSensorTable.includes(sensorType))
      {
      dataCheckedSensorTable.push(sensorType)
      }
      if(!dataSelectedSecORType.includes(sensorType))
      {
      dataSelectedSecORType.push(sensorType)
      }
    }
    else {
      console.log(sensorType,'checked false')
      console.log('dataCheckedSensorTable',dataCheckedSensorTable)
      if(undefined != primarySensor && primarySensor == "SPEED")
      {
      }
      else {
        dataCheckedSensorTable.splice( dataCheckedSensorTable.indexOf(sensorType), 1 );
      }
      dataSelectedSecORType.splice( dataSelectedSecORType.indexOf(sensorType), 1 );
    }
    console.log('dataChecked OR type SensorTable',dataCheckedSensorTable);
    console.log('dataSelectedSecORType',dataSelectedSecORType);
    this.setState({[name]:value, isSensorAdded:true, ['checkbox_row_or_'+sensorType]: value})
  }

  handleChange(e) {
      const { name, value } = e.target;
      this.setState({ [name]: value });
  }

  async handleCheckStatus()
  {
    console.log('inside handleCheckStatus')
    const vehicleDeviceData = await this.getCurrentDeviceData();
    console.log('vehicleDeviceData:',vehicleDeviceData);
    const engineStatus = await this.checkEngineStatus(vehicleDeviceData);
    console.log('engineStatus',engineStatus)
    if(engineStatus)
    {
      this.setState({isEngineStatusChecked:true, engineStatusColor:'green', engineStatus:'on'})
    }
    else {
      this.setState({isEngineStatusChecked:true, engineStatusColor:'red', engineStatus:'off'})
    }
  }

  async checkEngineStatus(vehicleDeviceData)
  {
    console.log('inside checkEngineStatus');
    const vehicleId = this.state.row.vehicleId;
    const deviceData = vehicleDeviceData.data[vehicleId][0];
    const {dataAssignedSensorTable, primarySensor, dataSelectedSecANDType, dataSelectedSecORType} = this.state;
    //TODO first check primarySensor;
    let primarySensorStatus = false;
    let secondaryANDSensorsStatus = false;
    let secondaryORSensorsStatus = false;
    //TODO need to first check if primary and secondary sensors not undefined
    // if primarySensor == not defined && secondaryAND && secondaryOR array length > 0
    if (undefined == primarySensor && dataSelectedSecANDType.length <= 0 && dataSelectedSecORType.length <= 0)
    {
        alert('please select atleast one sensor from primary or secondary table')
        return false;
    }

    if(undefined != primarySensor)
    {
      console.log('dataAssignedSensorTable',dataAssignedSensorTable)
      console.log('primarySensor',primarySensor)
      for(var i in dataAssignedSensorTable)
      {
        let sensorType = dataAssignedSensorTable[i].sensorType;
        if(sensorType == primarySensor)
        {
          //first found primary sensor raw value, min and max from dataAssignedSensorTable
          //then make decision
          let min = dataAssignedSensorTable[i].min;
          let max = dataAssignedSensorTable[i].max;
          let raw = dataAssignedSensorTable[i].raw;
          console.log('min',min,'max',max)
          console.log('raw',raw);
          if(undefined != raw)
          {
            if(raw >= min && raw <= max)
            {
              primarySensorStatus = true;
            }
            else {
              primarySensorStatus = false;
            }
          }
        }
      }
    }
    else {
      //TODO if primary not defined then go to secondary sensors
      console.log('primary sensor not defined')
      //alert('please first select primary sensor')
      //return false;
    }

    if(primarySensorStatus)
    {
      console.log('engine on')
      return true;
    }
    //TODO if primarySensorStatus false then check secondary sensors status
    else
    {
      //TODO now check secondary sensors
      //First check secondary sensors with AND conditions (always true)
      console.log('dataSelectedSecANDType',dataSelectedSecANDType)
      console.log('dataSelectedSecORType',dataSelectedSecORType)
      for(var i in dataAssignedSensorTable)
      {
        let sensorType = dataAssignedSensorTable[i].sensorType;
        if(dataSelectedSecANDType.includes(sensorType))
        {
          console.log('now checked secondary AND for sensor',sensorType);
          let min = dataAssignedSensorTable[i].min;
          let max = dataAssignedSensorTable[i].max;
          let raw = dataAssignedSensorTable[i].raw;
          console.log('min',min,'max',max)
          console.log('raw',raw);
          if(undefined != raw)
          {
            if(raw >= min && raw <= max)
            {
              secondaryANDSensorsStatus = true;
            }
            else {
              secondaryANDSensorsStatus = false;
              break;
            }
          }
        }
      }

      if(secondaryANDSensorsStatus)
      {
        console.log('engine on')
        return true;
      }
      else
      {
        //Second check secondary sensors with OR conditions (atleast one true)
        for(var i in dataAssignedSensorTable)
        {
          let sensorType = dataAssignedSensorTable[i].sensorType;
          if(dataSelectedSecORType.includes(sensorType))
          {
            console.log('now checked secondary OR for sensor',sensorType);
            let min = dataAssignedSensorTable[i].min;
            let max = dataAssignedSensorTable[i].max;
            let raw = dataAssignedSensorTable[i].raw;
            console.log('min',min,'max',max)
            console.log('raw',raw);
            if(undefined != raw)
            {
              if(raw >= min && raw <= max)
              {
                secondaryORSensorsStatus = true;
                break;
              }
              else
              {
                secondaryORSensorsStatus = false;
              }
            }
          }
        }
      }
    }
    //TODO now make decision
    console.log('primarySensorStatus',primarySensorStatus)
    console.log('secondaryANDSensorsStatus',secondaryANDSensorsStatus)
    console.log('secondaryORSensorsStatus',secondaryORSensorsStatus)
    return secondaryORSensorsStatus;
  }

  async handleSubmitHmrSetup()
  {
    console.log('insdie handleSubmitHmrSetup');
    const vehicleId = this.state.row.vehicleId;
    const {dataAssignedSensorTable, primarySensor, dataSelectedSecANDType, dataSelectedSecORType} = this.state;
    console.log('assginedSensors length',dataAssignedSensorTable.length)
    console.log('primarySensor',primarySensor)
    console.log('dataSelectedSecANDType',dataSelectedSecANDType)
    console.log('dataSelectedSecORType',dataSelectedSecORType)
    if(undefined == primarySensor)
    {
      alert('please select primary sensor')
      return false;
    }
    let data = {};
    let primarySensorData = {};
    let secondaryANDSensorsData = {};
    let secondaryORSensorsData = {};
    for(var i in dataAssignedSensorTable)
    {
      var sensorType = dataAssignedSensorTable[i].sensorType;
      if(undefined != sensorType)
      {
        console.log('sensorType',sensorType)
        if(undefined != primarySensor)
        {
          if(sensorType == primarySensor)
          {
            if(sensorType == "SPEED")
            {
              primarySensorData[sensorType.toLowerCase()] = {
                                              min:Number(this.state['min'+sensorType]), max:Number(this.state['max'+sensorType])
                                            };
            }
            else {
              primarySensorData[sensorType.toLowerCase()] = {};
            }
          }
        }
        //TODO for secondary sensors
        if(this.state['checkbox_row_and_'+sensorType])
        {
          console.log(sensorType,'added in Secondary and')
          if(sensorType == "SPEED")
          {
            secondaryANDSensorsData[sensorType.toLowerCase()] = {
                                              min:Number(this.state['min'+sensorType]), max:Number(this.state['max'+sensorType])
                                            };
          }
          else {
            secondaryANDSensorsData[sensorType.toLowerCase()] = {};
          }
        }
        if(this.state['checkbox_row_or_'+sensorType])
        {
          console.log(sensorType,'added Secondary or')
          if(sensorType == "SPEED")
          {
            secondaryORSensorsData[sensorType.toLowerCase()] = {
                                              min:Number(this.state['min'+sensorType]), max:Number(this.state['max'+sensorType])
                                            };
          }
          else {
            secondaryORSensorsData[sensorType.toLowerCase()] = {};
          }
        }
      }
    }
    console.log('data',data)
    console.log('primarySensorData',primarySensorData)
    console.log('secondaryANDSensorsData',secondaryANDSensorsData)
    console.log('secondaryORSensorsData',secondaryORSensorsData)
    let payload = {
                    hmr:{primary:primarySensorData, secondary:{and:secondaryANDSensorsData, or:secondaryORSensorsData}}
                  }
    console.log('payload: ',payload)
    console.log('payload: ',JSON.stringify(payload))
    const requestOptions = {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    const updateHmr = await fetch('http://159.65.6.196:5500/api/vehicle/hmr/'+vehicleId, requestOptions)
    const res = await updateHmr.json()
    console.log('Update vehicle hmr res',res)
    alert('hmr setup successfully done')
    this.getAssignedSensor();
  }

  componentWillUnmount() {
   // use intervalId from the state to clear the interval
   console.log('HmrSetup -->> inside componentWillUnmount')
   this.setState({
                   dataAssignedSensorTable:[],
                   dataCheckedSensorTable:[]
                 })
  }

  render() {
    console.log('HmrSetup inside render')
    const formatter = (cell, row, rowIndex, formatExtraData) => { return ( <FormGroup check><Input type="checkbox" name={"checkbox_row_"+row.sensorType} checked={this.state['checkbox_row_'+row.sensorType]} onClick={this.toggleCheckbox.bind(this, row)} /></FormGroup> ); }

    const columnsAssignedSensorTable = [{
      dataField: 'sn',
      text: 'SN'
    }, {
      dataField: 'sensorType',
      text: 'Sensor Type'
    },{
      dataField: 'devicePort',
      text: 'Device Port'
    },{
      dataField: 'min',
      text: 'Min'
    },{
      dataField: 'max',
      text: 'Max'
    },{
      dataField: 'checkToAdd',
      text: 'Check To Add',
      formatter: formatter
    }];

    return (
        <div>
        { this.state.isSensorAssigned &&
         <Row>
          <Col>
            <p><b>Assigned Sensor:</b></p>
          </Col>
          <Col md="2">
            <Button block color="primary" className="btn-pill" size="sm" onClick={this.getAssignedSensor.bind(this)}>Refresh</Button>
          </Col>
         </Row>
        }
        <Row>
        {
        /*
        //TODO BootstrapTable-2 checkbox state not changed while update to new version, state changed on previous version
        // so not using BootstrapTable-2, using normal bootstarp table
         this.state.isSensorAssigned &&
          <Col>
            <p><b>Sensor:</b></p>
            <BootstrapTable
              keyField='sn'
              hover bordered
              data={ this.state.dataAssignedSensorTable }
              columns={ columnsAssignedSensorTable }
            />
          </Col>
          */
        }
        {
          this.state.isSensorAssigned &&
          <Col>
          <Table hover bordered responsive size="sm" style={{textAlign:'center'}}>
            <thead>
            <tr>
              <th>SN</th>
              <th>Sensor Type</th>
              <th>Device Port</th>
              <th>Min</th>
              <th>Max</th>
              <th>Raw Data</th>
            </tr>
            </thead>
            <tbody>
            {
            this.state.dataAssignedSensorTable.map((item, index) =>
              <tr>
                <td>{item.sn}</td>
                <td>{item.sensorType}</td>
                <td>{item.devicePort}</td>
                <td>{item.min}</td>
                <td>{item.max}</td>
                <td>{item.raw}</td>
              </tr>
            )}
            </tbody>
          </Table>
          </Col>
        }
        </Row>
        <Row>
          <Col>
            <CardBody>
                <Col>
                  <Row>
                  <Col>
                  {
                    this.state.isSensorAssigned &&
                    <p><b>Primary Sensor</b></p>
                  }
                  {
                    this.state.isSensorAssigned &&
                    <Table hover bordered responsive size="sm" style={{textAlign:'center'}}>
                      <thead>
                      <tr>
                        <th>SN</th>
                        <th>Sensor Type</th>
                        <th>Select</th>
                      </tr>
                      </thead>
                      <tbody>
                      {
                      this.state.dataAssignedSensorTable.map((item, index) =>
                        <tr>
                          <td>{item.sn}</td>
                          <td>{item.sensorType}</td>
                          {
                          <td>
                            <FormGroup check><Input type="radio" name="radio" value={item.sensorType} checked={this.state['radio_primary_'+item.sensorType]} onChange={this.handlePrimarySensorRadio.bind(this)} /></FormGroup>
                          </td>
                          }
                        </tr>
                      )}
                      </tbody>
                    </Table>
                  }
                  </Col>
                  <Col>
                  {
                    this.state.isSensorAssigned &&
                    <p><b>Secondary Sensor</b></p>
                  }
                  {
                    this.state.isSensorAssigned &&
                    <Table hover bordered responsive size="sm" style={{textAlign:'center'}}>
                      <thead>
                      <tr>
                        <th>SN</th>
                        <th>Sensor Type</th>
                        <th>Always True</th>
                        <th>Atleast One True</th>
                      </tr>
                      </thead>
                      <tbody>
                      {
                      this.state.dataAssignedSensorTable.map((item, index) =>
                        <tr>
                          <td>{item.sn}</td>
                          <td>{item.sensorType}</td>
                          {
                          <td>
                            <FormGroup check><Input type="checkbox" name={"checkbox_row_and_"+item.sensorType} checked={this.state['checkbox_row_and_'+item.sensorType]} onClick={this.toggleCheckboxAndType.bind(this, item)} /></FormGroup>
                          </td>
                          }
                          {
                          <td>
                            <FormGroup check><Input type="checkbox" name={"checkbox_row_or_"+item.sensorType} checked={this.state['checkbox_row_or_'+item.sensorType]} onClick={this.toggleCheckboxOrType.bind(this, item)} /></FormGroup>
                          </td>
                          }
                        </tr>
                      )}
                      </tbody>
                    </Table>
                  }
                  </Col>
                  </Row>
                </Col>
            </CardBody>
          </Col>
        </Row>
        <Row>
          <Col>
            <CardBody>
              {
                this.state.dataCheckedSensorTable && this.state.dataCheckedSensorTable.map((item, index) =>

                <Col md="8" style={{marginLeft:'15%'}}>
                { item.includes('SPEED') &&
                  <p><b>{item}</b></p>
                }
                { item.includes('SPEED') &&
                <Table hover bordered responsive size="sm" style={{textAlign:'center'}}>
                  <thead>
                  <tr>
                    <th>Status</th>
                    <th>Min</th>
                    <th>Max</th>
                  </tr>
                  </thead>
                    <tbody>
                     <tr style={{height:'30px'}}>
                       <td>{'Engine on'}</td>
                       <td><Input type="text" style={{width:'40%', marginLeft:'30%', marginRight:'10%'}} id="text-input" name={"min"+item} value={this.state['min'+item]} onChange={this.handleChange}/></td>
                       <td><Input type="text" style={{width:'40%', marginLeft:'30%', marginRight:'10%'}} id="text-input" name={"max"+item} value={this.state['max'+item]} onChange={this.handleChange} /></td>
                     </tr>
                    </tbody>
                  </Table>
                  }
                  </Col>
                )}
                <Col md="8" style={{marginLeft:'15%'}}>
                <Row>
                {
                  this.state.isSensorAdded && this.state.isEngineStatusChecked &&
                  <Col md="5">
                    <p><b>Engine(HMR) Status:</b><i style={{color:this.state.engineStatusColor}}> {this.state.engineStatus} </i></p>
                  </Col>
                }
                {
                  this.state.isSensorAdded &&
                  <Col md="3">
                    <Button block color="primary" className="btn-pill" size="sm" onClick={this.handleCheckStatus.bind(this)}>Check Status</Button>
                  </Col>
                }
                {
                  this.state.isSensorAdded &&
                  <Col md="3">
                    <Button block color="primary" className="btn-pill" size="sm" onClick={this.handleSubmitHmrSetup.bind(this)}>Submit</Button>
                  </Col>
                }
                </Row>
                </Col>

              </CardBody>
          </Col>
        </Row>
        </div>
      )

  }

}
export default HmrSetup;
