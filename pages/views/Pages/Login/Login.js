import React, { Component } from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';

import Head from 'next/head'

import Router from 'next/router'
import Link from 'next/link'

import { connect } from 'react-redux';
import { alertActions } from '../../../../redux/_actions';
import { userActions } from '../../../../redux/_actions';

import Cookies from 'universal-cookie'
import { NextAuth } from 'next-auth/client'

class Login extends Component {

  constructor(props) {
      super(props);

      this.state = {
          username: '',
          password: '',
          submitted: false,
      };

      this.handleChange = this.handleChange.bind(this);
      this.handleLogin = this.handleLogin.bind(this);

    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleLogin(e) {
      console.log('handleLogin')
      e.preventDefault();
      //Router.push(`/containers/DefaultLayout`)
      this.setState({ submitted: true });
      const { username, password } = this.state;
      console.log('Login -->> handleSubmit, username, password ', username, password)
      const { dispatch } = this.props;
      dispatch(alertActions.clear());
      if (username && password) {
        // Save current URL so user is redirected back here after signing in
          const cookies = new Cookies()
          cookies.set('redirect_url', window.location.pathname, { path: '/' })
          console.log('Login -->> login with mobile number ',username)
          //dispatch(userActions.login('+91'+username, password));
          //let payload = {_id : '5b854d4d0bef8f0bebbbded2',password:'123456',phone:'+919999999999'}
          let payload = {phone:'+91'+username,password:password}
          NextAuth.signin(payload)
          .then(() => {
            //TODO no need to check email, direct
            //Router.push(`/auth/check-email?email=${this.state.email}`)
            Router.push('/auth/callback')
          })
          .catch(err => {
            alert('signin failed, please check username & password')
            //Router.push(`/auth/error?action=signin&type=mobile&mobile=${this.state.username}`)
          })

      }
      else {
        //alert('please enter mobile number')
      }
      }

  render() {
    const { username, password, submitted } = this.state;
    return (
      <div className="app flex-row align-items-center">
      <Head>
          <link rel='stylesheet' href='_next/static/style.css' />
          <link rel='stylesheet' href='_next/static/react-perfect-scrollbar.min.css' />
          <link rel='stylesheet' href='_next/static/min/css/coreui-icons.min.css' />
          <link rel='stylesheet' href='_next/static/min/css/simple-line-icons.css' />
          <link rel='stylesheet' href='_next/static/min/css/font-awesome.css' />
      </Head>
        <Container>
          <Row className="justify-content-center">
            <Col md="4">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" placeholder="Username" name="username" value={username} onChange={this.handleChange} />
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" placeholder="Password" name="password" value={password} onChange={this.handleChange} />
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                        {
                        //<Button color="primary" className="px-4" onClick={this.handleLogin}>Login</Button>
                        }
                      </Col>
                      <Col xs="6" className="text-right">
                      {
                        //<Button color="link" className="px-0">Forgot password?</Button>
                        <Button color="primary" className="px-4" onClick={this.handleLogin}>Login</Button>
                      }
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

//export default Login;

function mapStateToProps(state) {
    console.log('Login -->> mapStateToProps, state: ',state)
    const { loggingIn,loggedIn, user } = state.authentication;
    return {
        loggingIn,
        loggedIn,
        user
    };
}

export default connect(mapStateToProps)(Login);
