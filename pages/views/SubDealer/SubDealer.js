import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Form,
  FormGroup,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Label,
  FormText
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'

import Select from 'react-select';
import classnames from 'classnames';

import { NextAuth } from 'next-auth/client'

import Router from 'next/router'

//react-bootstrap-table (deprecated)
//import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
//react-bootstrap-table2
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import cellEditFactory from 'react-bootstrap-table2-editor';
//For table search
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
// react-table not working properly (failing)
//import ReactTable from 'react-table'
//import FilterableTable from 'react-filterable-table'

import DropdownTreeSelect from 'react-dropdown-tree-select'
import 'react-dropdown-tree-select/dist/styles.css'

import PropTypes from 'prop-types';

import { ClipLoader } from 'react-spinners';

let selectedNodesManage = [];

const { SearchBar } = Search;

const dataManageDropDown = [{
  label: 'search me',
  value: 'searchme',
  children: [
    {
      label: 'search me too',
      value: 'searchmetoo',
      children: [
        {
          label: 'No one can get me',
          value: 'anonymous'
        }
      ]
    }
  ]
},
{
  label:'second',
  value:'second'
},
{
  label:'third',
  value:'third'
},
{
  label:'fourth',
  value:'fourth',
  checked:true
},
]
const onChange = (currentNode, selectedNodes) => {
  console.log('onChange::', currentNode, selectedNodes)
  selectedNodesManage = selectedNodes;
}
const onAction = ({ action, node }) => {
  console.log(`onAction:: [${action}]`, node)
}
const onNodeToggle = currentNode => {
  console.log('onNodeToggle::', currentNode)
}

const optionsSubDealer = [{ value: 'all', label: 'All' }];

const selectRow = {
  mode: 'checkbox',
  clickToSelect: false,
  onSelect: (row, isSelect, rowIndex, e) => {
    console.log(row.id);
    console.log(isSelect);
    console.log(row);
    console.log(rowIndex);
    console.log(e);
  },
  onSelectAll: (isSelect, rows, e) => {
    console.log(isSelect);
    console.log(rows);
    console.log(e);
  }
};

const divStyle = {
  margin: '40px',
  border: '5px solid pink'
};

//
const dataBootstrapTable = [
  { id: 1, phoneNumber: '9799605400', name:'Ashwin', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
  { id: 2, phoneNumber: '9024038439', name:'Raji', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
  { id: 3, phoneNumber: '9024038434', name:'Aman', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
  { id: 4, phoneNumber: '9024038433', name:'Akshit', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
];

const dataUsersTable = [];
const dataSubDealersTable = [];

let deleteUserRow, deleteSubDealerRow;

let dealerRecord = "";

//const editFormatter = (cell, row, rowIndex, formatExtraData) => { console.log('editFormatter, row ',row); return ( <Button block color="primary" className="btn-pill" size="sm" >Edit</Button> ); }
//const deleteFormatter = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm">Delete</Button> ); }

class SubDealer extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleAddUser = this.toggleAddUser.bind(this);
    this.toggleEditUser = this.toggleEditUser.bind(this);
    this.editUser = this.editUser.bind(this);
    this.toggleDeleteUser = this.toggleDeleteUser.bind(this);
    this.confirmDeleteUser = this.confirmDeleteUser.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
    this.handleAddUser = this.handleAddUser.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSaveUser = this.handleSaveUser.bind(this);
    this.handleUpdateUser = this.handleUpdateUser.bind(this);
    this.handleAddSubDealer = this.handleAddSubDealer.bind(this);
    this.toggleAddSubDealer = this.toggleAddSubDealer.bind(this);
    this.handleSaveSubDealer = this.handleSaveSubDealer.bind(this);
    this.editSubDealer = this.editSubDealer.bind(this);
    this.toggleEditSubDealer = this.toggleEditSubDealer.bind(this);
    this.handleUpdateSubDealer = this.handleUpdateSubDealer.bind(this);
    this.toggleDeleteSubDealer = this.toggleDeleteSubDealer.bind(this);
    this.confirmDeleteSubDealer = this.confirmDeleteSubDealer.bind(this);
    this.deleteSubDealer = this.deleteSubDealer.bind(this);

    this.state = {
      session: props.session,
      isSignedIn: (props.session.user) ? true : false,
      manageCategory:this.props.managecategory,
      dropdownOpen: false,
      radioSelected: 2,
      selectedOption: null,
      isClearable: true,
      activeTab: '1',
      isOpenModalAddUser: false,
      isOpenModalEditUser: false,
      isOpenModalDeleteUser: false,
      isOpenModalAddSubDealer: false,
      isOpenModalEditSubDealer: false,
      isOpenModalDeleteSubDealer: false,
      dataManageDropDown:dataManageDropDown,
      dataUsersTable:dataUsersTable,
      dataSubDealersTable:dataSubDealersTable,
      optionsSubDealer:optionsSubDealer,
      userTableLoading:true,
      subDealerTableLoading:true,
    };
  }

  async componentDidMount() {
    console.log('SubDealer -->> inside componentDidMount');
    //TODO check user logged in or not:
    const session = await NextAuth.init({force: true})
    if(session.user)
    {
      console.log('user logged in')
    }
    else {
      console.log('user not logged in')
      Router.push('/auth/callback')
      return false;
    }
    console.log('after checking user state')
    this.getUsers();
    this.getManages();
  }

  async getUsers()
  {
    let dataUsersTable = [];
    this.setState({
                  dataUsersTable:dataUsersTable
                  })
    const res = await fetch('/subdealer/users')
    const resData = await res.json()
    console.log('data ',resData)
    //TODO need to check data length == 1, becuase dealer level user will have only one manage
    const data = resData.data[0];
    //TODO dealerId should not be undefined && null
    const dealerId = data.dealerId;
    const dealer = data.dealer;
    dealerRecord = dealer;
    const users = dealer.users;
    var snUsers = 1;
    for (var i in users)
    {
      var item = users[i];
      var manages = item.manages;
      var manageSubDealerName = "";
      for(var j in manages)
      {
        var manageSubDealer = manages[j].subDealer;
        manageSubDealerName += manageSubDealer.name+"\n";
      }
      dataUsersTable.push({
          "id" : snUsers,
          "userId": item.id,
          "dealerId":item.dealerId,
          "manageCategoryId":item.manageCategoryId,
          "phoneNumber" : item.phone,
          "name"  : item.firstName+' '+item.lastName,
          "manage" : manageSubDealerName,
          "role" : item.role,
          "status"  : item.status
        });
        snUsers++;
    }

    this.setState({
                  userTableLoading:false,
                  dataUsersTable:dataUsersTable
                  })
  }

  async getManages()
  {
    let dataSubDealersTable = []
    this.setState({
                    dataSubDealersTable:dataSubDealersTable
                  });
    let optionsSubDealer = [{ value: 'all', label: 'All' }]
    const fetchManage = await fetch('/subdealer/manage/fetch/'+this.state.manageCategory+'/subdealer');
    const res = await fetchManage.json();
    console.log('res',res);
    const data = res.data;
    const manage = data.manage;
    var snSubDealer = 1;
    for(var i in manage) {
      var item = manage[i];
      var dealer = item.dealer
      var subDealers = dealer.subDealers;
      for(var j in subDealers)
      {
        var subDealer = subDealers[j];
        optionsSubDealer.push({
          "value":subDealer.id,
          "label":subDealer.name
        });

        dataSubDealersTable.push({
            "id" : snSubDealer,
            "subDealerId":subDealer.id,
            "name"  : subDealer.name,
            "dealer" : dealer.name,
        });
        snSubDealer++;
      }
    }

    this.setState({
                  subDealerTableLoading:false,
                  dataSubDealersTable:dataSubDealersTable,
                  optionsSubDealer:optionsSubDealer
                  })
  }

  async checkDuplicateInObject(propertyName, inputArray) {
    var seenDuplicate = false,
        manageUsersObject = [];

    inputArray.map(function(item) {
      var itemPropertyName = item.users[propertyName];
      if (itemPropertyName in manageUsersObject) {
        manageUsersObject[itemPropertyName].duplicate = true;
        item.duplicate = true;
        seenDuplicate = true;
      }
      else {
        manageUsersObject[itemPropertyName] = item;
        delete item.duplicate;
      }
    });
    //return seenDuplicate;
    return manageUsersObject;
  }

  handleChange(e) {
      const { name, value } = e.target;
      this.setState({ [name]: value });
  }

  handleChangeSubDealerOption = (selectedOption) => {
      var subDealerId = selectedOption.value;
      this.setState({ selectedOption });
      console.log(`Option selected:`, selectedOption);
      console.log('subDealerId',subDealerId)
      //TODO in new bootstrap table 2 version it not change old value in formatter when same id/sn found
      if(undefined != subDealerId && subDealerId == "all")
      {
        this.getUsers();
      }
      else if(undefined != subDealerId)
      {
        //getting users of subDealer
        this.getSubDealerUsers(subDealerId);
      }
    }

    async getSubDealerUsers(subDealerId)
    {
      this.setState({dataUsersTable:[]})
      console.log('inside getSubDealerUsers, subDealerId',subDealerId)
      const fetchSubDealerUsers = await fetch('/subdealer/fetchusersanditsmanages/'+subDealerId);
      const resData = await fetchSubDealerUsers.json()
      let dataUsersTable = []
      var snUsers = 1;
      console.log('subDealerUsers data',resData)
      const data = resData.data;
      for(var i in data) {
        var user = data[i].users
        var manages = user.manages;
        var manageSubDealerName = "";
        for(var j in manages)
        {
          var manageSubDealer = manages[j].subDealer;
          manageSubDealerName += manageSubDealer.name+"\n";
        }
          dataUsersTable.push({
              "id" : snUsers,
              "userId": user.id,
              "dealerId":user.dealerId,
              "manageCategoryId":user.manageCategoryId,
              "phoneNumber" : user.phone,
              "name"  : user.firstName+' '+user.lastName,
              "manage" : manageSubDealerName,
              "role" : user.role,
              "status"  : user.status
            });
            snUsers++;
        }
        this.setState({
                      dataUsersTable:dataUsersTable
                      })
      }


  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  toggleAddUser() {
    console.log('inside toggleAddUser')
    this.setState({
      isOpenModalAddUser: !this.state.isOpenModalAddUser,
    });
  }

  async handleAddUser()
  {
    console.log('inside handleAddUser')
    const dealerItems = this.createSelectDealerItems(dealerRecord);
    const fetchSubDealer = await fetch('/subdealer/fetch/'+dealerRecord.id);
    const resData = await fetchSubDealer.json()
    console.log('subDealer data',resData)
    const subDealer = resData.data;
    let dataManageDropDown = []
    for(var i in subDealer) {
      var item = subDealer[i];
      dataManageDropDown.push({
          "label" : item.name,
          "value"  : item.id,
      });
    }

    this.setState({
                  dataManageDropDown:dataManageDropDown,
                  selectDealer:dealerItems,
                  })
    this.toggleAddUser();
  }

  createSelectDealerItems(dealer)
  {
    console.log('inside createSelectDealerItems, dealer',dealer)
    let items = [];
    if(undefined != dealer)
    {
      items.push(<option key={0} value={dealer.id}>{dealer.name}</option>);
    }
     return items;
  }

  onChangeSubDealers = (from, currentNode, selectedNodes) => {
    console.log('onChangeSubDealers::,from, currentNode, selectedNodes', from, currentNode, selectedNodes)
    selectedNodesManage = selectedNodes;
    const {dataManageDropDown, dataManagedDropDown} = this.state;
    if(from == "fromAddUser")
    {
      for(var i in dataManageDropDown)
      {
        var item = dataManageDropDown[i];
        if(item.value == currentNode.value)
        {
          dataManageDropDown[i].checked = currentNode.checked;
        }
      }
      this.setState({
                    dataManageDropDown:dataManageDropDown
                    })
    }
    else if(from == "fromEditUser") {
      for(var i in dataManagedDropDown)
      {
        var item = dataManagedDropDown[i];
        if(item.value == currentNode.value)
        {
          dataManagedDropDown[i].checked = currentNode.checked;
        }
      }
      this.setState({
                    dataManagedDropDown:dataManagedDropDown
                    })
    }
  }

  handleSaveUser = async ()  =>
  {
    console.log('SubDealer -->> inside handleSaveUser')
    const { phone,firstName,lastName,password,address,city,state,country,zip,role,status,dealer } = this.state;
    let manage = []
    if(this.validateSaveUser())
    {
      console.log('validated success')
      console.log('selectedNodesManage ', selectedNodesManage)

      for(var i in selectedNodesManage) {
          var item = selectedNodesManage[i];
          manage.push({
              "subDealerId" : item.value
            });
        }
      console.log('manage as: ',manage)
      let payload = {
                      phone:'+91'+phone,
                      firstName:firstName,
                      lastName:lastName,
                      password:password,
                      address:address,
                      city:city,
                      state:state,
                      country:country,
                      zipCode:zip,
                      role:role,
                      status:status,
                      dealerId:dealerRecord.id,
                      manageCategory: 'subDealer',
                      userLevel:'subDealer',
                      manage:manage
                    }
      console.log('payload: ',payload)
      console.log('payload: ',JSON.stringify(payload))
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json',
                    'X-CSRF-Token': await NextAuth.csrfToken()
                  },
        body : JSON.stringify(payload)
      };
      const res = await fetch('/subdealer/user/register', requestOptions)
      const data = await res.json()
      console.log('register user data ',data)
      selectedNodesManage = [];
      this.getUsers();
      this.toggleAddUser();
    }
    else
    {
      console.log('validation failed')
    }
  }

  validateSaveUser()
  {
    console.log('SubDealer -->> inside validateSaveUser')
    const { phone,firstName,lastName,password,address,city,state,country,zip,role,status } = this.state;
    console.log('form as',phone,firstName,lastName,password,address,city,state,country,zip,role,status)
    if(undefined != phone && phone.length > 9)
    {
    }
    else {
      alert('please enter valid phone number')
      return false;
    }
    if(undefined != firstName && undefined != lastName)
    {
    }
    else {
      alert('please enter name')
      return false;
    }
    if(undefined != password && password.length > 4)
    {
    }
    else {
      alert('please enter password atleast 5 character')
      return false;
    }
    if(undefined != address && undefined != city && undefined != state && undefined != country)
    {
    }
    else {
      alert('please enter valid address')
      return false;
    }
    if(undefined != zip && zip.length > 5)
    {
    }
    else {
      alert('please enter valid zipcode, alteast 6 digit')
      return false;
    }
    if(undefined != role && role != "select")
    {
    }
    else {
      alert('please select valid role')
      return false;
    }
    if(undefined != status && status != "select")
    {
    }
    else {
      alert('please select valid status')
      return false;
    }
    if(selectedNodesManage.length > 0)
    {
    }
    else {
      alert('please select atleast one manage')
      return false;
    }
    return true;
  }

  toggleEditUser() {
    console.log('inside toggleEditUser')
    this.setState({
      isOpenModalEditUser: !this.state.isOpenModalEditUser,
    });
  }

  async editUser(row) {
    //console.log('inside editUser', e.target.innerHTML)
    console.log('inside editUser, row', row)
    selectedNodesManage = [];
    const userRes = await fetch('/subdealer/user/'+row.userId)
    const user = await userRes.json();
    console.log('user: ',user)
    const manageCategory = "subdealer";
    const manageRes = await fetch('/subdealer/fetchusermanagesandavailablemanages/'+row.userId+'/'+manageCategory+'/'+row.dealerId)
    const manageByUser = await manageRes.json();
    const data = manageByUser.data;
    const allAvailableManages = data.allAvailableManages.docs.docs;
    const manage = data.manage;
    //const usersManage = data.manage;
    //const allCustomer = data.customer;
    console.log('manageByUser: ',manageByUser)
    let dataManagedDropDown = []
    for(var i in allAvailableManages) {
      var item = allAvailableManages[i];
      var itemFound = false;
      for(var j in manage)
      {
        var managedItem = manage[j]
        console.log('subDealer: ',item.id)
        //TODO changed to customerId from subDealerId
        console.log('managedSubDealer ', managedItem.subDealerId)
        //TODO changed to customerId from subDealerId
        if (item.id == managedItem.subDealerId )
        {
         itemFound = true;
        break;
        }
        else {
          itemFound = false;
          continue;
        }

      }
      console.log('itemFound', itemFound)
      if(itemFound)
        {
          dataManagedDropDown.push({
            "label" : item.name,
            "value"  : item.id,
            checked: true
          });
        }
        else {
          dataManagedDropDown.push({
            "label" : item.name,
            "value"  : item.id,
          });
        }

    }
    console.log('dataManagedDropDown:', dataManagedDropDown);
    selectedNodesManage = dataManagedDropDown;
    //
    const roles = this.createSelectRoleItems(user.role)
    const statuses = this.createSelectStatusItems(user.status)
    this.setState({
                  editUserId:row.userId,
                  editFirstName:user.firstName,
                  editLastName:user.lastName,
                  editAddress:user.address,
                  editCity:user.city,
                  editState:user.state,
                  editCountry:user.country,
                  editZip:user.zipCode,
                  editRole:user.role,
                  roles:roles,
                  editStatus:user.status,
                  statuses:statuses,
                  dataManagedDropDown:dataManagedDropDown,
                  editManageCategoryId:row.manageCategoryId
                  })

    this.toggleEditUser();
  }

  createSelectRoleItems(role)
  {
    //TODO roles came from database
    let roles = [{id:'1',name:'admin'},{id:'2',name:'non-admin'}]
    console.log('inside createSelectRoleItems, role, roles',role,roles)
    let items = [];
    if(undefined != role)
    {
      items.push(<option key={0} value={role}>{role}</option>);
      for(var i in roles)
      {
        if(roles[i].name != role)
        {
        items.push(<option key={i} value={roles[i].name}>{roles[i].name}</option>);
        }
      }
  }
  else {
     items = roles;
  }
     return items;
  }

  createSelectStatusItems(status)
  {
    //TODO roles came from database
    let statuses = [{id:'1',name:'active'},{id:'2',name:'inactive'}]
    let items = [];
    if(undefined != status)
    {
      items.push(<option key={0} value={status}>{status}</option>);
      for(var i in statuses)
      {
        if(statuses[i].name != status)
        {
        items.push(<option key={i} value={statuses[i].name}>{statuses[i].name}</option>);
        }
      }
  }
  else {
    items = statuses;
  }
     return items;
  }

  async handleUpdateUser ()
  {
    console.log('inside handleUpdateUser')
    const {editUserId, editManageCategoryId, editFirstName, editLastName, editAddress, editCity, editState, editCountry, editZip, editRole, editStatus} = this.state
    console.log('selectedNodesManage',selectedNodesManage);
    let manage = []
    if(this.validateUpdateUser())
    {
    for(var i in selectedNodesManage) {
        var item = selectedNodesManage[i];
        if(item.checked)
        {
        manage.push({
            "subDealerId" : item.value,
            "name"  : item.label,
          });
        }
      }
    console.log('manage as: ',manage)
    let payload = {
                    firstName:editFirstName,
                    lastName:editLastName,
                    address:editAddress,
                    city:editCity,
                    state:editState,
                    country:editCountry,
                    zipCode:editZip,
                    role:editRole,
                    status:editStatus,
                    manage:manage,
                    manageCategoryId:editManageCategoryId
                  }
    console.log('payload for update user: ',payload)
    console.log('payload: ',JSON.stringify(payload))
    const requestOptions = {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    const res = await fetch('/subdealer/updateuser/'+editUserId, requestOptions)
    const data = await res.json()
    console.log('updated user data ',data)
    selectedNodesManage = [];
    this.getUsers();
    this.toggleEditUser();
    }
    else {
      console.log('validation failed for update user')
    }
  }

  validateUpdateUser()
  {
    console.log('User_Management -->> inside validateSaveUser')
    const {editFirstName, editLastName, editAddress, editCity, editState, editCountry, editZip, editRole, editStatus} = this.state

    if(undefined != editFirstName && undefined != editLastName)
    {
    }
    else {
      alert('please enter name')
      return false;
    }
    if(undefined != editAddress && undefined != editCity && undefined != editState && undefined != editCountry)
    {
    }
    else {
      alert('please enter valid address')
      return false;
    }
    if(undefined != editZip && editZip.length > 5)
    {
    }
    else {
      alert('please enter valid zipcode, alteast 6 digit')
      return false;
    }
    if(undefined != editRole && editRole != "select")
    {
    }
    else {
      alert('please select valid role')
      return false;
    }
    if(undefined != editStatus && editStatus != "select")
    {
    }
    else {
      alert('please select valid status')
      return false;
    }
    if(selectedNodesManage.length > 0)
    {
    }
    else {
      alert('please select atleast one manage')
      return false;
    }
    return true;
  }

  confirmDeleteUser(row)
  {
    deleteUserRow = row;
    console.log('deleteUserRow',deleteUserRow)
    this.toggleDeleteUser();
  }

  toggleDeleteUser() {
    this.setState({
      isOpenModalDeleteUser: !this.state.isOpenModalDeleteUser,
    });
  }

  async deleteUser() {
    const row = deleteUserRow;
    console.log('inside deleteUser, row',row)
    const requestOptions = {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                }
    };
    //TODO need to confirm with user wants to delete or not
    const res = await fetch('/subdealer/deleteuser/'+row.userId, requestOptions)
    const data = await res.json()
    console.log('delete user response data ',data)
    this.getUsers();
    this.toggleDeleteUser();
  }

//////////////

  async handleAddSubDealer()
  {
    console.log('inside handleAddSubDealer')
    const dealerItems = this.createSelectDealerItems(dealerRecord);
    this.setState({selectDealer:dealerItems})
    this.toggleAddSubDealer();
  }

  toggleAddSubDealer()
  {
      console.log('inside toggleAddSubDealer')
      this.setState({
        isOpenModalAddSubDealer: !this.state.isOpenModalAddSubDealer,
      });
  }

  handleSaveSubDealer = async ()  =>
  {
    console.log('SubDealer -->> inside handleSaveSubDealer')
    const { subDealerName } = this.state;
    const dealerId = dealerRecord.id;
    console.log('subDealerName',subDealerName)
    if(undefined != dealerId && undefined != subDealerName && subDealerName != "" )
    {
    let payload = {
                    dealerId: dealerId,
                    name: subDealerName,
                  }
      console.log('payload: ',payload)
      console.log('payload: ',JSON.stringify(payload))
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json',
                    'X-CSRF-Token': await NextAuth.csrfToken()
                  },
        body : JSON.stringify(payload)
      };
      const res = await fetch('/subdealer/create', requestOptions)
      const data = await res.json()
      console.log('register subDealer data ',data)
      this.getManages();
      this.toggleAddSubDealer();
    }
    else {
      alert('please enter valid subdealer name')
    }
  }


  toggleEditSubDealer() {
    console.log('inside toggleEditSubDealer')
    this.setState({
      isOpenModalEditSubDealer: !this.state.isOpenModalEditSubDealer,
    });
  }

  async editSubDealer(row)
  {
    console.log('inside editSubDealer, row',row)
    const subDealerRes = await fetch('/subdealer/'+row.subDealerId)
    const subDealer = await subDealerRes.json();
    console.log('subDealer: ',subDealer)
    this.setState({
                  editSubDealerName:subDealer.name,
                  editSubDealerId:subDealer.id
    })
    this.toggleEditSubDealer();
  }

  async handleUpdateSubDealer ()
  {
    console.log('inside handleUpdateSubDealer')
    const {editSubDealerName, editSubDealerId} = this.state;
    console.log('edited subDealerName',editSubDealerName, editSubDealerId)
    if(undefined != editSubDealerName && editSubDealerName != "")
    {
    let payload = {
                  name:editSubDealerName
                  }
    const requestOptions = {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    console.log('payload: ',JSON.stringify(payload))
    const res = await fetch('/subdealer/'+editSubDealerId, requestOptions)
    const data = await res.json()
    console.log('updated subDealer data ',data)
    this.getManages();
    this.getUsers();
    this.toggleEditSubDealer();
    }
    else {
      alert('please enter valid subdealer name')
    }

  }

  confirmDeleteSubDealer(row)
  {
    deleteSubDealerRow = row;
    console.log('deleteSubDealerRow',deleteSubDealerRow)
    this.toggleDeleteSubDealer();
  }

  toggleDeleteSubDealer() {
    this.setState({
      isOpenModalDeleteSubDealer: !this.state.isOpenModalDeleteSubDealer,
    });
  }

  async deleteSubDealer()
  {
    const row = deleteSubDealerRow;
    console.log('inside deleteSubDealer, row',row)
    const requestOptions = {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                }
    };
    const res = await fetch('/subdealer/'+row.subDealerId, requestOptions)
    const data = await res.json()
    console.log('delete subDealer response data ',data)
    //TODO if delete success:false then show msg came from server
    this.getManages();
    this.getUsers();
    this.toggleDeleteSubDealer();
  }

  render() {
        console.log('SubDealer -->> inside render')
        const { selectedOption,isClearable,phone,firstName,lastName,password,address,city,state,country,zip } = this.state;
        const {editFirstName, editLastName, editAddress, editCity, editState, editCountry, editZip} = this.state
        const {subDealerName, editSubDealerName} = this.state;
        //const editFormatter = (cell, row, rowIndex, formatExtraData) => { console.log('editFormatter, row ',row); return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editUser.bind(this, row)}>Edit</Button> ); }
        //const editFormatter = (cell, row, rowIndex, formatExtraData) => { console.log('cell, row, rowIndex, formatExtraData=',cell,'::', row,'::', rowIndex,'::', formatExtraData); return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editUser.bind(this, row)}>Edit</Button> ); }
        const editFormatter = (cell, row, rowIndex, formatExtraData) => { return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editUser.bind(this, row)}>Edit</Button> ); }

        const deleteFormatter = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.confirmDeleteUser.bind(this, row)}>Delete</Button> ); }

        const editFormatterSubDealer = (cell, row, rowIndex, formatExtraData) => { return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editSubDealer.bind(this, row)}>Edit</Button> ); }

        const deleteFormatterSubDealer = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.confirmDeleteSubDealer.bind(this, row)}>Delete</Button> ); }

        const columnsUsersTable = [{
          dataField: 'id',
          text: 'SN',
          sort: true
        }, {
          dataField: 'phoneNumber',
          text: 'Phone Number',
          filter: textFilter()
        }, {
          dataField: 'name',
          text: 'Name',
          sort: true,
          filter: textFilter()
        }, {
          dataField: 'role',
          text: 'Role'
        }, {
          dataField: 'status',
          text: 'Status'
        }, {
          dataField: 'manage',
          text: 'Manage',
          filter: textFilter()
        }, {
          dataField: 'lastLogin',
          text: 'Last Login',
          hidden: true
        }, {
          dataField: 'edit',
          text: '',
          formatter: editFormatter,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          }
        }, {
          dataField: 'delete',
          text: '',
          formatter: deleteFormatter,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          },
          /*
          events: {
            onClick: e => { this.deleteUser() }
          }
          */
        }];

        const columnsSubDealersTable = [{
          dataField: 'id',
          text: 'SN',
          sort: true
        }, {
          dataField: 'name',
          text: 'Name',
          sort: true,
          filter: textFilter()
        }, {
          dataField: 'dealer',
          text: 'Dealer'
        }, {
          dataField: 'edit',
          text: '',
          formatter: editFormatterSubDealer,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          }
        }, {
          dataField: 'delete',
          text: '',
          formatter: deleteFormatterSubDealer,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          },
          /*
          events: {
            onClick: e => { this.deleteUser() }
          }
          */
        }];

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12">
            <Card>
              <CardBody>
                <p><b>SubDealer</b></p>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs="12" className="mb-4">
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '1' })}
                  onClick={() => { this.toggle('1'); }}
                >
                  User Management
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '2' })}
                  onClick={() => { this.toggle('2'); }}
                >
                  SubDealer Management
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={this.state.activeTab}>
              <TabPane tabId="1">
              <Row>
                <Col>
                  <Card>
                    <CardBody>
                    <Modal isOpen={this.state.isOpenModalAddUser} toggle={this.toggleAddUser}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleAddUser}>Add New User</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Phone</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="text-input" name="phone" placeholder="Phone" value={phone} onChange={this.handleChange} />
                              {
                                //<FormText color="muted">This is a help text</FormText>
                              }
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">First Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="firstName" name="firstName" placeholder="First Name" value={firstName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Last Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="lastName" name="lastName" placeholder="Last Name" value={lastName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Password</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="password" id="password" name="password" placeholder="Password" value={password} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Address</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="address" name="address" placeholder="Address" value={address} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">City</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="city" name="city" placeholder="City" value={city} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">State</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="state" name="state" placeholder="State" value={state} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Country</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="country" name="country" placeholder="Country" value={country} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Zip</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="zip" name="zip" placeholder="Zip" value={zip} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Role</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="role" id="selectRole" onChange={this.handleChange}>
                                <option value="select">Please Select</option>
                                <option value="admin">Admin</option>
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Status</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="status" id="selectStatus" onChange={this.handleChange}>
                                <option value="select">Please Select</option>
                                <option value="active">Active</option>
                                <option value="inactive">InActive</option>
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Dealer</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="dealer" id="selectDealer" onChange={this.handleChange}>
                              {
                                this.state.selectDealer
                              }
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Manage SubDealer</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <DropdownTreeSelect
                                data={this.state.dataManageDropDown}
                                onChange={this.onChangeSubDealers.bind(this, 'fromAddUser')}
                                onAction={onAction}
                                onNodeToggle={onNodeToggle}
                              />
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleSaveUser}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleAddUser}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalEditUser} toggle={this.toggleEditUser}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleEditUser}>Edit User</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">First Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="firstName" name="editFirstName" placeholder="First Name" value={editFirstName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Last Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="lastName" name="editLastName" placeholder="Last Name" value={editLastName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Address</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="address" name="editAddress" placeholder="Address" value={editAddress} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">City</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="city" name="editCity" placeholder="City" value={editCity} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">State</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="state" name="editState" placeholder="State" value={editState} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Country</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="country" name="editCountry" placeholder="Country" value={editCountry} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Zip</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="zip" name="editZip" placeholder="Zip" value={editZip} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Role</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="editRole" id="selectRole" onChange={this.handleChange}>
                                {
                                this.state.roles
                                }
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Status</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="editStatus" id="selectStatus" onChange={this.handleChange}>
                              {
                                this.state.statuses
                              }
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Manage</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <DropdownTreeSelect
                                data={this.state.dataManagedDropDown}
                                onChange={this.onChangeSubDealers.bind(this, 'fromEditUser')}
                                onAction={onAction}
                                onNodeToggle={onNodeToggle}
                              />
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleUpdateUser}>Update</Button>{' '}
                        <Button color="secondary" onClick={this.toggleEditUser}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalDeleteUser} toggle={this.toggleDeleteUser}
                           className={'modal-danger ' + this.props.className}>
                      <ModalHeader toggle={this.toggleDeleteUser}>User Delete</ModalHeader>
                      <ModalBody>
                        Are you sure to delete user
                      </ModalBody>
                      <ModalFooter>
                        <Button color="danger" onClick={this.deleteUser}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteUser}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                      {
                      this.state.userTableLoading &&
                      <div>
                      <div style={{marginLeft: '40%'}}>
                      <ClipLoader
                        sizeUnit={"px"}
                        size={50}
                        color={'#123abc'}
                        loading={this.state.userTableLoading}
                      />
                      </div>
                      <div style={{marginLeft: '35%'}}>
                      <p>Loading ... Please wait!</p>
                      </div>
                      </div>
                      }
                      { !this.state.userTableLoading &&
                      <ToolkitProvider
                            keyField="id"
                            data={ this.state.dataUsersTable }
                            columns={ columnsUsersTable }
                            search
                          >
                            {
                              props => (
                                <div>
                                  <Row className="align-items-center">
                                  <Col>
                                  <Col md="4">
                                  <p>SubDealer</p>
                                  <Select
                                    onChange={this.handleChangeSubDealerOption}
                                    defaultValue={this.state.optionsSubDealer[0]}
                                    isSearchable={true}
                                    isClearable={isClearable}
                                    options={this.state.optionsSubDealer}
                                    placeholder={'Select SubDealer'}
                                    backspaceRemovesValue={false}
                                  />
                                  </Col>
                                  </Col>

                                  <Col md="2">
                                  {
                                    //<h4>Input something at below input field:</h4>
                                  }
                                  <SearchBar
                                    { ...props.searchProps }
                                  />
                                  </Col>
                                  <Col md="2">
                                    <Button block color="primary" className="btn-pill" onClick={this.handleAddUser}>Add User</Button>
                                  </Col>
                                  </Row>
                                  <hr />
                                  <div style={{overflow: 'overlay'}}>
                                  <BootstrapTable
                                    { ...props.baseProps }
                                    filter={ filterFactory() }
                                    hover
                                  />
                                  </div>
                                </div>
                              )
                            }
                          </ToolkitProvider>
                        }
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="2">
              <Row>
                <Col>
                  <Card>
                    <CardBody>
                    <Modal isOpen={this.state.isOpenModalAddSubDealer} toggle={this.toggleAddSubDealer}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleAddSubDealer}>Add New SubDealer</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Dealer</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Input type="select" name="dealer" id="selectDealer" onChange={this.handleChange}>
                            {
                              this.state.selectDealer
                            }
                            </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="text-input" name="subDealerName" placeholder="Name" value={subDealerName} onChange={this.handleChange} />
                              {
                                //<FormText color="muted">This is a help text</FormText>
                              }
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleSaveSubDealer}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleAddSubDealer}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalEditSubDealer} toggle={this.toggleEditSubDealer}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleEditSubDealer}>Edit SubDealer</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="text-input" name="editSubDealerName" placeholder="Name" value={editSubDealerName} onChange={this.handleChange} />
                              {
                                //<FormText color="muted">This is a help text</FormText>
                              }
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleUpdateSubDealer}>Update</Button>{' '}
                        <Button color="secondary" onClick={this.toggleEditSubDealer}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalDeleteSubDealer} toggle={this.toggleDeleteSubDealer}
                           className={'modal-danger ' + this.props.className}>
                      <ModalHeader toggle={this.toggleDeleteSubDealer}>SubDealer Delete</ModalHeader>
                      <ModalBody>
                        Are you sure to delete subdealer
                      </ModalBody>
                      <ModalFooter>
                        <Button color="danger" onClick={this.deleteSubDealer}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteSubDealer}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    {
                    this.state.subDealerTableLoading &&
                    <div>
                    <div style={{marginLeft: '40%'}}>
                    <ClipLoader
                      sizeUnit={"px"}
                      size={50}
                      color={'#123abc'}
                      loading={this.state.subDealerTableLoading}
                    />
                    </div>
                    <div style={{marginLeft: '35%'}}>
                    <p>Loading ... Please wait!</p>
                    </div>
                    </div>
                    }
                    { !this.state.subDealerTableLoading &&
                    <ToolkitProvider
                          keyField="id"
                          data={ this.state.dataSubDealersTable }
                          columns={ columnsSubDealersTable }
                          search
                        >
                          {
                            props => (
                              <div>
                                <Row className="align-items-center">
                                <Col>
                                <p className="text-info"></p>
                                </Col>
                                <Col md="2">
                                {
                                  //<h4>Input something at below input field:</h4>
                                }
                                <SearchBar
                                  { ...props.searchProps }
                                />
                                </Col>
                                <Col md="2">
                                  <Button block color="primary" className="btn-pill" onClick={this.handleAddSubDealer}>Add SubDealer</Button>
                                </Col>
                                </Row>
                                <hr />
                                <div style={{overflow: 'overlay'}}>
                                <BootstrapTable
                                  { ...props.baseProps }
                                  filter={ filterFactory() }
                                  hover
                                />
                                </div>
                              </div>
                            )
                          }
                        </ToolkitProvider>
                        }
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </TabPane>
            </TabContent>
          </Col>
        </Row>
      </div>
    );
  }
}

export default SubDealer;
