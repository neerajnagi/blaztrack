import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Form,
  FormGroup,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Label
} from 'reactstrap';

import classnames from 'classnames';

import { NextAuth } from 'next-auth/client'

import "moment-duration-format";
import moment from "moment";
import { format } from "d3-format";
import _ from "underscore";

//TODO for datetime
import * as Datetime from 'react-datetime';
import 'react-datetime/css/react-datetime.css'

//
// Pond
import { TimeSeries, TimeRange, avg, percentile, median } from "pondjs";

import {
    AreaChart,
    Baseline,
    BoxChart,
    Brush,
    ChartContainer,
    ChartRow,
    Charts,
    LabelAxis,
    LineChart,
    Resizable,
    ValueAxis,
    YAxis,
    styler,
    Legend,
    EventMarker,
    ScatterChart,
} from "react-timeseries-charts";

//TODO start react-timeseries-charts

// Data
//const temperatures = require("./climate_data.json");

const ddosData = require("./data.json");

const requests = [];

let requestsSeries;

/*
_.each(ddosData, val => {
    const timestamp = moment(new Date(`2015-04-03T${val["time PST"]}`));
    const httpRequests = val["http requests"];
    requests.push([timestamp.toDate().getTime(), httpRequests]);
});

const requestsSeries = new TimeSeries({
    name: "requests",
    columns: ["time", "requests"],
    points: requests
});

*/

//
// Styles
//

const style = styler([
    { key: "requests", color: "#9467bd", width: 1 }
]);

const NullMarker = props => {
    return <g />;
};

//TODO end react-timeseries-charts

class GraphTest extends Component {
    constructor(props) {
      super(props);
      console.log('GraphTest -->>, props',this.props)
      this.state = {
                    max: 6000,
                    active: {
                    requests: false
                    },
                    timerange: null,
                    tracker: null,
                    trackerValue: "",
                    trackerEvent: null,
                    markerMode: "flag",
                    hover: null,
                    highlight: null,
                    selection: null,
                    graphStartDateTime: new Date(),
                    graphEndDateTime: new Date()
                  }
    this.handleRescale = _.debounce(this.rescale, 300);

    }

    rescale(timerange, active = this.state.active) {
      console.log('inside rescale')
      if(this.state.active.requests)
      {
        let max = 100;
        const maxRequests = requestsSeries.crop(this.state.timerange).max("requests");
        if (maxRequests > max && active.requests) max = maxRequests;
        this.setState({ max });
      }
    }

    handleSelectStartDateTime(date)
    {
      console.log('inside handleSelectStartDateTime')
      let dateTime = date._d;
      console.log('inside handleSelectDateTime, selected datetime',dateTime)
      let dateTimeFormatted = moment(dateTime).format("YYYY-MM-DDTHH:mm:ss.SSS[Z]")
      console.log('dateTimeFormatted',dateTimeFormatted)
      this.setState({
                      graphStartDateTime:dateTimeFormatted
                  })
    }

    handleSelectEndDateTime(date)
    {
      console.log('inside handleSelectEndDateTime')
      let dateTime = date._d;
      console.log('inside handleSelectDateTime, selected datetime',dateTime)
      let dateTimeFormatted = moment(dateTime).format("YYYY-MM-DDTHH:mm:ss.SSS[Z]")
      console.log('dateTimeFormatted',dateTimeFormatted)
      this.setState({
                      graphEndDateTime:dateTimeFormatted
                  })
    }

    showGraph()
    {
      console.log('show graph');
      const customerId = "5b6053fe17cefc25afb7b954";
      const vehicleId = "5b6057cfa2ea1b2935bb1881";
      const {graphStartDateTime, graphEndDateTime} = this.state;
      console.log('graphStartDateTime', graphStartDateTime)
      console.log('graphEndDateTime', graphEndDateTime)
      const startTimestamp = Math.round(new Date(graphStartDateTime).getTime())
      const endTimestamp = Math.round(new Date(graphEndDateTime).getTime())
      //TODO getting graph data from cassandra for showing graph
      this.getGraphData(customerId, vehicleId, startTimestamp, endTimestamp)
    }

    async getGraphData(customerId, vehicleId, startTimestamp, endTimestamp)
    {
      requestsSeries = null;
      let requests = [];
      console.log('inside getGraphData')
      var d_1 = new Date();
      var utc_1 = d_1.toUTCString();
      console.log('start time :',utc_1)
      //TODO need to remove and use came from function parameter
      const custId = '5b6053fe17cefc25afb7b954';
      const vehId = '5b6057cfa2ea1b2935bb1881'
      const startDT = startTimestamp;
      const endDT = endTimestamp;
      const fetchGraphData = await fetch('/vehicle/fetchgraphdata/'+custId+'/'+vehId+'/'+startDT+'/'+endDT);
      var d_2 = new Date();
      var utc_2 = d_2.toUTCString();
      console.log('end time :',utc_2)
      const resGraphData = await fetchGraphData.json();
      var d_3 = new Date();
      var utc_3 = d_3.toUTCString();
      console.log('after convert res to josn, end time :',utc_3)
      console.log('resGraphData',resGraphData);
      const data = resGraphData.data;
      console.log('data length',data.length);
      var d_4 = new Date();
      var utc_4 = d_4.toUTCString();
      console.log('before loop, start time :',utc_4)
      for(var i in data)
      {
        var item = data[i];
        console.log('insert_datetime',item.insert_datetime)
        const timestamp = moment(new Date(item.insert_datetime));
        const value = item.a6;
        console.log('timestamp',timestamp.toDate().getTime())
        requests.push([timestamp.toDate().getTime(), value]);
        //console.log('requests[i]',requests[i])
      }
      var d_5 = new Date();
      var utc_5 = d_5.toUTCString();
      console.log('after loop, end time :',utc_5)
      console.log('requests',requests[0], requests[data.length - 1])
      var d_6 = new Date();
      var utc_6 = d_6.toUTCString();
      console.log('before TimeSeries create, start time :',utc_6)
      requestsSeries = new TimeSeries({
          name: "requests",
          columns: ["time", "requests"],
          points: requests
      });
      var d_7 = new Date();
      var utc_7 = d_7.toUTCString();
      console.log('after TimeSeries create, end time :',utc_7)
      this.setState({
                    timerange:requestsSeries.range(),
                    active:{requests:true}
                  })
      this.rescale();
      var d_8 = new Date();
      var utc_8 = d_8.toUTCString();
      console.log('after seState timerange, end time :',utc_8)
    }

    handleTimeRangeChange = timerange => {
        console.log('inside handleTimeRangeChange, timerange',timerange)
        this.setState({ timerange });
        this.handleRescale(timerange);
    };

    handleActiveChange = key => {
        console.log('inside handleActiveChange')
        const active = this.state.active;
        active[key] = !active[key];
        this.setState({ active });
        this.handleRescale(this.state.timerange, active);
    };

    // wind scatter example

    handleSelectionChanged = point => {
        console.log('inside handleSelectionChanged, point', point)
        this.setState({
            selection: point
        });
    };

    handleMouseNear = point => {
        console.log('inside handleMouseNear, point',point)
        this.setState({
            highlight: point
        });
    };

    //


    // climate chart example

    handleTrackerChanged = t => {
        if (t) {
            console.log('inside handleTrackerChanged')
            if(this.state.active.requests)
            {
            const e = requestsSeries.atTime(t);
            const eventTime = new Date(
                e.begin().getTime() + (e.end().getTime() - e.begin().getTime()) / 2
            );
            const eventValue = e.get("temperature");
            const v = `${eventValue > 0 ? "+" : ""}${eventValue}°C`;
            console.log('eventValue , eventTime', eventValue, eventTime)
            this.setState({ tracker: eventTime, trackerValue: v, trackerEvent: e });
            }
        } else {
            this.setState({ tracker: null, trackerValue: null, trackerEvent: null });
        }
    };

    renderMarker = () => {
        console.log('inside renderMarker')
        if (!this.state.tracker) {
            return <NullMarker />;
        }
        if (this.state.markerMode === "flag") {
            return (
                <EventMarker
                    type="flag"
                    axis="axis1"
                    event={this.state.trackerEvent}
                    column="requests"
                    info={[{ label: "Request", value: this.state.trackerValue }]}
                    infoTimeFormat="%Y"
                    infoWidth={120}
                    markerRadius={2}
                    markerStyle={{ fill: "black" }}
                />
            );
        }
    };



    //

    renderChart = () => {
        console.log('inside renderChart')
        let charts = [];
        let max = 1000;
        /*
        if (this.state.active.requests) {
            const maxRequests = requestsSeries.crop(this.state.timerange).max("requests");
            if (maxRequests > max) max = maxRequests;
            charts.push(
                <LineChart
                    key="requests"
                    axis="axis1"
                    series={requestsSeries}
                    columns={["requests"]}
                    style={style}
                />
            );
        }
        */

        const axisStyle = {
            values: {
                labelColor: "grey",
                labelWeight: 100,
                labelSize: 11
            },
            axis: {
                axisColor: "grey",
                axisWidth: 1
            }
        };

        const darkAxis = {
            label: {
                stroke: "none",
                fill: "#AAA", // Default label color
                fontWeight: 200,
                fontSize: 14,
                font: '"Goudy Bookletter 1911", sans-serif"'
            },
            values: {
                stroke: "none",
                fill: "#888",
                fontWeight: 100,
                fontSize: 11,
                font: '"Goudy Bookletter 1911", sans-serif"'
            },
            ticks: {
                fill: "none",
                stroke: "#AAA",
                opacity: 0.2
            },
            axis: {
                fill: "none",
                stroke: "#AAA",
                opacity: 1
            }
        };

        // wind scatter example

          const highlight = this.state.highlight;
          const formatter = format(".2f");
          let text = `Speed: - mph, time: -:--`;
          let infoValues = [];
          if (highlight) {
              const speedText = `${formatter(highlight.event.get(highlight.column))} mph`;
              text = `
                  Speed: ${speedText},
                  time: ${this.state.highlight.event.timestamp().toLocaleTimeString()}
              `;
              infoValues = [{ label: "Speed", value: speedText }];
          }

        //

        return (
          this.state.active.requests &&
            <ChartContainer
                title="Graph"
                style={{
                    background: "#201d1e",
                    borderRadius: 8,
                    borderStyle: "solid",
                    borderWidth: 1,
                    borderColor: "#232122"
                }}
                timeAxisStyle={darkAxis}
                titleStyle={{
                    color: "#EEE",
                    fontWeight: 500
                }}
                padding={20}
                paddingTop={5}
                paddingBottom={0}
                enableDragZoom
                onTimeRangeChanged={this.handleTimeRangeChange}
                timeRange={this.state.timerange}
                maxTime={this.state.active.requests ? requestsSeries.range().end() : 0}
                minTime={this.state.active.requests ? requestsSeries.range().begin() : 0}
            >
                <ChartRow height="300">
                    <YAxis
                        id="axis1"
                        label="Requests"
                        showGrid
                        hideAxisLine
                        transition={300}
                        style={darkAxis}
                        labelOffset={-10}
                        min={0}
                        max={this.state.max}
                        format=",.0f"
                        width="60"
                        type="linear"
                    />
                    <Charts>
                      <LineChart
                          key="requests"
                          axis="axis1"
                          series={requestsSeries}
                          columns={["requests"]}
                          style={style}
                      />
                      <ScatterChart
                            axis="axis1"
                            series={requestsSeries}
                            columns={["requests"]}
                            style={style}
                            info={infoValues}
                            infoTimeFormat="%d-%m-%Y %I:%M %p"
                            infoHeight={28}
                            infoWidth={110}
                            infoStyle={{
                                fill: "black",
                                color: "#DDD"
                              }}
                            selected={this.state.selection}
                            onSelectionChange={p => this.handleSelectionChanged(p)}
                            onMouseNear={p => this.handleMouseNear(p)}
                            highlight={this.state.highlight}
                        />
                    </Charts>
                </ChartRow>
            </ChartContainer>
        );
    };

    render() {
    console.log('GraphTest -->> inside render')

      return (
        <div className="animated fadeIn">
          <Row>
            <Col xs="12">
              <Card>
                <CardBody>
                  <Row>
                  <Col>
                  <Row>
                  <Col md="3">
                  <Label>Start Date Time</Label>
                  <Datetime
                    defaultValue = {new Date()}
                    onChange= {this.handleSelectStartDateTime.bind(this)}
                    dateFormat = "DD-MM-YYYY"
                   />
                  </Col>
                  <Col md="3">
                  <Label>End Date Time</Label>
                  <Datetime
                    defaultValue = {new Date()}
                    onChange= {this.handleSelectEndDateTime.bind(this)}
                    dateFormat = "DD-MM-YYYY"
                   />
                  </Col>
                  <Col md="2">
                  <Button style={{marginTop:'35px'}} color="primary" className="btn-pill" size="sm" onClick={this.showGraph.bind(this)}>View Graph</Button>
                  </Col>
                  </Row>
                  </Col>
                  </Row>
                  <Row>
                  <Col style={{marginTop:'15px'}} md="8">
                  {
                    this.state.active.requests && <Resizable>{this.renderChart()}</Resizable>
                  }
                  </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      );

    }

  }

  export default GraphTest;
