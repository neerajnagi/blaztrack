import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Form,
  FormGroup,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Label,
  FormText
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'

import Select from 'react-select';
import classnames from 'classnames';
//react-bootstrap-table (deprecated)
//import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
//react-bootstrap-table2
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import cellEditFactory from 'react-bootstrap-table2-editor';
//For table search
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
// react-table not working properly (failing)
//import ReactTable from 'react-table'
//import FilterableTable from 'react-filterable-table'

import DropdownTreeSelect from 'react-dropdown-tree-select'
import 'react-dropdown-tree-select/dist/styles.css'

import PropTypes from 'prop-types';

let selectedNodesManage = [];

const { SearchBar } = Search;

const dataManageDropDown = [{
  label: 'search me',
  value: 'searchme',
  children: [
    {
      label: 'search me too',
      value: 'searchmetoo',
      children: [
        {
          label: 'No one can get me',
          value: 'anonymous'
        }
      ]
    }
  ]
},
{
  label:'second',
  value:'second'
},
{
  label:'third',
  value:'third'
},
{
  label:'fourth',
  value:'fourth',
  checked:true
},
]
const onChange = (currentNode, selectedNodes) => {
  console.log('onChange::', currentNode, selectedNodes)
  selectedNodesManage = selectedNodes;
}
const onAction = ({ action, node }) => {
  console.log(`onAction:: [${action}]`, node)
}
const onNodeToggle = currentNode => {
  console.log('onNodeToggle::', currentNode)
}

class QualityRanger extends React.Component {
  static propTypes = {
    value: PropTypes.number,
    onUpdate: PropTypes.func.isRequired
  }
  static defaultProps = {
    value: 0
  }
  getValue() {
    return parseInt(this.range.value, 10);
  }
  render() {
    const { value, onUpdate, ...rest } = this.props;
    return [
      <Input
        { ...rest }
        key="range"
        ref={ node => this.range = node }
        type="range"
        min="0"
        max="100"
      />,
      <Button
        key="submit"
        className="btn btn-default"
        onClick={ () => onUpdate(this.getValue()) }
      >
        done
      </Button>
    ];
  }
}

const optionsDealer = [
  { value: 'all', label: 'All' },
  { value: 'dealer1', label: 'Dealer1' },
  { value: 'dealer2', label: 'Dealer2' },
  { value: 'dealer3', label: 'Dealer3' },
  { value: 'dealer4', label: 'Dealer4' },
];

const selectRow = {
  mode: 'checkbox',
  //clickToSelect: true,
  //Disable single row selection
  clickToSelect: false,
  onSelect: (row, isSelect, rowIndex, e) => {
    console.log(row.id);
    console.log(isSelect);
    console.log(row);
    console.log(rowIndex);
    console.log(e);
  },
  onSelectAll: (isSelect, rows, e) => {
    console.log(isSelect);
    console.log(rows);
    console.log(e);
  }
};

const divStyle = {
  margin: '40px',
  border: '5px solid pink'
};

//
const dataBootstrapTable = [
  { id: 1, phoneNumber: '9799605400', name:'Ashwin', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
  { id: 2, phoneNumber: '9024038439', name:'Raji', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
  { id: 3, phoneNumber: '9024038434', name:'Aman', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
  { id: 4, phoneNumber: '9024038433', name:'Akshit', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
];

const dataUsersTable = [
];
const dataSubDealersTable = [
];

let deleteRow;

//const editFormatter = (cell, row, rowIndex, formatExtraData) => { console.log('editFormatter, row ',row); return ( <Button block color="primary" className="btn-pill" size="sm" >Edit</Button> ); }
//const deleteFormatter = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm">Delete</Button> ); }

class Dealer extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleAddUser = this.toggleAddUser.bind(this);
    this.toggleEditUser = this.toggleEditUser.bind(this);
    this.searchKeyPress = this.searchKeyPress.bind(this);
    this.editUser = this.editUser.bind(this);
    this.toggleDeleteUser = this.toggleDeleteUser.bind(this);
    this.confirmDeleteUser = this.confirmDeleteUser.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
    this.handleAddUser = this.handleAddUser.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSaveUser = this.handleSaveUser.bind(this);
    this.handleUpdateUser = this.handleUpdateUser.bind(this);
    this.handleAddSubDealer = this.handleAddSubDealer.bind(this);
    this.toggleAddSubDealer = this.toggleAddSubDealer.bind(this);
    this.handleSaveSubDealer = this.handleSaveSubDealer.bind(this);
    this.editSubDealer = this.editSubDealer.bind(this);
    this.toggleEditSubDealer = this.toggleEditSubDealer.bind(this);
    this.handleUpdateSubDealer = this.handleUpdateSubDealer.bind(this);
    this.deleteSubDealer = this.deleteSubDealer.bind(this);
    this.openAdmin = this.openAdmin.bind(this);
    this.closeAdmin = this.closeAdmin.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
      selectedOption: null,
      isClearable: true,
      activeTab: '1',
      isOpenModalAddUser: false,
      isOpenModalEditUser: false,
      isOpenModalDeleteUser: false,
      isOpenModalAddSubDealer: false,
      isOpenModalEditSubDealer: false,
      dataManageDropDown:dataManageDropDown,
      dataUsersTable:dataUsersTable,
      dataSubDealersTable:dataSubDealersTable,
      isAdmin:false
    };
  }

  async componentDidMount() {
    console.log('Dealer -->> inside componentDidMount')
    //getting users
    this.getUsersAndSubDealers();

  }

  openAdmin()
  {
    this.setState({isAdmin:true})
  }

  closeAdmin()
  {
    this.setState({isAdmin:false})
  }

  async getUsersAndSubDealers()
  {
    const res = await fetch('http://159.65.6.196:5500/api/users/userofdealer/5b60474f2b9ec018356bbca0')
    const resData = await res.json()
    console.log('users and subDealer ',resData)
    //{ id: 1, phoneNumber: '9799605400', name:'Ashwin', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
    let dataUsersTable = []
    let dataSubDealersTable = []
    const data = resData.data;
    const manage = data.manage
    const subDealer = data.subDealer
    const nonDublicate = await this.checkDuplicateInObject('phone',manage)
    console.log('nonDublicate',nonDublicate)
    var snUsers = 1;
    for (var i in nonDublicate){
      if (nonDublicate.hasOwnProperty(i))
      {
           var item = nonDublicate[i].users;
           console.log("Key is " + i);
           dataUsersTable.push({
               "id" : snUsers,
               "userId": item.id,
               "phoneNumber" : item.phone,
               "name"  : item.firstName+' '+item.lastName,
               "role" : item.role,
               "status"  : item.status,
               "lastLogin"  : '15 Aug 2018',
               "edit" : 'Edit',
               "delete" : 'Delete'
           });
           snUsers++;
      }
    }

    for(var j in subDealer) {
      var item = subDealer[j];
      dataSubDealersTable.push({
          "id" : ++j,
          "subDealerId":item.id,
          "name"  : item.name,
          "edit" : 'Edit',
          "delete" : 'Delete'
      });
    }

    this.setState({
                  dataUsersTable:dataUsersTable,
                  dataSubDealersTable:dataSubDealersTable
                  })
  }

  async checkDuplicateInObject(propertyName, inputArray) {
    var seenDuplicate = false,
        manageUsersObject = [];

    inputArray.map(function(item) {
      var itemPropertyName = item.users[propertyName];
      if (itemPropertyName in manageUsersObject) {
        manageUsersObject[itemPropertyName].duplicate = true;
        item.duplicate = true;
        seenDuplicate = true;
      }
      else {
        manageUsersObject[itemPropertyName] = item;
        delete item.duplicate;
      }
    });
    //return seenDuplicate;
    return manageUsersObject;
  }

  handleChange(e) {
      const { name, value } = e.target;
      this.setState({ [name]: value });
  }

  handleChangeSelectOption = (selectedOption) => {
      this.setState({ selectedOption });
      console.log(`Option selected:`, selectedOption);
    }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  toggleAddUser() {
    console.log('inside toggleAddUser')
    this.setState({
      isOpenModalAddUser: !this.state.isOpenModalAddUser,
    });
  }

  async handleAddUser()
  {
    console.log('inside handleAddUser')
    //getting all subDealer of a Dealer
    const res = await fetch('http://159.65.6.196:5500/api/subdealer?filter[where][dealerId]=5b60474f2b9ec018356bbca0')
    const data = await res.json()
    console.log('manage subDealer ',data)
    let dataManageDropDown = []
    for(var i in data) {
      var item = data[i];
      dataManageDropDown.push({
          "label" : item.name,
          "value"  : item.id,
      });
    }
    this.setState({dataManageDropDown:dataManageDropDown})
    this.toggleAddUser();
  }

  handleSaveUser = async ()  =>
  {
    console.log('Dealer -->> inside handleSaveUser')
    const { phone,firstName,lastName,password,address,city,state,country,zip,role,status } = this.state;
    let manage = []
    if(this.validateSaveUser())
    {
      console.log('validated success')
      console.log('selectedNodesManage ', selectedNodesManage)

      for(var i in selectedNodesManage) {
          var item = selectedNodesManage[i];
          manage.push({
              "subDealerId" : item.value
            });
        }
      console.log('manage as: ',manage)
      let payload = {
                      phone:'+91'+phone,
                      firstName:firstName,
                      lastName:lastName,
                      password:password,
                      address:address,
                      city:city,
                      state:state,
                      country:country,
                      zipCode:zip,
                      role:role,
                      status:status,
                      manageCategory: 'subDealer',
                      userLevel:'dealer',
                      manage:manage,
                      //manageCategoryId: '5b5f07ac31a8317acef6503d'
                    }
      console.log('payload: ',payload)
      console.log('payload: ',JSON.stringify(payload))
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
        //body: JSON.stringify({phone, password})
      };
      const res = await fetch('http://159.65.6.196:5500/api/users/register', requestOptions)
      const data = await res.json()
      console.log('register user data ',data)
      selectedNodesManage = [];
      this.getUsersAndSubDealers();
      this.toggleAddUser();
    }
    else
    {
      console.log('validation failed')
    }
  }

  validateSaveUser()
  {
    console.log('Dealer -->> inside validateSaveUser')
    const { phone,firstName,lastName,password,address,city,state,country,zip,role,status } = this.state;
    console.log('form as',phone,firstName,lastName,password,address,city,state,country,zip,role,status)
    if(undefined != phone && phone.length > 9)
    {
    }
    else {
      alert('please enter valid phone number')
      return false;
    }
    if(undefined != firstName && undefined != lastName)
    {
    }
    else {
      alert('please enter name')
      return false;
    }
    if(undefined != password && password.length > 4)
    {
    }
    else {
      alert('please enter password atleast 5 character')
      return false;
    }
    if(undefined != address && undefined != city && undefined != state && undefined != country)
    {
    }
    else {
      alert('please enter valid address')
      return false;
    }
    if(undefined != zip && zip.length > 5)
    {
    }
    else {
      alert('please enter valid zipcode, alteast 6 digit')
      return false;
    }
    if(undefined != role && role != "select")
    {
    }
    else {
      alert('please select valid role')
      return false;
    }
    if(undefined != status && status != "select")
    {
    }
    else {
      alert('please select valid status')
      return false;
    }
    if(selectedNodesManage.length > 0)
    {
    }
    else {
      alert('please select atleast one manage')
      return false;
    }
    return true;
  }

  toggleEditUser() {
    console.log('inside toggleEditUser')
    this.setState({
      isOpenModalEditUser: !this.state.isOpenModalEditUser,
    });
  }

  async editUser(row) {
    //console.log('inside editUser', e.target.innerHTML)
    console.log('inside editUser, row', row)
    selectedNodesManage = [];
    const userRes = await fetch('http://159.65.6.196:5500/api/users/'+row.userId)
    const user = await userRes.json();
    console.log('user: ',user)
    const manageRes = await fetch('http://159.65.6.196:5500/api/users/managebyuserandallsubdealer/5b60474f2b9ec018356bbca0/'+row.userId)
    const manageByUserAndSubDealer = await manageRes.json();
    const data = manageByUserAndSubDealer.data;
    const usersManage = data.manage;
    const allSubDealer = data.subDealer;
    console.log('manageByUserAndSubDealer: ',manageByUserAndSubDealer)
    let dataManagedDropDown = []
    for(var i in allSubDealer) {
      var item = allSubDealer[i];
      var itemFound = false;
      for(var j in usersManage)
      {
        var managedItem = usersManage[j]
        console.log('subDealer: ',item.id)
        console.log('managedSubDealer ', managedItem.subDealerId)
        if (item.id == managedItem.subDealerId )
        {
         itemFound = true;
        break;
        }
        else {
          itemFound = false;
          continue;
        }

      }
      console.log('itemFound', itemFound)
      if(itemFound)
        {
          dataManagedDropDown.push({
            "label" : item.name,
            "value"  : item.id,
            checked: true
          });
        }
        else {
          dataManagedDropDown.push({
            "label" : item.name,
            "value"  : item.id,
          });
        }

    }
    console.log('dataManagedDropDown:', dataManagedDropDown);
    selectedNodesManage = dataManagedDropDown;
    //
    const roles = this.createSelectRoleItems(user.role)
    const statuses = this.createSelectStatusItems(user.status)
    this.setState({
                  editUserId:row.userId,
                  editFirstName:user.firstName,
                  editLastName:user.lastName,
                  editAddress:user.address,
                  editCity:user.city,
                  editState:user.state,
                  editCountry:user.country,
                  editZip:user.zipCode,
                  editRole:user.role,
                  roles:roles,
                  editStatus:user.status,
                  statuses:statuses,
                  dataManagedDropDown:dataManagedDropDown
                  })

    this.toggleEditUser();
  }

  createSelectRoleItems(role)
  {
    //TODO roles came from database
    let roles = [{id:'1',name:'admin'},{id:'2',name:'non-admin'}]
    console.log('inside createSelectRoleItems, role, roles',role,roles)
    let items = [];
    if(undefined != role)
    {
      items.push(<option key={0} value={role}>{role}</option>);
      for(var i in roles)
      {
        if(roles[i].name != role)
        {
        items.push(<option key={i} value={roles[i].name}>{roles[i].name}</option>);
        }
      }
  }
  else {
     items = roles;
  }
     return items;
  }

  createSelectStatusItems(status)
  {
    //TODO roles came from database
    let statuses = [{id:'1',name:'active'},{id:'2',name:'inactive'}]
    let items = [];
    if(undefined != status)
    {
      items.push(<option key={0} value={status}>{status}</option>);
      for(var i in statuses)
      {
        if(statuses[i].name != status)
        {
        items.push(<option key={i} value={statuses[i].name}>{statuses[i].name}</option>);
        }
      }
  }
  else {
    items = statuses;
  }
     return items;
  }

  async handleUpdateUser ()
  {
    console.log('inside handleUpdateUser')
    const {editUserId, editFirstName, editLastName, editAddress, editCity, editState, editCountry, editZip, editRole, editStatus} = this.state
    console.log('selectedNodesManage',selectedNodesManage);
    let manage = []
    if(this.validateUpdateUser())
    {
    for(var i in selectedNodesManage) {
        var item = selectedNodesManage[i];
        if(item.checked)
        {
        manage.push({
            "id" : item.value,
            "name"  : item.label,
          });
        }
      }
    console.log('manage as: ',manage)
    let payload = {
                    id:editUserId,
                    firstName:editFirstName,
                    lastName:editLastName,
                    address:editAddress,
                    city:editCity,
                    state:editState,
                    country:editCountry,
                    zipCode:editZip,
                    role:editRole,
                    status:editStatus,
                    manage:manage,
                    manageCategoryId: '5b5f07ac31a8317acef6503d'
                  }
    console.log('payload for update user: ',payload)
    console.log('payload: ',JSON.stringify(payload))
    const requestOptions = {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body : JSON.stringify(payload)
      //body: JSON.stringify({phone, password})
    };
    const dealerId = '5b60474f2b9ec018356bbca0';
    const res = await fetch('http://159.65.6.196:5500/api/users/updateuserdealerlevel/'+editUserId+'/'+dealerId, requestOptions)
    const data = await res.json()
    console.log('updated user data ',data)
    selectedNodesManage = [];
    this.getUsersAndSubDealers();
    this.toggleEditUser();
    }
    else {
      console.log('validation failed for update user')
    }
  }

  validateUpdateUser()
  {
    console.log('Dealer -->> inside validateSaveUser')
    const {editFirstName, editLastName, editAddress, editCity, editState, editCountry, editZip, editRole, editStatus} = this.state

    if(undefined != editFirstName && undefined != editLastName)
    {
    }
    else {
      alert('please enter name')
      return false;
    }
    if(undefined != editAddress && undefined != editCity && undefined != editState && undefined != editCountry)
    {
    }
    else {
      alert('please enter valid address')
      return false;
    }
    if(undefined != editZip && editZip.length > 5)
    {
    }
    else {
      alert('please enter valid zipcode, alteast 6 digit')
      return false;
    }
    if(undefined != editRole && editRole != "select")
    {
    }
    else {
      alert('please select valid role')
      return false;
    }
    if(undefined != editStatus && editStatus != "select")
    {
    }
    else {
      alert('please select valid status')
      return false;
    }
    if(selectedNodesManage.length > 0)
    {
    }
    else {
      alert('please select atleast one manage')
      return false;
    }
    return true;
  }

  confirmDeleteUser(row)
  {
    deleteRow = row;
    console.log('deleteRow',deleteRow)
    this.toggleDeleteUser();
  }

  toggleDeleteUser() {
    this.setState({
      isOpenModalDeleteUser: !this.state.isOpenModalDeleteUser,
    });
  }

  async deleteUser() {
    const row = deleteRow;
    console.log('inside deleteUser, row',row)
    const requestOptions = {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' }
      //body: JSON.stringify({phone, password})
    };
    //const res = await fetch('http://159.65.6.196:5500/api/users/deleteuserdealerlevel/'+row.userId, requestOptions)
    const res = await fetch('http://159.65.6.196:5500/api/users/deleteuser/'+row.userId, requestOptions)
    const data = await res.json()
    console.log('delete user response data ',data)
    this.getUsersAndSubDealers();
    this.toggleDeleteUser();

  }

  async handleAddSubDealer()
  {
    console.log('inside handleAddSubDealer')
    this.toggleAddSubDealer();
  }

  toggleAddSubDealer()
  {
      console.log('inside toggleAddSubDealer')
      this.setState({
        isOpenModalAddSubDealer: !this.state.isOpenModalAddSubDealer,
      });
  }

  handleSaveSubDealer = async ()  =>
  {
    console.log('Dealer -->> inside handleSaveSubDealer')
    const { subDealerName } = this.state;
    console.log('subDealerName',subDealerName)
    let payload = {
                    dealerId:'5b60474f2b9ec018356bbca0',
                    name:subDealerName,
                  }
      console.log('payload: ',payload)
      console.log('payload: ',JSON.stringify(payload))
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
        //body: JSON.stringify({phone, password})
      };
      const res = await fetch('http://159.65.6.196:5500/api/subdealer/create', requestOptions)
      const data = await res.json()
      console.log('register subdealer data ',data)
      this.getUsersAndSubDealers();
      this.toggleAddSubDealer();
  }


  toggleEditSubDealer() {
    console.log('inside toggleEditSubDealer')
    this.setState({
      isOpenModalEditSubDealer: !this.state.isOpenModalEditSubDealer,
    });
  }

  async editSubDealer(row)
  {
    console.log('inside editSubDealer, row',row)
    const subDealerRes = await fetch('http://159.65.6.196:5500/api/subdealer/'+row.subDealerId)
    const subDealer = await subDealerRes.json();
    console.log('subDealer: ',subDealer)
    this.setState({
                  editSubDealerName:subDealer.name
    })
    this.toggleEditSubDealer();
  }

  async handleUpdateSubDealer ()
  {
    console.log('inside handleUpdateSubDealer')
    const {editSubDealerName} = this.state;
    console.log('edited subDealerName',editSubDealerName)
    this.toggleEditSubDealer();
  }

  deleteSubDealer()
  {
    console.log('inside deleteSubDealer')
  }

  searchKeyPress() {
    /*
    console.log('search key press')
    var input, filter, table, tr, td, i, td1;
    var targetTableColCount;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    console.log('searchKeyPress, tr length ',tr.length)
    var thLength = tr[0].getElementsByTagName("th").length;
    console.log('searchKeyPress, td length ',thLength)

    for (i = 0; i < tr.length; i++) {
      var rowData = '';
      if(i == 0)
      {
        //table header
        targetTableColCount = table.rows.item(i).cells.length;
        continue;
      }
      td = tr[i].getElementsByTagName("td")[0];
      td1 = tr[i].getElementsByTagName("td")[1];
      console.log('searchKeyPress, td ',td)
      console.log('searchKeyPress, td1 ',td1)

      if (td || td1) {
        if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          tr[i].style.display = "none";
        }
      }

  }
    */

      /*
        var input, filter, table, tr, td, i, td1;
        var targetTableColCount;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        var keyword =  document.getElementById("myInput");
    		var textSearchRegex = new RegExp(keyword, "ig"); // case in-sensitive
    		var rowDisplay, rowObj, rowHtml, match;
    		var firstRowIndex = 1;

    		for (var rowIndex = firstRowIndex; rowIndex < tr.length; rowIndex++) {
    			rowDisplay = '';
    			rowObj = table.rows.item(rowIndex);
    			//rowHtml = rowObj.innerHTML.replace(/<mark[^/>]*>/g,'').replace(/<\/mark>/g,''); // remove previous highlighting

    			if (keyword == '')
    				rowDisplay = 'table-row';
    			else {
    				match = rowHtml.replace(/<[^>]*>/g, '').match(textSearchRegex); // strip html tags and search for keyword
    				if(match) {
    					// Get unique matches: http://stackoverflow.com/a/21292834/1440057
    					match = match.sort().filter(function(element, index, array) { return index == array.indexOf(element); });
    					var tempHtml = rowHtml;
    					for (var i = 0; i < match.length; i++)
    						tempHtml = match[i];

    					if (tempHtml.search(/<\/mark>/g) > -1) {
    						rowHtml = tempHtml;
    						rowDisplay = 'table-row';
    					}
    					else // Keyword did not match with any column content
    						rowDisplay = 'none';
    				}
    				else // Keyword did not match even in the row text content
    					rowDisplay = 'none';
    			}

    			rowObj.innerHTML = rowHtml;
    			rowObj.style.display = rowDisplay;
    		}

    		// Check if 'no results' row needs to be added
    		//if (keyword != '' && this.options.noResultsText && this.table.innerHTML.search(/style=\"display: table-row;\"/g) == -1)
        */

  }

  render() {
    console.log('Dealer -->> inside render')
    /*
    if(!this.state.isAdmin)
    {
      return (
              <div>
                <Row>
                  <Col xs="10">
                  {
                    //<Button block color="primary" className="btn-pill col-md-6 pull-right" size="sm" onClick={this.editUser.bind(this)}>Admin</Button>
                  }
                  <Col md="2" className="float-right">
                  <Button block color="primary" className="btn-pill" size="sm" onClick={this.openAdmin}>Admin</Button>
                  </Col>
                  </Col>
                </Row>
              <Page_1 />
              </div>
      );
    }
    else {
    */
    const { selectedOption,isClearable,phone,firstName,lastName,password,address,city,state,country,zip } = this.state;
    const {editFirstName, editLastName, editAddress, editCity, editState, editCountry, editZip} = this.state
    const {subDealerName, editSubDealerName} = this.state;

        //const editFormatter = (cell, row, rowIndex, formatExtraData) => { console.log('editFormatter, row ',row); return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editUser.bind(this, row)}>Edit</Button> ); }
        const editFormatter = (cell, row, rowIndex, formatExtraData) => { return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editUser.bind(this, row)}>Edit</Button> ); }

        const deleteFormatter = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.confirmDeleteUser.bind(this, row)}>Delete</Button> ); }

        const editFormatterSubDealer = (cell, row, rowIndex, formatExtraData) => { return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editSubDealer.bind(this, row)}>Edit</Button> ); }

        const deleteFormatterSubDealer = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.deleteSubDealer.bind(this, row)}>Delete</Button> ); }

        const columnsUsersTable = [{
          dataField: 'id',
          text: 'SN',
          sort: true
        }, {
          dataField: 'phoneNumber',
          text: 'Phone Number',
          filter: textFilter(),
          events: {
            onClick: (e, row, rowIndex) => { alert('phoneNumber clicked') }
          },
        }, {
          dataField: 'name',
          text: 'Name',
          sort: true,
          filter: textFilter()
        }, {
          dataField: 'role',
          text: 'Role'
        }, {
          dataField: 'status',
          text: 'Status'
        }, {
          dataField: 'lastLogin',
          text: 'Last Login'
        }, {
          dataField: 'edit',
          text: '',
          formatter: editFormatter,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          },
          /*
          events: {
            onClick: (e, row, rowIndex) => { this.editUser(row) }
          },
          */
          editorRenderer: (editorProps, value, row, rowIndex, columnIndex) => (
            <QualityRanger { ...editorProps } value={ value } />
          )
        }, {
          dataField: 'delete',
          text: '',
          formatter: deleteFormatter,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          },
          /*
          events: {
            onClick: e => { this.deleteUser() }
          }
          */
        }];

        const columnsSubDealersTable = [{
          dataField: 'id',
          text: 'SN',
          sort: true
        }, {
          dataField: 'name',
          text: 'Name',
          sort: true,
          filter: textFilter()
        }, {
          dataField: 'edit',
          text: '',
          formatter: editFormatterSubDealer,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          },
          /*
          events: {
            onClick: (e, row, rowIndex) => { this.editUser(row) }
          },
          */
          editorRenderer: (editorProps, value, row, rowIndex, columnIndex) => (
            <QualityRanger { ...editorProps } value={ value } />
          )
        }, {
          dataField: 'delete',
          text: '',
          formatter: deleteFormatterSubDealer,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          },
          /*
          events: {
            onClick: e => { this.deleteUser() }
          }
          */
        }];

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12">
            <Card>
              <CardBody>
                <Row>
                {
                /*
                <Col md="1">
                <Button block color="primary" className="btn-pill" size="sm" onClick={this.closeAdmin}>Back</Button>
                </Col>
                */
                }
                <p className="float-center"><b>Dealer Admin</b></p>
                </Row>
                <Row>
                <Col md="3">
                <p>Dealer</p>
                <Select
                  onChange={this.handleChangeSelectOption}
                  defaultValue={optionsDealer[0]}
                  isClearable={isClearable}
                  options={optionsDealer}
                  placeholder={'Select Dealer'}
                  backspaceRemovesValue={false}
                />
                </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs="12" className="mb-4">
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '1' })}
                  onClick={() => { this.toggle('1'); }}
                >
                  User Management
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '2' })}
                  onClick={() => { this.toggle('2'); }}
                >
                  Dealer Management
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={this.state.activeTab}>
              <TabPane tabId="1">
              <Row>
                <Col>
                  <Card>
                    <CardBody>
                    {
                    /*
                    <Row className="align-items-center">
                      <Col>
                      <p className="text-info">4 Active, 2 Blocked, 6 Total Users</p>
                      </Col>
                      <Col md="3">
                      <InputGroup className="mb-3">
                      {

                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                              <i className="fa fa-search"></i>
                          </InputGroupText>
                        </InputGroupAddon>

                      }
                        <Input type="text" id="myInput" placeholder="Search" onKeyUp={this.searchKeyPress} />
                      </InputGroup>
                      </Col>
                      <Col col="6" sm="4" md="2" className="mb-3">
                        <Button block color="primary" className="btn-pill" onClick={this.handleAddUser}>Add User</Button>
                      </Col>
                    </Row>
                    */
                    }
                    <Modal isOpen={this.state.isOpenModalAddUser} toggle={this.toggleAddUser}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleAddUser}>Add New User</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Phone</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="text-input" name="phone" placeholder="Phone" value={phone} onChange={this.handleChange} />
                              {
                                //<FormText color="muted">This is a help text</FormText>
                              }
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">First Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="firstName" name="firstName" placeholder="First Name" value={firstName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Last Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="lastName" name="lastName" placeholder="Last Name" value={lastName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Password</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="password" id="password" name="password" placeholder="Password" value={password} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Address</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="address" name="address" placeholder="Address" value={address} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">City</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="city" name="city" placeholder="City" value={city} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">State</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="state" name="state" placeholder="State" value={state} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Country</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="country" name="country" placeholder="Country" value={country} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Zip</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="zip" name="zip" placeholder="Zip" value={zip} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Role</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="role" id="selectRole" onChange={this.handleChange}>
                                <option value="select">Please Select</option>
                                <option value="admin">admin</option>
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Status</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="status" id="selectStatus" onChange={this.handleChange}>
                                <option value="select">Please Select</option>
                                <option value="active">active</option>
                                <option value="inactive">inactive</option>
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Manage</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <DropdownTreeSelect
                                data={this.state.dataManageDropDown}
                                onChange={onChange}
                                onAction={onAction}
                                onNodeToggle={onNodeToggle}
                              />
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleSaveUser}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleAddUser}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalEditUser} toggle={this.toggleEditUser}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleEditUser}>Edit User</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">First Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="firstName" name="editFirstName" placeholder="First Name" value={editFirstName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Last Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="lastName" name="editLastName" placeholder="Last Name" value={editLastName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Address</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="address" name="editAddress" placeholder="Address" value={editAddress} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">City</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="city" name="editCity" placeholder="City" value={editCity} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">State</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="state" name="editState" placeholder="State" value={editState} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Country</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="country" name="editCountry" placeholder="Country" value={editCountry} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Zip</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="zip" name="editZip" placeholder="Zip" value={editZip} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Role</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="editRole" id="selectRole" onChange={this.handleChange}>
                                {
                                this.state.roles
                                }
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Status</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="editStatus" id="selectStatus" onChange={this.handleChange}>
                              {
                                this.state.statuses
                              }
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Manage</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <DropdownTreeSelect
                                data={this.state.dataManagedDropDown}
                                onChange={onChange}
                                onAction={onAction}
                                onNodeToggle={onNodeToggle}
                              />
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleUpdateUser}>Update</Button>{' '}
                        <Button color="secondary" onClick={this.toggleEditUser}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalDeleteUser} toggle={this.toggleDeleteUser}
                           className={'modal-danger ' + this.props.className}>
                      <ModalHeader toggle={this.toggleDeleteUser}>User Delete</ModalHeader>
                      <ModalBody>
                        Are you sure to delete user
                      </ModalBody>
                      <ModalFooter>
                        <Button color="danger" onClick={this.deleteUser}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteUser}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                      {
                        /*
                        <BootstrapTable keyField='id'
                        hover bordered responsive
                        data={ this.state.dataUsersTable }
                        columns={ columnsUsersTable }
                        filter={ filterFactory() }
                        selectRow={ selectRow }
                        pagination={ paginationFactory() }
                         />
                        */
                      }
                      <ToolkitProvider
                            keyField="id"
                            data={ this.state.dataUsersTable }
                            columns={ columnsUsersTable }
                            search
                          >
                            {
                              props => (
                                <div>
                                  <Row className="align-items-center">
                                  <Col>
                                  <p className="text-info">4 Active, 2 Blocked, 6 Total Users</p>
                                  </Col>
                                  <Col md="2">
                                  {
                                    //<h4>Input something at below input field:</h4>
                                  }
                                  <SearchBar
                                    { ...props.searchProps }
                                  />
                                  </Col>
                                  <Col md="2">
                                    <Button block color="primary" className="btn-pill" onClick={this.handleAddUser}>Add User</Button>
                                  </Col>
                                  </Row>
                                  <hr />
                                  <BootstrapTable
                                    { ...props.baseProps }
                                    filter={ filterFactory() }
                                    selectRow={ selectRow }
                                    pagination={ paginationFactory() }
                                  />
                                </div>
                              )
                            }
                          </ToolkitProvider>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="2">
              <Row>
                <Col>
                  <Card>
                    <CardBody>
                    {
                    /*
                    <Row className="align-items-center">
                      <Col>
                      <p className="text-info">Total SubDealers</p>
                      </Col>
                      <Col md="3">
                      <InputGroup className="mb-3">
                      {

                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                              <i className="fa fa-search"></i>
                          </InputGroupText>
                        </InputGroupAddon>

                      }
                        <Input type="text" id="myInput" placeholder="Search" onKeyUp={this.searchKeyPress} />
                      </InputGroup>
                      </Col>
                      <Col col="6" sm="4" md="2" className="mb-3">
                        <Button block color="primary" className="btn-pill" onClick={this.handleAddSubDealer}>Add SubDealer</Button>
                      </Col>
                    </Row>
                    */
                    }
                    <Modal isOpen={this.state.isOpenModalAddSubDealer} toggle={this.toggleAddSubDealer}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleAddSubDealer}>Add New SubDealer</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="text-input" name="subDealerName" placeholder="Name" value={subDealerName} onChange={this.handleChange} />
                              {
                                //<FormText color="muted">This is a help text</FormText>
                              }
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleSaveSubDealer}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleAddSubDealer}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalEditSubDealer} toggle={this.toggleEditSubDealer}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleEditSubDealer}>Edit SubDealer</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="text-input" name="editSubDealerName" placeholder="Name" value={editSubDealerName} onChange={this.handleChange} />
                              {
                                //<FormText color="muted">This is a help text</FormText>
                              }
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleUpdateSubDealer}>Update</Button>{' '}
                        <Button color="secondary" onClick={this.toggleEditSubDealer}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                {
                  /*
                  <BootstrapTable keyField='id'
                  hover bordered responsive
                  data={ this.state.dataSubDealersTable }
                  columns={ columnsSubDealersTable }
                  filter={ filterFactory() }
                  selectRow={ selectRow }
                  pagination={ paginationFactory() }
                   />
                   */
                   //cellEdit={ cellEditFactory({ mode: 'click', blurToSave: true }) }
                   //TODO cellEdit not working with selectRow, so commented this, it have alternative to use both
                   //selectRow={ selectRow }
                }
                  <ToolkitProvider
                        keyField="id"
                        data={ this.state.dataSubDealersTable }
                        columns={ columnsSubDealersTable }
                        search
                      >
                        {
                          props => (
                            <div>
                              <Row className="align-items-center">
                              <Col>
                              <p className="text-info">Total SubDealers</p>
                              </Col>
                              <Col md="2">
                              {
                                //<h4>Input something at below input field:</h4>
                              }
                              <SearchBar
                                { ...props.searchProps }
                              />
                              </Col>
                              <Col md="2">
                                <Button block color="primary" className="btn-pill" onClick={this.handleAddSubDealer}>Add SubDealer</Button>
                              </Col>
                              </Row>
                              <hr />
                              <BootstrapTable
                                { ...props.baseProps }
                                filter={ filterFactory() }
                                selectRow={ selectRow }
                                pagination={ paginationFactory() }
                              />
                            </div>
                          )
                        }
                      </ToolkitProvider>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </TabPane>
            </TabContent>
          </Col>
        </Row>
      </div>
    );

  //} //end of else

}
}

export default Dealer;
