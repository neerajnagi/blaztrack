import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Form,
  FormGroup,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Label,
  FormText
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'

import Select from 'react-select';
import classnames from 'classnames';

import { NextAuth } from 'next-auth/client'

import Router from 'next/router'

//react-bootstrap-table (deprecated)
//import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
//react-bootstrap-table2
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import cellEditFactory from 'react-bootstrap-table2-editor';
//For table search
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
// react-table not working properly (failing)
//import ReactTable from 'react-table'
//import FilterableTable from 'react-filterable-table'

import DropdownTreeSelect from 'react-dropdown-tree-select'
import 'react-dropdown-tree-select/dist/styles.css'

import PropTypes from 'prop-types';

import { ClipLoader } from 'react-spinners';

let selectedNodesManage = [];

let selectedNodesGroup = [];

const { SearchBar } = Search;

const dataManageDropDown = [{
  label: 'search me',
  value: 'searchme',
  children: [
    {
      label: 'search me too',
      value: 'searchmetoo',
      children: [
        {
          label: 'No one can get me',
          value: 'anonymous'
        }
      ]
    }
  ]
},
{
  label:'second',
  value:'second'
},
{
  label:'third',
  value:'third'
},
{
  label:'fourth',
  value:'fourth',
  checked:true
},
]
const onChange = (currentNode, selectedNodes) => {
  console.log('onChange::', currentNode, selectedNodes)
  selectedNodesManage = selectedNodes;
}

const optionsSubDealer = [{ value: 'all', label: 'All' }];

const optionsCustomer = [{ value: 'all', label: 'All' }];

const optionsGroup = [{ value: 'all', label: 'All' }];

const optionsSelectSubDealer = [];

const optionsSelectCustomer = [];

const selectRow = {
  mode: 'checkbox',
  clickToSelect: false,
  onSelect: (row, isSelect, rowIndex, e) => {
    console.log(row.id);
    console.log(isSelect);
    console.log(row);
    console.log(rowIndex);
    console.log(e);
  },
  onSelectAll: (isSelect, rows, e) => {
    console.log(isSelect);
    console.log(rows);
    console.log(e);
  }
};

const divStyle = {
  margin: '40px',
  border: '5px solid pink'
};

//
const dataBootstrapTable = [
  { id: 1, phoneNumber: '9799605400', name:'Ashwin', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
  { id: 2, phoneNumber: '9024038439', name:'Raji', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
  { id: 3, phoneNumber: '9024038434', name:'Aman', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
  { id: 4, phoneNumber: '9024038433', name:'Akshit', role:'admin', status:'Active', lastLogin:'02 Aug 2018', edit:'Edit', delete:'Delete' },
];

const dataUsersTable = [];
const dataProjectsTable = [];
const dataGroupsTable = [];

const dataManageCustomerDropDown = [];
const dataManageGroupDropDown = [];

const dataUnAssignGroupsDropDown = [];

let deleteUserRow, deleteProjectRow, deleteGroupRow;

//ref: https://github.com/react-bootstrap-table/react-bootstrap-table2/issues/252
//const editFormatter = (cell, row, rowIndex, formatExtraData) => { console.log('editFormatter, row ',row); return ( <Button block color="primary" className="btn-pill" size="sm" >Edit</Button> ); }
//const deleteFormatter = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm">Delete</Button> ); }

class Group extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleAddUser = this.toggleAddUser.bind(this);
    this.toggleEditUser = this.toggleEditUser.bind(this);
    this.editUser = this.editUser.bind(this);
    this.toggleDeleteUser = this.toggleDeleteUser.bind(this);
    this.confirmDeleteUser = this.confirmDeleteUser.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
    this.handleAddUser = this.handleAddUser.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onChangeGroups = this.onChangeGroups.bind(this);
    this.handleSaveUser = this.handleSaveUser.bind(this);
    this.handleUpdateUser = this.handleUpdateUser.bind(this);
    //
    this.handleAddProject = this.handleAddProject.bind(this);
    this.toggleAddProject = this.toggleAddProject.bind(this);
    this.onChangeGroupsAtAddProject = this.onChangeGroupsAtAddProject.bind(this);
    this.handleSaveProject = this.handleSaveProject.bind(this);
    this.editProject = this.editProject.bind(this);
    this.toggleEditProject = this.toggleEditProject.bind(this);
    this.handleUpdateProject = this.handleUpdateProject.bind(this);
    this.toggleDeleteProject = this.toggleDeleteProject.bind(this);
    this.confirmDeleteProject = this.confirmDeleteProject.bind(this);
    this.deleteProject = this.deleteProject.bind(this);
    this.onChangeGroupsAtEditProject = this.onChangeGroupsAtEditProject.bind(this);
    //
    this.handleAddGroup = this.handleAddGroup.bind(this);
    this.toggleAddGroup = this.toggleAddGroup.bind(this);
    this.handleSaveGroup = this.handleSaveGroup.bind(this);
    this.editGroup = this.editGroup.bind(this);
    this.toggleEditGroup = this.toggleEditGroup.bind(this);
    this.handleUpdateGroup = this.handleUpdateGroup.bind(this);
    this.toggleDeleteGroup = this.toggleDeleteGroup.bind(this);
    this.confirmDeleteGroup = this.confirmDeleteGroup.bind(this);
    this.deleteGroup = this.deleteGroup.bind(this);

    const manageCategory = this.props.managecategory;
    let isSubDealerShow;
    if(manageCategory == "dealer" || manageCategory == "subdealer")
    {
      isSubDealerShow = true;
    }
    else {
      isSubDealerShow = false;
    }
    console.log('Group -->> isSubDealerShow',isSubDealerShow)

    this.state = {
      session: props.session,
      isSignedIn: (props.session.user) ? true : false,
      manageCategory: manageCategory,
      dropdownOpen: false,
      radioSelected: 2,
      selectedOption: null,
      isClearable: true,
      activeTab: '1',
      isOpenModalAddUser: false,
      isOpenModalEditUser: false,
      isOpenModalDeleteUser: false,
      isOpenModalAddProject:false,
      isOpenModalEditProject: false,
      isOpenModalDeleteProject: false,
      isOpenModalAddGroup: false,
      isOpenModalEditGroup: false,
      isOpenModalDeleteGroup: false,
      dataManageDropDown:dataManageDropDown,
      dataUsersTable:dataUsersTable,
      dataProjectsTable:dataProjectsTable,
      dataGroupsTable:dataGroupsTable,
      optionsSubDealer:optionsSubDealer,
      optionsCustomer:optionsCustomer,
      optionsCustomerOfProjectMgmt:optionsCustomer,
      optionsCustomerOfGrpMgmt:optionsCustomer,
      optionsGroup:optionsGroup,
      optionsSelectSubDealer:optionsSelectSubDealer,
      optionsSelectCustomer:optionsSelectCustomer,
      dataManageCustomerDropDown:dataManageCustomerDropDown,
      dataManageGroupDropDown:dataManageGroupDropDown,
      dataUnAssignGroupsDropDown:dataUnAssignGroupsDropDown,
      isSubDealerShow:isSubDealerShow,
      selectCustomerValue:'',
      selectGroupValue:'',
      selectCustomerValueOfProjectMgmt:'',
      selectCustomerValueOfGrpMgmt:'',
      selectCustomerValueOfAddUser:'',
      selectCustomerValueOfAddGroup:'',
      userTableLoading:true,
      projectTableLoading:true,
      groupTableLoading:true
    };
  }

  async componentDidMount() {
    console.log('Group -->> inside componentDidMount')
    //TODO check user logged in or not:
    const session = await NextAuth.init({force: true})
    if(session.user)
    {
      console.log('user logged in')
    }
    else {
      console.log('user not logged in')
      Router.push('/auth/callback')
      return false;
    }
    console.log('after checking user state')
    //getting users
    this.getUsers();
    //TODO getManages based on manageCategory, if login user manage customer then subDealer manages not fetch
    this.getManages();
    //TODO getting projects based on userId & manageCategory
    this.getProjectsAndItsGroups();
  }

  async getUsers()
  {
    let dataUsersTable = []
    this.setState({
                  dataUsersTable:dataUsersTable
                });
    const res = await fetch('/group/users/'+this.state.manageCategory)
    const resData = await res.json()
    console.log('data ',resData)
    const data = resData.data;
    const users = data;
    var snUsers = 1;
    for (var i in users)
    {
      var item = users[i];
      var manages = item.manages;
      var manageGroupName = "";
      for(var j in manages)
      {
        var manageGroup = manages[j].group;
        manageGroupName += manageGroup.name+"\n";
      }
      dataUsersTable.push({
          "id" : snUsers,
          "userId": item.id,
          "customerId":item.customerId,
          "manageCategoryId":item.manageCategoryId,
          "phoneNumber" : item.phone,
          "name"  : item.firstName+' '+item.lastName,
          "manage" : manageGroupName,
          "role" : item.role,
          "status"  : item.status,
          "lastLogin"  : '15 Aug 2018',
        });
        snUsers++;
    }

    this.setState({
                  userTableLoading:false,
                  dataUsersTable:dataUsersTable
                  })
  }

  async getManages()
  {
    let dataGroupsTable = []
    let dataProjectsTable = [];
    this.setState({
                  dataGroupsTable:dataGroupsTable
                  });
    let optionsSubDealer = [{ value: 'all', label: 'All' }]
    let optionsCustomer = [{ value: 'all', label: 'All' }]
    let optionsGroup = [{ value: 'all', label: 'All' }]
    const fetchManage = await fetch('/group/manage/fetch/'+this.state.manageCategory+'/group');
    const res = await fetchManage.json();
    console.log('res',res);
    const data = res.data;
    const manage = data.manage;
    var snGroup = 1;
    var snProject = 1;
    for(var i in manage) {
      var item = manage[i];
      var subDealers = ""
      if(this.state.manageCategory == "dealer")
      {
      subDealers = item.dealer.subDealers;
      for(var j in subDealers)
      {
        var subDealer = subDealers[j];
        optionsSubDealer.push({
          "value":subDealer.id,
          "label":subDealer.name
        });
        var customers = subDealers[j].customers;
        for(var k in customers){
          var customer = customers[k];
          optionsCustomer.push({
            "value":customer.id,
            "label":customer.name
          });
          var groups = customers[k].groups;
          for(var l in groups)
          {
            var group = groups[l];
            optionsGroup.push({
              "value":group.id,
              "label":group.name
            });
            dataGroupsTable.push({
                "id" : snGroup,
                "groupId":group.id,
                "name"  : group.name,
                "customer" : customer.name,
            });
            snGroup++;
          }
        }
      }
      }
      else if(this.state.manageCategory == "subdealer") {
        subDealers = item.subDealer;
          var subDealer = subDealers;
          optionsSubDealer.push({
            "value":subDealer.id,
            "label":subDealer.name
          });
          var customers = subDealer.customers;
          for(var k in customers){
            var customer = customers[k];
            optionsCustomer.push({
              "value":customer.id,
              "label":customer.name
            });
            var groups = customers[k].groups;
            for(var l in groups)
            {
              var group = groups[l];
              optionsGroup.push({
                "value":group.id,
                "label":group.name
              });
              dataGroupsTable.push({
                  "id" : snGroup,
                  "groupId":group.id,
                  "name"  : group.name,
                  "customer" : customer.name,
              });
              snGroup++;
            }
          }
      }
      else if(this.state.manageCategory == "customer") {
          var customer = item.customer;
            optionsCustomer.push({
              "value":customer.id,
              "label":customer.name
            });
            var groups = customer.groups;
            for(var l in groups)
            {
              var group = groups[l];
              optionsGroup.push({
                "value":group.id,
                "label":group.name
              });
              dataGroupsTable.push({
                  "id" : snGroup,
                  "groupId":group.id,
                  "name"  : group.name,
                  "customer" : customer.name,
              });
              snGroup++;
            }
      }
      else {
        console.log('something went wrong')
      }


    }

    this.setState({
                  groupTableLoading:false,
                  dataGroupsTable:dataGroupsTable,
                  optionsSubDealer:optionsSubDealer,
                  optionsCustomer:optionsCustomer,
                  optionsCustomerOfProjectMgmt:optionsCustomer,
                  optionsCustomerOfGrpMgmt:optionsCustomer,
                  optionsGroup:optionsGroup,
                  selectCustomerValue:optionsCustomer[0],
                  selectGroupValue:optionsGroup[0],
                  selectCustomerValueOfProjectMgmt:optionsCustomer[0],
                  selectCustomerValueOfGrpMgmt:optionsCustomer[0]
                  })
  }

  async getProjectsAndItsGroups()
  {
    console.log('inside getProjectsAndItsGroups')
    let dataProjectsTable = [];
    this.setState({
                    dataProjectsTable:dataProjectsTable
                  })
    const userId = this.state.session.user.id;
    const fetchProjectAndItsGroup = await fetch('/project/fetchprojectanditsgroup/'+this.state.manageCategory);
    const res = await fetchProjectAndItsGroup.json();
    console.log('fetch project and its group, res',res);
    const data = res.data;
    const manage = data.manage;
    var snProject = 1;
    for(var i in manage) {
      var item = manage[i];
      var subDealers = ""
      if(this.state.manageCategory == "dealer")
      {
      subDealers = item.dealer.subDealers;
      for(var j in subDealers)
      {
        var subDealer = subDealers[j];
        var customers = subDealers[j].customers;
        for(var k in customers){
          var customer = customers[k];
          var projects = customers[k].projects;
          for(var l in projects)
          {
            var project = projects[l];
            var groups = project.groups;
            var addedGroupName = "";
            for(var m in groups)
            {
              var addedGroups = groups[m];
              addedGroupName += addedGroups.name+"\n";
            }
            dataProjectsTable.push({
                "sn" : snProject,
                "projectId":project.id,
                "name"  : project.name,
                "customer" : customer.name,
                "customerId":project.customerId,
                "group":addedGroupName
            });
            snProject++;
          }
        }
      }
      }
      else if(this.state.manageCategory == "subdealer") {
        subDealers = item.subDealer;
          var subDealer = subDealers;
          var customers = subDealer.customers;
          for(var k in customers){
            var customer = customers[k];
            var projects = customers[k].projects;
            for(var l in projects)
            {
              var project = projects[l];
              var groups = project.groups;
              var addedGroupName = "";
              for(var m in groups)
              {
                var addedGroups = groups[m];
                addedGroupName += addedGroups.name+"\n";
              }
              dataProjectsTable.push({
                  "sn" : snProject,
                  "projectId":project.id,
                  "name"  : project.name,
                  "customer" : customer.name,
                  "customerId":project.customerId,
                  "group":addedGroupName
              });
              snProject++;
            }

          }
      }
      else if(this.state.manageCategory == "customer") {
          var customer = item.customer;
            var projects = customer.projects;
            for(var l in projects)
            {
              var project = projects[l];
              var groups = project.groups;
              var addedGroupName = "";
              for(var m in groups)
              {
                var addedGroups = groups[m];
                addedGroupName += addedGroups.name+"\n";
              }
              dataProjectsTable.push({
                  "sn" : snProject,
                  "projectId":project.id,
                  "name"  : project.name,
                  "customer" : customer.name,
                  "customerId" : project.customerId,
                  "group":addedGroupName
              });
              snProject++;
            }
      }
      else {
        console.log('something went wrong')
      }
    }
    console.log('dataProjectsTable',dataProjectsTable);
    this.setState({
                    projectTableLoading:false,
                    dataProjectsTable:dataProjectsTable
                  })
}

  handleChange(e) {
      const { name, value } = e.target;
      this.setState({ [name]: value });
  }

  handleChangeSubDealerOption = (from, selectedOption) => {
      var subDealerId = selectedOption.value;
      if(from == "userManagement")
      {
        this.setState({ selectedOption,
                        selectCustomerValue:'',
                        selectGroupValue:'',
                        optionsGroup:[]
                      });
      }
      else if(from == "projectManagement") {
        this.setState({ selectedOption,
                        selectSubDealerValueForProject:selectedOption,
                        selectCustomerValueOfProjectMgmt:''
                      });
      }
      else if(from == "groupManagement") {
        this.setState({ selectedOption,
                        selectSubDealerValueForGroup:selectedOption,
                        selectCustomerValueOfGrpMgmt:''
                      });
      }
      else {
        console.log('something went wrong')
      }
      console.log(`handleChangeSubDealerOption, Option selected:`, selectedOption);
      console.log('subDealerId',subDealerId)
      if(undefined != subDealerId && subDealerId == "all")
      {
        //getting all customers
        this.getAllCustomers(from);
      }
      else if(undefined != subDealerId)
      {
        //getting customers of subDealer
        this.getSubDealerCustomers(from, subDealerId);
      }
    }

    async getAllCustomers(from)
    {
      let optionsSubDealer = [{ value: 'all', label: 'All' }]
      let optionsCustomer = [{ value: 'all', label: 'All' }]
      const fetchManage = await fetch('/group/manage/fetch/'+this.state.manageCategory+'/customer');
      const res = await fetchManage.json();
      console.log('res',res);
      const data = res.data;
      const manage = data.manage;
      var snGroup = 1;
      for(var i in manage) {
        var item = manage[i];
        var subDealers = ""
        if(this.state.manageCategory == "dealer")
        {
        subDealers = item.dealer.subDealers;
        for(var j in subDealers)
        {
          var subDealer = subDealers[j];
          optionsSubDealer.push({
            "value":subDealer.id,
            "label":subDealer.name
          });
          var customers = subDealers[j].customers;
          for(var k in customers){
            var customer = customers[k];

            optionsCustomer.push({
              "value":customer.id,
              "label":customer.name
            });
          }
        }
        }
        else if(this.state.manageCategory == "subdealer") {
          subDealers = item.subDealer;
            var subDealer = subDealers;
            optionsSubDealer.push({
              "value":subDealer.id,
              "label":subDealer.name
            });
            var customers = subDealer.customers;
            for(var k in customers){
              var customer = customers[k];
              optionsCustomer.push({
                "value":customer.id,
                "label":customer.name
              });
            }
        }
        else if(this.state.manageCategory == "customer") {
            var customer = item.customer;
              optionsCustomer.push({
                "value":customer.id,
                "label":customer.name
              });
        }
        else {
          subDealers = item.subDealer;
            var subDealer = subDealers;
            optionsSubDealer.push({
              "value":subDealer.id,
              "label":subDealer.name
            });
            var customers = subDealer.customers;
            for(var k in customers){
              var customer = customers[k];
              optionsCustomer.push({
                "value":customer.id,
                "label":customer.name
              });
            }
        }


      }
      if(from == "userManagement")
      {
        this.setState({
                      optionsCustomer:optionsCustomer
                      })
      }
      else if(from == "projectManagement")
      {
        this.setState({
                      optionsCustomerOfProjectMgmt:optionsCustomer
                      })
      }
      else if(from == "groupManagement")
      {
        this.setState({
                      optionsCustomerOfGrpMgmt:optionsCustomer
                      })
      }
      else {
        console.log('something went wrong')
      }
    }

    async getSubDealerCustomers(from, subDealerId)
    {
      console.log('inside getSubDealerCustomers, subDealerId',subDealerId)
      let optionsCustomer = []
      let optionsGroup = []
      const fetchSubDealerCustomers = await fetch('/subdealer/'+subDealerId+'/customers');
      const resData = await fetchSubDealerCustomers.json()
      console.log('resData',resData)
      const data = resData;
      if(undefined != data && data.length > 0)
      {
        if(from == "groupManagement")
        {
          optionsCustomer.push({ value: 'all', label: 'All' });
        }
        for(var i in data)
        {
          var customer = data[i]
          optionsCustomer.push({
            "value":customer.id,
            "label":customer.name
          });
        }
      }
      //TODO add condition
      if(from == "userManagement")
      {
        this.setState({
                      optionsCustomer:optionsCustomer,
                      optionsGroup:optionsGroup
                      })
      }
      else if(from == "projectManagement")
      {
        this.setState({
                      optionsCustomerOfProjectMgmt:optionsCustomer
                      })
      }
      else if(from == "groupManagement") {
        this.setState({
                      optionsCustomerOfGrpMgmt:optionsCustomer
                      })
      }
    }

  handleChangeCustomerOption = (from, selectedOption) => {
      var customerId = selectedOption.value;
      if(from == "userManagement")
      {
        this.setState({ selectedOption,
                        selectCustomerValue:selectedOption,
                        selectGroupValue:''
                        });
      }
      else if(from == "projectManagement") {
        this.setState({ selectedOption,
                        selectCustomerValueOfProjectMgmt:selectedOption
                      });
      }
      else if(from == "groupManagement") {
        this.setState({ selectedOption,
                        selectCustomerValueOfGrpMgmt:selectedOption
                      });
      }
      else {
        console.log('something went wrong')
      }
      console.log(`handleChangeCustomerOption, Option selected:`, selectedOption);
      console.log('customerId',customerId)
      if(undefined != customerId && customerId == "all")
      {
        //TODO need to get customer of subdealer
        //If selectSubDealerValueForGroup is any subdealer not all subdealer
        //So getting all customers based on subdealer id

        const {selectSubDealerValueForGroup, selectSubDealerValueForProject} = this.state;
        if(from == "userManagement")
        {
          //getting all groups
          this.getAllGroups(from);
        }
        else if(from == "groupManagement")
        {
            if(undefined != selectSubDealerValueForGroup && selectSubDealerValueForGroup.value != "all")
            {
              this.getAllCustomerGroupsOfSubDealer(selectSubDealerValueForGroup);
            }
            else {
              console.log('now getting all groups')
              this.getAllGroups(from);
            }
        }
        else if(from == "projectManagement")
        {
          if(undefined != selectSubDealerValueForProject && selectSubDealerValueForProject.value != "all")
          {
            this.getAllCustomerProjectsOfSubDealer(selectSubDealerValueForProject);
          }
          else {
            console.log('now getting all projects')
            this.getProjectsAndItsGroups();
          }
        }
        else {
          console.log('something went wrong')
        }
      }
      else if(undefined != customerId && customerId == "select")
      {
        //getting all groups of selected customer
      }
      else if(undefined != customerId)
      {
        if(from == "projectManagement")
        {
          //getting projects of customer
          this.getCustomerProjects(from, selectedOption);
        }
        else {
          //getting groups of customer
          this.getCustomerGroups(from, selectedOption);
        }

      }

    }

    async getAllCustomerGroupsOfSubDealer(selectedSubDealer)
    {
      console.log('inside getAllCustomerGroupsOfSubDealer, selectedSubDealer',selectedSubDealer)
      let dataGroupsTable = [];
      this.setState({
                    dataGroupsTable:dataGroupsTable
                });
      const subDealer = selectedSubDealer;
      const subDealerId= subDealer.value;
      const fetchCustomer = await fetch('http://159.65.6.196:5500/api/subdealer/'+subDealerId+'?filter[include][customers][groups]');
      const res = await fetchCustomer.json()
      console.log('all customers of subDealer, res',res)
      const customers = res.customers;
      let snGroup = 1;
      for(var i in customers)
      {
        var customer = customers[i];
        if(undefined != customer && customer.groups.length > 0)
        {
          var group = customer.groups[0];
          dataGroupsTable.push({
              "id" : snGroup,
              "groupId":group.id,
              "name"  : group.name,
              "customer" : customer.name
          });
          snGroup++;
        }
      }

      this.setState({
                    dataGroupsTable:dataGroupsTable
                })
    }

    async getAllGroups(from)
    {
      let dataGroupsTable = []
      if(from == "groupManagement")
      {
        this.setState({
                      dataGroupsTable:dataGroupsTable
                      })
      }
      let optionsGroup = [{ value: 'all', label: 'All' }]
      const fetchManage = await fetch('/group/manage/fetch/'+this.state.manageCategory+'/group');
      const res = await fetchManage.json();
      console.log('res',res);
      const data = res.data;
      const manage = data.manage;
      var snGroup = 1;
      for(var i in manage) {
        var item = manage[i];
        var subDealers = ""
        if(this.state.manageCategory == "dealer")
        {
        subDealers = item.dealer.subDealers;
        for(var j in subDealers)
        {
          var subDealer = subDealers[j];
          optionsSubDealer.push({
            "value":subDealer.id,
            "label":subDealer.name
          });
          var customers = subDealers[j].customers;
          for(var k in customers){
            var customer = customers[k];
            optionsCustomer.push({
              "value":customer.id,
              "label":customer.name
            });

            var groups = customers[k].groups;
            for(var l in groups)
            {
              var group = groups[l];
              optionsGroup.push({
                "value":group.id,
                "label":group.name
              });
              dataGroupsTable.push({
                  "id" : snGroup,
                  "groupId":group.id,
                  "name"  : group.name,
                  "customer" : customer.name,
              });
              snGroup++;
            }
          }
        }
        }
        else if(this.state.manageCategory == "subdealer") {
          subDealers = item.subDealer;
            var subDealer = subDealers;
            optionsSubDealer.push({
              "value":subDealer.id,
              "label":subDealer.name
            });
            var customers = subDealer.customers;
            for(var k in customers){
              var customer = customers[k];
              optionsCustomer.push({
                "value":customer.id,
                "label":customer.name
              });
              var groups = customers[k].groups;
              for(var l in groups)
              {
                var group = groups[l];
                optionsGroup.push({
                  "value":group.id,
                  "label":group.name
                });
                dataGroupsTable.push({
                    "id" : snGroup,
                    "groupId":group.id,
                    "name"  : group.name,
                    "customer" : customer.name,
                });
                snGroup++;
              }
            }
        }
        else if(this.state.manageCategory == "customer") {
            var customer = item.customer;
              optionsCustomer.push({
                "value":customer.id,
                "label":customer.name
              });
              var groups = customer.groups;
              for(var l in groups)
              {
                var group = groups[l];
                optionsGroup.push({
                  "value":group.id,
                  "label":group.name
                });
                dataGroupsTable.push({
                    "id" : snGroup,
                    "groupId":group.id,
                    "name"  : group.name,
                    "customer" : customer.name,
                });
                snGroup++;
              }
        }
        else {
          console.log('something went wrong')
        }

      }

      if(from == "userManagement")
      {
        this.setState({
                      optionsGroup:optionsGroup
                      })
      }
      else if(from == "groupManagement")
      {
        this.setState({
                      dataGroupsTable:dataGroupsTable
                      })
      }
      else
      {
        console.log('something went wrong')
      }
    }

    async getAllCustomerProjectsOfSubDealer(selectedSubDealer)
    {
      console.log('inside getAllCustomerProjectsOfSubDealer, selectedSubDealer',selectedSubDealer)
      let dataProjectsTable = [];
      this.setState({
                      dataProjectsTable:dataProjectsTable
                    })
      const subDealer = selectedSubDealer;
      const subDealerId= subDealer.value;
      const fetchCustomer = await fetch('http://159.65.6.196:5500/api/subdealer/'+subDealerId+'?filter[include][customers][projects][groups]');
      const res = await fetchCustomer.json()
      console.log('all customers of subDealer, res',res)
      const customers = res.customers;
      let snProject = 1;
      for(var i in customers)
      {
        var customer = customers[i];
        if(undefined != customer && customer.projects.length > 0)
        {
          var project = customer.projects[0];
          var groups = project.groups;
          var addedGroupName = "";
          for(var j in groups)
          {
            var addedGroups = groups[j];
            addedGroupName += addedGroups.name+"\n";
          }
          dataProjectsTable.push({
              "sn" : snGroup,
              "projectId":project.id,
              "name"  : project.name,
              "customer" : customer.name,
              "customerId":project.customerId,
              "group":addedGroupName
          });
          snProject++;
        }
      }

      this.setState({
                    dataProjectsTable:dataProjectsTable
                })
    }

    async getCustomerProjects(from, selectedOption)
    {
      console.log('insdie getCustomerProjects')
      let dataProjectsTable = []
      this.setState({
                      dataProjectsTable:dataProjectsTable
                    })
      let customerId = selectedOption.value;
      let customerName = selectedOption.label;
      console.log('inside getCustomerProjects, customerId',customerId)
      const fetchCustomerProjects = await fetch('http://159.65.6.196:5500/api/customer/'+customerId+'/projects?filter[include][groups]');
      const res = await fetchCustomerProjects.json()
      console.log('customer projects, res',res)
      const data = res;
      let snProject = 1;
      for(var i in data)
        {
          var project = data[i]
          var groups = project.groups;
          var addedGroupName = "";
          for(var j in groups)
          {
            var addedGroups = groups[j];
            addedGroupName += addedGroups.name+"\n";
          }
          dataProjectsTable.push({
              "sn" : snProject,
              "projectId":project.id,
              "name"  : project.name,
              "customer" : customerName,
              "customerId":project.customerId,
              "group":addedGroupName
          });
          snProject++;
        }
      this.setState({
                    dataProjectsTable:dataProjectsTable
                    })
    }

    async getCustomerGroups(from, selectedOption)
    {
      if(from == "groupManagement")
      {
        this.setState({
                      dataGroupsTable:[]
                      })
      }
      let customerId = selectedOption.value;
      let customerName = selectedOption.label;
      console.log('inside getCustomerGroups, customerId',customerId)
      let optionsGroup = []
      const fetchCustomerGroups = await fetch('/customer/'+customerId+'/groups');
      const resData = await fetchCustomerGroups.json()
      console.log('resData',resData)
      const data = resData;
      if(from == "userManagement")
      {
        if(undefined != data && data.length > 0)
        {
        optionsGroup.push({ value: 'all', label: 'All' });
          for(var i in data)
          {
            var group = data[i]
            optionsGroup.push({
              "value":group.id,
              "label":group.name
            });
          }
        }
      this.setState({
                    optionsGroup:optionsGroup
                    })
      }
      else if(from == "groupManagement")
      {
        let dataGroupsTable = []
        let snGroup = 1;
        for(var i in data)
        {
          var group = data[i]
          dataGroupsTable.push({
              "id" : snGroup,
              "groupId":group.id,
              "name"  : group.name,
              "customer" : customerName,
          });
          snGroup++;
        }
        this.setState({
                      dataGroupsTable:dataGroupsTable
                      })
      }
      else {
        {
          console.log('something went wrong')
        }
      }
    }

    handleChangeGroupOption = (selectedOption) => {
        var groupId = selectedOption.value;
        this.setState({
                      selectedOption,
                      selectGroupValue:selectedOption});
        console.log(`handleChangeGroupOption, Option selected:`, selectedOption);
        console.log('groupId',groupId)
        if(undefined != groupId && groupId == "all")
        {
          //selectCustomerValue
          //If selectCustomerValue is any customer not all customer
          //So getting all group users based on customerId
          const {selectCustomerValue} = this.state;
          if(undefined != selectCustomerValue && selectCustomerValue.value != "all")
          {
              this.getAllGroupUsersOfCustomer(selectCustomerValue);
          }
          else
          {
            //getting all customer users
            this.getUsers();
          }
        }
        else if(undefined != groupId && groupId == "select")
        {

        }
        else if(undefined != groupId)
        {
          //getting customers of subDealer
          this.getGroupUsers(groupId);
        }

    }

    async getAllGroupUsersOfCustomer(selectedCustomer)
    {
      console.log('inside getAllGroupUsersOfCustomer, selectedCustomer',selectedCustomer)
      let dataUsersTable = [];
      this.setState({
                    dataUsersTable:dataUsersTable
                  });
      const customer = selectedCustomer;
      const customerId= customer.value;
      const fetchGroupUsers = await fetch('http://159.65.6.196:5500/api/customer/'+customerId+'?filter[include][users][manages][group]');
      const res = await fetchGroupUsers.json()
      console.log('all group users of customer, res',res)
      const users = res.users;
      let snUsers = 1;
      for(var i in users)
      {
        var item = users[i];
        var manages = item.manages;
        var manageGroupName = "";
        for(var j in manages)
        {
          var manageGroup = manages[j].group;
          manageGroupName += manageGroup.name+"\n";
        }
        dataUsersTable.push({
            "id" : snUsers,
            "userId": item.id,
            "customerId":item.customerId,
            "manageCategoryId":item.manageCategoryId,
            "phoneNumber" : item.phone,
            "name"  : item.firstName+' '+item.lastName,
            "manage" : manageGroupName,
            "role" : item.role,
            "status"  : item.status,
            "lastLogin"  : '15 Aug 2018',
          });
          snUsers++;
      }

      this.setState({
                    userTableLoading:false,
                    dataUsersTable:dataUsersTable
                    })
    }

    async getGroupUsers(groupId)
    {
      console.log('inside getCustomerUsers, groupId',groupId)
      let dataUsersTable = []
      this.setState({
                    dataUsersTable:dataUsersTable
                  });
      const fetchGroupUsers = await fetch('/group/fetchusersanditsmanages/'+groupId);
      const resData = await fetchGroupUsers.json()
      var snUsers = 1;
      console.log('customerUsers data',resData)
      const data = resData.data;
      for(var i in data) {
        var user = data[i].users
        var manages = user.manages;
        var manageGroupName = "";
        for(var j in manages)
        {
          var manageGroup = manages[j].group;
          manageGroupName += manageGroup.name+"\n";
        }
          dataUsersTable.push({
              "id" : snUsers,
              "userId": user.id,
              "customerId":user.customerId,
              "manageCategoryId":user.manageCategoryId,
              "phoneNumber" : user.phone,
              "name"  : user.firstName+' '+user.lastName,
              "manage" : manageGroupName,
              "role" : user.role,
              "status"  : user.status,
              "lastLogin"  : '15 Aug 2018',
            });
            snUsers++;
        }
        this.setState({
                      dataUsersTable:dataUsersTable
                      })
    }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  toggleAddUser() {
    console.log('inside toggleAddUser')
    this.setState({
      isOpenModalAddUser: !this.state.isOpenModalAddUser,
    });
  }

  async handleAddUser()
  {
    console.log('inside handleAddUser')
    this.setState({selectCustomerValueOfAddUser:''})
    //TODO getting subDealers
    const {manageCategory} = this.state;
    if(manageCategory == "dealer" || manageCategory == "subdealer")
    {
    this.getSubDealers();
    }
    else {
      this.getCustomers();
    }
    this.toggleAddUser();
  }

  async getSubDealers()
  {
    let subDealersData = [];
    const fetchManage = await fetch('/group/manage/fetch/'+this.state.manageCategory+'/customer');
    const res = await fetchManage.json();
    console.log('res',res);
    const data = res.data;
    const manage = data.manage;
    for(var i in manage) {
      var item = manage[i];
      var subDealers = ""
      if(this.state.manageCategory == "dealer")
      {
      subDealers = item.dealer.subDealers;
      for(var j in subDealers)
      {
        var subDealer = subDealers[j];
        subDealersData.push({
          "value":subDealer.id,
          "label":subDealer.name
        });
      }
      }
      else if(this.state.manageCategory == "subdealer") {
        subDealers = item.subDealer;
          var subDealer = subDealers;
          subDealersData.push({
            "value":subDealer.id,
            "label":subDealer.name
          });
      }
      else {
        subDealers = item.subDealer;
          var subDealer = subDealers;
          subDealersData.push({
            "value":subDealer.id,
            "label":subDealer.name
          });
      }

    }

    this.setState({
                  optionsSelectSubDealer:subDealersData,
                  optionsSelectCustomer:optionsSelectCustomer,
                  dataManageCustomerDropDown:dataManageCustomerDropDown,
                  dataManageGroupDropDown:dataManageGroupDropDown,
                  dataUnAssignGroupsDropDown:dataUnAssignGroupsDropDown
                  })
  }

  handleSelectSubDealer = (from,selectedOption) => {
    console.log('inside handleSelectSubDealer');
    console.log(`Option selected:`, from,selectedOption);
    //TODO getting customer according to subDealer
    this.getCustomersOfSubDealer(selectedOption.value);
    //check condition between addUser and addGroup
    if(from == "fromAddUser")
    {
      this.setState({
                      subDealerId:selectedOption.value,
                      selectCustomerValueOfAddUser:''
                  })
    }
    else if(from == "fromAddProject") {
      this.setState({
                      subDealerId:selectedOption.value,
                      selectCustomerValueOfAddProject:''
                  })
    }
    else if(from == "fromAddGroup") {
      this.setState({
                      subDealerId:selectedOption.value,
                      selectCustomerValueOfAddGroup:''
                  })
    }
    else {
      console.log('something went wrong')
    }
  }

  async getCustomers()
  {
    let customersData = [];
    const fetchManage = await fetch('/group/manage/fetch/'+this.state.manageCategory+'/group');
    const res = await fetchManage.json();
    console.log('res',res);
    const data = res.data;
    const manage = data.manage;
    for(var i in manage) {
      var item = manage[i];
      var customer = item.customer;
      customersData.push({
        "value":customer.id,
        "label":customer.name
      });

    }
    this.setState({
                  optionsSelectCustomer:customersData,
                  dataManageGroupDropDown:dataManageGroupDropDown,
                  dataUnAssignGroupsDropDown:dataUnAssignGroupsDropDown
                  })
  }

  async getCustomersOfSubDealer(subDealerId)
  {
    let customersData = [];
    const fetchCustomer = await fetch('/subdealer/'+subDealerId+'/customers');
    const customer = await fetchCustomer.json();
    console.log('customer',customer);
    for(var i in customer) {
      var item = customer[i];
      customersData.push({
          "label" : item.name,
          "value"  : item.id,
      });
    }
    this.setState({
                  optionsSelectCustomer:customersData,
                  dataManageGroupDropDown:dataManageGroupDropDown,
                  dataUnAssignGroupsDropDown:dataUnAssignGroupsDropDown
                  })

  }

  handleSelectCustomer = (from,selectedOption) => {
    console.log('inside handleSelectCustomer');
    console.log(`Option selected:`, from,selectedOption);
    if(from == "fromAddUser")
    {
      //TODO getting customer according to subDealer
      this.getGroups(from, selectedOption.value);
      this.setState({
                    selectCustomerValueOfAddUser:selectedOption,
                    customerId:selectedOption.value
                  })
    }
    else if(from == "fromAddProject")
    {
      this.getUnAssignedGroups(from, selectedOption.value);
      this.setState({
                    selectCustomerValueOfAddProject:selectedOption,
                    customerId:selectedOption.value
                  })
    }
    else if(from == "fromAddGroup")
    {
      this.setState({
                    selectCustomerValueOfAddGroup:selectedOption,
                    customerId:selectedOption.value
                  })
    }
    else {
      console.log('something went wrong')
    }
  }

  async getGroups(from, customerId)
  {
    const fetchGroup = await fetch('/customer/'+customerId+'/groups');
    const group = await fetchGroup.json();
    console.log('group',group);
    let groups = [];
    for(var i in group) {
      var item = group[i];
      groups.push({
          "label" : item.name,
          "value"  : item.id,
      });
    }
    if(from == "fromAddUser")
    {
      this.setState({
                    dataManageGroupDropDown:groups
                    })
    }

  }

  async getUnAssignedGroups(from, customerId)
  {
    const fetchGroup = await fetch('/group/fetchunassignedgrouptoproject/'+customerId);
    const res = await fetchGroup.json();
    console.log('fetch unassigned groups',res);
    const data = res.data;
    const unAssignGroups = data.groups;
    let groups = [];
    for(var i in unAssignGroups) {
      var group = unAssignGroups[i];
      groups.push({
          "label" : group.name,
          "value"  : group.id,
      });
    }
    this.setState({
                  dataUnAssignGroupsDropDown:groups
                  })
  }

  onChangeGroups = (from, currentNode, selectedNodes) => {
    console.log('onChangeGroups::,from, currentNode, selectedNodes', from, currentNode, selectedNodes)
    selectedNodesManage = selectedNodes;
    const {dataManageGroupDropDown, dataManagedDropDown} = this.state;
    if(from == "fromAddUser")
    {
      for(var i in dataManageGroupDropDown)
      {
        var item = dataManageGroupDropDown[i];
        if(item.value == currentNode.value)
        {
          dataManageGroupDropDown[i].checked = currentNode.checked;
        }
      }
      this.setState({
                    dataManageGroupDropDown:dataManageGroupDropDown
                    })
    }
    else if(from == "fromEditUser") {
      for(var i in dataManagedDropDown)
      {
        var item = dataManagedDropDown[i];
        if(item.value == currentNode.value)
        {
          dataManagedDropDown[i].checked = currentNode.checked;
        }
      }
      this.setState({
                    dataManagedDropDown:dataManagedDropDown
                    })
    }
  }

  handleSaveUser = async ()  =>
  {
    console.log('Group -->> inside handleSaveUser')
    const { phone,firstName,lastName,password,address,city,state,country,zip,role,status,customerId } = this.state;
    let manage = []
    if(this.validateSaveUser())
    {
      console.log('validated success')
      console.log('selectedNodesManage ', selectedNodesManage)

      for(var i in selectedNodesManage) {
          var item = selectedNodesManage[i];
          manage.push({
              "groupId" : item.value
            });
        }
      console.log('manage as: ',manage)
      let payload = {
                      phone:'+91'+phone,
                      firstName:firstName,
                      lastName:lastName,
                      password:password,
                      address:address,
                      city:city,
                      state:state,
                      country:country,
                      zipCode:zip,
                      role:role,
                      status:status,
                      customerId:customerId,
                      manageCategory: 'group',
                      userLevel:'group',
                      manage:manage
                    }
      console.log('payload: ',payload)
      console.log('payload: ',JSON.stringify(payload))
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json',
                    'X-CSRF-Token': await NextAuth.csrfToken()
                  },
        body : JSON.stringify(payload)
      };
      const res = await fetch('/group/user/register', requestOptions)
      const data = await res.json()
      console.log('register group user data ',data)
      selectedNodesManage = [];
      this.getUsers();
      this.toggleAddUser();
    }
    else
    {
      console.log('validation failed')
    }
  }

  validateSaveUser()
  {
    console.log('Group -->> inside validateSaveUser')
    const { phone,firstName,lastName,password,address,city,state,country,zip,role,status } = this.state;
    console.log('form as',phone,firstName,lastName,password,address,city,state,country,zip,role,status)
    if(undefined != phone && phone.length > 9)
    {
    }
    else {
      alert('please enter valid phone number')
      return false;
    }
    if(undefined != firstName && undefined != lastName)
    {
    }
    else {
      alert('please enter name')
      return false;
    }
    if(undefined != password && password.length > 4)
    {
    }
    else {
      alert('please enter password atleast 5 character')
      return false;
    }
    if(undefined != address && undefined != city && undefined != state && undefined != country)
    {
    }
    else {
      alert('please enter valid address')
      return false;
    }
    if(undefined != zip && zip.length > 5)
    {
    }
    else {
      alert('please enter valid zipcode, alteast 6 digit')
      return false;
    }
    if(undefined != role && role != "select")
    {
    }
    else {
      alert('please select valid role')
      return false;
    }
    if(undefined != status && status != "select")
    {
    }
    else {
      alert('please select valid status')
      return false;
    }
    if(selectedNodesManage.length > 0)
    {
    }
    else {
      alert('please select atleast one manage')
      return false;
    }
    return true;
  }

  toggleEditUser() {
    console.log('inside toggleEditUser')
    this.setState({
      isOpenModalEditUser: !this.state.isOpenModalEditUser,
    });
  }

  async editUser(row) {
    //console.log('inside editUser', e.target.innerHTML)
    console.log('inside editUser, row', row)
    selectedNodesManage = [];
    const userRes = await fetch('/group/user/'+row.userId)
    const user = await userRes.json();
    console.log('user: ',user)
    const manageCategory = "group";
    const manageRes = await fetch('/group/fetchusermanagesandavailablemanages/'+row.userId+'/'+manageCategory+'/'+row.customerId)
    const manageByUser = await manageRes.json();
    console.log('manageByUser: ',manageByUser)
    const data = manageByUser.data;
    const allAvailableManages = data.allAvailableManages.docs.docs;
    const manage = data.manage;
    let dataManagedDropDown = []
    for(var i in allAvailableManages) {
      var item = allAvailableManages[i];
      var itemFound = false;
      for(var j in manage)
      {
        var managedItem = manage[j]
        console.log('group: ',item.id)
        console.log('managedGroup ', managedItem.groupId)
        if (item.id == managedItem.groupId )
        {
         itemFound = true;
        break;
        }
        else {
          itemFound = false;
          continue;
        }

      }
      console.log('itemFound', itemFound)
      if(itemFound)
        {
          dataManagedDropDown.push({
            //"label" : item.name,
            "label" : item.name,
            "value"  : item.id,
            checked: true
          });
        }
        else {
          dataManagedDropDown.push({
            //"label" : item.name,
            "label" : item.name,
            "value"  : item.id,
          });
        }

    }
    console.log('dataManagedDropDown:', dataManagedDropDown);
    selectedNodesManage = dataManagedDropDown;
    //
    const roles = this.createSelectRoleItems(user.role)
    const statuses = this.createSelectStatusItems(user.status)
    this.setState({
                  editUserId:row.userId,
                  editFirstName:user.firstName,
                  editLastName:user.lastName,
                  editAddress:user.address,
                  editCity:user.city,
                  editState:user.state,
                  editCountry:user.country,
                  editZip:user.zipCode,
                  editRole:user.role,
                  roles:roles,
                  editStatus:user.status,
                  statuses:statuses,
                  dataManagedDropDown:dataManagedDropDown,
                  editManageCategoryId:row.manageCategoryId
                  })

    this.toggleEditUser();
  }

  createSelectRoleItems(role)
  {
    //TODO roles came from database
    let roles = [{id:'1',name:'admin'},{id:'2',name:'non-admin'}]
    console.log('inside createSelectRoleItems, role, roles',role,roles)
    let items = [];
    if(undefined != role)
    {
      items.push(<option key={0} value={role}>{role}</option>);
      for(var i in roles)
      {
        if(roles[i].name != role)
        {
        items.push(<option key={i} value={roles[i].name}>{roles[i].name}</option>);
        }
      }
    }
    else
    {
       items = roles;
    }
    return items;
  }

  createSelectStatusItems(status)
  {
    //TODO roles came from database
    let statuses = [{id:'1',name:'active'},{id:'2',name:'inactive'}]
    let items = [];
    if(undefined != status)
    {
      items.push(<option key={0} value={status}>{status}</option>);
      for(var i in statuses)
      {
        if(statuses[i].name != status)
        {
        items.push(<option key={i} value={statuses[i].name}>{statuses[i].name}</option>);
        }
      }
    }
    else
    {
      items = statuses;
    }
     return items;
  }

  async handleUpdateUser ()
  {
    console.log('inside handleUpdateUser')
    const {editUserId, editManageCategoryId ,editFirstName, editLastName, editAddress, editCity, editState, editCountry, editZip, editRole, editStatus} = this.state
    console.log('selectedNodesManage',selectedNodesManage);
    let manage = []
    if(this.validateUpdateUser())
    {
    for(var i in selectedNodesManage) {
        var item = selectedNodesManage[i];
        if(item.checked)
        {
        manage.push({
            "groupId" : item.value,
            "name"  : item.label,
          });
        }
      }
    console.log('manage as: ',manage)
    let payload = {
                    firstName:editFirstName,
                    lastName:editLastName,
                    address:editAddress,
                    city:editCity,
                    state:editState,
                    country:editCountry,
                    zipCode:editZip,
                    role:editRole,
                    status:editStatus,
                    manage:manage,
                    manageCategoryId: editManageCategoryId
                  }
    console.log('payload for update user: ',payload)
    console.log('payload: ',JSON.stringify(payload))
    const requestOptions = {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    const groupId = '5b6055424943b326da064363';
    const res = await fetch('/group/updateuser/'+editUserId, requestOptions)
    const data = await res.json()
    console.log('updated group user data ',data)
    selectedNodesManage = [];
    this.getUsers();
    this.toggleEditUser();
    }
    else {
      console.log('validation failed for update user')
    }
  }

  validateUpdateUser()
  {
    console.log('User_Management -->> inside validateSaveUser')
    const {editFirstName, editLastName, editAddress, editCity, editState, editCountry, editZip, editRole, editStatus} = this.state

    if(undefined != editFirstName && undefined != editLastName)
    {
    }
    else {
      alert('please enter name')
      return false;
    }
    if(undefined != editAddress && undefined != editCity && undefined != editState && undefined != editCountry)
    {
    }
    else {
      alert('please enter valid address')
      return false;
    }
    if(undefined != editZip && editZip.length > 5)
    {
    }
    else {
      alert('please enter valid zipcode, alteast 6 digit')
      return false;
    }
    if(undefined != editRole && editRole != "select")
    {
    }
    else {
      alert('please select valid role')
      return false;
    }
    if(undefined != editStatus && editStatus != "select")
    {
    }
    else {
      alert('please select valid status')
      return false;
    }
    if(selectedNodesManage.length > 0)
    {
    }
    else {
      alert('please select atleast one manage')
      return false;
    }
    return true;
  }

  confirmDeleteUser(row)
  {
    deleteUserRow = row;
    console.log('deleteUserRow',deleteUserRow)
    this.toggleDeleteUser();
  }

  toggleDeleteUser() {
    this.setState({
      isOpenModalDeleteUser: !this.state.isOpenModalDeleteUser,
    });
  }

  async deleteUser() {
    const row = deleteUserRow;
    console.log('inside deleteUser, row',row)
    const requestOptions = {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                }
    };
    const res = await fetch('/group/deleteuser/'+row.userId, requestOptions)
    const data = await res.json()
    console.log('delete user response data ',data)
    this.getUsers();
    this.toggleDeleteUser();
  }

  /////////////
  // Project Management

  async handleAddProject()
  {
    console.log('inside handleAddProject')
    this.setState({selectCustomerValueOfAddProject:''})
    const {manageCategory} = this.state;
    if(manageCategory == "dealer" || manageCategory == "subdealer")
    {
    this.getSubDealers();
    }
    else {
      this.getCustomers();
    }
    this.toggleAddProject();
  }

  toggleAddProject()
  {
    console.log('inside toggleAddProject')
    this.setState({
      isOpenModalAddProject: !this.state.isOpenModalAddProject,
    });
  }

  onChangeGroupsAtAddProject = (currentNode, selectedNodes) => {
    console.log('onChangeGroupsAtAddProject::, currentNode', currentNode)
    console.log('selected Nodes', selectedNodes)
    selectedNodesGroup = selectedNodes;
    const {dataUnAssignGroupsDropDown} = this.state;
    for(var i in dataUnAssignGroupsDropDown)
    {
      var item = dataUnAssignGroupsDropDown[i];
      if(item.value == currentNode.value)
      {
        dataUnAssignGroupsDropDown[i].checked = currentNode.checked;
      }
    }
    this.setState({
                  dataUnAssignGroupsDropDown:dataUnAssignGroupsDropDown
                  })
  }

  handleSaveProject = async ()  =>
  {
    console.log('inside handleSaveProject')
    console.log('selectedNodesGroup ', selectedNodesGroup)
    const {selectCustomerValueOfAddProject, projectName} = this.state;
    console.log('customer',selectCustomerValueOfAddProject)
    const customerId = selectCustomerValueOfAddProject.value;
    let groups = [];
    for(var i in selectedNodesGroup) {
        var item = selectedNodesGroup[i];
        groups.push(item.value);
      }
    let payload = {
                    name:projectName,
                    customerId:customerId,
                    groups:groups
                  }
    console.log('save project payload',payload);
    console.log('payload: ',JSON.stringify(payload))
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    const res = await fetch('/project/create', requestOptions)
    const data = await res.json()
    console.log('created project with its manages',data)
    selectedNodesGroup = [];
    this.getProjectsAndItsGroups();
    this.toggleAddProject();
  }

  async editProject(row)
  {
    console.log('inside editProject, row',row)
    const customerId = row.customerId;
    const projectId = row.projectId;
    const projectName = row.name;
    const fetchProject = await fetch('/group/fetchunassignedgroupandprojectgroup/'+customerId+'/'+projectId)
    const res = await fetchProject.json();
    console.log('fetch project res',res)
    const data = res.data;
    const groups = data.groups;
    let projectGroupsDropDown = [];
    let assignedGroupsOnProject = [];
    for(var i in groups)
    {
      var group = groups[i];
      var groupProjectId = group.projectId;
      if (undefined != groupProjectId)
      {
        projectGroupsDropDown.push({
          "label" : group.name,
          "value"  : group.id,
          checked: true
        });
        assignedGroupsOnProject.push({
          "label" : group.name,
          "value"  : group.id,
          checked: true
        });
      }
      else {
        projectGroupsDropDown.push({
          "label" : group.name,
          "value"  : group.id,
          checked: false
        });
      }
    }

    this.setState({
                  editProjectName:projectName,
                  editProjectId:projectId,
                  assignedGroupsOnProject:assignedGroupsOnProject,
                  projectGroupsDropDown:projectGroupsDropDown,
                  selectedGroupsOfProject:assignedGroupsOnProject
                })
    this.toggleEditProject();
  }

  toggleEditProject()
  {
    console.log('inside toggleEditProject')
    this.setState({
      isOpenModalEditProject: !this.state.isOpenModalEditProject,
    });
  }

  onChangeGroupsAtEditProject = (currentNode, selectedNodes) => {
    console.log('onChangeGroupsAtEditProject::, currentNode', currentNode)
    console.log('selected Nodes', selectedNodes)
    const {projectGroupsDropDown, assignedGroupsOnProject} = this.state;
    console.log('projectGroupsDropDown before change',projectGroupsDropDown)
    for(var i in projectGroupsDropDown)
    {
      var item = projectGroupsDropDown[i];
      if(item.value == currentNode.value)
      {
        projectGroupsDropDown[i].checked = currentNode.checked;
      }
    }
    console.log('projectGroupsDropDown after change',projectGroupsDropDown)
    this.setState({
                  selectedGroupsOfProject:selectedNodes,
                  projectGroupsDropDown:projectGroupsDropDown
                  })
  }

  async handleUpdateProject()
  {
    console.log('inside handleUpdateProject')
    const {editProjectName, editProjectId, projectGroupsDropDown, assignedGroupsOnProject, selectedGroupsOfProject} = this.state;
    console.log('projectGroupsDropDown',projectGroupsDropDown);
    console.log('assignedGroupsOnProject',assignedGroupsOnProject);
    console.log('selectedGroupsOfProject',selectedGroupsOfProject);
    let groupsForUpdateProject = []
    for(var i in assignedGroupsOnProject)
    {
      var assignedGroup = assignedGroupsOnProject[i];
      var found = false;
      for(var j in selectedGroupsOfProject)
      {
        var selectedGroup = selectedGroupsOfProject[j];
        if(assignedGroup.value == selectedGroup.value)
        {
          found = true;
          break;
        }
        else {
          found = false;
        }
      }
      if(found)
      {
        console.log('same assignedGroup found', found)
        console.log('assignedGroup not removed')
      }
      else {
        console.log('same assignedGroup found', found)
        console.log('assignedGroup removed, assignedGroup',assignedGroup)
        groupsForUpdateProject.push({groupId:assignedGroup.value, operation:'removegroupfromproject'})
      }
    }

    for(var k in selectedGroupsOfProject)
    {
      var selectedGroup = selectedGroupsOfProject[k];
      var found = false;
      for(var l in assignedGroupsOnProject)
      {
        var assignedGroup = assignedGroupsOnProject[l];
        if(selectedGroup.value == assignedGroup.value)
        {
          found = true;
          break;
        }
        else {
          found = false;
        }
      }
      if(found)
      {
        console.log('same selectedGroup found', found)
        console.log('new group not added')
      }
      else {
        console.log('same selectedGroup found', found)
        console.log('new group added, selectedGroup',selectedGroup)
        groupsForUpdateProject.push({groupId:selectedGroup.value, operation:'addgroup'})
      }
    }
    console.log('groupsForUpdateProject',groupsForUpdateProject)
    let payload = {
                  name:editProjectName,
                  groups:groupsForUpdateProject
                  }
    const requestOptions = {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    console.log('payload: ',payload)
    console.log('payload JSON: ',JSON.stringify(payload))
    const updateProject = await fetch('/project/update/'+editProjectId, requestOptions)
    const res = await updateProject.json()
    console.log('updated project res',res)
    this.getProjectsAndItsGroups();
    this.toggleEditProject();
  }
  confirmDeleteProject(row)
  {
    deleteProjectRow = row;
    console.log('inside confirmDeleteProject, deleteProjectRow',deleteProjectRow)
    this.toggleDeleteProject();
  }
  toggleDeleteProject()
  {
    console.log('inside toggleDeleteProject')
    this.setState({
      isOpenModalDeleteProject: !this.state.isOpenModalDeleteProject,
    });
  }

  async deleteProject()
  {
    const row = deleteProjectRow;
    console.log('inside deleteProject, row',row)
    this.toggleDeleteProject();
  }

  ///////////////////
  // Group Management

  async handleAddGroup()
  {
    console.log('inside handleAddGroup')
    this.setState({selectCustomerValueOfAddGroup:''})
    const {manageCategory} = this.state;
    if(manageCategory == "dealer" || manageCategory == "subdealer")
    {
    this.getSubDealers();
    }
    else {
      this.getCustomers();
    }
    this.toggleAddGroup();
  }

  toggleAddGroup()
  {
      console.log('inside toggleAddGroup')
      this.setState({
        isOpenModalAddGroup: !this.state.isOpenModalAddGroup,
      });
  }

  handleSaveGroup = async ()  =>
  {
    console.log('Group -->> inside handleSaveGroup')
    const { groupName,customerId } = this.state;
    console.log('groupName, customerId',groupName,customerId)
    if(undefined == customerId)
    {
      alert('please select customer')
      return false;
    }
    if(undefined != customerId && undefined != groupName && groupName != "")
    {
    let payload = {
                    customerId:customerId,
                    name:groupName
                  }
      console.log('payload: ',payload)
      console.log('payload: ',JSON.stringify(payload))
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json',
                    'X-CSRF-Token': await NextAuth.csrfToken()
                  },
        body : JSON.stringify(payload)
      };
      const res = await fetch('/group/create', requestOptions)
      const data = await res.json()
      console.log('register group data ',data)
      this.getManages();
      this.getUsers();
      this.toggleAddGroup();
    }
    else {
      alert('please enter valid group name')
    }
  }


  toggleEditGroup() {
    console.log('inside toggleEditGroup')
    this.setState({
      isOpenModalEditGroup: !this.state.isOpenModalEditGroup,
    });
  }

  async editGroup(row)
  {
    console.log('inside editGroup, row',row)
    const groupRes = await fetch('/group/'+row.groupId)
    const group = await groupRes.json();
    console.log('group: ',group)
    this.setState({
                  editGroupName:group.name,
                  editGroupId:group.id
    })
    this.toggleEditGroup();
  }

  async handleUpdateGroup ()
  {
    console.log('inside handleUpdateGroup')
    const {editGroupName, editGroupId} = this.state;
    console.log('edited groupName',editGroupName, editGroupId)
    if(undefined != editGroupName && editGroupName != "")
    {
    let payload = {
                  name:editGroupName
                  }
    const requestOptions = {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    console.log('payload: ',JSON.stringify(payload))
    const res = await fetch('/group/'+editGroupId, requestOptions)
    const data = await res.json()
    console.log('updated group data ',data)
    this.getManages();
    this.getUsers();
    this.toggleEditGroup();
    }
    else {
      alert('please enter valid group name')
    }
  }

  confirmDeleteGroup(row)
  {
    deleteGroupRow = row;
    console.log('deleteGroupRow',deleteGroupRow)
    this.toggleDeleteGroup();
  }

  toggleDeleteGroup() {
    this.setState({
      isOpenModalDeleteGroup: !this.state.isOpenModalDeleteGroup,
    });
  }

  async deleteGroup() {
    const row = deleteGroupRow;
    console.log('inside deleteGroup, row',row)
    const requestOptions = {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                }
    };
    const res = await fetch('/group/'+row.groupId, requestOptions)
    const data = await res.json()
    console.log('delete group response data ',data)
    //TODO if delete success:false then show msg that came from server
    this.getManages();
    this.getUsers();
    this.toggleDeleteGroup();
  }

  render() {
        console.log('Group -->> inside render')
        const { selectedOption,isClearable,phone,firstName,lastName,password,address,city,state,country,zip } = this.state;
        const {editFirstName, editLastName, editAddress, editCity, editState, editCountry, editZip} = this.state
        const {groupName, editGroupName} = this.state;
        const {projectName, editProjectName} = this.state;

        //const editFormatter = (cell, row, rowIndex, formatExtraData) => { console.log('editFormatter, row ',row); return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editUser.bind(this, row)}>Edit</Button> ); }
        const editFormatter = (cell, row, rowIndex, formatExtraData) => { return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editUser.bind(this, row)}>Edit</Button> ); }

        const deleteFormatter = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.confirmDeleteUser.bind(this, row)}>Delete</Button> ); }

        const editFormatterProject = (cell, row, rowIndex, formatExtraData) => { return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editProject.bind(this, row)}>Edit</Button> ); }

        const deleteFormatterProject = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.confirmDeleteProject.bind(this, row)}>Delete</Button> ); }

        const editFormatterGroup = (cell, row, rowIndex, formatExtraData) => { return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.editGroup.bind(this, row)}>Edit</Button> ); }

        const deleteFormatterGroup = (cell, row, rowIndex, formatExtraData) => {  return ( <Button block color="primary" className="btn-pill" size="sm" onClick={this.confirmDeleteGroup.bind(this, row)}>Delete</Button> ); }

        const columnsUsersTable = [{
          dataField: 'id',
          text: 'SN',
          sort: true
        }, {
          dataField: 'phoneNumber',
          text: 'Phone Number',
          filter: textFilter()
        }, {
          dataField: 'name',
          text: 'Name',
          sort: true,
          filter: textFilter()
        }, {
          dataField: 'role',
          text: 'Role'
        }, {
          dataField: 'status',
          text: 'Status'
        }, {
          dataField: 'manage',
          text: 'Manage'
        }, {
          dataField: 'lastLogin',
          text: 'Last Login',
          hidden: true
        }, {
          dataField: 'edit',
          text: '',
          formatter: editFormatter,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          }
        }, {
          dataField: 'delete',
          text: '',
          formatter: deleteFormatter,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          },
          /*
          events: {
            onClick: e => { this.deleteUser() }
          }
          */
        }];

        const columnsProjectsTable = [{
          dataField: 'sn',
          text: 'SN',
          sort: true
        }, {
          dataField: 'name',
          text: 'Name',
          sort: true,
          filter: textFilter()
        }, {
          dataField: 'customer',
          text: 'Customer',
          sort: true,
          filter: textFilter()
        }, {
          dataField: 'group',
          text: 'Group',
          sort: true,
          filter: textFilter()
        }, {
          dataField: 'edit',
          text: '',
          formatter: editFormatterProject,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          }
        }, {
          dataField: 'delete',
          text: '',
          formatter: deleteFormatterProject,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          },
          hidden:true
        }];

        const columnsGroupsTable = [{
          dataField: 'id',
          text: 'SN',
          sort: true
        }, {
          dataField: 'name',
          text: 'Name',
          sort: true,
          filter: textFilter()
        }, {
          dataField: 'customer',
          text: 'Customer',
          sort: true,
          filter: textFilter()
        }, {
          dataField: 'edit',
          text: '',
          formatter: editFormatterGroup,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          }
        }, {
          dataField: 'delete',
          text: '',
          formatter: deleteFormatterGroup,
          style: {
            fontWeight: 'bold',
            fontSize: '16px',
            cursor:'pointer',
            color:'blue'
          },
        }];

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12">
            <Card>
              <CardBody>
                <p><b>Group</b></p>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs="12" className="mb-4">
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '1' })}
                  onClick={() => { this.toggle('1'); }}
                >
                  User Management
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '2' })}
                  onClick={() => { this.toggle('2'); }}
                >
                  Project Management
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '3' })}
                  onClick={() => { this.toggle('3'); }}
                >
                  Group Management
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={this.state.activeTab}>
              <TabPane tabId="1">
              <Row>
                <Col>
                  <Card>
                    <CardBody>
                    <Modal isOpen={this.state.isOpenModalAddUser} toggle={this.toggleAddUser}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleAddUser}>Add New User</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Phone</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="text-input" name="phone" placeholder="Phone" value={phone} onChange={this.handleChange} />
                              {
                                //<FormText color="muted">This is a help text</FormText>
                              }
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">First Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="firstName" name="firstName" placeholder="First Name" value={firstName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Last Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="lastName" name="lastName" placeholder="Last Name" value={lastName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Password</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="password" id="password" name="password" placeholder="Password" value={password} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Address</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="address" name="address" placeholder="Address" value={address} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">City</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="city" name="city" placeholder="City" value={city} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">State</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="state" name="state" placeholder="State" value={state} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Country</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="country" name="country" placeholder="Country" value={country} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Zip</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="zip" name="zip" placeholder="Zip" value={zip} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Role</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="role" id="selectRole" onChange={this.handleChange}>
                                <option value="select">Please Select</option>
                                <option value="admin">Admin</option>
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Status</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="status" id="selectStatus" onChange={this.handleChange}>
                                <option value="select">Please Select</option>
                                <option value="active">Active</option>
                                <option value="inactive">InActive</option>
                              </Input>
                            </Col>
                          </FormGroup>
                          {
                          //TODO not to show when customer level user logged in
                          this.state.isSubDealerShow &&
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">SubDealer</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Select
                              onChange={this.handleSelectSubDealer.bind(this,'fromAddUser')}
                              isClearable={isClearable}
                              options={this.state.optionsSelectSubDealer}
                              placeholder={'Select SubDealer'}
                              backspaceRemovesValue={false}
                            />
                            </Col>
                          </FormGroup>
                          }
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Customer</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Select
                              onChange={this.handleSelectCustomer.bind(this,'fromAddUser')}
                              value={this.state.selectCustomerValueOfAddUser}
                              isClearable={isClearable}
                              options={this.state.optionsSelectCustomer}
                              placeholder={'Select Customer'}
                              backspaceRemovesValue={false}
                            />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Manage Group</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <DropdownTreeSelect
                                data={this.state.dataManageGroupDropDown}
                                onChange={this.onChangeGroups.bind(this,'fromAddUser')}
                              />
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleSaveUser}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleAddUser}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalEditUser} toggle={this.toggleEditUser}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleEditUser}>Edit User</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">First Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="firstName" name="editFirstName" placeholder="First Name" value={editFirstName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Last Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="lastName" name="editLastName" placeholder="Last Name" value={editLastName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Address</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="address" name="editAddress" placeholder="Address" value={editAddress} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">City</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="city" name="editCity" placeholder="City" value={editCity} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">State</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="state" name="editState" placeholder="State" value={editState} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Country</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="country" name="editCountry" placeholder="Country" value={editCountry} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Zip</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="zip" name="editZip" placeholder="Zip" value={editZip} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Role</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="editRole" id="selectRole" onChange={this.handleChange}>
                                {
                                this.state.roles
                                }
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Status</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="select" name="editStatus" id="selectStatus" onChange={this.handleChange}>
                              {
                                this.state.statuses
                              }
                              </Input>
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Manage</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <DropdownTreeSelect
                                data={this.state.dataManagedDropDown}
                                onChange={this.onChangeGroups.bind(this,'fromEditUser')}
                              />
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleUpdateUser}>Update</Button>{' '}
                        <Button color="secondary" onClick={this.toggleEditUser}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalDeleteUser} toggle={this.toggleDeleteUser}
                           className={'modal-danger ' + this.props.className}>
                      <ModalHeader toggle={this.toggleDeleteUser}>User Delete</ModalHeader>
                      <ModalBody>
                        Are you sure to delete user
                      </ModalBody>
                      <ModalFooter>
                        <Button color="danger" onClick={this.deleteUser}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteUser}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                      {
                        this.state.userTableLoading &&
                        <div>
                        <div style={{marginLeft: '40%'}}>
                        <ClipLoader
                          sizeUnit={"px"}
                          size={50}
                          color={'#123abc'}
                          loading={this.state.userTableLoading}
                        />
                        </div>
                        <div style={{marginLeft: '35%'}}>
                        <p>Loading ... Please wait!</p>
                        </div>
                        </div>
                      }
                      {
                      !this.state.userTableLoading &&
                      <ToolkitProvider
                            keyField="id"
                            data={ this.state.dataUsersTable }
                            columns={ columnsUsersTable }
                            search
                          >
                            {
                              props => (
                                <div>
                                  <Row className="align-items-center">
                                  <Col>
                                  <Row>
                                  {
                                  //TODO not to show when customer level user logged in
                                  this.state.isSubDealerShow &&
                                  <Col md="4">
                                  <p>SubDealer</p>
                                  <Select
                                    onChange={this.handleChangeSubDealerOption.bind(this,'userManagement')}
                                    defaultValue={this.state.optionsSubDealer[0]}
                                    isClearable={isClearable}
                                    options={this.state.optionsSubDealer}
                                    placeholder={'Select SubDealer'}
                                    backspaceRemovesValue={false}
                                  />
                                  </Col>
                                  }
                                  <Col md="4">
                                  <p>Customer</p>
                                  <Select
                                    onChange={this.handleChangeCustomerOption.bind(this,'userManagement')}
                                    value={this.state.selectCustomerValue}
                                    isClearable={isClearable}
                                    options={this.state.optionsCustomer}
                                    placeholder={'Select Customer'}
                                    backspaceRemovesValue={false}
                                  />
                                  </Col>
                                  <Col md="4">
                                  <p>Group</p>
                                  <Select
                                    onChange={this.handleChangeGroupOption}
                                    value={this.state.selectGroupValue}
                                    isClearable={isClearable}
                                    options={this.state.optionsGroup}
                                    placeholder={'Select Group'}
                                    backspaceRemovesValue={false}
                                  />
                                  </Col>
                                  </Row>
                                  </Col>

                                  <Col md="2">
                                  {
                                    //<h4>Input something at below input field:</h4>
                                  }
                                  <SearchBar
                                    { ...props.searchProps }
                                  />
                                  </Col>
                                  <Col md="2">
                                    <Button block color="primary" className="btn-pill" onClick={this.handleAddUser}>Add User</Button>
                                  </Col>
                                  </Row>
                                  <hr />
                                  <div style={{overflow: 'overlay'}} className="table-responsive">
                                  <BootstrapTable
                                    { ...props.baseProps }
                                    filter={ filterFactory() }
                                    hover
                                  />
                                  </div>
                                </div>
                              )
                            }
                          </ToolkitProvider>
                          }
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="2">
              <Row>
                <Col>
                  <Card>
                    <CardBody>
                    <Modal isOpen={this.state.isOpenModalAddProject} toggle={this.toggleAddProject}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleAddProject}>Add New Project</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          {
                          //TODO not to show when customer level user logged in
                          this.state.isSubDealerShow &&
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">SubDealer</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Select
                              onChange={this.handleSelectSubDealer.bind(this,'fromAddProject')}
                              isClearable={isClearable}
                              options={this.state.optionsSelectSubDealer}
                              placeholder={'Select SubDealer'}
                              backspaceRemovesValue={false}
                            />
                            </Col>
                          </FormGroup>
                          }
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Customer</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Select
                              onChange={this.handleSelectCustomer.bind(this,'fromAddProject')}
                              value={this.state.selectCustomerValueOfAddProject}
                              isClearable={isClearable}
                              options={this.state.optionsSelectCustomer}
                              placeholder={'Select Customer'}
                              backspaceRemovesValue={false}
                            />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Groups</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <DropdownTreeSelect
                              data={this.state.dataUnAssignGroupsDropDown}
                              onChange={this.onChangeGroupsAtAddProject.bind(this)}
                            />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="text-input" name="projectName" placeholder="Name" value={projectName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleSaveProject}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleAddProject}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalEditProject} toggle={this.toggleEditProject}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleEditProject}>Edit Project</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="text-input" name="editProjectName" placeholder="Name" value={editProjectName} onChange={this.handleChange} />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Groups</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <DropdownTreeSelect
                                data={this.state.projectGroupsDropDown}
                                onChange={this.onChangeGroupsAtEditProject.bind(this)}
                              />
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleUpdateProject}>Update</Button>{' '}
                        <Button color="secondary" onClick={this.toggleEditProject}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalDeleteProject} toggle={this.toggleDeleteProject}
                           className={'modal-danger ' + this.props.className}>
                      <ModalHeader toggle={this.toggleDeleteProject}>Project Delete</ModalHeader>
                      <ModalBody>
                        Are you sure to delete project
                      </ModalBody>
                      <ModalFooter>
                        <Button color="danger" onClick={this.deleteProject}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteProject}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    {
                      this.state.projectTableLoading &&
                      <div>
                      <div style={{marginLeft: '40%'}}>
                      <ClipLoader
                        sizeUnit={"px"}
                        size={50}
                        color={'#123abc'}
                        loading={this.state.projectTableLoading}
                      />
                      </div>
                      <div style={{marginLeft: '35%'}}>
                      <p>Loading ... Please wait!</p>
                      </div>
                      </div>
                    }
                    {
                    !this.state.projectTableLoading &&
                    <ToolkitProvider
                          keyField="sn"
                          data={ this.state.dataProjectsTable }
                          columns={ columnsProjectsTable }
                          search
                        >
                          {
                            props => (
                              <div>
                                <Row className="align-items-center">
                                <Col>
                                <Row>
                                {
                                //TODO not to show when customer level user logged in
                                this.state.isSubDealerShow &&
                                <Col md="4">
                                <p>SubDealer</p>
                                <Select
                                  onChange={this.handleChangeSubDealerOption.bind(this,'projectManagement')}
                                  defaultValue={this.state.optionsSubDealer[0]}
                                  isClearable={isClearable}
                                  options={this.state.optionsSubDealer}
                                  placeholder={'Select SubDealer'}
                                  backspaceRemovesValue={false}
                                />
                                </Col>
                                }
                                <Col md="4">
                                <p>Customer</p>
                                <Select
                                  onChange={this.handleChangeCustomerOption.bind(this,'projectManagement')}
                                  value={this.state.selectCustomerValueOfProjectMgmt}
                                  isClearable={isClearable}
                                  options={this.state.optionsCustomerOfProjectMgmt}
                                  placeholder={'Select Customer'}
                                  backspaceRemovesValue={false}
                                />
                                </Col>
                                </Row>
                                </Col>
                                <Col md="2">
                                {
                                  //<h4>Input something at below input field:</h4>
                                }
                                <SearchBar
                                  { ...props.searchProps }
                                />
                                </Col>
                                <Col md="2">
                                  <Button block color="primary" className="btn-pill" onClick={this.handleAddProject}>Add Project</Button>
                                </Col>
                                </Row>
                                <hr />
                                <div style={{overflow: 'overlay'}}>
                                <BootstrapTable
                                  containerStyle={{width: '200%',overflowX: 'scroll'}}
                                  { ...props.baseProps }
                                  filter={ filterFactory() }
                                  hover
                                />
                                </div>
                              </div>
                            )
                          }
                        </ToolkitProvider>
                        }
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="3">
              <Row>
                <Col>
                  <Card>
                    <CardBody>
                    <Modal isOpen={this.state.isOpenModalAddGroup} toggle={this.toggleAddGroup}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleAddGroup}>Add New Group</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          {
                          //TODO not to show when customer level user logged in
                          this.state.isSubDealerShow &&
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">SubDealer</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Select
                              onChange={this.handleSelectSubDealer.bind(this,'fromAddGroup')}
                              isClearable={isClearable}
                              options={this.state.optionsSelectSubDealer}
                              placeholder={'Select SubDealer'}
                              backspaceRemovesValue={false}
                            />
                            </Col>
                          </FormGroup>
                          }
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="select">Customer</Label>
                            </Col>
                            <Col xs="12" md="9">
                            <Select
                              onChange={this.handleSelectCustomer.bind(this,'fromAddGroup')}
                              value={this.state.selectCustomerValueOfAddGroup}
                              isClearable={isClearable}
                              options={this.state.optionsSelectCustomer}
                              placeholder={'Select Customer'}
                              backspaceRemovesValue={false}
                            />
                            </Col>
                          </FormGroup>
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="text-input" name="groupName" placeholder="Name" value={groupName} onChange={this.handleChange} />
                              {
                                //<FormText color="muted">This is a help text</FormText>
                              }
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleSaveGroup}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleAddGroup}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalEditGroup} toggle={this.toggleEditGroup}
                           className={'modal-primary ' + this.props.className}>
                      <ModalHeader toggle={this.toggleEditGroup}>Edit Group</ModalHeader>
                      <ModalBody>
                      <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                          <FormGroup row>
                            <Col md="3">
                              <Label htmlFor="text-input">Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                              <Input type="text" id="text-input" name="editGroupName" placeholder="Name" value={editGroupName} onChange={this.handleChange} />
                              {
                                //<FormText color="muted">This is a help text</FormText>
                              }
                            </Col>
                          </FormGroup>
                        </Form>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={this.handleUpdateGroup}>Update</Button>{' '}
                        <Button color="secondary" onClick={this.toggleEditGroup}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    <Modal isOpen={this.state.isOpenModalDeleteGroup} toggle={this.toggleDeleteGroup}
                           className={'modal-danger ' + this.props.className}>
                      <ModalHeader toggle={this.toggleDeleteGroup}>Group Delete</ModalHeader>
                      <ModalBody>
                        Are you sure to delete group
                      </ModalBody>
                      <ModalFooter>
                        <Button color="danger" onClick={this.deleteGroup}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteGroup}>Cancel</Button>
                      </ModalFooter>
                    </Modal>
                    {
                      this.state.groupTableLoading &&
                      <div>
                      <div style={{marginLeft: '40%'}}>
                      <ClipLoader
                        sizeUnit={"px"}
                        size={50}
                        color={'#123abc'}
                        loading={this.state.groupTableLoading}
                      />
                      </div>
                      <div style={{marginLeft: '35%'}}>
                      <p>Loading ... Please wait!</p>
                      </div>
                      </div>
                    }
                    {
                    !this.state.groupTableLoading &&
                    <ToolkitProvider
                          keyField="id"
                          data={ this.state.dataGroupsTable }
                          columns={ columnsGroupsTable }
                          search
                        >
                          {
                            props => (
                              <div>
                                <Row className="align-items-center">
                                <Col>
                                <Row>
                                {
                                //TODO not to show when customer level user logged in
                                this.state.isSubDealerShow &&
                                <Col md="4">
                                <p>SubDealer</p>
                                <Select
                                  onChange={this.handleChangeSubDealerOption.bind(this,'groupManagement')}
                                  defaultValue={this.state.optionsSubDealer[0]}
                                  isClearable={isClearable}
                                  options={this.state.optionsSubDealer}
                                  placeholder={'Select SubDealer'}
                                  backspaceRemovesValue={false}
                                />
                                </Col>
                                }
                                <Col md="4">
                                <p>Customer</p>
                                <Select
                                  onChange={this.handleChangeCustomerOption.bind(this,'groupManagement')}
                                  value={this.state.selectCustomerValueOfGrpMgmt}
                                  isClearable={isClearable}
                                  options={this.state.optionsCustomerOfGrpMgmt}
                                  placeholder={'Select Customer'}
                                  backspaceRemovesValue={false}
                                />
                                </Col>
                                </Row>
                                </Col>
                                <Col md="2">
                                {
                                  //<h4>Input something at below input field:</h4>
                                }
                                <SearchBar
                                  { ...props.searchProps }
                                />
                                </Col>
                                <Col md="2">
                                  <Button block color="primary" className="btn-pill" onClick={this.handleAddGroup}>Add Group</Button>
                                </Col>
                                </Row>
                                <hr />
                                <div style={{overflow: 'overlay'}}>
                                <BootstrapTable
                                  containerStyle={{width: '200%',overflowX: 'scroll'}}
                                  { ...props.baseProps }
                                  filter={ filterFactory() }
                                  hover
                                />
                                </div>
                              </div>
                            )
                          }
                        </ToolkitProvider>
                        }
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </TabPane>
            </TabContent>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Group;
