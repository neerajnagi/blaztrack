import React, { Component } from 'react';
import { Bar, Line, Doughnut, Pie } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardColumns,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Form,
  FormGroup,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Label
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import classnames from 'classnames';

import { NextAuth } from 'next-auth/client'

import Router from 'next/router'

import Select from 'react-select';

import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import cellEditFactory from 'react-bootstrap-table2-editor';
//For table search
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
//
import Geocode from "react-geocode";
//Geocode.setApiKey("AIzaSyDtgi5Fa7jR5nmqAAJdmhKS9_mT9xV-n-I");
//
Geocode.setApiKey("AIzaSyAlxIjIkj-zR6Mx8v3qc1rN9DOtSw2UFAE");
//
import ReactModal from 'react-modal';
//
import Chart from "react-google-charts";
// Pond
import { TimeSeries, percentile } from "pondjs";
import {
    Charts,
    ChartContainer,
    ChartRow,
    YAxis,
    LineChart,
    Resizable,
    Legend,
    EventMarker,
    ScatterChart,
    BandChart,
    styler
} from "react-timeseries-charts";

//react-spinners
import { css } from 'react-emotion';
// First way to import
import { ClipLoader } from 'react-spinners';

import _ from "underscore";
import moment from "moment";
import { format } from "d3-format";
//TODO for datetime
import * as Datetime from 'react-datetime';
import 'react-datetime/css/react-datetime.css'
//google maps
import { compose, withProps, withHandlers, lifecycle } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap, Marker, Polyline } from "react-google-maps"
const { MarkerClusterer } = require("react-google-maps/lib/components/addons/MarkerClusterer");

const { SearchBar } = Search;

const customStyles = {
  content : {
    top : '47%',
    left : '58%',
    right : '-30%',
    bottom : '-20%',
    marginRight : '-10%',
    transform : 'translate(-50%, -50%)'
  },
  overlay: {
    backgroundColor: 'white'
            },
};

const reactModalStyle = {
  content : {
    position: 'absolute',
    top: '280px',
    left: '550px',
    right: '40px',
    bottom: '40px',
    border: '1px solid rgb(204, 204, 204)',
    background: 'rgb(255, 255, 255)',
    overflow: 'auto',
    borderRadius: '4px',
    outline: 'none',
    padding: '20px',
    marginRight : '-50%',
    transform : 'translate(-50%, -50%)',
    color:'lightsteelblue'
  },
  overlay: {
    //backgroundColor: 'papayawhip'
            },
}

const optionsDealer = [];

const optionsSubDealer = [];

const optionsCustomer = [];

const optionsProject = [];

const optionsGroup = [];

const dataVehicleSummaryTable = []

const dataVehiclesTable = [];

let markers = [];

const options = {
  onSizePerPageChange: (sizePerPage, page) => {
    console.log('Size per page change!!!');
    console.log('Newest size per page:' + sizePerPage);
    console.log('Newest page:' + page);
  },
  onPageChange: (page, sizePerPage) => {
    console.log('Page change!!!');
    console.log('Newest size per page:' + sizePerPage);
    console.log('Newest page:' + page);
  }
};

const MyExportCSV = (props) => {
  const handleClick = () => {
    props.onExport();
  };
  return (
    <div>
      <button className="btn btn-success" onClick={ handleClick }>Export to CSV</button>
    </div>
  );
};

const MapComponent = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAlxIjIkj-zR6Mx8v3qc1rN9DOtSw2UFAE&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px`, marginTop: '15%' }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props) =>
  <GoogleMap
    defaultZoom={8}
    defaultCenter={{ lat: -34.397, lng: 150.644 }}
  >
  {props.isMarkerShown && <Marker position={{ lat: -34.397, lng: 150.644 }} />}
  </GoogleMap>
)

const MapWithAMarkerClusterer = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAlxIjIkj-zR6Mx8v3qc1rN9DOtSw2UFAE&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `500px`, marginTop: '5%' }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withHandlers({
    onMarkerClustererClick: () => (markerClusterer) => {
      const clickedMarkers = markerClusterer.getMarkers()
      console.log(`Current clicked markers length: ${clickedMarkers.length}`)
      console.log(clickedMarkers)
    },
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap
    defaultZoom={6}
    defaultCenter={{ lat: 26.912434, lng: 75.787270 }}
  >
    <MarkerClusterer
      onClick={props.onMarkerClustererClick}
      averageCenter
      enableRetinaIcons
      gridSize={60}
    >
      {props.markers.map(marker => (
        <Marker
          key={marker.id}
          position={{ lat: marker.lat, lng: marker.lng }}
          onClick={props.onMarkerClick}
        />
      ))}
    </MarkerClusterer>
  </GoogleMap>
);

const pieOptions = {
  title: "Total Distance",
  pieHole: 0.6,
  slices: [
    {
      color: "#2BB673"
    },
    {
      color: "#d91e48"
    },
    {
      color: "#007fad"
    },
    {
      color: "#e9a227"
    },
    {
      color: "#e8a594"
    }
  ],
  legend: {
    position: "bottom",
    alignment: "center",
    textStyle: {
      color: "233238",
      fontSize: 14
    }
  },
  tooltip: {
    showColorCode: true
  },
  chartArea: {
    left: 0,
    top: 0,
    width: "100%",
    height: "80%"
  },
  fontName: "Roboto"
};

let totalDistancePieData = [["Range", "Count"]];

const chartEvents = [
  {
    eventName: "select",
    callback(chartWrapper) {
      console.log("selected ", chartWrapper);
      //console.log("Selected ", chartWrapper.getChart().getSelection());
    }
  }
];

//TODO react-chartjs-2

const vehicleStatusChart = {
  labels: [
    'Moving',
    'Enigne Idle',
    'Stoppage',
    'Not Reachable'
  ],
  datasets: [
    {
      data: [10, 5, 4, 2],
      backgroundColor: [
        'green',
        '#2eadd3',
        '#c69500',
        '#f5302e'
      ],
      hoverBackgroundColor: [
        'green',
        '#2eadd3',
        '#c69500',
        '#f5302e'
      ],
    }],
};

const engineStatusChart = {
  labels: [
    'On',
    'Off',
    'Not Found'
  ],
  datasets: [
    {
      data: [10, 5, 2],
      backgroundColor: [
        'green',
        '#c69500',
        'pink'
      ],
      hoverBackgroundColor: [
        'green',
        '#c69500',
        'pink'
      ],
    }],
};

const batteryStatusChart = {
  labels: [
    'Good',
    'Bad',
    'Disconnected',
    'Not Found'
  ],
  datasets: [
    {
      data: [10, 5, 3, 4],
      backgroundColor: [
        'green',
        'red',
        'brown',
        'pink'
      ],
      hoverBackgroundColor: [
        'green',
        'red',
        'brown',
        'pink'
      ],
    }],
};

const distanceCoveredChart = {
  labels: [
    '0-49 km',
    '50-99 km',
    '100-199 km',
    '200-299 km'
  ],
  datasets: [
    {
      data: [10, 5, 2, 4],
      backgroundColor: [
        '#2eadd3',
        '#c69500',
        'pink',
        'green'
      ],
      hoverBackgroundColor: [
        '#2eadd3',
        '#c69500',
        'pink',
        'green'
      ],
    }],
};

const fuelConsumptionChart = {
  labels: [
    '0-15 ltr',
    '16-30 ltr',
    '31-45 ltr',
    '46-60 ltr'
  ],
  datasets: [
    {
      data: [15, 10, 4, 2],
      backgroundColor: [
        '#2eadd3',
        '#c69500',
        'pink',
        'green'
      ],
      hoverBackgroundColor: [
        '#2eadd3',
        '#c69500',
        'pink',
        'green'
      ],
    }],
};

//

class Summary extends Component {

  constructor(props) {
    super(props);
    console.log('Summary -->>, props',this.props)

    const manageCategory = this.props.managecategory;
    let isDealerShow, isSubDealerShow, isCustomerShow = false;

    if(manageCategory == "dealer" || manageCategory == "subdealer")
    {
      isSubDealerShow = true;
    }
    else {
      isSubDealerShow = false;
    }
    if(manageCategory == "dealer" || manageCategory == "subdealer" || manageCategory == "customer")
    {
      isCustomerShow = true;
    }
    else {
      isCustomerShow = false;
    }

    this.state = {
      session: props.session,
      manageCategory: manageCategory,
      loading: true,
      vehicleTableLoading:true,
      selectedOption: null,
      isClearable: true,
      isDealerShow:isDealerShow,
      isSubDealerShow:isSubDealerShow,
      isCustomerShow:isCustomerShow,
      optionsDealer:optionsDealer,
      optionsSubDealer:optionsSubDealer,
      optionsCustomer:optionsCustomer,
      optionsGroup:optionsGroup,
      totalDistancePieData:totalDistancePieData,
      dataVehicleSummaryTable:dataVehicleSummaryTable,
      dataVehiclesTable:dataVehiclesTable,
      markers: []
    }

  }

  async componentDidMount() {
    console.log('inside componentDidMount')
    //TODO check user logged in or not:
    const session = await NextAuth.init({force: true})
    if(session.user)
    {
      console.log('user logged in')
    }
    else {
      console.log('user not logged in')
      Router.push('/auth/callback')
      return false;
    }
    console.log('after checking user state')
    this.getManages();
  }

  async getManages()
  {
    console.log('inside getManages')
    const {manageCategory} = this.state;
    let optionsSubDealer = []
    let optionsCustomer = []
    //let optionsGroup = [{ value: 'all', label: 'All' }]
    let optionsGroup = []
    let fetchManage;
    if(manageCategory == "group")
    {
      fetchManage = await fetch('/manage/fetch/'+this.state.manageCategory+'/vehicle');
    }
    else {
      fetchManage = await fetch('/manage/fetch/'+this.state.manageCategory+'/group');
    }
    const res = await fetchManage.json();
    console.log('res',res);
    const data = res.data;
    const manage = data.manage;
    for(var i in manage) {
      var item = manage[i];
      var subDealers = ""
      if(manageCategory == "dealer")
      {
      subDealers = item.dealer.subDealers;
      for(var j in subDealers)
      {
        var subDealer = subDealers[j];
        optionsSubDealer.push({
          "value":subDealer.id,
          "label":subDealer.name
        });
      }
      }
      else if(manageCategory == "subdealer") {
        subDealers = item.subDealer;
          var subDealer = subDealers;
          optionsSubDealer.push({
            "value":subDealer.id,
            "label":subDealer.name
          });
      }
      else if(manageCategory == "customer") {
            var customer = item.customer;
            optionsCustomer.push({
              "value":customer.id,
              "label":customer.name
            });
      }
      else if(manageCategory == "group") {
            var group = item.group;
              optionsGroup.push({
                "value":group.id,
                "label":group.name
              });
      }
      else {
          console.log('something went wrong')
      }
    }
    //TODO now getting customer and group according to manageCategory
    if(manageCategory == "dealer" || manageCategory == "subdealer")
      {
          //getting all customer of a subDealer
          const subDealer = optionsSubDealer[0];
          const subDealerId = subDealer.value;
          const customers = await this.getSubDealerCustomers(subDealerId);
          if(undefined != customers && customers.length > 0)
          {
            const customer = customers[0];
            const customerId = customer.value;
            const projects = await this.getCustomerProjects(customer);
            let project = [];
            let projectId;
            let groups;
            let group = [];
            let groupId;
            let projectFound = false;
            let all = false;
            //TODO if projects not found then find customerGroups
            if(undefined != projects && projects.length > 0)
            {
              project = projects[0];
              projectId = project.value;
              projectFound = true;
              groups = await this.getProjectGroups(project);
              group = groups[0];
              groupId = group.value;

            }
            else
            {
              projectFound = false;
              groups = await this.getCustomerGroups(customer);
              group = groups[0];
              groupId = group.value;
            }
            let vehiclesWithLastData = await this.getVehiclesWithLatestDeviceData(groupId, manageCategory, all)
            console.log('vehiclesWithLastData',vehiclesWithLastData)
            //TODO replace this method to calculate today summary
            ////await this.calculateHmrForToday(vehiclesWithLastData);
            //TODO show battery health pie chart using vehiclesWithLastData;
            ////this.manageDataForVehicleBatteryHealth(vehiclesWithLastData);
            this.setState({
                          optionsSubDealer:optionsSubDealer,
                          selectSubDealerValue:subDealer,
                          optionsCustomer:customers,
                          selectCustomerValue:customer,
                          optionsProject:projects,
                          selectProjectValue:project,
                          optionsGroup:groups,
                          selectGroupValue:group,
                          projectFound:projectFound
                          })
          }
          else
          {
            this.setState({
                          optionsSubDealer:optionsSubDealer,
                          selectSubDealerValue:subDealer
                          })
          }

      }
    else if(manageCategory == "customer") {
         //getting all groups of a customer
         const customer = optionsCustomer[0];
         const customerId = customer.value;
         //TODO need to check customer projects first, if not found then get customer groups
         //this.getCustomerProjects(customer);
         //this.getCustomerGroups(customer);
         if(undefined != customerId)
          {
           const projects = await this.getCustomerProjects(customer);
           let project = [];
           let projectId;
           let groups;
           let group = [];
           let groupId;
           let projectFound = false;
           let all = false;
           //TODO if projects not found then find customerGroups
           if(undefined != projects && projects.length > 0)
           {
             project = projects[0];
             projectId = project.value;
             projectFound = true;
             groups = await this.getProjectGroups(project);
             group = groups[0];
             groupId = group.value;

           }
           else
           {
             projectFound = false;
             groups = await this.getCustomerGroups(customer);
             group = groups[0];
             groupId = group.value;
           }
           let vehiclesWithLastData = await this.getVehiclesWithLatestDeviceData(groupId, manageCategory, all)
           console.log('vehiclesWithLastData',vehiclesWithLastData)
           this.setState({
                         optionsCustomer:optionsCustomer,
                         selectCustomerValue:customer,
                         optionsProject:projects,
                         selectProjectValue:project,
                         optionsGroup:groups,
                         selectGroupValue:group,
                         projectFound:projectFound
                         })
          }
          else {
            this.setState({
                          optionsCustomer:optionsCustomer,
                          selectCustomerValue:customer
                          })
          }

      }
    else if(manageCategory == "group")
      {
        const group = optionsGroup[0];
        const groupId = group.value;
        //TODO calling get vehicle with all true case and userId with manageCategory
        //const userId = this.state.session.user.id;
        //const all = true;
        //TODO all = false;
        const all = false;
        if(undefined != groupId)
        {
          let vehiclesWithLastData = await this.getVehiclesWithLatestDeviceData(groupId, manageCategory, all)
          console.log('vehiclesWithLastData',vehiclesWithLastData)
        }
        this.setState({
                      optionsGroup:optionsGroup,
                      selectGroupValue:group
                      })
      }
    else {
        console.log('something went wrong')
      }
    this.setState({loading:false})
  }

  async getSubDealerCustomers(subDealerId)
  {
    console.log('inside getSubDealerCustomers, subDealerId',subDealerId)
    let optionsCustomer = []
    if(undefined != subDealerId)
    {
        const fetchSubDealerCustomers = await fetch('/subdealer/'+subDealerId+'/customers');
        const resData = await fetchSubDealerCustomers.json()
        console.log('resData',resData)
        const data = resData;
        for(var i in data)
        {
          var customer = data[i]
          optionsCustomer.push({
            "value":customer.id,
            "label":customer.name
          });
        }
      }

    return optionsCustomer;
  }

  async getCustomerProjects(customer)
  {
    console.log('inside getCustomerProjects')
    let optionsProject = []
    if(undefined != customer)
    {
      //optionsGroup = [{ value: 'all', label: 'All Groups' }]
      optionsProject = []
      let customerId = customer.value;
      let customerName = customer.label;
      const fetchCustomerProjects = await fetch('/customer/'+customerId+'/projects');
      const res = await fetchCustomerProjects.json()
      console.log('fetch cutomer projects, res',res)
      const data = res;
      for(var i in data)
      {
        var project = data[i]
        optionsProject.push({
          "value":project.id,
          "label":project.name
        });
      }
    }

    return optionsProject;
  }


  async getProjectGroups(project)
  {
      console.log('inside getProjectGroups')
      let optionsGroup = []
      if(undefined != project)
      {
        //optionsGroup = [{ value: 'all', label: 'All Groups' }]
        optionsGroup = []
        let projectId = project.value;
        let projectName = project.label;
        const fetchProjectGroups = await fetch('/project/'+projectId+'/groups');
        const resData = await fetchProjectGroups.json()
        console.log('resData',resData)
        const data = resData;
        for(var i in data)
        {
          var group = data[i]
          optionsGroup.push({
            "value":group.id,
            "label":group.name
          });
        }
      }

      return optionsGroup;
  }

  async getCustomerGroups(customer)
  {
    console.log('inside getCustomerGroups, customer',customer)
    let optionsGroup = []
    if(undefined != customer)
    {
      //optionsGroup = [{ value: 'all', label: 'All Groups' }]
      optionsGroup = []
      let customerId = customer.value;
      let customerName = customer.label;
      const fetchCustomerGroups = await fetch('/customer/'+customerId+'/groups');
      const resData = await fetchCustomerGroups.json()
      console.log('resData',resData)
      const data = resData;
      for(var i in data)
      {
        var group = data[i]
        optionsGroup.push({
          "value":group.id,
          "label":group.name
        });
      }
    }

    return optionsGroup;
  }

  handleChangeSubDealerOption = async (selectedOption) => {
    console.log('inside handleChangeSubDealerOption, selectedOption',selectedOption)
    //getting all customer of a subDealer
    const subDealer = selectedOption;
    const subDealerId = subDealer.value;
    let customers = [];
    customers = await this.getSubDealerCustomers(subDealerId);
    this.setState({
                  selectSubDealerValue:selectedOption,
                  optionsCustomer:customers,
                  optionsProject:optionsProject,
                  optionsGroup:optionsGroup,
                  selectCustomerValue:[],
                  selectProjectValue:[],
                  selectGroupValue:[]
                  });
  }

  handleChangeCustomerOption = async (selectedOption) => {
    console.log('inside handleChangeCustomerOption, selectedOption',selectedOption)
    //getting all groups of a customer
    const customer = selectedOption;
    //TODO need to check customer projects, if projects not found then found its groups
    let projects = [];
    let projectFound = false;;
    let groups = [];
    projects = await this.getCustomerProjects(customer);
    if(undefined != projects && projects.length)
    {
      projectFound = true;
    }
    else {
      projectFound = false;
      groups = await this.getCustomerGroups(customer);
    }
    console.log('projects',projects)
    console.log('groups',groups)
    this.setState({
                  selectCustomerValue:selectedOption,
                  projectFound:projectFound,
                  optionsProject:projects,
                  optionsGroup:groups,
                  selectProjectValue:[],
                  selectGroupValue:[]
                  });
  }

  handleChangeProjectOption = async (selectedOption) => {
    console.log('inside handleChangeProjectOption, selectedOption',selectedOption)
    //getting all groups of a project
    const project = selectedOption;
    const groups = await this.getProjectGroups(project);
    this.setState({
                  selectProjectValue:selectedOption,
                  optionsGroup:groups,
                  selectGroupValue:[]
                  });
  }

  handleChangeGroupOption = async (selectedOption) => {
    console.log('inside handleChangeGroupOption, selectedOption',selectedOption)
    //getting vehicle of all & selected Group
    const group = selectedOption;
    const groupId = group.value;
    const manageCategory = this.state.manageCategory;
    let all = false;
    this.setState({
                  selectGroupValue:selectedOption
                  });

    //TODO calling get vehicle with all true/false case and userId/groupId with manageCategory
    if(groupId == "all")
    {
      all = true;
      //if manageCategory is group then groupId replace with userId
      if(manageCategory == "group")
      {
        //TODO with userId
        const userId = this.state.session.user.id;
        let vehiclesWithLastData = await this.getVehiclesWithLatestDeviceData(userId, manageCategory, all)
        console.log('vehiclesWithLastData',vehiclesWithLastData)
      }
      else {
        //TODO in other case with customerId
        const customerId = this.state.selectCustomerValue.value;
        let vehiclesWithLastData = await this.getVehiclesWithLatestDeviceData(customerId, manageCategory, all)
        console.log('vehiclesWithLastData',vehiclesWithLastData)
      }
    }
    else {
      all = false;
      let vehiclesWithLastData = await this.getVehiclesWithLatestDeviceData(groupId, manageCategory, all)
      console.log('vehiclesWithLastData',vehiclesWithLastData);
      //TODO need to replace this method with calculate today summary
      //TODO get data and calculate hmr for today
      ////await this.calculateHmrForToday(vehiclesWithLastData);
    }
  }

  //TODO need to transfer this code to server side
  async getVehiclesWithLatestDeviceData(id, manageCategory, all)
  {
    console.log('inside getVehiclesWithLatestDeviceData')
    console.log('id, manageCategory, all', id, ',',manageCategory, ',', all)
    const fetchVehicle = await fetch('/vehicle/fetchvehicleforsitedata/'+id+'/'+manageCategory+'/'+all);
    const res = await fetchVehicle.json();
    console.log('fetchVehicle, res',res);
    const data = res.data;
    let vehiclesWithLatestDeviceData = {};
    let dataForCassQry = [];
    let dataFromApi = {};
    var snVehicles = 1;
    if(all)
    {
        if(manageCategory == "group")
        {
          //res came with manage
          const manage = data;
          for(var i in manage)
          {
            var item = manage[i];
            const group = item.group;
            const vehicles = group.vehicles;
            for(var j in vehicles)
            {
              var itemVehicles = vehicles[j];
              dataForCassQry.push({
                "customerId":group.customerId,
                "vehicleId":itemVehicles.id
              });
              var sensorAssignments = itemVehicles.sensorAssignments;
              //console.log('sensorAssignments',sensorAssignments)
              for(var l in sensorAssignments)
              {
                var itemSensorAssignment = sensorAssignments[l]
              }

              var vehicleCategory = "";

              if(undefined != itemVehicles.vehicleCategory && itemVehicles.vehicleCategory != "")
              {
                vehicleCategory = itemVehicles.vehicleCategory
              }

              var driver;
              if(undefined != itemVehicles.driverName || undefined != itemVehicles.driverMobileNo)
              {
                driver = {name:itemVehicles.driverName, mobileNo:itemVehicles.driverMobileNo}
              }
              else
              {
                driver = {name:'', mobileNo:''}
              }

              var hmrParam;
              if(undefined != itemVehicles.hmrParam)
              {
                hmrParam = itemVehicles.hmrParam;
              }
              else {
                hmrParam = "NA";
              }

              let dataForTable = {
                                  customerId:group.customerId,
                                  vehicleId:itemVehicles.id,
                                  group:group.name,
                                  vehicle:itemVehicles.regNo,
                                  sensorAssignments:sensorAssignments,
                                  vehicleCategory:vehicleCategory,
                                  driver:driver,
                                  hmrParam:hmrParam
                                }
              dataFromApi[itemVehicles.id] = dataForTable
            }
          }
        }
        else {
          // res came with group
          const group = data
          for(var i in group)
          {
            var item = group[i];
            const customerId = item.customerId;
            var vehicles = item.vehicles;
            for(var j in vehicles)
            {
              var itemVehicles = vehicles[j];
              dataForCassQry.push({
                "customerId":customerId,
                "vehicleId":itemVehicles.id
              });
              var sensorAssignments = itemVehicles.sensorAssignments;
              //console.log('sensorAssignments',sensorAssignments)
              for(var l in sensorAssignments)
              {
                var itemSensorAssignment = sensorAssignments[l]
              }

              var vehicleCategory = "";

              if(undefined != itemVehicles.vehicleCategory && itemVehicles.vehicleCategory != "")
              {
                vehicleCategory = itemVehicles.vehicleCategory
              }

              var driver;
              if(undefined != itemVehicles.driverName || undefined != itemVehicles.driverMobileNo)
              {
                driver = {name:itemVehicles.driverName, mobileNo:itemVehicles.driverMobileNo}
              }
              else
              {
                driver = {name:'', mobileNo:''}
              }

              var hmrParam;
              if(undefined != itemVehicles.hmrParam)
              {
                hmrParam = itemVehicles.hmrParam;
              }
              else {
                hmrParam = "NA";
              }

              let dataForTable = {
                                  customerId:customerId,
                                  vehicleId:itemVehicles.id,
                                  group:item.name,
                                  vehicle:itemVehicles.regNo,
                                  sensorAssignments:sensorAssignments,
                                  vehicleCategory:vehicleCategory,
                                  driver:driver,
                                  hmrParam:hmrParam
                                }
              dataFromApi[itemVehicles.id] = dataForTable
            }
          }

        }
    }
    else
    {
      //res came with vehicle
      const vehicle = data
      for(var i in vehicle)
      {
        var item = vehicle[i];
        var group = item.group;
        dataForCassQry.push({
          "customerId":group.customerId,
          "vehicleId":item.id
        });
        var sensorAssignments = item.sensorAssignments;
        //console.log('sensorAssignments',sensorAssignments);
        for(var k in sensorAssignments)
        {
          var itemSensorAssignment = sensorAssignments[k]
        }

        var vehicleCategory = "";

        if(undefined != item.vehicleCategory && item.vehicleCategory != "")
        {
          vehicleCategory = item.vehicleCategory
        }

        var driver;
        if(undefined != item.driverName || undefined != item.driverMobileNo)
        {
          driver = {name:item.driverName, mobileNo:item.driverMobileNo}
        }
        else
        {
          driver = {name:'', mobileNo:''}
        }

        var hmrParam;
        if(undefined != item.hmrParam)
        {
          hmrParam = item.hmrParam;
        }
        else {
          hmrParam = "NA";
        }

        let dataForTable = {
                            customerId:group.customerId,
                            vehicleId:item.id,
                            group:group.name,
                            vehicle:item.regNo,
                            sensorAssignments:sensorAssignments,
                            vehicleCategory:vehicleCategory,
                            driver:driver,
                            hmrParam:hmrParam
                          }
        dataFromApi[item.id] = dataForTable
      }
    }

    //TODO now getting data from cassandra from dataForCassQry array, its have customerId and vehicleId
    console.log('fetching data for vehicles, dataForCassQry',dataForCassQry)
    console.log(JSON.stringify(dataForCassQry))
    let payload = {
                    data:dataForCassQry
                  }
    const requestOptions = {
      credentials: 'include',
      method: 'POST',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    const vehicleFetch = await fetch('/vehicle/fetch/',requestOptions);
    const resVehicle = await vehicleFetch.json();
    console.log('resVehicle',resVehicle);
    var dataFromCassandra = resVehicle.data;
    console.log('dataFromApi',dataFromApi)
    console.log('dataFromCassandra',dataFromCassandra)
    vehiclesWithLatestDeviceData = {dataForCassQry:dataForCassQry, dataFromApi:dataFromApi, latestDataFromCassandra:dataFromCassandra};
    /*
    this.setState({
                  loading:false
                  })
    */
    this.setState({
                  dataVehiclesTable:[],
                  vehicleTableLoading:true
                  })
    this.showVehiclesTable(vehiclesWithLatestDeviceData);

    return vehiclesWithLatestDeviceData;
  }

  async showVehiclesTable(vehiclesWithLatestDeviceData)
  {
    console.log('inside showVehiclesTable')
    const dataVehiclesTable = [];
    markers = [];
    //TODO getting address(location) of vehicle from lat/lng;
    const dataFromCassandra = vehiclesWithLatestDeviceData.latestDataFromCassandra;
    const dataForAddress = dataFromCassandra;
    const vehiclesAddress = await this.getVehiclesAddress(dataForAddress);
    console.log('vehiclesAddress', vehiclesAddress);
    //now showing data on table from both data(loopback, cassandra)
    const dataForCassQry = vehiclesWithLatestDeviceData.dataForCassQry;
    const dataFromApi = vehiclesWithLatestDeviceData.dataFromApi;
    var snVehicles = 1;
    for(var i in dataForCassQry)
    {
      var item = dataForCassQry[i]
      var vehicleId = item.vehicleId
      var apiData = dataFromApi[vehicleId];
      var cassDataArray = dataFromCassandra[vehicleId];
      var cassData = cassDataArray[0];
      var vehicleAddress = vehiclesAddress[vehicleId];
      let assignedSensorsDetails = {};
      let hmrParam = apiData.hmrParam;
      let vehicleAndSensorStatus = {};
      if(undefined != cassDataArray && cassDataArray != "data not found" && cassDataArray != "some error occured")
      {
        let sensorAssignments = apiData.sensorAssignments;
        assignedSensorsDetails = await this.getAssignedSensorsDetails(sensorAssignments);
        vehicleAndSensorStatus = await this.getVehicleAndSensorStatus(assignedSensorsDetails, cassData, hmrParam);
      }
      else {
        console.log(cassDataArray)
        vehicleAndSensorStatus['vehiclestatus'] = "NA"
      }
      console.log('vehicleAndSensorStatus:',vehicleAndSensorStatus)
      //TODO
      let status;
      if(undefined != vehicleAndSensorStatus)
      {
        if(vehicleAndSensorStatus.hasOwnProperty('vehiclestatus'))
        {
          status = vehicleAndSensorStatus['vehiclestatus'];
        }
        else {
          status = "NA";
        }
      }
      else {
        status = "NA";
      }
      let engine;
      if(undefined != vehicleAndSensorStatus)
      {
        if(vehicleAndSensorStatus.hasOwnProperty('engine'))
        {
          engine = vehicleAndSensorStatus['engine'];
        }
        else {
          engine = "NA";
        }
      }
      else {
        engine = "NA";
      }
      let power;
      if(undefined != vehicleAndSensorStatus)
      {
        if(vehicleAndSensorStatus.hasOwnProperty('power'))
        {
          power = vehicleAndSensorStatus['power'];
        }
        else {
          power = "NA";
        }
      }
      else {
        power = "NA";
      }
      let ignition;
      if(undefined != vehicleAndSensorStatus)
      {
        if(vehicleAndSensorStatus.hasOwnProperty('ignition'))
        {
          ignition = vehicleAndSensorStatus['ignition'];
        }
        else {
          ignition = "NA";
        }
      }
      else {
        ignition = "NA";
      }
      ////
      let insert_datetime;
      if(cassData.insert_datetime)
      {
        insert_datetime = moment(cassData.insert_datetime).format("DD/MM/YYYY LT")
      }
      else {
        insert_datetime = "NA";
      }
      let category;
      if(apiData.vehicleCategory != "")
      {
        category = apiData.vehicleCategory.name;
      }
      else {
        category = "NA";
      }
      let location;
      if(undefined != vehicleAddress && undefined != vehicleAddress.address)
      {
        location = vehicleAddress.address;
      }
      else {
        location = "NA";
      }
      let speed;
      if(undefined != cassData.speed)
      {
        speed = parseInt(cassData.speed);
      }
      else {
        speed = "NA";
      }
      if(undefined != cassData.latitude && undefined != cassData.longitude)
      {
        //markers.push({lat:cassData.latitude,lng:cassData.longitude,id:snVehicles})
      }
      console.log('data from api through vehicleId',apiData)
      console.log('data from cassandra through vehicleId',cassData)
      dataVehiclesTable.push({
          "sn" : snVehicles,
          "group": apiData.group || 'NA',
          "vehicle":apiData.vehicle || 'NA',
          "vehicleId":apiData.vehicleId,
          "customerId":apiData.customerId,
          "status":status,
          "engine":engine,
          "power":power,
          "ignition" : ignition,
          "category":category,
          "date"  : insert_datetime,
          "latitude": cassData.latitude,
          "longitude": cassData.longitude,
          "location"  : location,
          "speed": speed,
          "imei": cassData.deviceid || 'NA',
          "assignedSensorsDetails":assignedSensorsDetails,
          "hmrParam":hmrParam,
          "rawData":cassData
        });
        snVehicles++;
    }
    console.log('dataVehiclesTable',dataVehiclesTable)
    this.setState({
                  vehicleTableLoading:false,
                  dataVehiclesTable:dataVehiclesTable,
                  markers: []
                  })
    //this.setState({ markers: [{lat:26.922070,lng:75.778885,id:1}, {lat:26.449895, lng:74.639916, id:2}] });
    console.log('markers with lat/lng of vehicles',markers)
    //TODO now showing vehicle summary table using dataVehiclesTable
    this.showVehicleSummary(dataVehiclesTable);
  }

  async getAssignedSensorsDetails(sensorAssignments)
  {
    console.log('inside getAssignedSensorsDetails');
    let assignedSensorsDetails = {};
    for(var i in sensorAssignments)
    {
      var item = sensorAssignments[i];
      var gpsDevice = item.gpsDevice;
      var devicePort= item.devicePort;
      var devicePortName;
      var sensor = item.sensor;
      var sensorType = sensor.sensorType;
      var min = item.min;
      var max = item.max;
      var raw;
      if(undefined != sensorType && undefined != sensorType.name)
      {
        let sensorTypeName = sensorType.name.toLowerCase();
        if(sensorTypeName == "fuel" || sensorTypeName == "enginestatus")
        {
          continue;
        }

        if(undefined == min)
        {
          min = "NA";
        }
        if(undefined == max)
        {
          max = "NA";
        }
        if(undefined != devicePort && undefined != devicePort.name)
        {
          devicePortName = devicePort.name;
        }
        else {
          devicePortName = "NA";
        }
        console.log('sensorTypeName:',sensorType.name);
        console.log('min:',min,', max:',max);
        console.log('raw:',raw)
        console.log('devicePortName:',devicePortName);
        assignedSensorsDetails[sensorType.name] = {devicePort:devicePortName,min:min,max:max}
      }
    }
    return assignedSensorsDetails;
  }

  async getVehicleAndSensorStatus(assignedSensorsDetails, cassData, hmrParam)
  {
    console.log('inside getVehicleAndSensorStatus');
    let isSensorAssignmentsFound = false;
    let vehicleAndSensorStatus = {};
    let vehicleStatus;
    if(undefined != assignedSensorsDetails && assignedSensorsDetails.length > 0)
    {
      console.log('assignedSensorsDetails',assignedSensorsDetails);
      for(var i in assignedSensorsDetails)
      {
        let sensorType = i;
        let sensorStatus;
        let column = cassandraColName[assignedSensorsDetails[i].devicePort];
        let min = assignedSensorsDetails[i].min;
        let max = assignedSensorsDetails[i].max;
        let raw;
        if(undefined != cassData[column])
        {
          raw = cassData[column];
        }
        else
        {
          raw = "NA";
        }
        console.log('min',min,'max',max)
        console.log('raw',raw);
        if(undefined != raw && raw != "NA")
        {
          if(raw >= min && raw <= max)
          {
            sensorStatus = true;
            vehicleAndSensorStatus[i.toLowerCase()] = "on"
          }
          else {
            sensorStatus = false;
            vehicleAndSensorStatus[i.toLowerCase()] = "off"
          }
        }
        else {
          vehicleAndSensorStatus[i.toLowerCase()] = "NA"
        }
      }
      isSensorAssignmentsFound = true;
    }
    else {
      isSensorAssignmentsFound = false;
    }

    let speed = cassData.speed;
    if(undefined != speed)
    {
      if(speed > 0)
      {
        vehicleStatus = "Moving"
      }
      else
      {
        vehicleStatus = "Stoppage";
      }
    }
    else
    {
      console.log('speed not found')
      vehicleStatus = "NA";
    }

    //TODO now check engine status
    if(isSensorAssignmentsFound)
    {
      //TODO define in before loop
      ////let hmrParam = apiData.hmrParam;
      if(undefined != hmrParam && hmrParam != "NA")
      {
        let engineStatus = await this.checkEngineStatus(assignedSensorsDetails, hmrParam, cassData);
        console.log('enginestatus',engineStatus)
        if(undefined != engineStatus)
        {
          let status = engineStatus.status;
          let msg = engineStatus.msg;
          if(status == "on")
          {
            if(vehicleStatus != "Moving")
            {
              vehicleStatus = "Engine Idle";
            }
            vehicleAndSensorStatus['engine'] = status;
          }
          else if(status == "off")
          {
            vehicleAndSensorStatus['engine'] = status;
          }
          else
          {
            vehicleAndSensorStatus['engine'] = status;
          }
        }
      }
    }

    //TODO now check raw data last insert_datetime is not less then defined notReachableTime
    //if less then vehicleStatus is Not Reachable;
    let insert_datetime = cassData.insert_datetime;
    if(undefined != insert_datetime)
    {
      console.log('insert_datetime',insert_datetime);
      //TODO if insert_datetime < notReachableTime then vehcile status is "Not Reachable"
    }
    else {
      vehicleStatus = "NA";
    }

    console.log('vehicleAndSensorStatus:',vehicleAndSensorStatus)
    return vehicleAndSensorStatus;
  }

  //TODO check engine status using assignedSensorsDetails & hmrParam
  async checkEngineStatus(assignedSensorsDetails, hmrParam, rawData)
  {
    console.log('inside checkEngineStatus');
    //TODO need to validate availability of hmrParam;
    //TODO first check primarySensor;
    let engineStatus = {};
    let primarySensor;
    if(hmrParam.hasOwnProperty('primary'))
    {
      primarySensor = hmrParam.primary;
    }
    else {
      engineStatus = {status: 'not known', msg:'primary sensor not found'}
      return engineStatus;
    }
    let secondarySensor;
    if(hmrParam.hasOwnProperty('secondary'))
    {
      secondarySensor = hmrParam.secondary;
    }
    let dataSelectedSecANDType;
    let dataSelectedSecORType;
    if(undefined != secondarySensor)
    {
      if(secondarySensor.hasOwnProperty('and'))
      {
        dataSelectedSecANDType = secondarySensor.and;
      }
      if(secondarySensor.hasOwnProperty('or'))
      {
        dataSelectedSecORType = secondarySensor.or;
      }
    }
    console.log('primarySensor',primarySensor);
    console.log('secondarySensor',secondarySensor);
    console.log('dataSelectedSecANDType',dataSelectedSecANDType);
    console.log('dataSelectedSecORType',dataSelectedSecORType);
    //TODO need to first check if primary and secondary sensors not undefined
    if(undefined == primarySensor && undefined == dataSelectedSecANDType && undefined == dataSelectedSecORType)
    {
      console.log('primary and secondary sensors not found in hmrParam')
      engineStatus = {status:'not known', msg:'hmr not defined'}
      return engineStatus;
    }
    let primarySensorStatus = false;
    let secondaryANDSensorsStatus = false;
    let secondaryORSensorsStatus = false;

    if(undefined != primarySensor)
    {
      console.log('assignedSensorsDetails',assignedSensorsDetails)
      console.log('primarySensor',primarySensor)
      for(var i in assignedSensorsDetails)
      {
        let sensorType = i.toLowerCase();
        if(sensorType == primarySensor)
        {
          //first found primary sensor raw value, min and max from dataAssignedSensorTable
          //then make decision
          let column = cassandraColName[assignedSensorsDetails[i].devicePort];
          let min = assignedSensorsDetails[i].min;
          let max = assignedSensorsDetails[i].max;
          //let raw = assignedSensorsDetails[i].raw;
          let raw;
          if(undefined != cassData[column])
          {
            raw = cassData[column];
          }
          else
          {
            raw = "NA";
          }
          console.log('min',min,'max',max)
          console.log('raw',raw);
          if(undefined != raw && raw != "NA")
          {
            if(raw >= min && raw <= max)
            {
              primarySensorStatus = true;
            }
            else {
              primarySensorStatus = false;
            }
          }
        }
      }
    }
    else {
      console.log('primary sensor not defined')
    }
    if(primarySensorStatus)
    {
      console.log('engine on')
      engineStatus = {status:'on', msg:'checked with primary sensor'}
      return engineStatus;
    }
    //TODO if primarySensorStatus false then check secondary sensors status
    else
    {
      //TODO now check secondary sensors
      //First check secondary sensors with AND conditions (always true)
      console.log('dataSelectedSecANDType',dataSelectedSecANDType)
      console.log('dataSelectedSecORType',dataSelectedSecORType)
      if(undefined != dataSelectedSecANDType)
      {
        for(var i in assignedSensorsDetails)
        {
          let sensorType = i.toLowerCase();;
          if(dataSelectedSecANDType.hasOwnProperty(sensorType))
          {
            console.log('now checked secondary AND for sensor',sensorType);
            let column = cassandraColName[assignedSensorsDetails[i].devicePort];
            let min = assignedSensorsDetails[i].min;
            let max = assignedSensorsDetails[i].max;
            //let raw = assignedSensorsDetails[i].raw;
            let raw;
            if(undefined != cassData[column])
            {
              raw = cassData[column];
            }
            else
            {
              raw = "NA";
            }
            console.log('min',min,'max',max)
            console.log('raw',raw);
            if(undefined != raw && raw != "NA")
            {
              if(raw >= min && raw <= max)
              {
                secondaryANDSensorsStatus = true;
              }
              else {
                secondaryANDSensorsStatus = false;
                break;
              }
            }
          }
        }
      }
      else {
        console.log('secondary AND type sensors not defined')
      }
      if(secondaryANDSensorsStatus)
      {
        console.log('engine on')
        engineStatus = {status:'on', msg:'checked with secondary AND type sensors'}
        return engineStatus;
      }
      else
      {
        //Second check secondary sensors with OR conditions (atleast one true)
        if(undefined != dataSelectedSecORType)
        {
          for(var i in assignedSensorsDetails)
          {
            let sensorType = i.toLowerCase();
            if(dataSelectedSecORType.hasOwnProperty(sensorType))
            {
              console.log('now checked secondary OR for sensor',sensorType);
              let column = cassandraColName[assignedSensorsDetails[i].devicePort];
              let min = assignedSensorsDetails[i].min;
              let max = assignedSensorsDetails[i].max;
              //let raw = assignedSensorsDetails[i].raw;
              let raw;
              if(undefined != cassData[column])
              {
                raw = cassData[column];
              }
              else
              {
                raw = "NA";
              }
              console.log('min',min,'max',max)
              console.log('raw',raw);
              if(undefined != raw && raw != "NA")
              {
                if(raw >= min && raw <= max)
                {
                  secondaryORSensorsStatus = true;
                  break;
                }
                else {
                  secondaryORSensorsStatus = false;
                }
              }
            }
          }
        }
        else {
          console.log('secondary OR type sensors not defined')
        }

        if(secondaryORSensorsStatus)
        {
          console.log('engine on')
          engineStatus = {status:'on', msg:'checked with secondary OR type sensors'}
          return engineStatus;
        }
        else {
          console.log('engine off')
          engineStatus = {status:'off', msg:'checked with secondary OR type sensors'}
          return engineStatus;
        }
      }
    }
    return engineStatus;
  }

  //TODO need to transfer this code to server side
  async getVehiclesAddress(dataForAddress)
  {
    console.log('inside getVehiclesAddress')
    console.log('dataForAddress',dataForAddress)
    let vehiclesAddress = {};
    for(var i in dataForAddress)
    {
      var item = dataForAddress[i];
      console.log('item',item)
      if(undefined != item[0].latitude && undefined != item[0].longitude)
      {
        console.log('lat/lng not undefined')
        var deviceData = item[0];
        var latitude = deviceData.latitude;
        var longitude = deviceData.longitude;
        var geocodeRes = await Geocode.fromLatLng(latitude, longitude).then(
          response => {
            const address = response.results[0].formatted_address;
            console.log('address',address);
            return  {'address':address};
          },
          error => {
            console.error('error',error);
            return  {'address':'Not Found'};
          }
        );
        console.log('geocodeRes',geocodeRes)
        vehiclesAddress[i] = geocodeRes;
      }
      else {
        console.log('lat/lng undefined')
        vehiclesAddress[i] = {'address':'Not Found'};
      }
    }
    return vehiclesAddress;
  }

  async showVehicleSummary(dataVehiclesTable)
  {
    console.log('inside showVehicleSummary');
    let vehicleSummaryData = Array.from(dataVehiclesTable);
    console.log('dataVehiclesTable',dataVehiclesTable)
    let dataVehicleSummaryTable = [];
    for(var i in vehicleSummaryData)
    {
      let item = vehicleSummaryData[i];
      dataVehicleSummaryTable.push({
        "sn" : item.sn,
        "group": item.group,
        "vehicle":item.vehicle,
        "vehicleId":item.vehicleId,
        "customerId":item.customerId,
        "status":item.status,
        "engine":item.engine,
        "power":item.power,
        "ignition" : item.ignition,
        "speed": item.speed
      })
    }
    this.setState({
      dataVehicleSummaryTable:dataVehicleSummaryTable
    })
  }

  async getTodaysVehiclesData(vehiclesWithLastData)
  {
    console.log('inside getTodaysVehiclesData')
    let dataForCassQry = vehiclesWithLastData.dataForCassQry;
    let todaysVehiclesDeviceData = [];
    let payload = {
                    data:dataForCassQry
                  }
    const requestOptions = {
      credentials: 'include',
      method: 'POST',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    let startDateTime = new Date();
    //TODO Date.setHours(hour, min, sec, millisec)
    startDateTime.setHours(0,0,0,0);
    console.log('startDateTime',startDateTime)
    let endDateTime = new Date();
    console.log('endDateTime',endDateTime)
    let startDT = Math.floor(startDateTime.getTime());
    let endDT = Math.floor(endDateTime.getTime());
    console.log('startDT', startDT);
    console.log('endDT', endDT);
    const todayData = await fetch('/vehicle/todaydata/'+startDT+'/'+endDT,requestOptions);
    const res = await todayData.json();
    console.log('todayData res as startDT & endDT',res)
    todaysVehiclesDeviceData = res;
    return todaysVehiclesDeviceData;
  }

//TODO code transfer to server side
/*

  async calculateHmr(vehiclesWithLastData, devicesData)
  {
    console.log('insdie calculateHmr');
    let vehicles = vehiclesWithLastData.dataFromApi;
    let vehicle;
    let deviceData;
    let vehicleHmrParam;
    let vehiclesForHmr = {};
    let calculatedHmr = {};
    let vehiclesForDevicePort = [];
    for(var i in vehicles)
    {
      vehicle = vehicles[i];
      deviceData = devicesData[i];
      if(undefined != vehicle && undefined != vehicle.hmrParam)
      {
        vehicleHmrParam = vehicle.hmrParam;
      }
      else {
        //continue;
      }
      if(undefined != deviceData && deviceData != "data not found")
      {
      vehiclesForDevicePort.push(vehicle.vehicleId);
      console.log('device data length', deviceData.length)
      vehiclesForHmr[vehicle.vehicleId] = {};
      vehiclesForHmr[vehicle.vehicleId].hmrParam = vehicleHmrParam;
      vehiclesForHmr[vehicle.vehicleId].deviceData = deviceData;
      }
      else {
        //continue;
      }
    }
    console.log('vehiclesForDevicePort',vehiclesForDevicePort);
    let vehiclesSensorDevicePort = await this.getVehiclesSensorDevicePort(vehiclesForDevicePort);
    console.log('vehiclesSensorDevicePort',vehiclesSensorDevicePort);
    console.log('vehiclesForHmr',vehiclesForHmr)
    //let sensorsForHmr = [];
    for(var i in vehiclesForHmr)
    {
      let sensorsForHmr = {};
      sensorsForHmr[i] = {};
      console.log('vehiclesSensorDevicePort[i], i:',i)
      for(var j in vehiclesSensorDevicePort[i])
        {
          if(vehiclesForHmr[i].hmrParam.hasOwnProperty(j))
          {
            console.log('inside if, j',j,', vehiclesSensorDevicePort[i][j]',vehiclesSensorDevicePort[i][j]);
            sensorsForHmr[i][j] = vehiclesSensorDevicePort[i][j];
          }
          else {
            console.log('inside else, j',j)
          }
        }
      console.log('sensorsForHmr::',sensorsForHmr);
      let vehicleHmr = await this.calculateVehicleHmr(sensorsForHmr[i], vehiclesForHmr[i].hmrParam, vehiclesForHmr[i].deviceData);
      calculatedHmr[vehicle.vehicleId] = {};
      calculatedHmr[vehicle.vehicleId].idle = vehicleHmr.idle;
      calculatedHmr[vehicle.vehicleId].production = vehicleHmr.production;
    }

    for (var i in calculatedHmr)
    {
      //console.log('calculatedHmr loop, vehicle hmr',calculatedHmr[i])
    }
    console.log('calculatedHmr',JSON.stringify(calculatedHmr))
  }

  async getVehiclesSensorDevicePort(vehiclesForDevicePort)
  {
    console.log('inside getVehiclesSensorDevicePort')
    let vehiclesSensorDevicePort = {};
    if(undefined != vehiclesForDevicePort && vehiclesForDevicePort.length > 0)
    {
      let payload = {
                      vehicles:vehiclesForDevicePort
                    }
      const requestOptions = {
        credentials: 'include',
        method: 'POST',
        headers: { 'Content-Type': 'application/json',
                    'X-CSRF-Token': await NextAuth.csrfToken()
                  },
        body : JSON.stringify(payload)
      };
      console.log('payload json',JSON.stringify(payload))
      const sensorWithDevicePort = await fetch(' http://159.65.6.196:5500/api/vehicle/vehiclewithsensorassignmentatsummary', requestOptions);
      const res = await sensorWithDevicePort.json();
      console.log('sensorWithDevicePort, res',res)
      const data = res.data;
      for(var i in data)
      {
        var sensorAssignments = data[i].sensorAssignments;
        vehiclesSensorDevicePort[data[i].id] = {}
        if(sensorAssignments.length > 0)
        {
          vehiclesSensorDevicePort[data[i].id]['speed'] = 'speed';
        }
        for(var j in sensorAssignments)
        {
          //vehiclesSensorDevicePort[data[i].id][sensorAssignments[j].sensor.sensorType.lowercaseName] = sensorAssignments[j].devicePort.lowercaseName;
          vehiclesSensorDevicePort[data[i].id][sensorAssignments[j].sensor.sensorType.lowercaseName] = sensorAssignments[j].devicePort.dbColumnName;
        }
      }
    }
    return vehiclesSensorDevicePort;
  }

  async calculateVehicleHmr(sensorsForHmr, vehicleHmrParam, deviceData)
  {
    console.log('inside calculateVehicleHmr')
    console.log('sensorsForHmr',sensorsForHmr,', vehicleHmrParam',vehicleHmrParam)
    let vehicleHmr = {};
    //TODO we calculate hmr using data;
    var data;
    var sensor;
    var sensorValue;
    var min;
    var max;
    var status = {};
    for(var i in deviceData)
    {
      data = deviceData[i];
      for(var j in sensorsForHmr)
      {
        sensor = j;
        sensorValue = data[sensorsForHmr[j]]
        //console.log('sensor:',sensor,', sensor value:',sensorValue)
        status[sensor] = {};
        for(var k in vehicleHmrParam[j])
        {
          min = vehicleHmrParam[j][k]['min'];
          max = vehicleHmrParam[j][k]['max'];
          //console.log('sensor status:',k,', min:',min,', max:',max)
          if(sensorValue >= min && sensorValue <= max)
          {
            //console.log('status is:', k)
            status[sensor][k] = true;
          }
          else {
            status[sensor][k] = false;
          }
        }
      }
      console.log('now sensor status:',status)
    }
    vehicleHmr = {idle: '10 min', production: '20 min', marching: '5 min'}
    return vehicleHmr;
  }

  */

  //TODO  new function, all calculation in server side
  async calculateHmrForToday(vehiclesWithLastData)
  {
    console.log('inside calculateHmrForToday')
    let dataForQry = vehiclesWithLastData.dataForCassQry;
    let vehiclesData = vehiclesWithLastData.dataFromApi;
    let payload = {
                    data:dataForQry,
                    vehiclesData:vehiclesData
                  }
    const requestOptions = {
      credentials: 'include',
      method: 'POST',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    let startDateTime = new Date();
    startDateTime.setHours(0,0,0,0);
    console.log('startDateTime',startDateTime)
    let endDateTime = new Date();
    console.log('endDateTime',endDateTime)
    let startDT = Math.floor(startDateTime.getTime());
    let endDT = Math.floor(endDateTime.getTime());
    console.log('startDT', startDT);
    console.log('endDT', endDT);
    let forDays = "oneday";
    const calculateHmr = await fetch('/vehicle/calculatehmr/'+startDT+'/'+endDT+'/'+forDays,requestOptions);
    const res = await calculateHmr.json();
    console.log('calculated hmr res:',res)
  }

  async manageDataForVehicleBatteryHealth(vehiclesWithLastData)
  {
    console.log('inside manageDataForVehicleBatteryHealth')
    let latestVehiclesData = vehiclesWithLastData.latestDataFromCassandra;
    var buckets = {};
    var bucket1 = "bucket1";
    var bucket2 = "bucket2";
    var bucket3 = "bucket3";
    var bucket4 = "bucket4";
    buckets[bucket1] = {count:0, status:'disconnected', vehicles: []};
    buckets[bucket2] = {count:0, status:'bad health', vehicles: []};
    buckets[bucket3] = {count:0, status:'good health', vehicles: []};
    buckets[bucket4] = {count:0, status:'not found', vehicles: []};
    for(var i in latestVehiclesData)
    {
      var latestItem = latestVehiclesData[i];
      var vehicleId = i;
      console.log('latestItem',latestItem)
      if(undefined != latestItem && latestItem != "data not found" )
      {
        console.log('data found for vehicle',i)
        var obj = {};
        var battery = latestItem[0].power
        if(undefined != battery)
        {
          if(battery < 2.0)
          {
            obj = buckets[bucket1]
            ++obj.count;
            obj.vehicles.push(vehicleId)
          }
          else if(2.0 < battery && battery < 11.5)
          {
            obj = buckets[bucket2]
            ++obj.count;
            obj.vehicles.push(vehicleId)
          }
          else if(11.5 < battery && battery < 14.0) {
            obj = buckets[bucket3]
            ++obj.count;
            obj.vehicles.push(vehicleId)
          }
          else if(14.0 < battery && battery < 23.5)
          {
            obj = buckets[bucket2]
            ++obj.count;
            obj.vehicles.push(vehicleId)
          }
          else if(23.5 < battery)
          {
            obj = buckets[bucket3]
            ++obj.count;
            obj.vehicles.push(vehicleId)
          }
          else {
            console.log('something went wrong')
          }
        }
        else {
          console.log('data not found for vehicle',i)
          obj = buckets[bucket4]
          ++obj.count;
          obj.vehicles.push(vehicleId)
        }
      }
      else {
        console.log('data not found for vehicle',i)
        obj = buckets[bucket4]
        ++obj.count;
        obj.vehicles.push(vehicleId)
      }
    }
    console.log('battery health buckets',buckets)
  }

  async getYesterdayVehiclesData(vehiclesWithLastData)
  {
    console.log('inside getYesterdayVehiclesData')
    let yeseterdayVehiclesDeviceData = [];
    let dataForCassQry = vehiclesWithLastData.dataForCassQry;
    let latestDataFromCassandra = vehiclesWithLastData.latestDataFromCassandra;
    let payload = {
                    data:dataForCassQry
                  }
    const requestOptions = {
      credentials: 'include',
      method: 'POST',
      headers: { 'Content-Type': 'application/json',
                  'X-CSRF-Token': await NextAuth.csrfToken()
                },
      body : JSON.stringify(payload)
    };
    let endDateTime = new Date();
    //TODO Date.setHours(hour, min, sec, millisec)
    endDateTime.setHours(0,0,0,0);
    console.log('endDateTime',endDateTime)
    let startDateTime = new Date(endDateTime);
    startDateTime.setDate(endDateTime.getDate() - 1);
    console.log('startDateTime',startDateTime)
    let startDT = Math.floor(startDateTime.getTime());
    let endDT = Math.floor(endDateTime.getTime());
    console.log('startDT', startDT);
    console.log('endDT', endDT);
    const vehicleFetch = await fetch('/vehicle/yesterdaydata/'+startDT+'/'+endDT,requestOptions);
    const res = await vehicleFetch.json();
    var dataFromCassandra = res;
    console.log('dataFromCassandra as startDT & endDT',dataFromCassandra)
    yeseterdayVehiclesDeviceData = dataFromCassandra;
    return yeseterdayVehiclesDeviceData;
  }

  async manageTodayAndYesterdayDataForTotalDistance(vehiclesWithLastData, yeseterdayVehiclesData)
  {
    console.log('inside manageTodayAndYesterdayDataForTotalDistance')
    let latestVehiclesData = vehiclesWithLastData.latestDataFromCassandra;
    let yesterdayDataOfVehicles = yeseterdayVehiclesData;
    let differenceInOdometer = []
    console.log('latestVehiclesData', latestVehiclesData)
    var min = 0;
    var max = 0;
    var first = true;
    for(var i in latestVehiclesData)
    {
      var latestItem = latestVehiclesData[i];
      var yesterdayItem = yesterdayDataOfVehicles[i];
      console.log('latestItem',latestItem)
      console.log('yesterdayItem',yesterdayItem)
      if(undefined != latestItem && latestItem != "data not found" && yesterdayItem != "data not found")
      {
        console.log('data found');
        var latestValue = latestItem[0].totalodometer;
        var yesterdayValue = yesterdayItem[0].totalodometer;
        console.log('latestValue:',latestValue,', yesterdayValue:',yesterdayValue)
        //TODO need to change, data not correct in cass so logic change here
        //var diff = Number(latestValue) - Number(yesterdayValue);
        //var diff = Number(yesterdayValue) - Number(latestValue);
        var diff = Number(latestValue)/1000 - Number(yesterdayValue)/1000;
        if(first)
        {
            min = diff;
            max = diff;
        }
        first = false;
        min = diff < min ? diff : min;
        max = diff > max ? diff : max;
        differenceInOdometer[i] = {diff:diff};
      }
      else {
        console.log('data not found');
      }
    }
    console.log('differenceInOdometer', differenceInOdometer);
    console.log('min value', min,', max',max)
    //var rangeDiff = max - min;
    //console.log('rangeDiff', rangeDiff);
    var bucketDiff = Math.ceil(max / 5);
    //TODO need to change
    console.log('bucketDiff abs',bucketDiff)
    var bucket1Max = bucketDiff * 1;
    var bucket2Max = bucketDiff * 2;
    var bucket3Max = bucketDiff * 3;
    var bucket4Max = bucketDiff * 4;
    var bucket5Max = bucketDiff * 5;
    var buckets = {};
    var bucket1 = "bucket1";
    var bucket2 = "bucket2";
    var bucket3 = "bucket3";
    var bucket4 = "bucket4";
    var bucket5 = "bucket5";
  //  var obj;
    var vehicleId;
    var item;
    var diff;
    buckets[bucket1] = {count:0, min:0, max:bucket1Max, vehicles: []};
    buckets[bucket2] = {count:0, min:bucket1Max + 1, max:bucket2Max, vehicles: []}
    buckets[bucket3] = {count:0, min:bucket2Max + 1, max:bucket3Max, vehicles: []}
    buckets[bucket4] = {count:0, min:bucket3Max + 1, max:bucket4Max, vehicles: []}
    buckets[bucket5] = {count:0, min:bucket4Max + 1, max:bucket5Max, vehicles: []}
    for(var i in differenceInOdometer)
    {
      item = differenceInOdometer[i];
      diff = item.diff;
      vehicleId = i;
      var obj = {};
      if(diff <= bucket1Max)
      {
        obj = buckets[bucket1]
        ++obj.count;
        obj.vehicles.push(vehicleId)
        //buckets[bucket1] = obj;
      }
      else if(diff <= bucket2Max)
      {
        obj = buckets[bucket2]
        ++obj.count;
        obj.vehicles.push(vehicleId)
      }
      else if(diff <= bucket3Max)
      {
        obj = buckets[bucket3]
        ++obj.count;
        obj.vehicles.push(vehicleId)
      }
      else if(diff <= bucket4Max)
      {
        obj = buckets[bucket4]
        ++obj.count;
        obj.vehicles.push(vehicleId)
      }
      else if(diff <= bucket5Max)
      {
        obj = buckets[bucket5]
        ++obj.count;
        obj.vehicles.push(vehicleId)
      }
      else {
        console.log('diff not in any bucket')
      }
    }
    console.log('total distance buckets',buckets)
    let totalDistancePieData = [["Range", "Count"]]
    for(var i in buckets)
    {
      var bucket = buckets[i]
      //if(undefined != bucket && bucket.vehicles.length > 0)
      if(undefined != bucket)
      {
        var min = bucket.min
        var max = bucket.max;
        //TODO +2 added for testing, need to remove
        var count = Number(bucket.count)+2;
        totalDistancePieData.push([min.toString()+'-'+max.toString(), count])
      }
    }
    console.log('totalDistancePieData', totalDistancePieData)
    this.setState({
                  totalDistancePieData:totalDistancePieData
                  })
  }

  handleOnSelect = (row, isSelect) => {
    console.log('inside handleOnSelect, row, isSelect',row,isSelect)
    if(isSelect)
    {
      if(undefined != row.latitude && undefined != row.longitude)
      {
        markers.push({lat:row.latitude,lng:row.longitude,id:row.sn})
      }

    }
    else {
      //TODO need to remove item from markers array
      for(var i in markers)
      {
      if(markers[i].id == row.sn)
      {
        markers.splice( i, 1 );
      }
      }
    }
    console.log('selected marker with lat/lng',markers);
    //TODO need to assign new reference, so all markers will show, same issue like pathCoordinates
    //this.setState({markers:markers})
    this.setState({markers:[...markers]})
  }

  handleOnSelectAll = (isSelect, rows) => {
    console.log('inside handleOnSelectAll, rows, isSelect',rows,isSelect)
    if(isSelect)
    {
      for(var i in rows)
      {
        var row = rows[i];
        if(undefined != row.latitude && undefined != row.longitude)
        {
          markers.push({lat:row.latitude,lng:row.longitude,id:row.sn})
        }
      }
    }
    else {
      for(var i in rows)
      {
        var row = rows[i];
        for(var i in markers)
        {
        if(markers[i].id == row.sn)
        {
          markers.splice( i, 1 );
        }
        }
      }
    }
    this.setState({markers:[...markers]})
  }

  handleMarkerClick = (props, marker) => {
    console.log('inside handleMarkerClick, props, marker',props, marker)
  }

  render() {
    console.log('Summary -->> inside render')
    const { selectedOption,isClearable } = this.state;
    console.log('markers: ',this.state.markers)

    const columnsVehicleSummaryTable = [{
      dataField: 'sn',
      text: 'SN',
      sort: true
    },
    {
      dataField: 'vehicle',
      text: 'Vehicle',
      filter: textFilter({
        placeholder: 'Vehicle',
      })
    },
    {
      dataField: 'status',
      text: 'Status',
      filter: textFilter({
        placeholder: 'Status',
      })
    },
    {
      dataField: 'speed',
      text: 'Speed',
      filter: textFilter({
        placeholder: 'Speed',
      })
    },
    {
      dataField: 'engine',
      text: 'Engine',
      filter: textFilter({
        placeholder: 'Engine',
      })
    },
    {
      dataField: 'fuel',
      text: 'Fuel',
      filter: textFilter({
        placeholder: 'Fuel',
      })
    },
    {
      dataField: 'distance',
      text: 'Distance',
      filter: textFilter({
        placeholder: 'Distance',
      })
    },
    {
      dataField: 'fuelConsumption',
      text: 'Fuel Consumption',
      filter: textFilter({
        placeholder: 'Consumption',
      })
    },
    {
      dataField: 'engineOnTime',
      text: 'Engine On/Idle Time',
      filter: textFilter({
        placeholder: '',
      })
    },
    {
      dataField: 'stoppageTime',
      text: 'Stoppage Time',
      filter: textFilter({
        placeholder: '',
      })
    },
    {
      dataField: 'movingTime',
      text: 'Moving Time',
      filter: textFilter({
        placeholder: '',
      })
    }
    ];

    const columnsVehiclesTable = [{
      dataField: 'sn',
      text: 'SN',
      sort: true
    },
    {
      dataField: 'vehicle',
      text: 'Vehicle',
      filter: textFilter({
        placeholder: 'Vehicle',
      })
    }
    ];

    const selectRow = {
      mode: 'checkbox',
      clickToSelect: true,
      onSelect: this.handleOnSelect,
      onSelectAll: this.handleOnSelectAll
    };

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12">
            <Card>
              <CardBody>
                <p><b>Summary</b></p>
                {
                !this.state.loading &&
                <Row>
                {
                this.state.isDealerShow &&
                <Col md="3">
                <p>Dealer</p>
                <Select
                  onChange={this.handleChange}
                  defaultValue={this.state.optionsDealer[0]}
                  isClearable={isClearable}
                  options={this.state.optionsDealer}
                />
                </Col>
                }
                {
                this.state.isSubDealerShow &&
                <Col md="3">
                <p>SubDealer</p>
                <Select
                  onChange={this.handleChangeSubDealerOption.bind(this)}
                  value={this.state.selectSubDealerValue}
                  isClearable={isClearable}
                  options={this.state.optionsSubDealer}
                />
                </Col>
                }
                {
                this.state.isCustomerShow &&
                <Col md="3">
                <p>Customer</p>
                <Select
                  onChange={this.handleChangeCustomerOption.bind(this)}
                  value={this.state.selectCustomerValue}
                  isClearable={isClearable}
                  options={this.state.optionsCustomer}
                />
                </Col>
                }
                {
                this.state.projectFound &&
                <Col md="3">
                <p>Project</p>
                <Select
                  onChange={this.handleChangeProjectOption.bind(this)}
                  value={this.state.selectProjectValue}
                  isClearable={isClearable}
                  options={this.state.optionsProject}
                />
                </Col>
                }
                <Col md="3">
                <p>Group</p>
                <Select
                  onChange={this.handleChangeGroupOption.bind(this)}
                  value={this.state.selectGroupValue}
                  isClearable={isClearable}
                  options={this.state.optionsGroup}
                />
                </Col>
                </Row>
                }
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card>
              <CardBody>
                <Col>
                  { !this.state.loading &&
                  <Row>
                  {
                  /*
                  <Col md="3">
                    <p style={{textAlign:'center'}}>Total Distance</p>
                    <Chart
                      chartType="PieChart"
                      data={this.state.totalDistancePieData}
                      options={pieOptions}
                      graph_id="TotalDistancePie"
                      width={"100%"}
                      height={"250px"}
                      chartEvents={chartEvents}
                    />
                  </Col>
                  <Col md="3">
                    <p style={{textAlign:'center'}}>Battery Health</p>
                    <Chart
                      chartType="PieChart"
                      data={this.state.totalDistancePieData}
                      options={pieOptions}
                      graph_id="BatteryHealthPie"
                      width={"100%"}
                      height={"250px"}
                      legend_toggle
                    />
                  </Col>
                  */
                  }
                  </Row>
                  }
                  {
                    !this.state.loading &&
                    <Row>
                      <Col xs="12" sm="6" md="4">
                        <Card>
                          <CardHeader>
                            Vehicle Status
                          </CardHeader>
                          <CardBody>
                            <div className="chart-wrapper">
                              <Doughnut data={vehicleStatusChart} />
                            </div>
                          </CardBody>
                        </Card>
                      </Col>
                      {
                      /*
                      <Col xs="12" sm="6" md="4">
                        <Card>
                          <CardHeader>
                            Battery Status
                          </CardHeader>
                          <CardBody>
                            <div className="chart-wrapper">
                              <Doughnut data={batteryStatusChart} />
                            </div>
                          </CardBody>
                        </Card>
                      </Col>
                      */
                      }
                      <Col xs="12" sm="6" md="4">
                        <Card>
                          <CardHeader>
                            Engine Status
                          </CardHeader>
                          <CardBody>
                            <div className="chart-wrapper">
                              <Doughnut data={engineStatusChart} />
                            </div>
                          </CardBody>
                        </Card>
                      </Col>
                      <Col xs="12" sm="6" md="4">
                        <Card>
                          <CardHeader>
                            Distance Covered
                          </CardHeader>
                          <CardBody>
                            <div className="chart-wrapper">
                              <Doughnut data={distanceCoveredChart} />
                            </div>
                          </CardBody>
                        </Card>
                      </Col>
                      <Col xs="12" sm="6" md="4">
                        <Card>
                          <CardHeader>
                            Fuel Consumption
                          </CardHeader>
                          <CardBody>
                            <div className="chart-wrapper">
                              <Doughnut data={fuelConsumptionChart} />
                            </div>
                          </CardBody>
                        </Card>
                      </Col>
                    </Row>
                  }
                </Col>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card>
              <CardBody>
              {
              this.state.loading &&
              <div>
              <div style={{marginLeft: '40%'}}>
              <ClipLoader
                sizeUnit={"px"}
                size={50}
                color={'#123abc'}
                loading={this.state.loading}
              />
              </div>
              <div style={{marginLeft: '35%'}}>
              <p>Loading ... Please wait!</p>
              </div>
              </div>
              }
              { !this.state.loading &&
              <ToolkitProvider
                    keyField="sn"
                    data={ this.state.dataVehicleSummaryTable }
                    columns={ columnsVehicleSummaryTable }
                    search
                  >
                    {
                      props => (
                        <div>
                          <Row className="align-items-center">
                          <Col>
                          <Row>
                          <Col md="3">
                          <SearchBar
                            { ...props.searchProps }
                          />
                          </Col>
                          </Row>
                          </Col>
                          <Col md="2">
                          <MyExportCSV { ...props.csvProps } />

                          </Col>
                          </Row>
                          <hr />
                          <div style={{overflow: 'overlay'}}>
                          <BootstrapTable
                            { ...props.baseProps }
                            filter={ filterFactory() }
                            hover
                          />
                          </div>
                        </div>
                      )
                    }
                  </ToolkitProvider>
                  }
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card>
              <CardBody>
              <Col>
              <Row>
              <Col md="3">
              { !this.state.vehicleTableLoading &&
              <ToolkitProvider
                    keyField="sn"
                    data={ this.state.dataVehiclesTable }
                    columns={ columnsVehiclesTable }
                    search
                  >
                    {
                      props => (
                        <div>
                          <Row className="align-items-center">
                          <Col>
                          <Row>
                          <Col md="3">
                          {
                            /*
                            <SearchBar
                              { ...props.searchProps }
                            />
                            */
                          }
                          </Col>
                          </Row>
                          </Col>
                          </Row>
                          <hr />
                          <div style={{overflow: 'overlay', height:'500px'}}>
                          <BootstrapTable
                            { ...props.baseProps }
                            keyField='sn'
                            filter={ filterFactory() }
                            selectRow={ selectRow }
                            pagination={ paginationFactory() }
                            hover
                          />
                          </div>
                        </div>
                      )
                    }
                  </ToolkitProvider>
                  }
                  </Col>
                  {
                  !this.state.loading &&
                  <Col>
                    <MapWithAMarkerClusterer
                      markers={this.state.markers}
                      onMarkerClick={this.handleMarkerClick}
                       />
                  </Col>
                  }
                  </Row>
                </Col>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )

  }

}
export default Summary;
