import React, { Component } from 'react';
//import { HashRouter, Route, Switch } from 'react-router-dom';

// Styles
// CoreUI Icons Set
/*
   //   <HashRouter>
   //     <Switch>
          <Route exact path="/login" name="Login Page" component={Login} />
          <Route exact path="/register" name="Register Page" component={Register} />
          <Route exact path="/404" name="Page 404" component={Page404} />
          <Route exact path="/500" name="Page 500" component={Page500} />
          <Route path="/" name="Home" component={DefaultLayout} />
        </Switch>
      </HashRouter>

*/



// Import Flag Icons Set
/*
import 'flag-icon-css/css/flag-icon.min.css';
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css'; */
// Import Main styles for this application
//import '../static/scss/style.css'

// Containers
import { DefaultLayout,DefaultHeader } from './containers';
// Pages
import { Login, Page404, Page500, Register } from './views/Pages';

import {Provider} from 'react-redux';
import { store } from '../redux/_helpers';

// import { renderRoutes } from 'react-router-config';

//import '_next/static/style.css'

import cookie from "js-cookie";

import { NextAuth } from 'next-auth/client'
import Cookies from 'universal-cookie'
import Page from '../components/page'

class MyApp extends Page {

  constructor(props) {
      super(props);
      console.log('App -->> inside constructor, props',props)
      console.log('App --> inside constructor, cookie ',cookie.get('id'))
      this.state = {
        session: props.session,
        name:'',
      }
    }

    async componentDidMount() {
      const session = await NextAuth.init({force: true})
      console.log('App -->> componentDidMount, session: ',session)
      this.setState({
        session: session,
        isSignedIn: (session.user) ? true : false
      })

      // If the user bounces off to link/unlink their account we want them to
      // land back here after signing in with the other service / unlinking.
      const cookies = new Cookies()
      cookies.set('redirect_url', window.location.pathname, { path: '/' })
    }

  render() {
    if (this.props.session && this.props.session.user) {
    return (
      <Provider store={store}>
       <DefaultLayout {...this.props} sidepage={'Dashboard'}/>
      </Provider>
    );
  }
  else {
    return (
      <Provider store={store}>
       <Login/>
      </Provider>
    );
  }
  }
}

export default MyApp;
