import Document, { Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    console.log('_document.js -->> inside getInitialProps')
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <html>
        <Head>
          <style>{`body { margin: 0 } /* custom! */`}</style>
	       <link rel='stylesheet' href='@coreui/icons/css/coreui-icons.min.css' />

         <link rel='stylesheet' href='_next/static/style.css' />
         <link rel='stylesheet' href='_next/static/react-perfect-scrollbar.min.css' />
         <link rel='stylesheet' href='_next/static/min/css/coreui-icons.min.css' />
         <link rel='stylesheet' href='_next/static/min/css/simple-line-icons.css' />
         <link rel='stylesheet' href='_next/static/min/css/font-awesome.css' />
        </Head>
        <body className="custom_class">
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}
