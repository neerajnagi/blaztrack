export default {
  items: [
    {
      name: 'Dashboard',
      url: '/',
      icon: 'icon-puzzle',
      component:'Dashboard',
    },
    {
      title: true,
      name: 'User Management',
    },
    {
      name: 'Dealer',
      url: '/dealer',
      icon: 'icon-puzzle',
      component:'Dealer',
    },
    {
      name: 'SubDealer',
      url: '/subDealer',
      icon: 'icon-puzzle',
      component:'SubDealer',
    },
    {
      name: 'Customer',
      url: '/customer',
      icon: 'icon-puzzle',
      component:'Customer',
    },
    {
      name: 'Group',
      url: '/group',
      icon: 'icon-puzzle',
      component:'Group',
    },
    {
      name: 'Vehicle',
      url: '/vehicle',
      icon: 'icon-puzzle',
      component:'Vehicle',
    },
  ],
};
