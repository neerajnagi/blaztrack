import React from 'react'
import Link from 'next/link'
import Page from '../../components/page'
//import Layout from '../../components/layout'

export default class extends Page {

  static async getInitialProps({req, query}) {
    let props = await super.getInitialProps({req})
    console.log('auth/error.js -->  getInitialProps, props: ',props)
    props.action = query.action || null
    props.type = query.type || null
    props.service = query.service || null
    return props
  }

  render() {
    if (this.props.action == 'signin' && this.props.type == 'oauth') {
      return(
          <div className="text-center mb-5">
            <h1 className="display-4 mt-5 mb-3">Unable to sign in</h1>
          </div>
      )
    }  else {
      return(

          <div className="text-center mb-5">
            <h1 className="display-4 mt-5">Error signing in</h1>
            <p className="lead">An error occured while trying to sign in.</p>
          </div>

      )
    }
  }
}
