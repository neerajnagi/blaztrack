import React, { Component } from 'react';
import {
  Badge,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  NavItem,
  NavLink,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader
 } from 'reactstrap';
import PropTypes from 'prop-types';
import Head from 'next/head'
import { AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../../static/assets/img/brand/logo.svg'
import sygnet from '../../../static/assets/img/brand/sygnet.svg'

import Router from 'next/router'
import { NextAuth } from 'next-auth/client'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {

  constructor(props) {
      super(props);
      this.handleLogoutUser = this.handleLogoutUser.bind(this);
      this.handleUserProfile = this.handleUserProfile.bind(this);
      this.toggleUserProfile = this.toggleUserProfile.bind(this);
      console.log('DefaultHeader -->> inside constructor, this.props ',this.props.session)
      this.state = {
                    session: props.session,
                    isOpenModalUserProfile:false,
                    user:''
                  }
  }

  handleUserProfile(e)
  {
    console.log('DefaultHeader -->> inside handleUserProfile')
    e.preventDefault();
    this.getUser();
  }

  async getUser()
  {
    const userId = this.state.session.user.id
    const getUser = await fetch('/user/fetch',{credentials: 'include'});
    const res = await getUser.json()
    const data = res.data
    console.log('user res ',res)
    this.setState({
                  user:data
                })
    this.toggleUserProfile();
  }

  toggleUserProfile() {
    console.log('inside toggleEditSubDealer')
    this.setState({
      isOpenModalUserProfile: !this.state.isOpenModalUserProfile,
    });
  }

  handleLogoutUser(e) {
      console.log('DefaultHeader -->> inside handleLogoutUser')
      e.preventDefault();
      NextAuth.signout()
      .then(() => {
        Router.push('/auth/callback')
      })
      .catch(err => {
        Router.push('/auth/error?action=signout')
      })
  }

  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;
    const {session, user} = this.state;

    return (
/*

import 'flag-icon-css/css/flag-icon.min.css';
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css'; */

      <React.Fragment>
        <Head>
            <link rel='stylesheet' href='_next/static/style.css' />
            <link rel='stylesheet' href='_next/static/react-perfect-scrollbar.min.css' />
            <link rel='stylesheet' href='_next/static/min/css/coreui-icons.min.css' />
            <link rel='stylesheet' href='_next/static/min/css/simple-line-icons.css' />
            <link rel='stylesheet' href='_next/static/min/css/font-awesome.css' />
        </Head>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        {
        /*
        <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: 'Logo' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'Logo' }}
        />
        */
        }
        {
          <AppNavbarBrand
          />
        }
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        {
        /*
        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink href="/">Dashboard</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <NavLink href="#/users">Users</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <NavLink href="#">Settings</NavLink>
          </NavItem>
        </Nav>
        */
        }
        <Nav className="ml-auto" navbar>
          <NavItem className="d-md-down-none">
            <NavLink href="#"><i className="icon-bell"></i>
            {
              //<Badge pill color="danger">5</Badge>
            }
            </NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink href="#"><i className="icon-list"></i></NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink href="#"><i className="icon-location-pin"></i></NavLink>
          </NavItem>
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              {
                //<img src={'static/assets/img/avatars/6.jpg'} className="img-avatar" alt="user@blaztrack.com" />
              }
              {
                <img src={'static/assets/img/avatars/6.jpg'} className="img-avatar" alt="user" />
              }
            </DropdownToggle>
            <DropdownMenu right style={{ right: 'auto' }}>
              {
              /*
              <DropdownItem header tag="div" className="text-center"><strong>Account</strong></DropdownItem>
              <DropdownItem><i className="fa fa-bell-o"></i> Updates<Badge color="info">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-envelope-o"></i> Messages<Badge color="success">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-tasks"></i> Tasks<Badge color="danger">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-comments"></i> Comments<Badge color="warning">42</Badge></DropdownItem>
              <DropdownItem header tag="div" className="text-center"><strong>Settings</strong></DropdownItem>
              <DropdownItem><i className="fa fa-wrench"></i> Settings</DropdownItem>
              <DropdownItem><i className="fa fa-usd"></i> Payments<Badge color="secondary">42</Badge></DropdownItem>
              */
              }
              <DropdownItem header tag="div" className="text-center"><strong>Account</strong></DropdownItem>
              <DropdownItem onClick={this.handleUserProfile}><i className="fa fa-user"></i> Profile</DropdownItem>
              {
                //<DropdownItem><i className="fa fa-file"></i> Projects<Badge color="primary">42</Badge></DropdownItem>
              }
              <DropdownItem divider />
              {
                //<DropdownItem><i className="fa fa-shield"></i> Lock Account</DropdownItem>
              }
              <DropdownItem onClick={this.handleLogoutUser}><i className="fa fa-lock"></i> Logout</DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
        {
        //<AppAsideToggler className="d-md-down-none" />
        }
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
        <Modal isOpen={this.state.isOpenModalUserProfile} toggle={this.toggleUserProfile}
               className={'modal-primary ' + this.props.className}>
          <ModalHeader toggle={this.toggleUserProfile}>Profile</ModalHeader>
          <ModalBody>
            User : {user.firstName +' '+ user.lastName}
          </ModalBody>
        </Modal>
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
