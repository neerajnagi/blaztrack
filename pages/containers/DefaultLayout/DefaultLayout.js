import React, { Component } from 'react';
//import { Redirect, Route, Switch } from 'react-router-dom';
import { Container , Switch} from 'reactstrap';
//import AppSidebarNav from '../../AppSidebarNav'


import {
  AppAside,
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
import navigation from '../../_nav';
// routes config
//import routes from '../../routes';
import DefaultAside from './DefaultAside';
import DefaultFooter from './DefaultFooter';
import DefaultHeader from './DefaultHeader';

import 'bootstrap/dist/css/bootstrap.min.css' ;
///  <AppSidebarNav navConfig={navigation} {...this.props} />

import Dashboard from '../../views/Dashboard'
import Summary from '../../views/Summary'
import Dealer from '../../views/Dealer';
import SubDealer from '../../views/SubDealer';
import Customer from '../../views/Customer';
import Group from '../../views/Group';
import Vehicle from '../../views/Vehicle';
import GraphTest from '../../views/GraphTest';

import Head from 'next/head'

import { NextAuth } from 'next-auth/client'

import Router from 'next/router'

import Link from 'next/link'

import Page from '../../../components/page'

//react-spinners
import { css } from 'react-emotion';
// First way to import
import { ClipLoader } from 'react-spinners';

let sidePage = "";

let navigationItem = [];

let manageCategory = "";

class DefaultLayout extends Component {
  constructor(props) {
    super(props);
    console.log('DefaultLayout -->> inside constructor, this.props ',this.props.session)
    console.log('DefaultLayout -->> inside constructor, props ',props.session)
    sidePage = props.sidepage;
    this.state = {session: props.session,
                  isSignedIn: (props.session.user) ? true : false,
                  sidePage:sidePage,
                  navigation:navigationItem,
                  manageCategory:manageCategory,
                  isPageLoaded:false,
                };
    this.handleMenuClick = this.handleMenuClick.bind(this);
  }

  handleMenuClick(value)
  {
    console.log('page, ',value)
    const page = this.components[value];
    this.setState({sidePage:page})
  }

  components = {
        Summary:Summary,
        Dashboard:Dashboard,
        Dealer: Dealer,
        SubDealer: SubDealer,
        Customer: Customer,
        Group: Group,
        Vehicle:Vehicle,
        GraphTest:GraphTest
    };

  async componentDidMount() {
    console.log('DefaultLayout -->> inside componentDidMount')
    const session = await NextAuth.init({force: true})
    if(session.user)
    {
      console.log('user logged in')
    }
    else {
      console.log('user not logged in')
      Router.push('/auth/callback')
      return false;
    }
    console.log('after checking user state')
    console.log('sidePage:',sidePage)
    //this.setState({sidePage:this.components['Dashboard']})
    this.setState({sidePage:this.components['Summary']})
    this.getProfile();
    this.fetchUser();
  }

  async getProfile() {
    console.log('inside getProfile')
    fetch('/account/user', {
      credentials: 'include'
    })
    .then(r => r.json())
    .then(user => {
      console.log('user',user);
    })
  }

  async fetchUser()
  {
    const res = await fetch('/user/fetch',{credentials: 'include'});
    const resData = await res.json()
    //console.log('resData',resData)
    const data = resData.data;
    const manageCategory = data.manageCategory;
    const lowercaseName = manageCategory.lowercaseName;
    this.sideMenuItemsList(lowercaseName)
  }

  sideMenuItemsList(manageCategory)
  {
    console.log('manageCategory',manageCategory)
    console.log('navigation',this.state.navigation)
    //let itemsArray = this.state.navigation;
    let itemsArray = {items:[
        {
          name: 'Summary',
          url: '/',
          icon: 'icon-puzzle',
          component:'Summary',
        },
        {
          name: 'Dashboard',
          url: '/',
          icon: 'icon-puzzle',
          component:'Dashboard',
        },
      ],
    };
    let otherMenus = [];
    if(manageCategory == "dealer")
    {
      console.log('itemsArray, before',itemsArray)
      otherMenus = [
      {
        name: 'SubDealer',
        url: '/subDealer',
        icon: 'icon-puzzle',
        component:'SubDealer',
      },
      {
        name: 'Customer',
        url: '/customer',
        icon: 'icon-puzzle',
        component:'Customer',
      },
      {
        name: 'Group',
        url: '/group',
        icon: 'icon-puzzle',
        component:'Group',
      },
      {
        name: 'Vehicle',
        url: '/vehicle',
        icon: 'icon-puzzle',
        component:'Vehicle',
      }
      ];
      /*
      for(var i in otherMenus)
      {
        itemsArray.items.push(otherMenus[i]);
      }
      console.log('itemsArray, after',itemsArray)
      */
    }
    else if(manageCategory == "subdealer")
    {
      console.log('itemsArray, before',itemsArray)
      otherMenus = [
      {
        name: 'Customer',
        url: '/customer',
        icon: 'icon-puzzle',
        component:'Customer',
      },
      {
        name: 'Group',
        url: '/group',
        icon: 'icon-puzzle',
        component:'Group',
      },
      {
        name: 'Vehicle',
        url: '/vehicle',
        icon: 'icon-puzzle',
        component:'Vehicle',
      }];
    }
    else if(manageCategory == "customer")
    {
      console.log('itemsArray, before',itemsArray)
      otherMenus = [
      {
        name: 'Group',
        url: '/group',
        icon: 'icon-puzzle',
        component:'Group',
      },
      {
        name: 'Vehicle',
        url: '/vehicle',
        icon: 'icon-puzzle',
        component:'Vehicle',
      }];
    }
    else if(manageCategory == "group")
    {
      console.log('itemsArray, before',itemsArray)
      otherMenus = [
      {
        name: 'Vehicle',
        url: '/vehicle',
        icon: 'icon-puzzle',
        component:'Vehicle',
      }];
    }
    else {

      //nothing to add in AppSidebarNav items

    }
    for(var i in otherMenus)
    {
      itemsArray.items.push(otherMenus[i]);
    }
    console.log('itemsArray, after',itemsArray)
    this.setState({
                  navigation:itemsArray,
                  manageCategory:manageCategory,
                  isPageLoaded:true
                  });
  }

  render() {
    //const TagName = this.components[this.props.sidePage];
    const TagName = this.state.sidePage;
    if(this.state.isSignedIn && this.state.isPageLoaded)
    {
    return (
      <div className="app">
        <AppHeader fixed>
          <DefaultHeader {...this.props} />
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg" style={{background:'dimgray'}}>
            <AppSidebarHeader />
            <AppSidebarForm />
            {
              //<AppSidebarNav navConfig={navigation} {...this.props} handleMenu={this.handleMenuClick.bind(this)} />
              <AppSidebarNav navConfig={this.state.navigation} {...this.props} handleMenu={this.handleMenuClick.bind(this)} />
            }
            {
              <AppSidebarFooter />
            }
          </AppSidebar>
          <main className="main">
          {
          <TagName {...this.props} managecategory={this.state.manageCategory}/>
          }
          </main>
        </div>
      </div>
    );
  }//end of if
  else {
    return (
      <div className="app">
        <div style={{
                position:'absolute',
                top:'50%',
                left:'50%',
                margin:'-50px 0px 0px -50px'
              }}
          >
        <ClipLoader
          sizeUnit={"px"}
          size={50}
          color={'#123abc'}
          loading={this.state.loading}
        />
        </div>
        <div style={{
                position:'absolute',
                top:'62%',
                left:'46%',
                margin:'-50px 0px 0px -50px'
              }}
          >
        <p>Loading ... Please wait!</p>
        </div>
      </div>
    )
  }
  }
}

export default DefaultLayout;
