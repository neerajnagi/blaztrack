import './polyfill'
import React from 'react';
import {render} from 'react-dom';
import ReactDOM from 'react-dom';
//import '../_next/static/min/css/coreui-icons.min.css'
//import '../_next/static/min/css/flag-icon.min.css';
//import '../static/react-perfect-scrollbar.min.css'
import '../static/scss/style.css'
import App from './App';
import Page from '../components/page'
// disable ServiceWorker
// import registerServiceWorker from './registerServiceWorker';
export var window = {}
//ReactDOM.render(<App />, document.getElementById('root'));
// disable ServiceWorker
// registerServiceWorker();

export default class extends Page {
  render() {
    return (
        <App {...this.props} />
    )
  }
}
