'use strict';

const fetch = require('node-fetch');

const cassandraColName = {
                          'Digital 1':'din1', 'Digital 2':'din2', 'Digital 3':'din3', 'Digital 4':'din4',
                          'Analog 1':'ain1', 'Analog 2':'ain2', 'Analog 3':'ain3', 'Analog 4':'ain4',
                          'External Bat':'power', 'Internal Bat':'battery', 'Speed':'speed'
                          };

async function vehiclesForSiteData(id, manageCategory, all)
{
  console.log('inside getVehiclesForSiteData');
  const fetchVehicle = await fetch('http://159.65.6.196:5500/api/vehicle/fetchvehicleforsitedata/'+id+'/'+manageCategory+'/'+all);
  const res = await fetchVehicle.json();
  console.log('fetchVehicle, res',res);
  const data = res.data;
  let vehiclesData = [];
  if(all)
  {
      if(manageCategory == "group")
      {
        //res came with manage
        const manage = data;
        for(var i in manage)
        {
          var item = manage[i];
          const group = item.group;
          const vehicles = group.vehicles;
          for(var j in vehicles)
          {
            var itemVehicles = vehicles[j];
            var sensorAssignments = itemVehicles.sensorAssignments;
            //console.log('sensorAssignments',sensorAssignments)
            for(var l in sensorAssignments)
            {
              var itemSensorAssignment = sensorAssignments[l]
            }

            var vehicleCategory = "";

            if(undefined != itemVehicles.vehicleCategory && itemVehicles.vehicleCategory != "")
            {
              vehicleCategory = itemVehicles.vehicleCategory
            }

            var driver;
            if(undefined != itemVehicles.driverName || undefined != itemVehicles.driverMobileNo)
            {
              driver = {name:itemVehicles.driverName, mobileNo:itemVehicles.driverMobileNo}
            }
            else
            {
              driver = {name:'', mobileNo:''}
            }

            var hmrParam;
            if(undefined != itemVehicles.hmrParam)
            {
              hmrParam = itemVehicles.hmrParam;
            }
            else {
              hmrParam = "NA";
            }

            vehiclesData.push({
              "customerId":group.customerId,
              "vehicleId":itemVehicles.id,
              "group":group.name,
              "vehicle":itemVehicles.regNo,
              "sensorAssignments":sensorAssignments,
              "vehicleCategory":vehicleCategory,
              "driver":driver,
              "hmrParam":hmrParam
            });
          }
        }
      }
      else {
        // res came with group
        const group = data
        for(var i in group)
        {
          var item = group[i];
          const customerId = item.customerId;
          var vehicles = item.vehicles;
          for(var j in vehicles)
          {
            var itemVehicles = vehicles[j];
            var sensorAssignments = itemVehicles.sensorAssignments;
            //console.log('sensorAssignments',sensorAssignments)
            for(var l in sensorAssignments)
            {
              var itemSensorAssignment = sensorAssignments[l]
            }

            var vehicleCategory = "";

            if(undefined != itemVehicles.vehicleCategory && itemVehicles.vehicleCategory != "")
            {
              vehicleCategory = itemVehicles.vehicleCategory
            }

            var driver;
            if(undefined != itemVehicles.driverName || undefined != itemVehicles.driverMobileNo)
            {
              driver = {name:itemVehicles.driverName, mobileNo:itemVehicles.driverMobileNo}
            }
            else
            {
              driver = {name:'', mobileNo:''}
            }

            var hmrParam;
            if(undefined != itemVehicles.hmrParam)
            {
              hmrParam = itemVehicles.hmrParam;
            }
            else {
              hmrParam = "NA";
            }

            vehiclesData.push({
              "customerId":customerId,
              "vehicleId":itemVehicles.id,
              "group":item.name,
              "vehicle":itemVehicles.regNo,
              "sensorAssignments":sensorAssignments,
              "vehicleCategory":vehicleCategory,
              "driver":driver,
              "hmrParam":hmrParam
            });
          }
        }

      }
  }
  else
  {
    //res came with vehicle
    const vehicle = data
    for(var i in vehicle)
    {
      var item = vehicle[i];
      var group = item.group;
      var sensorAssignments = item.sensorAssignments;
      //console.log('sensorAssignments',sensorAssignments);
      for(var k in sensorAssignments)
      {
        var itemSensorAssignment = sensorAssignments[k]
      }

      var vehicleCategory = "";

      if(undefined != item.vehicleCategory && item.vehicleCategory != "")
      {
        vehicleCategory = item.vehicleCategory
      }

      var driver;
      if(undefined != item.driverName || undefined != item.driverMobileNo)
      {
        driver = {name:item.driverName, mobileNo:item.driverMobileNo}
      }
      else
      {
        driver = {name:'', mobileNo:''}
      }

      var hmrParam;
      if(undefined != item.hmrParam)
      {
        hmrParam = item.hmrParam;
      }
      else {
        hmrParam = "NA";
      }

      vehiclesData.push({
        "customerId":group.customerId,
        "vehicleId":item.id,
        "group":group.name,
        "vehicle":item.regNo,
        "sensorAssignments":sensorAssignments,
        "vehicleCategory":vehicleCategory,
        "driver":driver,
        "hmrParam":hmrParam
      });
    }
  }
  console.log('vehiclesData',vehiclesData)
  return vehiclesData;
}

function sensorAssignments(sensorAssignments)
{
  console.log('inside sensorAssignments')
  let assignedSensorsDetails = {};
  for(var i in sensorAssignments)
  {
    var item = sensorAssignments[i];
    var gpsDevice = item.gpsDevice;
    var devicePort= item.devicePort;
    var devicePortName;
    var sensor = item.sensor;
    var sensorType = sensor.sensorType;
    var min = item.min;
    var max = item.max;
    var cassColName;
    if(undefined != sensorType && undefined != sensorType.name)
    {
      let sensorTypeName = sensorType.name.toLowerCase();
      if(sensorTypeName == "fuel" || sensorTypeName == "enginestatus")
      {
        continue;
      }

      if(undefined == min)
      {
        min = "NA";
      }
      if(undefined == max)
      {
        max = "NA";
      }
      if(undefined != devicePort && undefined != devicePort.name)
      {
        devicePortName = devicePort.name;
        cassColName = cassandraColName[devicePortName];
      }
      else {
        devicePortName = "NA";
        cassColName = "NA";
      }
      console.log('sensorTypeName:',sensorType.name);
      console.log('min:',min,', max:',max);
      console.log('raw:',raw)
      console.log('devicePortName:',devicePortName);
      assignedSensorsDetails[sensorType.name] = {devicePort:devicePortName,min:min,max:max,dbColName:cassColName}
    }
  }
  return assignedSensorsDetails;
}

async function vehicleAndSensorsStatus(assignedSensorsDetails, cassData, hmrParam)
{
  console.log('inside vehicleAndSensorsStatus');
  let isSensorAssignmentsFound = false;
  let vehicleAndSensorStatus = {};
  let vehicleStatus;
  if(undefined != assignedSensorsDetails && assignedSensorsDetails.length > 0)
  {
    console.log('assignedSensorsDetails',assignedSensorsDetails);
    for(var i in assignedSensorsDetails)
    {
      let sensorType = i;
      let sensorStatus;
      let column = assignedSensorsDetails[i].dbColName;
      let min = assignedSensorsDetails[i].min;
      let max = assignedSensorsDetails[i].max;
      let raw;
      if(undefined != cassData[column])
      {
        raw = cassData[column];
      }
      else
      {
        raw = "NA";
      }
      console.log('min',min,'max',max)
      console.log('raw',raw);
      if(undefined != raw && raw != "NA")
      {
        if(raw >= min && raw <= max)
        {
          sensorStatus = true;
          vehicleAndSensorStatus[i.toLowerCase()] = "on"
        }
        else {
          sensorStatus = false;
          vehicleAndSensorStatus[i.toLowerCase()] = "off"
        }
      }
      else {
        vehicleAndSensorStatus[i.toLowerCase()] = "NA"
      }
    }
    isSensorAssignmentsFound = true;
  }
  else {
    isSensorAssignmentsFound = false;
  }

  let speed = cassData.speed;
  if(undefined != speed)
  {
    if(speed > 0)
    {
      vehicleStatus = "Moving"
    }
    else
    {
      vehicleStatus = "Stoppage";
    }
  }
  else
  {
    console.log('speed not found')
    vehicleStatus = "NA";
  }

  //TODO now check engine status
  if(isSensorAssignmentsFound)
  {
    //TODO define in before loop
    ////let hmrParam = apiData.hmrParam;
    if(undefined != hmrParam && hmrParam != "NA")
    {
      let engineStatus = await this.checkEngineStatus(assignedSensorsDetails, hmrParam, cassData);
      console.log('enginestatus',engineStatus)
      if(undefined != engineStatus)
      {
        let status = engineStatus.status;
        let msg = engineStatus.msg;
        if(status == "on")
        {
          if(vehicleStatus != "Moving")
          {
            vehicleStatus = "Engine Idle";
          }
          vehicleAndSensorStatus['engine'] = status;
        }
        else if(status == "off")
        {
          vehicleAndSensorStatus['engine'] = status;
        }
        else
        {
          vehicleAndSensorStatus['engine'] = status;
        }
      }
    }
  }

  //TODO now check raw data last insert_datetime is not less then defined notReachableTime
  //if less then vehicleStatus is Not Reachable;
  let insert_datetime = cassData.insert_datetime;
  if(undefined != insert_datetime)
  {
    console.log('insert_datetime',insert_datetime);
    //TODO if insert_datetime < notReachableTime then vehcile status is "Not Reachable"
  }
  else {
    vehicleStatus = "NA";
  }

  console.log('vehicleAndSensorStatus:',vehicleAndSensorStatus)
  return vehicleAndSensorStatus;
}

function engineStatus(assignedSensorsDetails, hmrParam, rawData)
{
  console.log('inside engineStatus');
  let engineStatus = {};
  let primarySensor;
  if(hmrParam.hasOwnProperty('primary'))
  {
    primarySensor = hmrParam.primary;
  }
  else {
    engineStatus = {status: 'not known', msg:'primary sensor not found'}
    return engineStatus;
  }
  let secondarySensor;
  if(hmrParam.hasOwnProperty('secondary'))
  {
    secondarySensor = hmrParam.secondary;
  }
  let dataSelectedSecANDType;
  let dataSelectedSecORType;
  if(undefined != secondarySensor)
  {
    if(secondarySensor.hasOwnProperty('and'))
    {
      dataSelectedSecANDType = secondarySensor.and;
    }
    if(secondarySensor.hasOwnProperty('or'))
    {
      dataSelectedSecORType = secondarySensor.or;
    }
  }
  console.log('primarySensor',primarySensor);
  console.log('secondarySensor',secondarySensor);
  console.log('dataSelectedSecANDType',dataSelectedSecANDType);
  console.log('dataSelectedSecORType',dataSelectedSecORType);
  //TODO need to first check if primary and secondary sensors not undefined
  if(undefined == primarySensor && undefined == dataSelectedSecANDType && undefined == dataSelectedSecORType)
  {
    console.log('primary and secondary sensors not found in hmrParam')
    engineStatus = {status:'not known', msg:'hmr not defined'}
    return engineStatus;
  }
  let primarySensorStatus = false;
  let secondaryANDSensorsStatus = false;
  let secondaryORSensorsStatus = false;

  if(undefined != primarySensor)
  {
    console.log('assignedSensorsDetails',assignedSensorsDetails)
    console.log('primarySensor',primarySensor)
    for(var i in assignedSensorsDetails)
    {
      let sensorType = i.toLowerCase();
      if(sensorType == primarySensor)
      {
        //first found primary sensor raw value, min and max from dataAssignedSensorTable
        //then make decision
        let column = assignedSensorsDetails[i].dbColName;
        let min = assignedSensorsDetails[i].min;
        let max = assignedSensorsDetails[i].max;
        //let raw = assignedSensorsDetails[i].raw;
        let raw;
        if(undefined != cassData[column])
        {
          raw = cassData[column];
        }
        else
        {
          raw = "NA";
        }
        console.log('min',min,'max',max)
        console.log('raw',raw);
        if(undefined != raw && raw != "NA")
        {
          if(raw >= min && raw <= max)
          {
            primarySensorStatus = true;
          }
          else {
            primarySensorStatus = false;
          }
        }
      }
    }
  }
  else {
    console.log('primary sensor not defined')
  }
  if(primarySensorStatus)
  {
    console.log('engine on')
    engineStatus = {status:'on', msg:'checked with primary sensor'}
    return engineStatus;
  }
  //TODO if primarySensorStatus false then check secondary sensors status
  else
  {
    //TODO now check secondary sensors
    //First check secondary sensors with AND conditions (always true)
    console.log('dataSelectedSecANDType',dataSelectedSecANDType)
    console.log('dataSelectedSecORType',dataSelectedSecORType)
    if(undefined != dataSelectedSecANDType)
    {
      for(var i in assignedSensorsDetails)
      {
        let sensorType = i.toLowerCase();;
        if(dataSelectedSecANDType.hasOwnProperty(sensorType))
        {
          console.log('now checked secondary AND for sensor',sensorType);
          let column = assignedSensorsDetails[i].dbColName;
          let min = assignedSensorsDetails[i].min;
          let max = assignedSensorsDetails[i].max;
          //let raw = assignedSensorsDetails[i].raw;
          let raw;
          if(undefined != cassData[column])
          {
            raw = cassData[column];
          }
          else
          {
            raw = "NA";
          }
          console.log('min',min,'max',max)
          console.log('raw',raw);
          if(undefined != raw && raw != "NA")
          {
            if(raw >= min && raw <= max)
            {
              secondaryANDSensorsStatus = true;
            }
            else {
              secondaryANDSensorsStatus = false;
              break;
            }
          }
        }
      }
    }
    else {
      console.log('secondary AND type sensors not defined')
    }
    if(secondaryANDSensorsStatus)
    {
      console.log('engine on')
      engineStatus = {status:'on', msg:'checked with secondary AND type sensors'}
      return engineStatus;
    }
    else
    {
      //Second check secondary sensors with OR conditions (atleast one true)
      if(undefined != dataSelectedSecORType)
      {
        for(var i in assignedSensorsDetails)
        {
          let sensorType = i.toLowerCase();
          if(dataSelectedSecORType.hasOwnProperty(sensorType))
          {
            console.log('now checked secondary OR for sensor',sensorType);
            let column = assignedSensorsDetails[i].dbColName;
            let min = assignedSensorsDetails[i].min;
            let max = assignedSensorsDetails[i].max;
            //let raw = assignedSensorsDetails[i].raw;
            let raw;
            if(undefined != cassData[column])
            {
              raw = cassData[column];
            }
            else
            {
              raw = "NA";
            }
            console.log('min',min,'max',max)
            console.log('raw',raw);
            if(undefined != raw && raw != "NA")
            {
              if(raw >= min && raw <= max)
              {
                secondaryORSensorsStatus = true;
                break;
              }
              else {
                secondaryORSensorsStatus = false;
              }
            }
          }
        }
      }
      else {
        console.log('secondary OR type sensors not defined')
      }

      if(secondaryORSensorsStatus)
      {
        console.log('engine on')
        engineStatus = {status:'on', msg:'checked with secondary OR type sensors'}
        return engineStatus;
      }
      else {
        console.log('engine off')
        engineStatus = {status:'off', msg:'checked with secondary OR type sensors'}
        return engineStatus;
      }
    }
  }
  return engineStatus;
}

module.exports = {

        fetchVehiclesForSiteData : async function(id, manageCategory, all) {
          console.log('inside vehicle helpers, fetchVehiclesForSiteData')
          let fetchedVehicles = await vehiclesForSiteData(id, manageCategory, all);
      		return fetchedVehicles;
        },

        getSensorAssignments : async function(sensorAssignments) {
          console.log('inside vehicle helpers, getSensorAssignments')
          let assignedSensors = await sensorAssignments(sensorAssignments);
      		return assignedSensors;
        },

        getVehicleAndSensorStatus : async function(assignedSensorsDetails, cassData, hmrParam) {
          console.log('inside vehicle helpers, getVehicleAndSensorStatus')
          const checkedStatus = await vehicleAndSensorsStatus(assignedSensorsDetails, cassData, hmrParam);
      		return checkedStatus;
        },

        checkEngineStatus : async function(assignedSensorsDetails, hmrParam, rawData) {
          console.log('inside vehicle helpers, checkEngineStatus')
          const checkedEngineStatus = await engineStatus(assignedSensorsDetails, hmrParam, rawData);
      		return checkedEngineStatus;
        }


}
