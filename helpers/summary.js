'use strict';

const fetch = require('node-fetch');
const vehicleHelpers = require('./vehicle')

async function getSummary(vehicles, deviceData, startDT, endDT)
{
  console.log('inside getSummary')
  ////let assignedSensors = vehicleHelpers.getSensorAssignments(sensorAssignments);
  ////console.log('assignedSensors',assignedSensors)
  return {'vehicle':'test', 'status':'off','engineHours':'10'}
}

module.exports = {

        getVehiclesSummary : async function(vehicles, deviceData, startDT, endDT) {
          console.log('inside summary helpers, getVehiclesSummary')
          let calculatedSummary;
          const calculateSummary = await getSummary(vehicles, deviceData, startDT, endDT);
          if(undefined != calculateSummary)
          {
            calculatedSummary = calculateSummary;
          }
      		return calculatedSummary;
        },

}
