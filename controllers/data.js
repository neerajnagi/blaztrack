'use strict';
//TODO cassandra client
//const cassandra = require('cassandra-driver');
//const client = new cassandra.Client({ contactPoints: ['192.168.1.101'], keyspace: 'blaztrack' });
//TODO getting client from other class
const cassandra = require('../cassandra')
const client = cassandra.client;
module.exports = {

getDataFromCassandra : function() {

  		//var query = 'SELECT * FROM gps_data_cf LIMIT 10';
      var query = 'SELECT * FROM gps_data_cf_v2 LIMIT 10';
      var resultArr = [];
		  var eResult = [];
      //console.log("client: ",client);
      return new Promise((resolve, reject) => {
      client.stream(query)
      .on('readable', function () {
        // 'readable' is emitted as soon a row is received and parsed
        let row;
        while (row = this.read()) {
          //console.log('row %s', row);
          //resultArr.push(JSON.parse(row));
          resultArr.push(row);
        }
      })
      .on('end', function () {
        // Stream ended, there aren't any more rows
        console.log("got results&&&::: ", resultArr.length);
        if (resultArr && resultArr.length > 0) {
        /*
				for (var i in resultArr) {
					console.log(resultArr[i]);
          eResult.push({
						"latitude" : resultArr[i].latitude,
            "longitude" : resultArr[i].longitude,
					})
				}
        */
        //console.log('eResult',eResult)
        //console.log('resultArr',resultArr)
				resolve(resultArr)
			} else {
				resolve('no data found');
			}
      })
      .on('error', function (err) {
        // Something went wrong: err is a response error from Cassandra
        resolve('some error occured');
      });

    })

    }
}
