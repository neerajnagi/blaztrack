'use strict';
//TODO getting client from other class
const cassandra = require('../cassandra')
const client = cassandra.client;

function getLastDataOfDevice(customerId, vehicleId)
{
  console.log('customer, vehicle', customerId, ',',vehicleId)
  //var query = 'SELECT * FROM gps_data_cf where customerid = ? AND vehicleid = ? limit 1';
  var query = 'SELECT * FROM gps_data_cf_v2 where customerid = ? AND vehicleid = ? limit 1';
  var resultArr = [];
  var eResult = [];
  console.log('before return new Promise')
  return new Promise((resolve, reject) => {
  client.stream(query, [customerId, vehicleId])
  .on('readable', function () {
    // 'readable' is emitted as soon a row is received and parsed
    let row;
    while (row = this.read()) {
      //console.log('row %s', row);
      //resultArr.push(JSON.parse(row));
      resultArr.push(row);
    }
  })
  .on('end', function () {
    // Stream ended, there aren't any more rows
    console.log("got results&&&::: ", resultArr.length);
    if (resultArr && resultArr.length > 0) {
    //console.log('resultArr',resultArr)
    resolve(resultArr);
  } else {
    console.log('no data found');
    resolve('data not found');
  }
  })
  .on('error', function (err) {
    // Something went wrong: err is a response error from Cassandra
    console.log('some error occured');
    resolve('some error occured');
  });

})

}

function graphDataFromQuery(customerId, vehicleId, startDT, endDT)
{
  console.log('inside graphDataFromQuery')
  console.log('customer, vehicle, startDT, endDT', customerId,vehicleId,startDT,endDT)
  console.log('before query execute, time', new Date())
  //var startTimestamp = '1537893552447';
  //var endTimestamp = '1538498352448';
  //var query = 'SELECT insert_datetime, adc1 FROM gps_data_cf where customerid = ? AND vehicleid = ? AND insert_datetime >= '+startDT+' AND insert_datetime <= '+endDT+'  ORDER BY insert_datetime ASC';
  var query = 'SELECT insert_datetime, ain1 FROM gps_data_cf_v2 where customerid = ? AND vehicleid = ? AND insert_datetime >= '+startDT+' AND insert_datetime <= '+endDT+'  ORDER BY insert_datetime ASC';
  var resultArr = [];
  console.log('before return new Promise')
  return new Promise((resolve, reject) => {
  client.stream(query, [customerId, vehicleId])
  .on('readable', function () {
    // 'readable' is emitted as soon a row is received and parsed
    let row;
    let c = 0;
    while (row = this.read()) {
      c++;
      //console.log('row', JSON.stringify(row));
      //resultArr.push(JSON.parse(row));
      if(c==150){
        //resultArr.push(row);
        c=0;
      }
      resultArr.push(row);
    }
  })
  .on('end', function () {
    // Stream ended, there aren't any more rows
    console.log('after query execute, time', new Date())
    console.log("got results&&&::: ", resultArr.length);
    if (resultArr && resultArr.length > 0) {
    resolve(resultArr);
  } else {
    console.log('no data found');
    resolve('data not found');
  }
  })
  .on('error', function (err) {
    // Something went wrong: err is a response error from Cassandra
    console.log('some error occured');
    resolve('some error occured');
  });

})
}

function getDeviceDataOfToday(customerId, vehicleId, startDT, endDT)
{
  console.log('inside getDeviceDataOfToday')
  console.log('customer, vehicle, startDT, endDT', customerId,vehicleId,startDT,endDT)
  console.log('before query execute, time', new Date())
  //var query = 'SELECT * FROM gps_data_cf where customerid = ? AND vehicleid = ? AND insert_datetime >= '+startDT+' AND insert_datetime <= '+endDT+' ORDER BY insert_datetime ASC';
  var query = 'SELECT * FROM gps_data_cf_v2 where customerid = ? AND vehicleid = ? AND insert_datetime >= '+startDT+' AND insert_datetime <= '+endDT+' ORDER BY insert_datetime ASC';
  var resultArr = [];
  console.log('before return new Promise')
  return new Promise((resolve, reject) => {
  client.stream(query, [customerId, vehicleId])
  .on('readable', function () {
    // 'readable' is emitted as soon a row is received and parsed
    let row;
    while (row = this.read()) {
      resultArr.push(row);
    }
  })
  .on('end', function () {
    // Stream ended, there aren't any more rows
    console.log("got results&&&::: ", resultArr.length);
    if (resultArr && resultArr.length > 0) {
    resolve(resultArr);
  } else {
    console.log('no data found');
    resolve('data not found');
  }
  })
  .on('error', function (err) {
    // Something went wrong: err is a response error from Cassandra
    console.log('some error occured');
    resolve('some error occured');
  });

})
}

function getAnalog1Data(customerId, vehicleId)
{
  console.log('customer, vehicle', customerId, ',',vehicleId)
  //var query = 'SELECT adc1 FROM gps_data_cf where customerid = ? AND vehicleid = ? limit 1';
  var query = 'SELECT ain1 FROM gps_data_cf_v2 where customerid = ? AND vehicleid = ? limit 1';
  var resultArr = [];
  var eResult = [];
  console.log('before return new Promise')
  return new Promise((resolve, reject) => {
  client.stream(query, [customerId, vehicleId])
  .on('readable', function () {
    // 'readable' is emitted as soon a row is received and parsed
    let row;
    while (row = this.read()) {
      //console.log('row %s', row);
      //resultArr.push(JSON.parse(row));
      resultArr.push(row);
    }
  })
  .on('end', function () {
    // Stream ended, there aren't any more rows
    console.log("got results&&&::: ", resultArr.length);
    if (resultArr && resultArr.length > 0) {
    resolve(resultArr);
  } else {
    console.log('no data found');
    resolve('data not found');
  }
  })
  .on('error', function (err) {
    // Something went wrong: err is a response error from Cassandra
    console.log('some error occured');
    resolve('some error occured');
  });

})

}

module.exports = {

    getGpsDeviceData : async function(data) {

      console.log('inside device controllers')
      var vehicles = data;
      var vehiclesData = {};
      for(var i=0; i < vehicles.length; i++)
      {
        var item = vehicles[i];
        var customerId = item.customerId
        var vehicleId = item.vehicleId
        var dataFromQuery = await getLastDataOfDevice(customerId, vehicleId)
        //console.log('dataFromQuery',dataFromQuery);
        vehiclesData[vehicleId] = dataFromQuery;
      }
      //console.log('vehiclesData',vehiclesData)
      return vehiclesData;

    },

    getGraphData : async function(customerId, vehicleId, startDT, endDT) {

        console.log('inside device controllers, getGraphData')
        var vehiclesData = {};
        console.log('in vehicle controllers, start time', new Date());
        var dataFromQuery = await graphDataFromQuery(customerId, vehicleId, startDT, endDT)
        console.log('in vehicle controllers, end time', new Date());
        console.log('dataFromQuery length',dataFromQuery.length);
        //TODO
        return dataFromQuery;

    },

    getTodayDeviceData : async function(data, startDT, endDT) {

        console.log('inside device Summary controllers, getTodayDeviceData')
        var vehicles = data;
        var vehiclesData = {};
        for(var i=0; i < vehicles.length; i++)
        {
          var item = vehicles[i];
          var customerId = item.customerId
          var vehicleId = item.vehicleId
          var dataFromQuery = await getDeviceDataOfToday(customerId, vehicleId, startDT, endDT)
          vehiclesData[vehicleId] = dataFromQuery;
        }
        return vehiclesData;

    },

    getVoltageForAnalog1 : async function(customerId, vehicleId) {

          console.log('inside device controllers, getVoltageForAnalog1')
          var dataFromQuery = await getAnalog1Data(customerId, vehicleId);
      		return dataFromQuery;

        }
}
