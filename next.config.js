/*const withComponentImages = require("next-component-images");
 
module.exports = withComponentImages();*/
const path = require('path')
const glob = require('glob')
/*
const resolve = require('resolve')
const withCSS = require('@zeit/next-css')
*/

const withCSS = require('@zeit/next-css')
const withSass = require('@zeit/next-sass')
const withProgressBar = require('next-progressbar')

module.exports = withProgressBar(withCSS(withSass({
  webpack (config, options) {
    config.module.rules.push({
      test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 100000
        }
      }
    })

    return config
  }
})))


/*
module.exports ={
  cssModules: true,






  webpack: (config) => {
    config.module.rules.push(
      {
        test: /\.css$/,
        use: [
          {
            loader: 'emit-file-loader',
            options: {
              name: 'dist/[path][name].[ext].js',
            },
          },
          {
            loader: 'babel-loader',
            options: {
              babelrc: false,
              extends: path.resolve(__dirname, './.babelrc'),
            },
          },
          'styled-jsx-css-loader',
        ],
      }
    );

    return config;
  }
};



*/

