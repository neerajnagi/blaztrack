import { userConstants } from '../_constants';

import cookie from "js-cookie";

//let user = JSON.parse(localStorage.getItem('user'));\
//let user = false;
let user = cookie.get('id')
const initialState = user ? { loggedIn: true, user } : {};

export function authentication(state = initialState, action) {
  console.log('authentication.reducer.js -->> state ',state)
  console.log('authentication.reducer.js -->> action ',action)
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case userConstants.LOGIN_FAILURE:
      return {};
    case userConstants.LOGOUT:
      return {};
      case userConstants.OTPVALIDATE_REQUEST:
        return {
          validating: true
        };
      case userConstants.OTPVALIDATE_SUCCESS:
        return {
          validated: true,
          user: action.user
        };
      case userConstants.OTPVALIDATE_FAILURE:
        return {
          loggedIn: true,
          user: action.user
        };
    default:
      return state
  }
}
