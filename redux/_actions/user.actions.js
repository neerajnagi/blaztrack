import { userConstants } from '../_constants';
import { userService } from '../_services';
import { alertActions } from './';
//import { history } from '../_helpers';

import Router from 'next/router'

export const userActions = {
    login,
    logout,
    register,
    getAll,
    otpValidate,
    delete: _delete
};

function login(username, password) {
    return dispatch => {
        dispatch(request({ username }));

        userService.login(username, password)
            .then(
                user => {
                    console.log('user.action.js -->> user authenticate response', user)
                    if(user.data.user)
                    {
                    dispatch(success(user));
                    //history.push('/');
                    Router.push(`/containers/DefaultLayout`)
                    }
                    else {
                      dispatch(failure('user not found'));
                      dispatch(alertActions.error('user not found'));
                    }
                },
                error => {
                    console.log('user.action.js -->> error')
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}

function register(user) {
    return dispatch => {
        dispatch(request(user));

        userService.register(user)
            .then(
                user => {
                    dispatch(success());
                    //history.push('/login');
                    dispatch(alertActions.success('Registration successful'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}

function getAll() {
  /*
    return dispatch => {
        dispatch(request());

        userService.getAll()
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: userConstants.GETALL_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
    */

    return dispatch => {
        dispatch(request());

      let users =  userService.getAll()
      console.log('user.action.js -->> getAll, user ',users)
      if(users != null)
      {
      dispatch(success(JSON.parse(users)))
      }
      else {
        dispatch(failure("users not found"))
      }
    };

    function request() { return { type: userConstants.GETALL_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
}

function otpValidate(enteredOTP_SHA, codeFormServer, user) {

  return dispatch => {
      dispatch(request());
    let userNode = user.data.node;
    let validate =  userService.otpValidate(enteredOTP_SHA, codeFormServer, userNode)
    console.log('user.actions.js -->> validate ',validate)

    if(validate)
    {
      let user = localStorage.getItem('user');
      console.log('user.action.js -->> otpValidate, user ',user)
      dispatch(success(JSON.parse(user)))
      //history.push('/');
    }
    else {
      dispatch(failure(user));
      dispatch(alertActions.error('otp not matched'));
    }

  };

  function request() { return { type: userConstants.OTPVALIDATE_REQUEST } }
  function success(user) { return { type: userConstants.OTPVALIDATE_SUCCESS, user } }
  function failure(error) { return { type: userConstants.OTPVALIDATE_FAILURE, user } }

}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        userService.delete(id)
            .then(
                user => dispatch(success(id)),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(id) { return { type: userConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: userConstants.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: userConstants.DELETE_FAILURE, id, error } }
}
