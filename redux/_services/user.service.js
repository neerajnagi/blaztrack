//import config from 'config';
//import { authHeader } from '../_helpers';

import cookie from "js-cookie";

export const userService = {
    login,
    logout,
    otpValidate,
    getAll
};

function login(username, password) {
    console.log('user.service.js -->> inside login')
    console.log('user.service.js -->> username ', username)
    let phone = username;
    console.log('user.service.js -->> phone ', phone)
    let payload = {
                  "phone":"+917073062879",
                  "password":"123456"
                  }
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({phone, password})
    };
    console.log('requestOptions ',requestOptions)

    return fetch('http://159.65.6.196:5500/api/users/auth', requestOptions)
        .then(handleResponse)
        .then(user => {
            // login successful if there's a jwt token in the response
            console.log('user.service -->> login, user: ', user)
            let userData = user.data.user;
            if (userData.token) {
                console.log('user saving as ',userData)
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                //localStorage.setItem('user', JSON.stringify(userData));
                //TODO localStorage not available with SSR so set userId in cookie
                cookie.set('id',userData.id);
            }

            return user;
        });
}

function logout() {
    // remove user from local storage to log user out
    console.log('user.service.js -->> inside logout')
    localStorage.removeItem('user');
}

function getAll() {

    var user = localStorage.getItem('user');
    console.log('user.serive.js -->> getAll, user ',user)
    return user;

  }

  function otpValidate(enteredOTP_SHA, codeFormServer, userNode) {

      if(enteredOTP_SHA === codeFormServer)
      {
        console.log('otp matched, valid user')
        var user = localStorage.setItem('user', JSON.stringify(userNode));
        console.log('user.serive.js -->> otpValide, user ',user)
        return true;
      }
      else {
        localStorage.removeItem('user');
        return false;
      }


    }

function handleResponse(response) {
    console.log('user.service.js -->> inside handleResponse')
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                //location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}
