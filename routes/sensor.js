/**
 * Example sensor routes
 **/
'use strict'

const fetch = require('node-fetch');

module.exports = (expressApp, functions) => {

  if (expressApp === null) {
    throw new Error('expressApp option must be an express server instance')
  }

  // Expose a route to return user profile if logged in with a session
  expressApp.post('/sensor/calibratelineartank', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      fetch('http://159.65.6.196:5500/api/Sensor/calibratelineartank', requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('calibrate linear tank success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to calibrate linear tank', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.post('/sensor/calibratecanonicaltank', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      fetch('http://159.65.6.196:5500/api/Sensor/calibratecanonicaltank', requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('calibrate canonical tank success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to calibrate canonical tank', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/sensorType', (req, res) => {
    if (req.user) {
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/sensorType')
      .then(res => res.json())
      .then(json => {
        console.log('sensorType success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get sensorType', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/sensorMake', (req, res) => {
    if (req.user) {
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/sensorMake')
      .then(res => res.json())
      .then(json => {
        console.log('sensorMake success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get sensorMake', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/sensorModel/:sensorMakeId', (req, res) => {
    if (req.user) {
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/sensorModel?filter[where][sensorMakeId]='+req.params.sensorMakeId)
      .then(res => res.json())
      .then(json => {
        console.log('sensorModel success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get sensorModel', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.post('/sensorsetup', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      fetch('http://159.65.6.196:5500/api/SensorAssignment/sensorsetup', requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('sensor setup success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to create sensor setup', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

}
