'use strict'

const fetch = require('node-fetch');

module.exports = (expressApp, functions) => {

  if (expressApp === null) {
    throw new Error('expressApp option must be an express server instance')
  }

  expressApp.get('/manage/fetch/:manageCategory/:tillPopulate', (req, res) => {
    if (req.user) {
      console.log('get /manage/fetch');
      console.log('/manage/fetch, request time',new Date())
      var userId = req.user.id;
      var manageCategory = req.params.manageCategory;
      var tillPopulate = req.params.tillPopulate;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/manage/fetch/'+userId+'/'+manageCategory+'/'+tillPopulate)
      .then(res => res.json())
      .then(json => {
        console.log('manages success')
        console.log('/manage/fetch, response time',new Date())
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch manages', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  }
