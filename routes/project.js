'use strict'

const fetch = require('node-fetch');

module.exports = (expressApp, functions) => {

  if (expressApp === null) {
    throw new Error('expressApp option must be an express server instance')
  }

  // Expose a route to return user profile if logged in with a session

  expressApp.get('/project/:projectId/groups', (req, res) => {
    if (req.user) {
      console.log('/project/groups')
      console.log('customerId',req.params.customerId)
      fetch('http://159.65.6.196:5500/api/project/'+req.params.projectId+'/groups')
      .then(res => res.json())
      .then(json => {
        console.log('project groups success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get project groups', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/project/fetchprojectanditsgroup/:manageCategory', (req, res) => {
    if (req.user) {
      console.log('/project/groups')
      var userId = req.user.id;
      var manageCategory = req.params.manageCategory;
      fetch('http://159.65.6.196:5500/api/manage/fetchprojectanditsgroup/'+userId+'/'+manageCategory)
      .then(res => res.json())
      .then(json => {
        console.log('fetch project & its groups success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get project & its groups', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.post('/project/create', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      fetch('http://159.65.6.196:5500/api/project/createandupdategroup', requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('created project',json)
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to create project', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.patch('/project/update/:projectId', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'PATCH',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      var projectId = req.params.projectId;
      fetch('http://159.65.6.196:5500/api/project/update/'+projectId, requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('update project success')
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to update project', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

}
