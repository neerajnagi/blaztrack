/**
 * Example account management routes
 **/
'use strict'
const dataCtrl =  require('../controllers/data')
const deviceCtrl =  require('../controllers/device')

module.exports = (expressApp, functions) => {

  if (expressApp === null) {
    throw new Error('expressApp option must be an express server instance')
  }

  // Expose a route to return user profile if logged in with a session
  expressApp.get('/device/data', (req, res) => {
    if (req.user) {
      dataCtrl.getDataFromCassandra()
      .then(data => {
        //console.log('device data',data);
        res.json({data:data})
      }
      )
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/device/fetch/:manageCategory', (req, res) => {
    if (req.user) {
      console.log('get /device/fetch')
      console.log('/device/fetch, request time',new Date())
      var userId = req.user.id;
      var manageCategory = req.params.manageCategory;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/gpsdevice/fetchdevice/'+userId+'/'+manageCategory)
      .then(res => res.json())
      .then(json => {
        console.log('devices success')
        console.log('/device/fetch, response time',new Date())
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch devices', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/device/fetchvoltage/:customerId/:vehicleId', (req, res) => {
    if (req.user) {
      var customerId = req.params.customerId;
      var vehicleId = req.params.vehicleId;
      deviceCtrl.getVoltageForAnalog1(customerId,vehicleId)
      .then(data => {
        console.log('device Voltage data success');
        res.json({data:data})
      }
      )
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/device/fetchdeviceport/:vehicleId', (req, res) => {
    if (req.user) {
      var vehicleId = req.params.vehicleId;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/deviceassignment/fetchdeviceport/'+vehicleId)
      .then(res => res.json())
      .then(json => {
        console.log('device port success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch device port', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/devicemodel', (req, res) => {
    if (req.user) {
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/devicemodel')
      .then(res => res.json())
      .then(json => {
        console.log('device model success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch device model', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.post('/deviceassignment/create', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      fetch('http://159.65.6.196:5500/api/deviceassignment/create', requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('deviceAssignment success')
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to create device assignment', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.delete('/deviceassignment/unassign/:gpsDeviceId/:vehicleId', (req, res) => {
    if (req.user) {
      var requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
      };
      var gpsDeviceId = req.params.gpsDeviceId;
      var vehicleId = req.params.vehicleId;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/deviceassignment/unassign/'+gpsDeviceId+'/'+vehicleId, requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('deviceUnAssignment success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to delete device assignment'})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.post('/device/createandassignvehicle/:isAssign', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      var isAssign = req.params.isAssign;
      fetch('http://159.65.6.196:5500/api/gpsdevice/createandassignvehicle/'+isAssign, requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('device create and assign vehicle success')
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to create device and assign vehicle', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

}
