/**
 * Example account management routes
 **/
'use strict'

const fetch = require('node-fetch');

module.exports = (expressApp, functions) => {

  if (expressApp === null) {
    throw new Error('expressApp option must be an express server instance')
  }

  // Expose a route to return user profile if logged in with a session

  expressApp.post('/group/user/register', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      fetch('http://159.65.6.196:5500/api/users/register', requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('created group user',json)
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to register user', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/group/user/:userId', (req, res) => {
    if (req.user) {
      var userId = req.params.userId;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/users/'+userId)
      .then(res => res.json())
      .then(json => {
        console.log('group user',json)
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get user', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.patch('/group/updateuser/:userId', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'PATCH',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      var userId = req.params.userId;
      fetch('http://159.65.6.196:5500/api/users/updategroupleveluser/'+userId, requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('updated group user',json)
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to update user', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.delete('/group/deleteuser/:userId', (req, res) => {
    if (req.user) {
      var requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
      };
      var userId = req.params.userId;
      fetch('http://159.65.6.196:5500/api/users/deleteuser/'+userId, requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('deleted user',json)
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to delete user', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.post('/group/create', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      fetch('http://159.65.6.196:5500/api/group/create', requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('created group',json)
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to create group', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/group/:groupId', (req, res) => {
    if (req.user) {
      var groupId = req.params.groupId;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/group/'+groupId)
      .then(res => res.json())
      .then(json => {
        console.log('group',json)
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get group', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.patch('/group/:groupId', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'PATCH',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      var groupId = req.params.groupId;
      fetch('http://159.65.6.196:5500/api/group/'+groupId, requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('updated group',json)
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to update group', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.delete('/group/:groupId', (req, res) => {
    if (req.user) {
      var requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
      };
      var groupId = req.params.groupId;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/group/delete/'+groupId, requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('deleted group',json)
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to delete group', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/group/users/:manageCategory', (req, res) => {
    if (req.user) {
      var userId = req.user.id;
      var manageCategory = req.params.manageCategory;
      //TODO fetching data from loopback api
      //http://159.65.6.196:5500/api/users/fetchcustomerlevelusers/{userid}/{managecategory}
      fetch('http://159.65.6.196:5500/api/users/fetchgrouplevelusers/'+userId+'/'+manageCategory)
      .then(res => res.json())
      .then(json => {
        console.log('group users success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch group level users', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/group/fetch/:manageCategory', (req, res) => {
    if (req.user) {
      var userId = req.user.id;
      var manageCategory = req.params.manageCategory;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/group/fetchgroup/'+userId+'/'+manageCategory)
      .then(res => res.json())
      .then(json => {
        console.log('groups success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch groups', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/group/manage/fetch/:manageCategory/:tillPopulate', (req, res) => {
    if (req.user) {
      console.log('get /group/manage/fetch');
      console.log('/group/manage/fetch, request time',new Date())
      var userId = req.user.id;
      var manageCategory = req.params.manageCategory;
      var tillPopulate = req.params.tillPopulate;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/manage/fetch/'+userId+'/'+manageCategory+'/'+tillPopulate)
      .then(res => res.json())
      .then(json => {
        console.log('groups manages success')
        console.log('/group/manage/fetch, response time',new Date())
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch manages', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/group/fetchusersanditsmanages/:groupId', (req, res) => {
    if (req.user) {
      var groupId = req.params.groupId;
      //TODO fetching data from loopback api
      console.log('groupId',groupId)
      fetch('http://159.65.6.196:5500/api/users/fetchusersanditsmanages/'+groupId+'/group')
      .then(res => res.json())
      .then(json => {
        console.log('group user and its manages success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch group users and its manages', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/group/fetchusermanagesandavailablemanages/:userId/:manageCategory/:customerId', (req, res) => {
    if (req.user) {
      var userId = req.params.userId;
      var manageCategory = req.params.manageCategory;
      var customerId = req.params.customerId;
      //TODO fetching data from loopback api
      console.log('customerId',customerId)
      fetch('http://159.65.6.196:5500/api/users/fetchusermanagesandavailablemanages/'+userId+'/'+manageCategory+'/'+customerId)
      .then(res => res.json())
      .then(json => {
        console.log('groups user manages and avaialbe manages success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch groups user manages and availabe manages', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/group/fetchunassignedgroupandprojectgroup/:customerId/:projectId', (req, res) => {
    if (req.user) {
      var customerId = req.params.customerId;
      var projectId = req.params.projectId;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/group/fetchunassignedgroupandprojectgroup/'+customerId+'/'+projectId)
      .then(res => res.json())
      .then(json => {
        console.log('unAssignedGroups and project groups success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch unAssignedGroups and project groups', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/group/fetchunassignedgrouptoproject/:customerId', (req, res) => {
    if (req.user) {
      var customerId = req.params.customerId;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/group/fetchunassignedgrouptoproject/'+customerId)
      .then(res => res.json())
      .then(json => {
        console.log('fetch unAssignedGroups success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch unAssignedGroups', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

}
