/**
 * Example account management routes
 **/
'use strict'
const fetch = require('node-fetch');
const deviceCtrl =  require('../controllers/device')
const hmr = require('../hmr/hmr')
const vehicleHelpers = require('../helpers/vehicle')
const summaryHelpers = require('../helpers/summary')

module.exports = (expressApp, functions) => {

  if (expressApp === null) {
    throw new Error('expressApp option must be an express server instance')
  }

  // Expose a route to return user profile if logged in with a session

  // Expose a route to return user profile if logged in with a session
  expressApp.get('/vehicle/fetch/:manageCategory', (req, res) => {
    if (req.user) {
      console.log('get /vehicle/fetch/:manageCategory')
      console.log('/vehicle/fetch, request time', new Date())
      var userId = req.user.id;
      var manageCategory = req.params.manageCategory;
      fetch('http://159.65.6.196:5500/api/vehicle/fetchvehicle/'+userId+'/'+manageCategory)
      .then(res => res.json())
      .then(json => {
        console.log('vehicles success')
        console.log('/vehicle/fetch/:manageCategory, reqponse time',new Date())
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch vehicles', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/vehicle/manage/fetch/:manageCategory/:tillPopulate', (req, res) => {
    if (req.user) {
      var userId = req.user.id;
      var manageCategory = req.params.manageCategory;
      var tillPopulate = req.params.tillPopulate;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/manage/fetch/'+userId+'/'+manageCategory+'/'+tillPopulate)
      .then(res => res.json())
      .then(json => {
        console.log('vehicle manages success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch manages', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/group/:groupId/vehicles', (req, res) => {
    if (req.user) {
      var groupId = req.params.groupId;
      //fetch('http://159.65.6.196:5500/api/group/'+groupId+'/vehicles')
      fetch('http://159.65.6.196:5500/api/vehicle?filter[where][deleted]=0&filter[where][groupId]='+groupId+'&filter[include][vehicleCategory]')
      .then(res => res.json())
      .then(json => {
        console.log('group vehicles success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get group vehicles', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.post('/vehicle/create', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      fetch('http://159.65.6.196:5500/api/vehicle/create', requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('created vehicle',json)
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to create vehicle', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/vehicle/:vehicleId', (req, res) => {
    if (req.user) {
      var vehicleId = req.params.vehicleId;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/vehicle/'+vehicleId)
      .then(res => res.json())
      .then(json => {
        console.log('vehicle',json)
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get vehicle', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/vehicle/fetchvehiclebycustomer/:customerId', (req, res) => {
    if (req.user) {
      var customerId = req.params.customerId;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/vehicle/fetchvehiclebycustomer/'+customerId)
      .then(res => res.json())
      .then(json => {
        console.log('vehicle of customer success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch vehicle of customer', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/vehicle/fetchvehicleforsitedata/:id/:manageCategory/:all', (req, res) => {
    if (req.user) {
      console.log('get /vehicle/fetchvehicleforsitedata')
      console.log('/vehicle/fetchvehicleforsitedata, request time',new Date())
      var id = req.params.id;
      var manageCategory = req.params.manageCategory;
      var all = req.params.all;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/vehicle/fetchvehicleforsitedata/'+id+'/'+manageCategory+'/'+all)
      .then(res => res.json())
      .then(json => {
        console.log('vehicles for site data success')
        console.log('/vehicle/fetchvehicleforsitedata, response time',new Date())
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch vehicle for site data', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  //TODO for fetching device data for vehicle
  expressApp.post('/vehicle/fetch/', (req, res) => {
    console.log('inside /vehicle/fetch')
    console.log('/vehicle/fetch, request time',new Date())
    if (req.user) {
      //console.log('vehicle fetch, req.body',req.body)
      var data = req.body.data
      deviceCtrl.getGpsDeviceData(data)
      .then(data => {
        console.log('device data from cass success');
        console.log('/vehicle/fetch, response time',new Date())
        res.json({data:data})
      }
      )
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/vehicle/fetchgraphdata/:customerId/:vehicleId/:startDT/:endDT', (req, res) => {
    if (req.user) {
      console.log('inside vehicle route');
      console.log('in vehicle route, start time',new Date())
      deviceCtrl.getGraphData(req.params.customerId, req.params.vehicleId, req.params.startDT, req.params.endDT)
      .then(data => {
        console.log('in vehicle route, end time',new Date())
        console.log('device data for graph, data length',data.length);
        res.json({data:data})
        //return data;
      }
      )
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/vehicle/fetchgraphdata/:customerId/:vehicleId/:startDT/:endDT/count', (req, res) => {
    if (req.user) {
      console.log('inside vehicle route');
      console.log('in vehicle route, start time',new Date())
      deviceCtrl.getGraphData(req.params.customerId, req.params.vehicleId, req.params.startDT, req.params.endDT)
      .then(data => {
        console.log('in vehicle route, end time',new Date())
        console.log('device count for graph, data length',data.length);
        res.json({count:data.length})
        //return data;
      }
      )
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/vehiclecategory', (req, res) => {
    if (req.user) {
      fetch('http://159.65.6.196:5500/api/vehiclecategory/')
      .then(res => res.json())
      .then(json => {
        console.log('vehicle category success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get vehicle category', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.post('/vehicle/todaydata/:startDT/:endDT', (req, res) => {
    if (req.user) {
      console.log('/vehicle/todaydata/:startDT/:endDT');
      var startDT = req.params.startDT;
      var endDT = req.params.endDT;
      var data = req.body.data;
      //TODO fetching data from loopback api
      deviceCtrl.getTodayDeviceData(data, startDT, endDT)
      .then(json => {
        console.log('get today device data success')
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch vehicle data', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.post('/vehicle/summary/:startDT/:endDT', (req, res) => {
    if (req.user) {
      console.log('/vehicle/summary/:startDT/:endDT');
      var id = req.body.id;
      var manageCategory = req.body.manageCategory;
      var all = req.body.all;
      var startDT = req.params.startDT;
      var endDT = req.params.endDT;
      //TODO fetching data from loopback api
      //TODO need to rename/change function
      vehicleHelpers.fetchVehiclesForSiteData(id, manageCategory, all)
      .then(vehicles => {
        console.log('vehicles',vehicles)
        deviceCtrl.getTodayDeviceData(vehicles, startDT, endDT)
        .then(deviceData => {
        console.log('deviceData',deviceData);
        summaryHelpers.getVehiclesSummary(vehicles, deviceData, startDT, endDT)
        .then(vehiclesSummary => {
        console.log('vehiclesSummary',vehiclesSummary);
        res.json(vehiclesSummary);
        })
        .catch(err => {
          console.log('inside calculate hmr catch, err',err)
          return res.status(500).json({error: 'Unable to get device data', code:500})
        })
        })
        .catch(err => {
          console.log('inside calculate hmr catch, err',err)
          return res.status(500).json({error: 'Unable to get device data', code:500})
        })
      })
      .catch(err => {
        console.log('err',err);
        return res.status(500).json({error: 'Unable to fetch vehicle data', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

}
