/**
 * Example account management routes
 **/
'use strict'

const fetch = require('node-fetch');

module.exports = (expressApp, functions) => {

  if (expressApp === null) {
    throw new Error('expressApp option must be an express server instance')
  }

  // Expose a route to return user profile if logged in with a session
  expressApp.get('/subdealer/users', (req, res) => {
    if (req.user) {
      var userId = req.user.id;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/users/fetchsubdealerlevelusers/'+userId)
      .then(res => res.json())
      .then(json => {
        console.log('subDealer users success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch subdealer level users', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.post('/subdealer/user/register', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      fetch('http://159.65.6.196:5500/api/users/register', requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('created subdealer user',json)
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to register user', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/subdealer/user/:userId', (req, res) => {
    if (req.user) {
      var userId = req.params.userId;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/users/'+userId)
      .then(res => res.json())
      .then(json => {
        console.log('subDealer user',json)
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get user', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.patch('/subdealer/updateuser/:userId', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'PATCH',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      var userId = req.params.userId;
      fetch('http://159.65.6.196:5500/api/users/updateusersubdealerlevel/'+userId, requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('updated subdealer user',json)
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to update user', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.delete('/subdealer/deleteuser/:userId', (req, res) => {
    if (req.user) {
      var requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
      };
      var userId = req.params.userId;
      fetch('http://159.65.6.196:5500/api/users/deleteuser/'+userId, requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('deleted subdealer user',json)
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to delete user', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.post('/subdealer/create', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      fetch('http://159.65.6.196:5500/api/subdealer/create', requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('created subDealer',json)
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to create subdealer', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/subdealer/:subDealerId', (req, res) => {
    if (req.user) {
      var subDealerId = req.params.subDealerId;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/subdealer/'+subDealerId)
      .then(res => res.json())
      .then(json => {
        console.log('subDealer',json)
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get subdealer', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.patch('/subdealer/:subDealerId', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'PATCH',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      var subDealerId = req.params.subDealerId;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/subdealer/'+subDealerId, requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('updated subDealer',json)
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to update subdealer', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.delete('/subdealer/:subDealerId', (req, res) => {
    if (req.user) {
      var requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
      };
      var subDealerId = req.params.subDealerId;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/subdealer/delete/'+subDealerId, requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('deleted subDealer',json)
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to delete subdealer', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/subdealer/fetch/:dealerId', (req, res) => {
    if (req.user) {
      var userId = req.user.id;
      //TODO fetching data from loopback api
      console.log('dealerId',req.params.dealerId)
      fetch('http://159.65.6.196:5500/api/subdealer/fetch/'+req.params.dealerId)
      .then(res => res.json())
      .then(json => {
        console.log('subDealers success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch subdealers', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/subdealer/manage/fetch/:manageCategory/:tillPopulate', (req, res) => {
    if (req.user) {
      var userId = req.user.id;
      var manageCategory = req.params.manageCategory;
      var tillPopulate = req.params.tillPopulate;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/manage/fetch/'+userId+'/'+manageCategory+'/'+tillPopulate)
      .then(res => res.json())
      .then(json => {
        console.log('subdealer manages success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch manages', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/subdealer/:subDealerId/customers', (req, res) => {
    if (req.user) {
      console.log('get /subdealer/customers')
      var subDealerId = req.params.subDealerId;
      //TODO fetching data from loopback api
      console.log('/subdealer/customers, request time',new Date())
      fetch('http://159.65.6.196:5500/api/subdealer/'+subDealerId+'/customers?filter[where][deleted]=0')
      .then(res => res.json())
      .then(json => {
        console.log('subdealer customers success')
        console.log('/subdealer/customers, response time',new Date())
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get subdealer customers', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/subdealer/fetchusersanditsmanages/:subDealerId', (req, res) => {
    if (req.user) {
      var subDealerId = req.params.subDealerId
      //TODO fetching data from loopback api
      console.log('subDealerId',subDealerId)
      fetch('http://159.65.6.196:5500/api/users/fetchusersanditsmanages/'+subDealerId+'/subdealer')
      .then(res => res.json())
      .then(json => {
        console.log('subdealer users and its manages success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch subdealer users and its manages', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/subdealer/fetchusermanagesandavailablemanages/:userId/:manageCategory/:dealerId', (req, res) => {
    if (req.user) {
      var userId = req.params.userId;
      var manageCategory = req.params.manageCategory;
      var dealerId = req.params.dealerId;
      //TODO fetching data from loopback api
      console.log('dealerId',dealerId)
      fetch('http://159.65.6.196:5500/api/users/fetchusermanagesandavailablemanages/'+userId+'/'+manageCategory+'/'+dealerId)
      .then(res => res.json())
      .then(json => {
        console.log('subdealers user manages and availabe manages success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch subdealers user manages and available manages', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

}
