/**
 * Example customer management routes
 **/
'use strict'

const fetch = require('node-fetch');

module.exports = (expressApp, functions) => {

  if (expressApp === null) {
    throw new Error('expressApp option must be an express server instance')
  }

  // Expose a route to return user profile if logged in with a session

  expressApp.post('/customer/user/register', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      fetch('http://159.65.6.196:5500/api/users/register', requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('created customer user:',json)
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to register user', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/customer/user/:userId', (req, res) => {
    if (req.user) {
      var userId = req.params.userId;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/users/'+userId)
      .then(res => res.json())
      .then(json => {
        console.log('customer user',json)
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get user', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.patch('/customer/updateuser/:userId', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'PATCH',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      var userId = req.params.userId;
      fetch('http://159.65.6.196:5500/api/users/updatecustomerleveluser/'+userId, requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('updated customer user',json)
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to update user', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.delete('/customer/deleteuser/:userId', (req, res) => {
    if (req.user) {
      var requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
      };
      var userId = req.params.userId;
      fetch('http://159.65.6.196:5500/api/users/deleteuser/'+userId, requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('deleted customer user',json)
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to delete user', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.post('/customer/create', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      fetch('http://159.65.6.196:5500/api/customer/createcustomerwithdefaultgroup', requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('created customer',json)
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to create customer', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/customer/:customerId', (req, res) => {
    if (req.user) {
      var customerId = req.params.customerId;
      fetch('http://159.65.6.196:5500/api/customer/'+customerId)
      .then(res => res.json())
      .then(json => {
        console.log('customer',json)
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get customer', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.patch('/customer/:customerId', (req, res) => {
    if (req.user) {
      console.log('req body',req.body)
      var payload = req.body;
      var requestOptions = {
        method: 'PATCH',
        headers: { 'Content-Type': 'application/json' },
        body : JSON.stringify(payload)
      };
      var customerId = req.params.customerId;
      fetch('http://159.65.6.196:5500/api/customer/'+customerId, requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('updated customer',json)
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to update customer', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.delete('/customer/:customerId', (req, res) => {
    if (req.user) {
      var requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
      };
      var customerId = req.params.customerId;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/customer/delete/'+customerId, requestOptions)
      .then(res => res.json())
      .then(json => {
        console.log('deleted customer',json)
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to delete customer', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/customer/users/:manageCategory', (req, res) => {
    if (req.user) {
      var userId = req.user.id;
      var manageCategory = req.params.manageCategory;
      //TODO fetching data from loopback api
      //http://159.65.6.196:5500/api/users/fetchcustomerlevelusers/{userid}/{managecategory}
      fetch('http://159.65.6.196:5500/api/users/fetchcustomerlevelusers/'+userId+'/'+manageCategory)
      .then(res => res.json())
      .then(json => {
        console.log('customer users success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get customer level users', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/customer/fetch/:manageCategory', (req, res) => {
    if (req.user) {
      var userId = req.user.id;
      var manageCategory = req.params.manageCategory;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/customer/fetchcustomer/'+userId+'/'+manageCategory)
      .then(res => res.json())
      .then(json => {
        console.log('customers success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch customers', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/customer/manage/fetch/:manageCategory/:tillPopulate', (req, res) => {
    if (req.user) {
      var userId = req.user.id;
      var manageCategory = req.params.manageCategory;
      var tillPopulate = req.params.tillPopulate;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/manage/fetch/'+userId+'/'+manageCategory+'/'+tillPopulate)
      .then(res => res.json())
      .then(json => {
        console.log('customer manages success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch manages', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/customer/:customerId/groups', (req, res) => {
    if (req.user) {
      console.log('get /customer/groups')
      console.log('/customer/groups, request time',new Date())
      var userId = req.user.id;
      //TODO fetching data from loopback api
      console.log('customerId',req.params.customerId)
      fetch('http://159.65.6.196:5500/api/customer/'+req.params.customerId+'/groups')
      .then(res => res.json())
      .then(json => {
        console.log('groups success')
        console.log('/customer/groups, response time',new Date())
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get customer groups', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/customer/fetchusersanditsmanages/:customerId', (req, res) => {
    if (req.user) {
      var customerId = req.params.customerId
      //TODO fetching data from loopback api
      console.log('customerId',customerId)
      fetch('http://159.65.6.196:5500/api/users/fetchusersanditsmanages/'+customerId+'/customer')
      .then(res => res.json())
      .then(json => {
        console.log('customer users and its manages success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch customer users and its manages', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/customer/fetchusermanagesandavailablemanages/:userId/:manageCategory/:subDealerId', (req, res) => {
    if (req.user) {
      var userId = req.params.userId;
      var manageCategory = req.params.manageCategory;
      var subDealerId = req.params.subDealerId;
      //TODO fetching data from loopback api
      console.log('subDealerId',subDealerId)
      fetch('http://159.65.6.196:5500/api/users/fetchusermanagesandavailablemanages/'+userId+'/'+manageCategory+'/'+subDealerId)
      .then(res => res.json())
      .then(json => {
        console.log('customers user manages and availabe manages success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch customers user manages and available manages', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

  expressApp.get('/customer/:customerId/projects', (req, res) => {
    if (req.user) {
      console.log('/customer/projects, request time',new Date())
      var userId = req.user.id;
      //TODO fetching data from loopback api
      console.log('customerId',req.params.customerId)
      fetch('http://159.65.6.196:5500/api/customer/'+req.params.customerId+'/projects')
      .then(res => res.json())
      .then(json => {
        console.log('customer projects success')
        //return json;
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to get customer projects', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

}
