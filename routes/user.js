/**
 * Example account management routes
 **/
'use strict'

const fetch = require('node-fetch');

module.exports = (expressApp, functions) => {

  if (expressApp === null) {
    throw new Error('expressApp option must be an express server instance')
  }

  // Expose a route to return user profile if logged in with a session
  expressApp.get('/user/fetch', (req, res) => {
    if (req.user) {
      var userId = req.user.id;
      //TODO fetching data from loopback api
      fetch('http://159.65.6.196:5500/api/users/fetch/'+userId)
      .then(res => res.json())
      .then(json => {
        console.log('user',json)
        //TODO need to return response only particular json field not fully res json
        res.json(json)
      })
      .catch(err => {
        return res.status(500).json({error: 'Unable to fetch user', code:500})
      })
    } else {
      return res.status(403).json({error: 'User Not Authorized', code:403})
    }
  })

}
