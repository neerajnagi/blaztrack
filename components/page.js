import React from 'react'
//import Layout from '../components/layout'
import { NextAuth } from 'next-auth/client'

export default class extends React.Component {

  static async getInitialProps({req}) {
    console.log('components/page.js -->  getInitialProps')
    return {
      session: await NextAuth.init({req}),// Add this.props.session to all pages
      lang: 'en'// Add a lang property for accessibility
    }
  }

}
